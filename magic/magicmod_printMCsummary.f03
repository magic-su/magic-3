! This subroutine prints final output in MagiC (priorly collected )
subroutine PrintMCsummary(stdout_local,NAV_,&
        NMCSteps_,NMCStepsEquil_,AccCntMType_,&
        EELS_,EINTS_,&
        ENERS_,VirNB_accum_,VirEL_accum_,VirB_accum_)
!Global variables
    use mcmod
    use iomod, only: NAtom, BOX, NMType, NameMType, EPS, FILRDF, FILPOT, MCStepAtomMType,MCTransStepMType,MCRotStepMType,&
        NMolMType
    implicit none
    integer(4), intent(in):: NAV_, stdout_local
    integer(8), intent(in):: AccCntMType_(:,:,:),NMCSteps_,NMCStepsEquil_
    real(8), intent(in)::  EELS_,EINTS_,ENERS_,VirNB_accum_,VirEL_accum_,VirB_accum_          

! Local variables
    real(8) FNR, pressure_av
    integer(4) I 
    character(len=5) :: fmt !Auxilary variable for output format specification
    
    write((stdout_local),*)'MagiC '
        write((stdout_local),*)' Monte Carlo'

    write((stdout_local),*)'-----------------------------------'
    if(LReadRDF)then
        write((stdout_local),*)' Inverse simulation'
        write((stdout_local),*)' File with original RDF:',FILRDF
    else
        write((stdout_local),*)' Direct simulation'
        write((stdout_local),*)' Potential is read from ',FILPOT
    end if
    if(NAV_.gt.0)then
        FNR=1./dfloat(NAV_)
    else
        FNR=0.
    end if
    write((stdout_local),*)'Number of molecules:   ',NMol
    write((stdout_local),*)'Number of species:     ',NMType
    write((stdout_local),'(a,3f12.3)')' Box sizes:            ',BOX
    write((stdout_local),*)'Sampled configurations:',NAV_
    write((stdout_local),*)'Number of molecules of each species:'
    do I=1,NMType ! TODO simplify
        write((stdout_local),'(i4,i6,3x,a,f6.2)')I,NMolMType(I),NameMType(I)
    end do
    write((stdout_local),*)
    write((stdout_local),*)'Total MC steps:        ',NMCSteps_
    write((stdout_local),*)'MC steps with averaging',NMCSteps_-NMCStepsEquil_
    write(fmt,'(i3)') NMType*3
    write(stdout_local,'(a,'//fmt//'a8)')&
                     ' Molecule Type          ',(NameMType(:))
    write((stdout_local),'(a,'//fmt//'f8.3)')&
                     ' MCStepAtom(MolType)    ',MCStepAtomMType(:)
    write((stdout_local),'(a,'//fmt//'f8.3)')&
                     ' MCTranStep(MolType)    ',MCTransStepMType(:)
    write((stdout_local),'(a,'//fmt//'f8.3)')&
                     ' MCRotStep(MolType)     ',MCRotStepMType(:)                     
    write((stdout_local),'(a,'//fmt//'f8.3)')&
                     ' Acceptance ratio atoms ',dfloat(AccCntMType_(:,1,1))/AccCntMType_(:,2,1)
    write((stdout_local),'(a,'//fmt//'f8.3)')&
                     ' Acceptance ratio trans ',dfloat(AccCntMType_(:,1,2))/AccCntMType_(:,2,2)
    write((stdout_local),'(a,'//fmt//'f8.3)')&
                     ' Acceptance ratio rot   ',dfloat(AccCntMType_(:,1,3))/AccCntMType_(:,2,3)
    write((stdout_local),*)'Temperature            ',TEMP
    write((stdout_local),*)'Dielectric permittivity ',EPS
    write((stdout_local),*)'Electrostatic energy:  ',EELS_*FKJM*FNR
    write((stdout_local),*)'Bond energy:           ',EINTS_*FKJM*FNR
    write((stdout_local),*)'------------------------------------'
    write((stdout_local),*)'Average energy:        ',ENERS_*FKJM*FNR
#ifdef pressure    
    pressure_av=NAtom*FPR-((VirNB_accum_+VirB_accum_+VirEL_accum_)/3.)*FPR*FNR-&
                ((VirNB_frozen+VirB_frozen)/3.)*FPR
    write((stdout_local),*)'Pressure                ',pressure_av
    write((stdout_local),'(a,4f12.2)')'Pressure components    ',&
             -(VirNB_accum_*FNR+VirNB_frozen)*FPR/3.0,-VirEL_accum_*FNR*FPR/3.,&
             -(VirB_accum_*FNR+VirB_frozen)*FPR/3.,NAtom*FPR
#endif             
! Here I can add printing of RDF for non inverse run.
end subroutine PrintMCsummary 