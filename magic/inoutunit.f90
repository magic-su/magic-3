! Helper module to control output-file units.

MODULE InOutUnit
implicit none
public
    integer(4) :: stdout, iPrint ! General output unit, and verbosity
    integer(4) :: stdout_trj ! Trajectory output unit
    integer(4) :: stdout_snapshot ! Final snapshot output unit
    integer(4) :: stdout_imc = 6 ! Output IMC to the standard pipe
    integer(4) :: stdout_main = 6 ! Main Output to the standard pipe
    integer(4), parameter :: stdout_pot = 12! Potential file output unit
    integer(4), parameter :: stdout_crosscorr = 14 ! Output unit for cross correlation matrix
    integer(4), parameter::  stdout_devnull = 7 ! Dummy output unit for /dev/null
    
    integer(4), parameter :: stdin     = 4  ! Input unit for main input file
    integer(4), parameter :: stdin_rdf = 20, stdin_rdfpot_inc=21 ! Input unit for RDF-file     
    
    integer(4), parameter :: stdin_pot = 22, stdin_pot_inc=23! Input unit for potential-file 
    integer(4), parameter :: stdin_mcm = 24 ! Input unit for mcm-file 
    integer(4), parameter :: stdin_crd = 25 ! Input unit for starting coordinates-file 
    integer(4), parameter :: stdin_frozencrd = 26 ! Input unit for frozen molecules coordinates
    integer(4), parameter :: stdin_crosscorr = 27 ! Input unit for cross correlation matrix
    integer(4), parameter :: stdin_externaltraj = 28 ! Input unit for reading external trajectory
    integer(4), parameter :: stdin_exclusions = 29 ! Input unit for reading exclusions file
    
    
    contains
subroutine SetInOut(pID, iPrint_, pSize)
        integer(4),intent(in):: pID, iPrint_, pSize
        
        if (pID.gt.0) then ! Parallel
            stdout =  102 + 1 + pID
            stdout_trj =  102 + 1 + (pSize+1) + pID
            stdout_snapshot =  102 + 1 + (pSize+1)*2 + pID
        else
            stdout = 6
            stdout_trj = 10
            stdout_snapshot = 11
        endif
end subroutine  

END MODULE InOutUnit
