\section{MagicTools object-classes reference}

\subsection{ Tabulated functions: RDFs, potentials, etc.}
\subsubsection{DF:  Distribution Function \label{DF}}
DF is a base class representing a single Distribution Function (e.g. RDF, bond length distribution, angle distribution, intermolecular potential, angle-bending potential, correction to potential, etc.)
The class contains properties and methods, which are common for every function, however, some methods are redefined when necessary to keep function specificity:
\begin{description}
\item[{\large Properties:}]
\item[Name] Name of the DF
\item[FullName] Completely resolved name of the DF, including type and kind of the DF
\item[x,y] numpy arrays storing tabulated values of the argument and the function
\item[Min,Max]  Range of distance/angle values where the function is defined
\item[Type] Type of the function: NB, B (pairwise bond), A (angle-bond)
\item[Kind] Kind of the function: RDF, Potential
\item[Npoints] Number of points in a table defining the  function
\item[Resol] Resolution of the table defining the function
\item[AtomTypes] (NB only) Names of the atom/bead types involved in the interaction represented by the function
\item[BondNumber] (B/A only) Number of the bond represented by the function.
\item[MolTypeName] (B/A only) Name of the Molecular type the bond function refers to.
\item[AtomGroups] (B/A only) List of atom pairs/triplets involved in the bond

\item[{\large Methods:}]
\item[Write()] Write the function into a file-stream
\item[Plot()] Plot the function
\item[Save()] Save the function in a tabulated for to a text file.
\item[Average()] Make a new DF which is average of given list of DFs
\item[ExtendRange()]  Extend range of the DF
\item[ExtendTail()]  Extend the tail range of NB distribution function in to RcutNB
\item[CutTail()]  Cut the tail of NB RDF/potential
\item[IsSimilar()]  Check if the DF is similar to given one, based on its Type, Kind, AtomTypes, MolecularType and Bond number
\item[ChangeResolution()]  Change resolution of the potential. The new points will have an average value between closest neighbors
\item[Distance()]  Calculate the distance between this and the given DF
\end{description}
\textbf{Examples:}\\
\verb|import DF| - import the class \\
\verb|RDFref=MagicTools.ReadRDF('dmpc.400ns.v2.rdf')| - read a set of RDFs, which 
is and object of the class DFset, containing a number of DF-objects.\\
\verb|rdfNB=RDFref.DFs[0]| - Access the first function of the set. It is an object of class DF representing a non-bonded RDF. \\
\verb|rdfNB.g| - Access the table of the function\\
\verb|rdfNB.Plot()| - Plot the function\\
\verb|rdfNB.AtomTypes| - See what bead/atom types are involved in the function.\\
\verb|rdfB=RDFref.DFs_B[0]| - Access the first pairwise bond related function of the set (they are indexed from 0)\\
\verb|rdfB.MolTypeName| - See the name of the molecular type the bond function belongs to.

\subsubsection{DFset - set of Distribution Functions} \label{DFset}
Class representing a set of Distribution Functions (RDF, Potential, Potential correction, etc.)

\begin{description}
	\item[{\large Properties:}]
	\item[Name] - Name of the set (typically name of the file the set was read from)
	\item[NTypes] - Number of different atom types used in the set
	\item[AtomTypes] - Names of the atom types involved in the set
	\item[Min, Max] - Range of distance values for non-bonded interaction functions
	\item[Npoints] - Number of points in non-bonded interaction functions
	\item[DFs] - List of functions (all functions in the set)
	\item[DFs\_NB] - List of non-bonded interaction functions 
	\item[DFs\_B] - List of pairwise bond interaction functions
	\item[DFs\_A] - List of angle-bending bond interaction functions
	\item[NPairBondsExclude, NAngleBondsExclude] - Two dictionaries defining exclusions for molecular types involved in the DFset

	\item[{\large Methods:}]
	\item[DFset()] - Construct the object from provided rdf/pot file (recommended way) or from the provided parameters. 
	\item[Write()] - Write the set of functions to the file (\hyperref[RDFnPOT]{.rdf or pot}).
	\item[Plot()] - Plot the set of functions
	\item[Reduce()] - Compare the set to the provided one and extract similar functions. Useful to extract functions related to one molecule from larger set of functions.
	\item[SetTitle()] - Set title for the DFset and for every DF of the set to have nice legends in massive plots
	\item[AddCore()]  - Add repulsive core to the Non-bonded potentials and sets Rmin=0
	\item[CutTail(RcutNB)]  - Shorten the range of NB potentials in the set to RcutNB
	\item[ChangeResolution(NewResol)]  - Changes resolution of the set. NewResol - tuple of 3 values (NB, B, A).
	\item[ExtendRange(RcutNB)] description - Extend the range of NB potentials in the set to RcutNB
	\item[SetPlotProperty(key,value)]  - Set plot-related keyword property for the DFset and for every function of the set. Used for fine control of the pictures in massive plots
\end{description}
\textbf{\large Examples:}\\
\\\verb|RDFref=MagicTools.ReadRDF('dmpc.400ns.rdf')| - read a set of RDFs, which is and object of the class DFset.
\\\verb|RDFref.Plot()| - Plot the functions.
\\\verb|RDFref.Write('RDFref.rdf')| - Write the set to the file.
\\\verb|print(RDFref.Name)| - Print the specific property of the set (Name)
\\\verb|RDFref.DFs[0]| - Access the first function of the set (they are indexed from 0)
\\\verb|RDFref.DFs_B[0]| - Access the first pairwise bond related function of the set (they are indexed from 0)

\subsection{Topology}
\subsubsection{System: Top level object representing the whole molecular system \label{System}}
Creating the system-object is the first step for all topology-related operations. There are two possibilities for it: Make an empty stub system, and then populate it with underlying structures; or create it using existing MagiC core input file \verb|magic.inp| and corresponding molecular topology files \verb|*.mcm|.
The user can also explicitly provide list of molecular type names (mcmfile) and number of molecules of each type (NMolMType) and box size (Box), see examples below.
\begin{description}
	\item[{\large Properties:}]
		\item[MolTypes] - Molecular types of the system
		\item[Molecules] - Molecules belonging to the system
		\item[BondTypes] - Bond types (both pairwise and angle-bending) belonging to the system
		\item[PairBondTypes] - Pairwise bond types belonging to the system
		\item[AngleBondTypes] - Angle-bending bond types belonging to the system
		\item[Bonds] - Bonds (both pairwise and angle-bending) belonging to the system
		\item[PairBonds] - Pairwise bonds belonging to the system
		\item[AngleBonds] - Angle-bending bonds belonging to the system
		\item[AtomTypes] - List of atom types defined in the system
		\item[Atoms] - List of atoms belonging to the system
		\item[Sites] - List of sites, i.e. atoms belonging to molecular types of the system

\item[]
\item[{\large Methods:}]

\item[\underline {Construct and populate the system:}]
\item[AddMolType] - Add molecular type to the system
\item[AddAtomType] - Add atom type to the system
\item[ReadGeometry] - Read system's geometry from XMOL file
\item[SetExclusions] - Set exclusion rules for the system
\item[ImputeSameAsBond] - Update BondTypes in the system according to the provided set of potentials/RDFs

\item[\underline {Write to files:}]
\item[WriteLAMMPSData] - Write the system's topology to LAMMPS data file
\item[WriteGromacsTopology] - Write the system's topology to GROMACS-topology file topfile.top
\item[WriteGALAMOSTxml] - Write the system's topology to GALAMOST XML format and
write records for the tabulated potentials as GALAMOST python script
\item[WriteGALAMOSTExclusions] - Create a set of two exclusion files for GALAMOST
\item[WriteMCMs] - Save all molecular types of the system to corresponding MCM-files
\item[WriteGeometryGRO] - Write system's geometry as .gro file
\item[WriteAsRDFinp] - Print the system as lines for RDF.inp file. Useful when writing script generating RDF.inp file

\item[\underline {Search and resolve names to objects:}]
\item[GetBondType] - Find bond type by MolTypeName:BondNumber
\item[GetAtomType] - Find atom type by it's name
\item[GetMolType] - Find molecular type by it's name

\item[IsSystemMatchRDFs] - Check if current geometry of the system matches the given set of RDFs
\end{description}
\textbf{\large Examples:}
\begin{Verbatim}[fontsize=\small]
system0 = System() # empty system
system1 = System(input='magic.inp') # same system as specified in MagiC.Core input file
system2 = System(mcmfile=['MT1.CG', 'MT2.CG'], NMolMType=[10, 10], Box=[10., 10., 10.])
system3 = System(input='magic.inp', dfset='potentials.pot', geometry='start.xmol')
\end{Verbatim}

\subsubsection{MolType: Topology of a single molecular type \label{MolType}}
Molecular Type is a container for storing molecular topology, i.e. atoms (and their types ) and bond-types (and bonds). It must belong to some \hyperref[System]{System}. In addition to the topology, MolType stores the list of actual \hyperref[Molecule]{molecules} of this type. 

The molecular type instance can be read from mcm-file, otherwise an stub molecular type will be created.  When the molecular type is read from file, it automatically gets one corresponding molecule assigned.

\begin{description}
\item[{\large Properties:}]
\item[Name] - The molecular type name
\item[System] - The system which the Molecular Type belongs to
\item[Molecules] - Molecules of the Molecular Type
\item[BondTypes (also PairBondTypes and AngleBondTypes)] - List of BondTypes belonging to the Molecular Type
\item[Bonds (also PairBonds and AngleBond)] - List of Bonds belonging to the Molecular Type
\item[Atoms] - List of atoms belonging to molecules of the molecular type

\item[]
\item[{\large Methods:}]
\item[AddMolecule] - Add molecule to the MolType
\item[Write2MCM] - Write the molecular file to a mcm-file
\end{description}
\textbf{\large Examples:}
\begin{Verbatim}
moltype_DNA = MagicTools.MolType('DNA.CG', system) # Read molecular type from file
moltype_stub = MagicTools.MolType('stub', system) # Create a stub
\end{Verbatim}

\subsubsection{Molecule \label{Molecule}}
Class representing a single molecule.

\begin{description}
	\item[{\large Properties:}]
	\item[MolType] - Molecular Type of the molecule
	\item[Name] - Name of the molecule, either user-provided or generated from MolecularType name and molecule number
	\item[Number] - Serial number of the molecule within all molecules of the corresponding molecular type
	\item[ID] - Serial number of the molecule within all molecules of the system
	\item[Atoms] - List of atoms belonging to the molecule
	\item[Bonds (also PairBonds and AngleBond)] - List of Bonds belonging to the Molecule
	\item[]
	\item[{\large Methods:}]
	\item[AddAtom(atom)] - Add the atom to the molecule
	\item[AddBond(bond)] - Add the bond to the molecule
\end{description}

\subsubsection{BondType: \label{BondType}}
The BondType is a group of bonds (pairwise or angle-bending), which are belonging to the same molecular type and described by the same interactions potential.

\begin{description}
	\item[{\large Properties:}]
	\item[MolType] - Molecular Type the BondType belongs to
	\item[Name] - Bond Type name for text representation
	\item[ID] - Serial number of the Bond Type within all BondTypes of the System
	\item[Number] - Serial number of the BondType within all BondTypes of the MolecularType
	\item[Bonds] - List of Bonds belonging to this BondType
	\item[AtomGroups] - List of atom groups (duplets/triplets), each group represents one bond of the BondType
	\item[]
	\item[{\large Methods:}]
	\item[AddBond(bond)] - Add bond to the BondType
	\item[Write2MCM(stream)] - Write the bond type to the output-stream in mcm-file format.
	\item[WriteAsRDFinp()] - Print the BondType as line for RDF.inp file. Useful when writing script generating RDF.inp
\end{description}

\subsubsection{Bond: \label{Bond}}
Class representing a single Bond connecting two or three atoms, depending on kind of the bond

\begin{description}
	\item[{\large Properties:}]
	\item[BondType] - Bond Type of the Bond
	\item[Molecule] - Molecule the bond belongs to
	\item[Name] - Bond name for text representation
	\item[ID] - Serial number of the Bond Type within all BondTypes of the System
	\item[Number] - Serial number of the bond within all bonds in the system having the same kind (pairwise or angle-bending)
	\item[Atoms] - Pair or Triplet of atoms bonded by the bond
	\item[Value] - Distance or angle of the bond
\end{description}

\subsubsection{AtomType \label{AtomType}}
Class representing an atom type.
\begin{description}
	\item[{\large Properties:}]
	\item[System] -  The system which the Atom Type belongs to
	\item[Number] -  Serial number of the atom type
	\item[Atoms] - List of Atoms belonging to this AtomType
	\item[Charge] - Charge of the atom-type as average over charges of all atoms of the type in a.u.
	\item[Mass] - Mass of the atom-type as average over masses of all atoms of the type in a.u.

\item[]
\item[{\large Methods:}]
	\item[WriteAsRDFinp()] - Return a string representation of AtomType for RDF.inp
	\item[AddAtom(atom)] - Add atom to the AtomType
\end{description}

\subsubsection{Atom \label{Atom}}
Class representing a single atom

\begin{description}
	\item[{\large Properties:}]
\item[AtomType(AtomType)] - Type of the atom
\item[Molecule (Molecule)] - The molecule where the atoms belongs to
\item[Name(str)] - Name of the atom
\item[Number(int)] - Atom's serial number within all atoms of the molecule
\item[ID(int)] - Atom's serial number within all atoms of the system
\item[Charge (float)] - Charge of the atom in electron charges
\item[Mass (float)] - Mass of the atom in a.u.
\item[R (np.ndarray)] - Coordinates of the atom (A)
\item[Bonds ] - List of bonds (Pairwise and angle) where the atom is involved
\item[BondedAtoms (also PairBondedAtoms, AngleBondedAtoms)] -  List of atoms bonded to this one.  Both angle- and pairwise bonds are taken into account

\item[]
\item[{\large Methods:}]
\item[IsBonded(that\_atom)] - Check if that atom is bonded to this one
\item[Distance(that\_atom)] - Calculate Euclidian Distance from that atom to this one
\item[PBC()] - Apply periodic boundary conditions to the atom's coordinates (inplace!)
\item[Write2MCM(stream)] - Write atom to the mcmfile-stream
\end{description}