Subroutine CollectMCsummary(NMCSteps)
    use dynarray
    use mcmod, only: ENERS, EELS, EINTS, VirB_accum, VirNB_accum, VirEL_accum, NAV, AccCntMType
#ifdef mpi    
    include 'mpif.h'
#endif
!Global variables
    integer(8), intent(in) :: NMCSteps
! Local variables
    integer(4) i, ierror
    
    call TimerStart(T_MPI)
    if (.not.allocated(global_AccCntMType)) &
        allocate(global_AccCntMType(size(AccCntMType,1),size(AccCntMtype,2),size(AccCntMtype,3)))
    global_AccCntMType=0

#ifdef mpi
    if (ID.gt.0) then ! parallel execution
        call MPI_BARRIER(MPI_COMM_WORLD,ierror)
    ! Collect NAV
        call MPI_REDUCE(NAV,global_NAV,1,MPI_INTEGER,MPI_SUM,0,MPI_COMM_WORLD,ierror)
    ! Collect NMCSteps
        call MPI_REDUCE(NMCSteps,global_NMCSteps,1,MPI_INTEGER8,MPI_SUM,0,MPI_COMM_WORLD,ierror)
    ! Collect AccCntMType(:)
        call MPI_REDUCE(AccCntMType,global_AccCntMType,size(AccCntMType),MPI_INTEGER8,MPI_SUM,0,MPI_COMM_WORLD,ierror)    
    ! Collect EELS
        call MPI_REDUCE(EELS,global_EELS,1,MPI_REAL8,MPI_SUM,0,MPI_COMM_WORLD,ierror)
    ! Collect EINTS
        call MPI_REDUCE(EINTS,global_EINTS,1,MPI_REAL8,MPI_SUM,0,MPI_COMM_WORLD,ierror)
    ! Collect ENERS
        call MPI_REDUCE(ENERS,global_ENERS,1,MPI_REAL8,MPI_SUM,0,MPI_COMM_WORLD,ierror)
    
        call MPI_REDUCE(VirNB_accum,global_VirNB_accum,1,MPI_REAL8,MPI_SUM,0,MPI_COMM_WORLD,ierror)
        call MPI_REDUCE(VirB_accum,global_VirB_accum,1,MPI_REAL8,MPI_SUM,0,MPI_COMM_WORLD,ierror)
        call MPI_REDUCE(VirEL_accum,global_VirEL_accum,1,MPI_REAL8,MPI_SUM,0,MPI_COMM_WORLD,ierror)

        if (ID.eq.1) then ! Calculate mean values and write debug/perform check
            if (global_NAV.ne.(NAV*psize)) write(*,*) 'MPI ERROR in NAV - reduction', global_NAV,'!=',NAV*psize
            if (global_NMCSteps.ne.(psize*NMCSteps))&
                write(*,*)'MPI ERROR in NMCSteps-reduction, global_NMCSteps!=NMCSteps*psize', global_NMCSteps,'!=',NMCSteps*psize
        endif    
    endif ! 
#endif   
    if (ID.eq.0) then ! serial execution
        global_NAV = NAV
        global_EELS = EELS
        global_EINTS = EINTS
        global_ENERS = ENERS
        global_VirB_accum = VirB_accum
        global_VirNB_accum = VirNB_accum
        global_VirEL_accum = VirEL_accum
        global_AccCntMType = AccCntMType
    endif  
    call TimerStop(T_MPI)
end subroutine
