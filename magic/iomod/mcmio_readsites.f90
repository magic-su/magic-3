subroutine MCMioReadSites(STypeSite, NameSTypeSite, l_ReadSiteTypeNumbers)
    implicit none
    ! Global variables
    integer(4), allocatable, intent(out):: STypeSite(:)
    character(atom_name_len), allocatable, intent(out):: NameSTypeSite(:)                          
    logical, intent(out)::      l_ReadSiteTypeNumbers

    ! Local variables
    character(filename_text) iFilename
    character(mcm_text) str, args(20)
    integer iostt, iSite, iMType, BeginSite, EndSite, IE,i,j, nargs 
    
    l_ReadSiteTypeNumbers = .false.
    call reshape(SumMassMType,NMType)
    call reshape(NSiteMType,NMType)
    NSite=0 ! Total Number of sites 
!    write(*,*) 'DEBUG readsites', NameMType(:)
    do iMType=1, NMType
        !  1 Open the .mcm file
!        write(*,*) 'DEBUG', trim(NameMType(iMType))
        iFilename=(trim(adjustl(NameMType(iMType)))//'.mcm')		
!        write(*,*) 'DEBUG', iFilename
        open(unit=stdin_mcm,file=iFilename,status='old',iostat=iostt)
        if (iostt .ne. 0) then
            write(stdout,*) 'ERROR: Reading MCM-file: mcm-file ',iFilename,' is not found!'
            stop 303
        endif            
        if(IPRINT.ge.5) write(stdout,*) '*** MOLECULE ',trim(adjustl(NameMType(iMType))),',  Type  No.',iMType       
        STR=TAKESTR(stdin_mcm,Iostt)
    !  Number of atoms
        read(STR,*,iostat=iostt) iSite
        if (iostt .ne. 0) then
            write(stdout,*) 'ERROR: Reading MCM-file ',iFilename,' line 1'
            stop 511
        endif
        if(IPRINT.ge.6)  write(stdout,*)'*** NR OF Sites (ATOMS) in the molecular type: ',iSite
        NSiteMType(iMType)=iSite     
        NSite=NSite+iSite
        call reshape(NameSite,NSite)
        call reshape(MTypeSite,NSite)
        call reshape2(RSite,NSite,3)                       
        call reshape(MassSite,NSite)
        call reshape(ChargeSite,NSite)
        call reshape(STypeSite,NSite)
        call reshape(NameSTypeSite,NSite)               
       
!   Cycle over atoms
        BeginSite=NSite-iSite+1
        EndSite=NSite
        MTypeSite(BeginSite:EndSite)=iMType
        do I=BeginSite, EndSite
            STR=TAKESTR(stdin_mcm,iostt)            
            call parse(str,' ',args,nargs)
            select case (nargs)
                case (7)
                    if (l_ReadSiteTypeNumbers) then
                        write(stdout,*) 'Error: Inconsistency in the mcm-files:',&
                        ' Some of them have exactly specified atomtype-numbers and some does not.'
                        stop 417
                    endif
                    l_ReadSiteTypeNumbers = .false.
                case (8)
                    l_ReadSiteTypeNumbers = .true.
                case default
                    write(stdout,*) 'ERROR: Reading MCM-file ',iFilename,' line ', i-BeginSite+2
                    write(stdout,*) 'Number of arguments in the line is not equal to 8 or 7',& 
                        ' (Name, X, Y, Z, Mass, Charge, [IType,] NameType)'
                    stop 512                    
            end select
            NameSite(I)=adjustl(trim(args(1)))
            call str2value(args(2),RSite(i,1),iostt)
            call str2value(args(3),RSite(i,2),iostt)
            call str2value(args(4),RSite(i,3),iostt)
            call str2value(args(5),MassSite(i),iostt)
            call str2value(args(6),ChargeSite(i),iostt)
            if (nargs.eq.8) then
                call str2value(args(7),STypeSite(i),iostt)                
            endif
            NameSTypeSite(i)=strip(args(nargs))
            if (iostt .ne. 0) then
               write(stdout,*) 'ERROR: Reading MCM-file ',iFilename,' line ', i-BeginSite+2
               stop 513
            endif
            NameSTypeSite(i)=strip(NameSTypeSite(i))
            if(IPRINT.ge.6) write(stdout,'(a5,a4,a4,3f7.3,a3,f8.4,a3,f6.3,a15,i3,a14,a8)') &
                'Site ',NameSite(I),'  R=',(RSite(i,J),J=1,3), ' M=',MassSite(I),' Q=',ChargeSite(I),&
                ' SiteTypeIndex=',STypeSite(I), ' SiteTypeName=', NameSTypeSite(i)                                   
        end do ! Over sites of the molecule 
        
        SumMassMType(iMType) = SUM(MassSite(BeginSite:EndSite))        
        write(stdout,*)' Total mass of molecular type ',trim(NameMType(iMType)), ' = ',SumMassMType(iMType)
        if(IPRINT.ge.6) write(stdout,*)'Reading MCM-file. Setting local COM to 0.  MolecularType  ',trim(NameMType(iMType))
        call MCMioSetCOMZero(BeginSite,EndSite)        
                
        close(stdin_mcm)
        
    enddo ! End of Loop over Molecular Types
    write(stdout,*) ''
    write(stdout,*) 'Total number of Sites in the system:', NSite
    end subroutine MCMioReadSites
