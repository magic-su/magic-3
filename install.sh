#!/bin/bash

#checking arguments
function usage {
   echo "Usage: $0 ARG1 ARG2"
   echo
   echo "where ARG1 can be one of [intel oracle gfortran], which is used when making CGTRAJ, "
   echo "      ARG2 can be one of [intel intel-mpi oracle oracle-mpi gfortran gfortran-mpi], for Magic."
   exit 1
}

ARG1=${1:-gfortran}
ARG2=${2:-gfortran-mpi}

if [[ ! "intel intel-mpi oracle oracle-mpi gfortran gfortran-mpi" =~ "$ARG1" ]]; then
   echo \"$ARG1\" is not allowed to compile CGTRAJ. Please use one of \[intel oracle gfortran\].
   usage 
fi
if [[ ! "intel-mpi oracle-mpi gfortran-mpi" =~ "$ARG2" ]]; then
   echo \"$ARG2\" is not allowed to compile Magic. Please use one of \[intel intel-mpi oracle oracle-mpi gfortran gfortran-mpi\].
   usage
fi
if [[ $# -gt 2 ]]; then usage ; fi

# Define location of MAGIC.
DIR=$( cd $( dirname ${BASH_SOURCE[0]} ) && pwd )
MAGIC=$DIR
XDRDIR=$DIR/lib/xdrfile-1.1.4
echo "Location of MAGIC's root folder: " $MAGIC

export PATH=$PATH:$MAGIC/bin
if [[ -d $MAGIC/bin ]]; then rm -r $MAGIC/bin ; fi
if [ ! -d $MAGIC/bin ]; then 
   mkdir $MAGIC/bin
fi

# Build xdrfile-library, needed for TRR and XTC files (GROMACS trajectry format)
echo "     Building xdrfile library"
echo
cd $XDRDIR
./configure --enable-shared --prefix=$XDRDIR > make.output
make clean
make install
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$XDRDIR

# INSTALL CGTRAJ -------------------------------------------------
echo
echo "Installing CGTraj"
echo
cd $MAGIC/CGTraj
if [ -f $XDRDIR/lib/libxdrfile.a ]; then
    ln -f -s $XDRDIR/lib/libxdrfile.a ./libxdrfile.a
fi
# if you are using Intel Fortran uncomment the line below.
# make intel
# if if you are using Oracle Solaris studio uncomment the line below.
# make oracle
# The default compiler is GNU Fortran
 make clean
 make $ARG1
# Build xmol2xtc tool
if [ -f $MAGIC/CGTraj/libxdrfile.a ]; then
 make xmol2xtc
fi

# linking compiled binary to bin folder

cd $MAGIC/bin
ln -s $MAGIC/CGTraj/xmol2xtc $MAGIC/bin/xmol2xtc
ln -s $MAGIC/CGTraj/cgtraj cgtraj

# INSTALL MAGIC -------------------------------------------------------------
echo
echo "Installing Magic"
echo
cd $MAGIC/magic
# Here you need to specify the compiler you are using after "make" command
# Possible options:
# intel
# intel-mpi
# oracle
# oracle-mpi
# gfortran
# gfortran-mpi
# Default choice - serial code by GNU Fortran
make clean $ARG1

cd $MAGIC/bin
ln -f -s $MAGIC/magic/magic-* $MAGIC/bin

# INSTALL RDF and MagicTools -------------------------------------------------------
echo
echo "Installing RDF and MagicTools"
echo
export PYTHONPATH=$MAGIC/MagicTools
export PYTHONPATH=$XDRDIR/src/python:$PYTHONPATH
cd $MAGIC/bin
ln -f -s $MAGIC/MagicTools/rdf.py ./rdf.py
cd ..

export PATH=$PATH:$MAGIC/bin
