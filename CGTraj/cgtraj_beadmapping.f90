!     
! Author: Alexander Mirzoev
! This module keeps utilities for reading and sequencing BeadMapping input file
!
Module CGtrajMod
    
contains
  subroutine readln(ifile,line,IE)
    implicit none
    character*1024 l,ll,line
    integer IE,ifile
    line=''
    IE=0
    do
        read(ifile,'(a)',end=99) l
        ll=ADJUSTL(l)
        if ((ll(1:1).ne.'#').and.(ll(1:1).ne.'!')) exit
    enddo
    line=ll
    return
99  IE=1
    line=''
    return
  end subroutine
  subroutine UnWrap(strargs, nstrargs, intargs, nintargs)
    use dynarray
    use strings
    implicit none
    
    character(*), intent(in) :: StrArgs(:)  ! parsed comma-separated strings from bead-definition
    integer(4), intent(in) :: NStrArgs
    integer(4), allocatable, intent(out):: IntArgs(:) ! integer array of atom numbers
    integer(4), intent(out) :: NIntArgs
    integer(4) ierr,val,i,j, pos, ibeg, iend, cnt
    
    call reshape(IntArgs, NStrArgs) ! initial allocation of int-array, same size as string-array
    NIntArgs=0
    do i=1, NStrArgs ! over all strings
        if (scan(StrArgs(i),'-').eq.0) then ! no '-' symbol - convert to string
            call str2value(trim(adjustl(StrArgs(i))),val,ierr)
            if (ierr.ne.0) then
                write(*,*) 'Error while reading bead definition, parsing string ',StrArgs(i)
                stop
            endif
            NIntArgs=NIntArgs+1
            if (NIntArgs.gt.size(IntArgs)) call reshape(IntArgs,NIntArgs*2)
            IntArgs(NIntArgs)=val
        elseif (scan(StrArgs(i),'-').eq.scan(StrArgs(i),'-',back=.true.)) then ! only one '-' symbol
            pos=scan(StrArgs(i),'-')
            !reading first value (one before -)
            call str2value(StrArgs(i)(:pos-1),ibeg,ierr)
            if (ierr.ne.0) then
                write(*,*) 'Error while reading bead definition, parsing string ',StrArgs(i)
                stop
            endif            
            !reading last value (one after -)
            call str2value(StrArgs(i)(pos+1:),iend,ierr)
            if (ierr.ne.0) then
                write(*,*) 'Error while reading bead definition, parsing string ',StrArgs(i)
                stop
            endif                        
            if (iend.le.ibeg) then
                write(*,*) 'Error while reading bead definition, wrong range limits. parsing string ',StrArgs(i)
                stop
            endif
            NIntArgs=NIntArgs+(iend-ibeg+1)
            if (NIntArgs.gt.size(IntArgs)) call reshape(IntArgs,NIntArgs*2)
            cnt=NIntArgs-(iend-ibeg+1)
            do j=ibeg,iend
                cnt=cnt+1
                IntArgs(cnt)=j
            enddo            
        else ! More than one '-' symbol - terminate with error
            write(*,*) 'Error: more then one "-" symbol found in string ',StrArgs(i)
            exit
        endif
    enddo    
  end subroutine
  
  subroutine ReadBMInput(iFilename,NCGforMolType,NameCGforMolType,NAtomsInCG,CGAtoms,CGTrajOutFile, LMolTypes)
    use strings
    use dynarray
    implicit none
! Global variables
    character(*) iFilename
    integer(4) MaxNmolTypes,MaxNCGforMolType,MaxAtomsInCG,iNmolTypes,iNCGforMolType,iAtomsInCG
    character*12, allocatable:: LMolTypes(:) ! List of molecular type names
    character*12,allocatable:: NameCGforMolType(:,:) ! Name of CG-bead
    integer, allocatable:: NCGforMolType(:),NAtomsInCG(:,:),CGAtoms(:,:,:) !List of atoms belong to CG
    character(128) CGTrajOutFile !Name of CG-trajectory file

! Local variables
    integer(4) :: iostt,BMStartPoint,BMEndPoint,iend=0,ierr=0,i,j,k,m,nargs,val
    character*1024, allocatable :: text(:),args(:)
    character*1024 s,str,line,before
    integer(4), allocatable:: IntArgs(:) ! integer array of atom numbers
    integer(4) NIntArgs
    open(unit=4,file=iFilename,status='old',iostat=iostt)
    if (iostt .ne. 0) then
        write(*,*)' BeadMapping input file ',iFilename,' not found!'
	stop
    endif
! First pass throught file - scanning for BeadMapping EndBeadMapping positions
    i=0
    iend=0
    ierr=0
    do
        i=i+1
        if (iend.ne.0) exit        
        call readln(4,line,ierr)
        if (ierr==1) then
            write(*,*) 'End of BM input file reached'
            exit
        endif

        if (UPPERCASE(line)=='BEADMAPPING') then
            BMStartPoint=i
            write(*,*) 'Beadmapping section start point detected'
        endif
        if (UPPERCASE(line)=='ENDBEADMAPPING') then
            BMEndPoint=i
            write(*,*) 'Beadmapping section end point detected'
            exit
        endif
    enddo    
    if (BMEndpoint==0) then
        write(*,*) 'BMEndPoint not detected'
        stop
    endif
    if (BMStartPoint==0) then
        write(*,*) 'BMStartPoint not detected'
        stop
    endif
    if (BMEndPoint.le.BMStartPoint) then
        write(*,*) 'BMEndPoint<BMStartPoint'
        stop
    endif
    close(4)
! Second pass - reading and storing lines with BM-scheme into array text
    open(unit=4,file=iFilename,status='old',iostat=iostt)
    allocate(text(BMEndPoint-BMStartPoint+1))
    i=0
    do
        i=i+1
        if (iend.ne.0) exit
        call readln(4,line,ierr)
        if (ierr==1) then
            write(*,*) 'End of BM input file reached'
            exit
        endif
        if (i>=BMStartPoint) text(i-BMStartPoint+1)=line
        if (i==BMEndPoint) exit
    enddo
    close(4)
! Pass through array TEXT, removing all spaces from the text.
    do i=1,size(TEXT)
        s=text(i)
!        write(*,*) '!',text(i),'!'
        call removesp(s)
        text(i)=s
!        write(*,*) '#',text(i),'#'
    enddo
! Pass through array text, counting number of CGMolecularType entries
    MaxNmolTypes=0
    MaxNCGforMolType=0
    MaxAtomsInCG=0
    i=0
    do
        i=i+1        
        if (i.gt.size(text)) exit
!        write(*,*) '+',UPPERCASE(trim(text(i))),'+'
        str=text(i)
        call split(str,':',before)
        if (UPPERCASE(trim(adjustl(before)))=='CGMOLECULARTYPE') then
            write(*,*) 'Found CGMolecularType line:',i
            MaxNMolTypes=MaxNMolTypes+1
            do j=i+1,size(text)
                if (UPPERCASE(trim(text(j)))=='ENDCGMOLECULARTYPE') exit
            enddo
            if (UPPERCASE(trim(text(j))).ne.'ENDCGMOLECULARTYPE') then
                write(*,*) 'Absent closing ENDCGMOLECULARTYPE directive'
                stop
            else
                write(*,*) 'Found EndCGMolecularType line:',j
            endif
            if (MaxNCGforMolType.le.(j-i-2)) MaxNCGforMolType=(j-i-2)
            do k=i+1,j-1 ! Over all lines of this CGMolType section
                if ((strcount(',',text(k))+1).gt.MaxAtomsInCG) MaxAtomsInCG=strcount(',',text(k))+1
            enddo
        else
            if (UPPERCASE(trim(adjustl(before)))=='CGTRAJECTORYOUTPUTFILE') then
                CGTrajOutFile=trim(str)
                write(*,*) 'Output trajectory file:',CGTrajOutFile
            endif
        endif

    enddo
    write(*,*) 'MaxNmolTypes=',MaxNmolTypes,' MaxNCGforMolType=',MaxNCGforMolType,' MaxAtomsInCG=',MaxAtomsInCG
    if (CGTrajOutFile=='cgtrajout.xmol') &
        write(*,*) 'Trajectory Output filename not detected, use default name:',trim(CGTrajOutFile)
    allocate(LMolTypes(MaxNMolTypes)) !
    allocate(NCGforMolType(MaxNMolTypes))!
    allocate(NameCGforMolType(MaxNCGforMolType,MaxNMolTypes))!
    allocate(NAtomsInCG(MaxNCGforMolType,MaxNMolTypes)) !
    allocate(CGAtoms(MaxNCGforMolType,MaxNMolTypes,MaxAtomsInCG)) !
    allocate(args(MaxAtomsInCG+2))
!    allocate(NameCGMolType(MaxNMolTypes))

! Now we have allocated all necessary arrays, so it is time to finally read all the data
    i=0
    iNMolTypes=0
    do
        i=i+1
        if (i.gt.size(text)) exit
        str=text(i)
        call split(str,':',before)
        if (UPPERCASE(trim(before))=='CGMOLECULARTYPE') then
            iNMolTypes=iNMolTypes+1
            iNCGforMolType=0
            write(*,*) ''
            write(*,*) 'CGMolecularType:',iNMolTypes
            do j=i+1,size(text)
                if (UPPERCASE(trim(text(j)))=='ENDCGMOLECULARTYPE') exit
            enddo
            if ((UPPERCASE(trim(text(j))).ne.'ENDCGMOLECULARTYPE')) then
                write(*,*) 'Absent closing EndCGMolecularType directive'
                stop
            else
                write(*,*) 'EndCGMolecularType:',iNMolTypes
            endif
            do k=i,j-1
                str=text(k)
                call split(str,':',before)
                select case (UPPERCASE(TRIM(ADJUSTL(before))))
                    case ('CGMOLECULARTYPE')
                        write(*,*) 'CGMolecularType:',trim(str)
                        LMolTypes(iNMolTypes)=trim(str)
                    case('PARENTTYPE')
                        write(*,*) 'ParentType:',trim(str)
! Add check if such arent type exists in NAMOL
                    case default ! Bead definition record
                        iNCGforMolType=iNCGforMolType+1
                        NameCGforMolType(iNCGforMolType,iNMolTypes)=trim(before)
                        write(*,*) 'Bead', iNCGforMolType, 'Name:',trim(before)
                        call split(str,':',before) ! Split the line second time
                        call str2value(trim(adjustl(before)),val,ierr)
                        if (ierr.ne.0) then
                            write(*,*) 'Error while reading bead definition: check the number of beads'
                            stop
                        endif
                        NAtomsInCG(iNCGforMolType,iNMolTypes)=val
                        call parse(str,',',args,nargs)                        
!                        write(*,*) 'Before shortcut unwrap: ',val,'atoms:',(trim(adjustl(args(m))),' ', m=1,nargs)                        
                        call unwrap(args,nargs,IntArgs,NIntArgs)
!                        write(*,*) 'After shortcut unwrap: ',val,'atoms:',(IntArgs(m),' ', m=1,nIntArgs)                        
                        ! Now we need to convert strings to integers and fill data arrays:
                        if (NAtomsInCG(iNCGforMolType,iNMolTypes).ne.(nIntArgs)) then
                            write(*,*) 'Error in bead definition number of beads:',NAtomsInCG(iNCGforMolType,iNMolTypes),&
                                       ' is not consistent with number of actual arguments:',nIntArgs
                            stop
                        endif               
                        if (size(CGAtoms,3).lt.nIntArgs) call reshape3(CGAtoms,size(CGAtoms,1),size(CGAtoms,2),nIntArgs)
                        CGAtoms(iNCGforMolType,iNMolTypes,1:nIntArgs)=IntArgs(1:NIntArgs)
                end select
                NCGforMolType(iNMolTypes)=iNCGforMolType
            enddo
        endif

    enddo
  end subroutine
end module




