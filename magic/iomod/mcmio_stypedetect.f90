subroutine MCMioSTypeDetect(NameSTypeSite,STypeSite, l_ReadSiteTypeNumbers, NSType, NameSType)
    implicit none
    character(atom_name_len), allocatable,  intent(in)                :: NameSTypeSite(:)
    character(atom_name_len), allocatable, intent(out)  :: NameSType(:)
    integer(4), allocatable,  intent(inout)                          :: STypeSite(:)
    integer(4), intent(out)                             :: NSType
    
    logical, intent(in)::      l_ReadSiteTypeNumbers
    character(atom_name_len) FMT
    integer(4) :: iSite, jSite, iSType
    
    if (.not. l_ReadSiteTypeNumbers) then ! Autodetect SiteTypeNumbers
        STypeSite = 0
        NSType = 0
        do iSite=1, NSite
            if (STypeSite(iSite) .eq. 0 ) then
                NSType = NSType + 1
                do jSite = iSite, NSite
                    if (NameSTypeSite(iSite).eq.NameSTypeSite(jSite)) STypeSite(jSite) = NSType
                enddo
            endif            
        enddo
    else
        NSType = maxval(STypeSite(1:NSite)) ! If type numbers are provided  use the largest value as number of SiteTypes
    endif
    
    ! Check no more zero values in STypeSite
    if (.not. all(STypeSite(1:NSite).gt.0)) then
        write(stdout,*) 'ERROR: Reading MCM-files: SiteType Index is not specified/autodetected for the following sites:'
        write(stdout,*) 'Check your MCM-files.',&            
            'You can also delete indexes in the files and let MagiC.Core autodetect the indexes from the names.' 
            do concurrent (iSite = 1:NSite, STypeSite(iSite).eq.0)
                write(stdout, *) 'Site: ', iSite, ':', NameSite(iSite)                
            enddo
            stop 518            
    endif 
    ! Check min>0, max <= NSType
    if (minval(STypeSite(1:NSite)).lt.1 .or. maxval(STypeSite(1:NSite)).gt.NSType) then
        write(stdout,*) 'ERROR: Reading MCM-files: the smallest SiteType Index is less than 1', &
                        ' or the largest SiteType Index is larger than number of distinct site types'
        write(stdout,*) 'Check your MCM-files.',&            
                        'You can also delete indexes in the files and let MagiC.Core autodetect the indexes from the names.' 
        stop 519
    endif
    ! Check all range of integer values are present [min:max]
    do iSType = 1, NSType
        if (count(STypeSite(:).eq.iSType).eq.0) then
            write(stdout,*) 'ERROR: Reading MCM-files: can not find any sites of type ', iSType                 
            write(stdout,*) 'Check your MCM-files.',&            
                            'You can also delete indexes in the files and let MagiC.Core autodetect the indexes from the names.' 
            stop 520            
        endif
    enddo
    ! Check atoms having same SiteType have same SiteName
    do iSite = 1, NSite
        iSType = STypeSite(iSite)
        do jSite = iSite+1, NSite
            if ((STypeSite(iSite).eq.STypeSite(jSite)) .neqv. (NameSTypeSite(iSite).eq.NameSTypeSite(jSite))) then
                write(stdout,*) 'ERROR: Reading MCM-files: Mismatching SiteTypeIndex and SiteTypeName:'  
                write(stdout,*)  &
                    'Site: ', iSite, 'has SiteTypeIndex: ',STypeSite(iSite),' and SiteTypeName ',NameSTypeSite(iSite) 
                write(stdout,*)  &
                    'Site: ', jSite, 'has SiteTypeIndex: ',STypeSite(jSite),' and SiteTypeName ',NameSTypeSite(jSite) 
                write(stdout,*) 'Check your MCM-files.',&          
                                'You can also delete indexes in the files and let MagiC.Core autodetect the indexes from the names.' 
                stop 521            
                
            endif
        enddo
    enddo
    ! Construct NameSType
    call reshape(NameSType, NSType)
    do concurrent (iSType = 1:NSType, iSite=1:NSite, STypeSite(iSite).eq.iSType )
        NameSType(iSType) = NameSTypeSite(iSite)
    enddo    
    write(stdout, '(a,i0)') 'Number of Site/Atom Types: ', NSType
    do iSType=1, NSType        
        write(FMT,'(i3)') atom_name_len+1
        write(stdout,'(a18,a'//trim(FMT)//',a1,i0,a3)',advance='no') '   Sites of type ',strip(NameSType(iSType)),'(',iSType,') :'        
        do iSite=1, NSite
            if (STypeSite(iSite).eq.iSType) write(stdout,'(a'//FMT//',a1,i0,a1)',advance='no') strip(NameSite(iSite)),&
            '(',iSite,')'
        enddo
        write(stdout,*) ''
    enddo
    call reshape(MTypeSType, NSite) ! TODO: This array is only used for detection of frozen atoms. Think how to remove it.
    MTypeSType(STypeSite(1:NSite))=MTypeSite(1:NSite)
end subroutine MCMioSTypeDetect
