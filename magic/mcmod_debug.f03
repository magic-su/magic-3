subroutine MCmodDebugDumpTrj(ID,IREP)
! Global Variables
    use precision
    use InOutUnit
    use utilsmod, only: MakeSaveFileName
    use TrajectoryIO, only: SaveFrame
    implicit none
    integer(4) IREP,ID       
#ifdef debug    
! Local Variables
    character(filename_text) SaveFileName ! Local variable to work with, without changing global variable BaseOutFileName
    integer(4) i
    SaveFileName = MakeSaveFileName(BaseOutFileName, '.debug.trj.xmol', IREP=IREP, ID=ID)
    open(unit=stdout_snapshot,file=SaveFileName,status='unknown')
    do i=1, cnt_Ri_vec_buffer
        call SaveFrame('xmol', stdout_snapshot, NAtom, real(BOX,4), NameSite(SiteAtom(1:NAtom)), Ri_vec_buffer(:,:,i), 0.0)
    enddo
    close(stdout_snapshot)
    write((stdout),*)'Debug trajectory buffer file written ',SaveFilename
#else
    write(stdout,*) 'Please inspect the last snapshot dumped in the file ',&
        MakeSaveFileName(BaseOutFileName, '.start.xmol', IREP=IREP, ID=ID)
    write(stdout,*) 'For further investigation, recompile MagiC Core with -Ddebug feature enabled and rerun the simulation'
    write(stdout,*) 'In this case MagiC will dump every frame since the last successfull energy calculation.'
    write(stdout,*) 'NOTE! This may require a lot of RAM if energy calculations are performed rarely.'        
#endif    
end subroutine