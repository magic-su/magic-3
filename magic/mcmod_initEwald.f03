subroutine MCmodInitEwald()

    use iomod, only: NAtom, BOX
    implicit none
    
    real(8),parameter:: PI=3.1415926535897932d0
    
    real(8) CUTK,CUTK2,FK(3),RK2,AFF
    integer(4) KMAX(3),IKV,IKZ,IKX,IKY,KY1,KY2,I

    ALPHA=AF/RECUT
    CUTK2=4.d0*ALPHA**2*FQ
    CUTK=sqrt(CUTK2)
    FK=2.d0*PI/BOX
    KMAX=CUTK/FK+1
    IKV=0
    do IKZ=-KMAX(3),KMAX(3)
        do IKX=0,KMAX(1)
            if(IKX.eq.0)then
                if(IKZ.gt.0)then
                    KY1=0
                else
                    KY1=1
                end if
                KY2=KMAX(2)
            else
                KY1=-KMAX(2)
                KY2=KMAX(2)
            end if
            do IKY=KY1,KY2
                RK2=sum((FK**2)*([IKX,IKY,IKZ]**2))
                if(RK2.le.CUTK2)then
#ifdef PBC_TO
                    if (abs(mod(IKX+IKY+IKZ,2)).eq.0) IKV=IKV+1 ! For truncated octahedron we consider only Kvec with even sum of coordinates, odd will be zero
#else
                    IKV=IKV+1
#endif
                end if
            end do
        end do
    end do
    if (.not. allocated(Recvec)) then
       allocate(RKV(IKV), Recvec(3,ikv))
       recvec=0.d0
       RKV(:)=0.d0
    endif
    IKV=0
    do IKZ=-KMAX(3),KMAX(3)
        do IKX=0,KMAX(1)
            if(IKX.eq.0)then
                if(IKZ.gt.0)then
                    KY1=0
                else
                    KY1=1
                end if
                KY2=KMAX(2)
            else
                KY1=-KMAX(2)
                KY2=KMAX(2)
            end if
            do IKY=KY1,KY2
                RK2=sum((FK**2)*([IKX,IKY,IKZ]**2))
#ifdef PBC_TO
                if((RK2.le.CUTK2).and.(abs(mod(IKX+IKY+IKZ,2).eq.0)))then
#else
                if(RK2.le.CUTK2)then
#endif
                    IKV=IKV+1
                    Recvec(:,IKV)=FK*[IKX,IKY,IKZ]
                    RKV(IKV)=4.*PI**2*COULF/(VOL*PI)*dexp(-0.25*RK2/ALPHA**2)/RK2
                end if
            end do
        end do
    end do
    NKV=IKV
    write((stdout),*)'Kmax=',KMAX,'    Num. of k-vectors ',NKV
    if (.not. allocated(SSIN)) allocate(SSIN(NKV))
    if (.not. allocated(SCOS)) allocate(SCOS(NKV))
    if (.not. allocated(D_SIN)) allocate(D_SIN(NKV))
    if (.not. allocated(D_COS)) allocate(D_COS(NKV))
    if (.not. allocated(sigmasin)) allocate(sigmasin(NKV))
    if (.not. allocated(sigmacos)) allocate(sigmacos(NKV))
!    allocate(SSIN(NKV),SCOS(NKV),D_SIN(NKV),D_COS(NKV),sigmasin(NKV),sigmacos(NKV))
    SSIN(:)=0.d0; SCOS(:)=0.d0; D_SIN(:)=0.d0; D_COS(:)=0.d0; sigmasin(:)=0.d0; sigmacos(:)=0.d0
!    endif
    ESF=-sum(ChargeAtom(1:NAtom)**2)*ALPHA*COULF/sqrt(PI)
    write((stdout),*)' el. self-energy ',ESF*FKJM
    return
end subroutine

