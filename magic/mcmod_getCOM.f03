!====================== MCGETCOM ======================================
! Calculate center of mass of the given molecule I. Update relative coordinates of the molecule's atoms

subroutine MCmodGetCOM(iMol)

    use iomod, only: BOX
    implicit none

    integer(4),intent(in):: iMol

!----------Local Variables----------------------------------------------
    real(8) Rcom(3), dR(3), HBOX(3),dx(3)
    integer AtomFirst, AtomLast, iAtom
    HBOX=BOX*0.5
    AtomFirst=AdrAtomMol(iMol)+1
    AtomLast=AdrAtomMol(iMol+1)
    ! Calculate COM in absolute coordinates
    Rcom=sum(Ri_vec(1:3,AtomFirst:AtomLast)*spread(MassSite(SiteAtom(AtomFirst):SiteAtom(AtomLast)),1,3),dim=2)/&
        SumMassMType(MTypeMol(iMol))
    ! Move COM in the origin box if necessary
    dX=RCOM
    call MCPBC(dX)
    dR=dX-Rcom ! Vector to shift atoms of the molecule
    Rcom=dX
    ! Shift atoms of the molecule
    Ri_vec(:,AtomFirst:AtomLast)=Ri_vec(:,AtomFirst:AtomLast)+spread(dR,2,AtomLast-AtomFirst+1)
    ! Calculate distances relative to COM
    forall (iAtom=AtomFirst:AtomLast)  ! This cycle is not required, as it shall produce the same values as they were initially.
    ! But it is kept for consistency check
        Wvec(:,iAtom)=Ri_vec(:,iAtom)-Rcom
    end forall
    XvecCOMMol(:,imol)=Rcom
    return
end subroutine
