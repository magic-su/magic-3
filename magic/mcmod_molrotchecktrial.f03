subroutine MCmodMolRotCheckTrial(iMol, deltar,phi)
    use iomod, only: NAtom
    implicit none
    integer(4), intent(in):: iMol
    real(8), intent(in):: deltaR(3),phi                
    !Local
    integer(4) NAtomsInMol, iAtom, jAtom,i, AtomFirst, AtomLast, j
    real(8) Rcom(3), Rcom_new(3),dR(3), Force_, Energy_, factor
    real(8), allocatable:: RicomOld(:,:),RicomNew(:,:), RijM_vec_new(:,:,:)

    call TimerStart(T_MCRotNewCoords)
    ENB_new = 0.0; EelNB_new=0.0
    ENB_old = 0.0; EelNB_old=0.0
    VirNB_old = 0.0; VirNB_new = 0.0
    ! Atoms in the molecule    
    NAtomsInMol=NSiteMType(MTypeMol(iMol))
    AtomFirst=AdrAtomMol(iMol)+1
    AtomLast=AdrAtomMol(iMol+1)
    ! Calculate COM of the molecule
    Rcom=sum(Ri_vec(1:3,AtomFirst:AtomLast)*spread(MassSite(SiteAtom(AtomFirst):SiteAtom(AtomLast)),1,3),dim=2)/&
        SumMassMType(MTypeMol(iMol))
    Rcom_new=Rcom
    ! Shift it to the origin box
    call MCPBC(Rcom_new)
    dR=Rcom_new-Rcom
    Ri_vec(:,AtomFirst:AtomLast)=Ri_vec(:,AtomFirst:AtomLast)+spread(dR,2,NAtomsInMol)    
    ! Calculate The new positions of the atoms after rotation with respect to COM
    allocate(RicomOld(3,NAtomsInMol))
    allocate(RicomNew(3,NAtomsInMol))
    call reshape3(RijM_vec_new,3,NAtom,NAtomsInMol)
    RicomOld(1:3,1:NAtomsInMol) = Ri_vec(1:3,AtomFirst:AtomLast)-spread(Rcom_new,2,NAtomsInMol)
    RicomNew=RicomOld    
    call Rotate2Mult(RiCOMNew,phi,deltar)
    ! New distances in the system
!DIR$ SIMD
    ! with atoms belonging to other molecules
    do concurrent (iAtom=1:NAtomsInMol, jAtom=1:NAtom)
            RijM_vec_new(1:3, jAtom, iAtom) = Svec(1:3, jAtom) - Ri_vec(1:3, iAtom+AtomFirst-1) - &
             (RiCOMNew(1:3, iAtom)-RiCOMOld(1:3, iAtom))
    end do
    ! with atoms belonging to the same molecule
    do concurrent (i=1:NAtomsInMol, j=1:NAtomsInMol)
        RijM_vec_new(1:3,j+AtomFirst-1,i)=RiCOMNew(1:3,j)-RiCOMNew(1:3,i)
    enddo    
    ! Apply PBC for the new vector distances
    call PBC_vecM(RijM_vec_new(1:3,1:NAtom,1:NAtomsInMol))                
    ! Calculate distance modulus
    do concurrent( i=1:NAtomsInMol, jAtom=1:NAtom)
	RijM_new(jAtom, i) = sqrt(RijM_vec_new(1,jAtom,i)**2 + RijM_vec_new(2,jAtom,i)**2 + RijM_vec_new(3,jAtom,i)**2)
    enddo
    call TimerStop(T_MCRotNewCoords)
    !Set all new energies to zero
    call TimerStart(T_MCRotNewArr)            
    call TimerStop(T_MCRotNewArr)            
    ! Calculate NB energy terms, where MaskNB and r<rcut
    call TimerStart(T_MCRotNBcount)
    do i=1, NAtomsInMol
        iAtom=AdrAtomMol(iMol)+i
        do jatom=1, NAtom
            if (MolAtom(iAtom).eq.MolAtom(jAtom)) then 
                factor = 0.5
            else
                factor = 1.0
            endif
            if (MaskNB_SR(jAtom,iAtom)) then                
                if (RijM_new(jAtom,i)<RCUT) then            
                   call EnergyForce(RijM_new(jAtom,i),STypeAtom(jAtom),STypeAtom(iAtom), Energy_, Force_)                
                   ENB_new = ENB_new + Energy_*factor
#ifdef pressure                       
                    VIRNB_new = VIRNB_new + Force_*RijM_new(jAtom,i)                   
#endif                    
                endif                
                if (Rij(jAtom, iAtom)<RCUT) then        
                   call EnergyForce(Rij(jAtom, iAtom),STypeAtom(jAtom),STypeAtom(iAtom), Energy_, Force_)                
                   
                   ENB_old = ENB_old + Energy_*factor
#ifdef pressure                       
                    VIRNB_old = VIRNB_old + Force_*Rij(jAtom,iAtom)                   
#endif                    
                endif                

            endif
            if (MaskNB_EL(jAtom,iAtom)) then                                
                    if ((RijM_new(jAtom,i)<RECUT)) then          
                        Energy_ = ChargeAtom(iAtom)*ChargeAtom(jAtom)*COULF*derfc(ALPHA*RijM_new(jAtom,i))/RijM_new(jAtom,i)
                        EelNB_new = EelNB_new + Energy_*factor
                    endif
                    if ((Rij(jAtom,iAtom)<RECUT)) then          
                        Energy_ = ChargeAtom(iAtom)*ChargeAtom(jAtom)*COULF*derfc(ALPHA*Rij(jAtom,iAtom))/Rij(jAtom,iAtom)
                        EelNB_old = EelNB_old + Energy_*factor
                    endif
            endif

        enddo       
    enddo
    call TimerStop(T_MCRotNBcount)
end subroutine
