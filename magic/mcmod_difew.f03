!========================= DIFEW ================================
!
!  Change of reciprocal Ewald energy if iAtom-th particle change
!  position from Rold to Rnew
!
subroutine MCmodDifEw(Rnew,CHI,DEW, Rold)
! Global variables
    implicit none
    real(8), intent(in):: Rnew(3),CHI,Rold(3)
    real(8), intent(out):: DEW

    ! Local variables
    real(8) SCO,SCN,SCP,SINR,DSN,DCS,DDD, sine,cosine
    integer(4) IKV
    !   reciprocal space Evald
    call TimerStart(T_DifEw)
    DEW=0.d0
!DEC$ SIMD
    do concurrent(IKV=1:NKV)
        SCO=0.5*dot_product(Recvec(:,IKV),Rold)
        SCN=0.5*dot_product(Recvec(:,IKV),Rnew)
        SCO = 0.5*(Recvec(1,IKV)*Rold(1)+Recvec(2,IKV)*Rold(2)+Recvec(3,IKV)*Rold(3))
        SCN = 0.5*(Recvec(1,IKV)*Rnew(1)+Recvec(2,IKV)*Rnew(2)+Recvec(3,IKV)*Rnew(3))
        SCP=SCO+SCN
        SINR=sin(SCN-SCO)*CHI*2.d0
        sine=sin(SCP)
        cosine = cos(SCP)
        DSN=cosine*SINR
        DCS=-sine*SINR
        D_SIN(IKV)=DSN
        D_COS(IKV)=DCS
        DDD=((2.d0*SSIN(IKV)+DSN)*DSN+(2.d0*SCOS(IKV)+DCS)*DCS)*RKV(IKV)
        DEW=DEW+DDD
    end do
    call TimerStop(T_DifEw)
    return
end  subroutine MCmodDIFEW