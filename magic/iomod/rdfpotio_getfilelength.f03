! Return length of the RDF/potential file
integer(4) function GetFileLength(iFilename, obj_type)    
    use precision
    use InOutUnit, only: stdin, stdout, iPrint
    
    character(*), intent(in) :: iFilename
    character(len=*), intent(in):: obj_type

    ! Local variables
    character(input_text) l
    integer ierr, iostt
    logical l_exist
    
    ! Check if the file exist
    inquire(file=ifilename, exist=l_exist)
    if (.not. l_exist) then
        write(stdout,*) 'ERROR: file not found: ', ifilename
        stop 10
    endif
 
    ! Open the file and scan it  for number of lines (variable getfilelength)
    open(unit=stdin, file=iFilename, status='old', iostat=iostt)
    if (iostt .ne. 0) then
        write(stdout,*)'ERROR: ',trim(obj_type),' input file ',iFilename,' can not be opened for reading!'
	stop 10
    endif
    getfilelength = 0
    ierr = 0    
    do 
        read(stdin,'(a)', iostat=ierr) l                
        if (is_iostat_end(ierr)) then
            if (iPrint.ge.5) then
                write(stdout,*) 'End of the ', trim(obj_type), ' file ',trim(iFilename),' reached.', getfilelength, ' lines read.'                
            endif
            exit
        else if (ierr.ne.0) then
            write(stdout,*)'ERROR: while reading ',trim(obj_type),' from file ',iFilename
            stop 10
        endif
        getfilelength = getfilelength + 1        
    enddo    
    close(stdin)    
end function