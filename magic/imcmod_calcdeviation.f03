subroutine CalcDeviation(DevS, DevRDF)
    !Global
    real(8), intent(out):: DevS, DevRDF
    !Local    
    integer is,js, NR, iBond
    real(8) dDevS, dDevRDF
        
    DevS=0
    DevRDF=0
    do iS=1, NSType
        do jS=iS, NSType
            if(Btest(StateNB(jS,iS),6)) then ! If the RDF was sampled
                Devs=devs+sum((Sref_NB_riSjS(:, jS, iS)-Scalc_NB_riSjS(:,jS,iS))**2)
                DevRDF=devRDF+sum((RDFref_NB_riSjS(:, jS, iS)-RDFcalc_NB_riSjS(:,jS,iS))**2)
            endif
        enddo
    enddo
    do iBond=1, NBOND
        if (Btest(StateB(iBond),6)) then ! if the RDF was sampled
            if (Btest(StateB(iBond),9)) & ! if this is actual bond (not linked)
                Devs=devs&
                    +sum((Sref_B_riBond(1:NPointsPotB(iBond), iBond)-Scalc_B_riBond(1:NPointsPotB(iBond),iBond))**2)
            DevRDF=devRDF&
                +sum((RDFref_B_riBond(1:NPointsPotB(iBond), iBond)-RDFcalc_B_riBond(1:NPointsPotB(iBond),iBond))**2)
        endif
    enddo
end subroutine CalcDeviation
