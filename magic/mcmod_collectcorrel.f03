subroutine MCmodCollectCorrel()
    ! Subroutine calculating Sij, and cross-correlators
    use iomod, only: NAtom, InvMode, LExternalTraj
    implicit none    
    
    integer(4) iAtom, jAtom, iBond, NR, nphi, iAngle, i, j, iSType,jSType
    type(Angle) :: A

    integer(8), allocatable::  Si_instant(:)

    call TimerStart(T_Average)
    allocate(Si_instant(size(CORS)))   
    Si_instant(:)=0        
    ! Treat Non-bonded pairs
    do iAtom=1, NAtom
        iSType=STypeAtom(iAtom)
        do jAtom=iAtom+1, NAtom
            jSType=STypeAtom(jAtom)
            if (BTest(StateNB(jSType,iSType),6)) then ! if we calculate RDF for this pair of atom types
                if (MaskNB_SR(jAtom,iAtom).and.(AdrGlobIndexAtomij(jAtom,iAtom).ge.0)) then ! if the atoms are not excluded from NB interactions and if the AdrGlobIndex is defined for the pair
  !                  if(Rij(jAtom,iAtom).lt.RCUT.and.Rij(jAtom,iAtom).gt.0.0) then ! if the atoms are not too far from each other
                    if(Rij(jAtom,iAtom).lt.RCUT) then ! if the atoms are not too far from each other
                        NR=INT(Rij(jAtom,iAtom)/RDFINC)+1
                        if (IndexGlobNB_riSjS(NR,jSType,iSType).ne.AdrGlobIndexAtomij(jAtom,iAtom)+NR) then
                            write(*,*)'Error MCmod Average NB: ',&
                                IndexGlobNB_riSjS(NR,jSType,iSType),AdrGlobIndexAtomij(jAtom,iAtom)+NR,NR,&
                                iSType, jstype, jAtom, iatom
                                if (LExternalTraj) cycle
                                stop 412
                        endif
                        Si_instant(AdrGlobIndexAtomij(jAtom,iAtom)+NR)=Si_instant(AdrGlobIndexAtomij(jAtom,iAtom)+NR)+1
                    else
                        npairsrdf(jSType,iSType)=npairsrdf(jSType,iSType)+1
                    endif
                endif
            endif
        enddo
    enddo
    
    do iAtom=1,NAtom
        do i=1,NBondsAtom(iAtom)
            iBond=BondNum(i,iAtom)
            if (Btest(StateB(iBond),6).and.Btest(StateB(iBond),5)) then ! if we calculate RDF for the bond
                jAtom=jAtomBond(i,iAtom)            
                if (jAtom.gt.iAtom) then ! if the atoms are not excluded from bonded
                    if(Rij(jAtom,iAtom).le.RMinPotB(iBond))then
                        NR=1
                    else if (Rij(jAtom,iAtom).ge.RMaxPotB(iBond)) then
                        NR=NPointsPotB(iBond)
                    else
                        NR=(Rij(jAtom,iAtom)-RMinPotB(iBond))*NPointsPotB(iBond)/(RMaxPotB(iBond)-RMinPotB(iBond))+1
                    endif
!                    if (IndexGlobB_riBond(NR,iBond).ne.AdrGlobIndexAtomij(jAtom,iAtom)+NR) then
!                        write(*,*)'Error MCmod Average B:', IndexGlobB_riBond(NR,iBond),AdrGlobIndexAtomij(jAtom,iAtom)+NR,&
!                        NR, iBOnd, bond_isjs(SiteAtom(iAtom),SiteAtom(jAtom)), jAtom, iAtom,SiteAtom(iAtom),SiteAtom(jAtom)
!                    endif
                    Si_instant(AdrGlobIndexAtomij(jAtom,iAtom)+NR)=Si_instant(AdrGlobIndexAtomij(jAtom,iAtom)+NR)+1
                endif
            endif
        enddo
    enddo
    ! Treat covalent angle bond interactions
    do iAngle=1, NAngles
        A=Angles(iAngle)
        if (BTEST(StateB(A%Bond),6).and.(A%LMove)) then !if we calculate RDF for the bond
            if (A%phi.le.RMinPotB(A%Bond)) then
              Nphi=1
            else if (A%phi.ge.RMaxPotB(A%Bond)) then
              Nphi=NPointsPotB(A%Bond)
            else
              Nphi=(A%phi-RMinPotB(A%Bond))*NPointsPotB(A%Bond)/(RMaxPotB(A%Bond)-RMinPotB(A%Bond))+1
            endif
!            if ((A%LMove) .and. (Nphi.gt.0.0)) Si_instant(IndexGlobB_riBond(Nphi,A%Bond))=Si_instant(IndexGlobB_riBond(Nphi,A%Bond))+1
!            if ((A%LMove)) Si_instant(IndexGlobB_riBond(Nphi,A%Bond))=Si_instant(IndexGlobB_riBond(Nphi,A%Bond))+1
            Si_instant(IndexGlobB_riBond(Nphi,A%Bond))=Si_instant(IndexGlobB_riBond(Nphi,A%Bond))+1
        endif
    enddo    
    

    !  correlators           Si_instant(I) = S_alpha   (alpha = I)    
    
    CORS=CORS+Si_instant   
    if(InvMode .ge. 1) then ! (if IMC or NG)
        call TimerStart(T_CrossCorrel)
        call MCModUpdateCrossCorrel(Si_instant)
        call TimerStop(T_CrossCorrel)
    endif    
    NAV=NAV+1
    EINTS=EINTS+EINT
    ENERS=ENERS+ENER
    EELS=EELS+EEL
#ifdef pressure
    VirNB_accum=VirNB_accum+VirNB; VirB_accum=VirB_accum+VirB; VirEL_accum=VirEL_accum-EEL    
#endif

    deallocate(Si_instant)    
    call TimerStop(T_Average)
end subroutine

subroutine MCModUpdateCrossCorrel(Si_instant)
    integer(8), intent(in):: Si_instant(:)
    integer(4) i, j
    do I=1,size(Si_instant) ! Over all points of joint potential
        if(Si_instant(i).gt.0) then ! if             
            do J=I,size(Si_instant) ! 
                if(Si_instant(j).gt.0) &
                    CORP(J,I)=CORP(J,I)+Si_instant(i)*Si_instant(J)     !  S_alpha * S_gamma
            end do
        end if	
    end do
end subroutine

