subroutine ReadTableCommon(text, bTable, eTable, r, U, PNPoints, PRMAX, PRMIN, PTYPE, df_kind)    
! Global Variables
    character(*), intent(in)    :: text(:),  df_kind, PType
    integer(4), intent(inout)   :: PNPoints
    integer(4), intent(in)      :: bTable, eTable
    real(8), intent(in)         :: PRMax, PRMin
    real(8), allocatable, intent(out) :: r(:), U(:)    

! Local Variables
    integer  k,j,ierr
    character(rdfpot_text) str
    real tmp_r, tmp_U
    ! Reading the RDF/potential table:
    ! First pass - check for data consistency: Count number of meaningful lines, check read errors 
    k=0
    do j=bTable+1, eTable-1
        call ReadLineFromText(text,j,str,ierr)
        k=k+1
        read(str,*,iostat=ierr) tmp_r, tmp_U
        if (ierr.ne.0) then
            write(stdout,*) 'ERROR: Reading ',trim(df_kind),' Table: Line ',j,':',str
            stop 163
        endif
    enddo
       
    if (k.ne.PNPoints) then
        if (str_cmp(PType,'NB').and.str_cmp(df_kind,'RDF')) then
            PNPoints=k
        else
            write(stdout,*) 'ERROR: Reading ',trim(df_kind),': actual number of lines in the table: ',k, &
            ' is not consistent with defined value of NPoints: ', PNPoints
            stop 164
        endif
    endif

    if (allocated(r)) deallocate(r,U)
    allocate(r(PNPoints))
    allocate(U(PNPoints))   
   ! Second pass - reading  the table
    k=0
    do j=bTable+1, eTable-1
        call ReadLineFromText(text,j,str,ierr)
        k=k+1
        read(str,*,iostat=ierr) tmp_r, tmp_U
        r(k)=tmp_r
        U(k)=tmp_U
        if ((r(k).gt.PRMAX).or.(r(k).lt.PRMIN)) then
            write(stdout,*) 'ERROR: Reading ',trim(df_kind),' Table: Line ',j,':',str
            write(stdout,*) 'The actual point ',r(k),' is beyond the range Min:Max, which is ',PRMIN,':',PRMAX
            stop 165
        endif
    enddo
    
    ! Check that R-values are within range MIN:MAX
    do j=1, size(r)
        if (r(j).gt.PRMAX) then
            write(stdout,*) 'ERROR: Reading ',trim(df_kind),' Table: Line ',j,':',str
            stop 166
        endif               
    enddo
end subroutine
