subroutine ReadPropBA(text, iFileName, b, e, SameAsBondNameMType, SameAsBondNumber, iBond, df_kind, df_type)
    ! Global Variables
    character(*), intent(in)         :: text(:)
    character(*), intent(in)         :: iFilename    
    character(*), intent(in)         :: df_kind
    integer, intent(in)::  b, e, df_type    
    character(mol_name_len), intent(inout):: SameAsBondNameMType(:)
    integer, intent(inout) :: SameAsBondNumber(:)
    integer(4), intent(out) :: iBond
!    character(*), intent(out)        ::  
! Local Variables
    integer  i,j,k,m,ierr, tmpNPairs, tmp, PNPairs, BondNumber, MolType
    integer, allocatable:: AtomTriplets(:,:)
    character(rdfpot_text) PairStr(3),field, l, str
    character(rdfpot_text), allocatable :: PairsStr(:)
    character(mol_name_len) SameAsBondNameMType_temp, NameMolType
    integer(4) SameAsBondNumber_temp
    character(7) str_grp, str_bond

    logical flag

    select case (df_type)
        case (2)
            str_grp = 'pair'
            str_bond = 'pair'
        case (3)
            str_grp = 'triplet'
            str_bond = 'angle'
        case default
            stop "Wrong df_type parameter value, Shall be 2 or 3"
    end select
    BondNumber=0
    NameMolType=''
    MolType = 0
    SameAsBondNameMType_temp = ''
    SameAsBondNumber_temp = 0
    do j=b, e ! Read properties which are specific to RDF/potential-type
        call ReadLineFromText(text,j,str,ierr)
        l=str
        call split(l,'=',field)
        select case (str_cln(field))
            case ('NTRIPLETS', 'NPAIRS')
                call str2value(trim(adjustl(l)),PNPairs,ierr)
                if (ierr.ne.0) then
                    write(stdout,*) 'ERROR: Reading ',df_kind,' File: ',trim(iFilename),&
                    ' line:',j,' Can not read the actual value of ', trim(field),' :',l
                    write(stdout,*) str
                    stop 130                
                endif
            case ('TRIPLETS', 'PAIRS')
                call removesp(l)
                allocate(PairsStr(StrCount(',',l) + 2))
                call parse(l,',',PairsStr,tmpNPairs)
                if (tmpNPairs.eq.0) then
                    write(stdout,*) 'ERROR: Reading ',df_kind,' File: ',trim(iFilename),' line:',j,&
                    ' Can not read the list of atom ', trim(field),' involved in angle-distribution'
                    write(stdout,*) str
                    stop 131             
                endif
                if (tmpNPairs.ne.PNpairs) then
                    write(stdout,*) 'ERROR: Reading ',df_kind,' File: ',trim(iFilename),&
                    ' line:',j,' Declared number of atomic', trim(str_grp),' :',PNpairs,&
                    ' differs from actual number of stated ', trim(str_grp),' :',tmpNPairs
                    write(stdout,*) str
                    stop 132
                endif
                if (allocated(AtomTriplets)) deallocate(AtomTriplets)
                allocate(AtomTriplets(tmpNPairs,3))
                do i=1, tmpNPairs
                    l=PairsStr(i)
                    call removesp(l)
                    call parse(l,'-',PairStr,tmp)
                    if (tmp.ne.df_type) then
                        write(stdout,*) 'ERROR: Reading ',df_kind,' File: ',trim(iFilename),&
                        ' line:',j,' Can not read the atom numbers of the ',str_grp,' ',i,&
                        ' involved in the ',trim(str_bond),'-bond distribution'
                        write(stdout,*) l
                        stop 133
                    endif
                    do k=1, df_type
                        call str2value(trim(adjustl(PairStr(k))),AtomTriplets(i,k),ierr)
                        if (ierr.ne.0) then
                            write(stdout,*) 'ERROR: Reading ',df_kind,' File: ',trim(iFilename),' line:',j,&
                                       ' Can not read the atomic ',str_grp,' included in ',trim(str_bond),'-bond distribution.',l                    
                            write(stdout,*) (trim(adjustl(PairStr(m))), m=1,df_type)
    !                        '"',trim(adjustl(PairStr(1))),'" ',' "',trim(adjustl(PairStr(2))),'"',&
    !                                    ' "',trim(adjustl(PairStr(3))),'"'
                            stop 134
                        endif
                    enddo
                enddo
                deallocate(PairsStr)
            case ('MOLTYPE')
                call removesp(l)
                NameMolType=adjustl(trim(l))
                MolType = GetMolTypeByName(NameMolType)
                if (MolType .eq. 0) then ! Nothing found
                    write(stdout,*) 'ERROR: Reading ',df_kind,' File: ',trim(iFilename),' line:',j, &
                                    ' unknown Molecular type ',NameMolType
                    stop 135
                endif

            case ('BONDNUMBER')
                call str2value(trim(adjustl(l)),BondNumber,ierr)
                if (ierr.ne.0) then
                    write(stdout,*) 'ERROR: Reading ',df_kind,' File: ',trim(iFilename),' line:',j, &
                            ' Can not read the actual number of the bond ',l
                    write(stdout,*) str
                    stop 136
                endif
            case ('SAMEASBOND')
!                write(stdout,*) 'Debug SameAsBond=',trim(adjustl(l))
                call split(l,':',field) ! split on moltype(l) and bondnumber(field)
                SameAsBondNameMType_temp = field
                call str2value(trim(adjustl(l)), i, ierr)
                if (ierr.ne.0) then
                    write(stdout,*) 'ERROR: Reading ',df_kind,' File: ',trim(iFilename),' line:',j, &
                            ' Can not read the bond number',l
                    write(stdout,*) str
                    stop 137
                endif
                SameAsBondNumber_temp = i
!                write(*,*) 'DEBUG: ReadRDFPropA:', '',df_kind,'', RDFSameAsBondNameMType_temp, 'BondNumer=',i

            case default
                continue !write(stdout,*) 'Debug: Unknown line:',trim(adjustl(str))
        end select
    enddo
    if (BondNumber.eq.0) then
        write(stdout,*) 'ERROR: Reading ',df_kind,' File: ',trim(iFilename),' Lines:',b,':', e, ' BondNumber is not stated'
        stop 138
    endif

    if (MolType.eq.0) then
        write(stdout,*) 'ERROR: Reading ',df_kind,' File: ',trim(iFilename),' Lines:',b,':', e, ' Molecular type name is not stated'
        stop 139
    endif

    iBond=sum(NBondMType(1:MolType-1))+BondNumber

    if (SameAsBondNameMType_temp .ne. '') then
        SameAsBondNameMType(iBond) = SameAsBondNameMType_temp
        SameAsBondNumber(iBond) = SameAsBondNumber_temp
    endif

    if (PNPairs.ne.NSiteGroupsInBond(iBond)) then
        write(stdout,*) 'ERROR: Reading ',df_kind,' File: ',trim(iFilename),&
        ' Bond distribution for bond ', BondNumber, ' of MolType ', trim(NameMType(MolType))
        write(stdout,*) 'Number of site/atom groups defined for the bond: ',PNPairs, ' differs from'
        write(stdout,*) 'number of site/atom groups defined in the mcm file : ', NSiteGroupsInBond(iBond)
        stop 140
    endif

    do i=1, PNPairs ! Pairs stated in the RDF-file
        flag=.false.
        if (Bond_iSjS(AtomTriplets(i,1)+AdrSiteMType(MolType),AtomTriplets(i,df_type)+AdrSiteMType(MolType)).ne.iBond) then
            write(stdout,*) 'ERROR: Reading ',df_kind,' File: ',trim(iFilename),' distribution for bond '&
                    , BondNumber, 'of MolType ', trim(NameMType(MolType))
            write(stdout,*)'According to the molecular description in mcm-file, site-group:',&
                AtomTriplets(i,1:df_type),' does not belong to the bond ',BondNumber
            write(stdout,*) Bond_iSjS(AtomTriplets(i,1)+AdrSiteMType(MolType),AtomTriplets(i,df_type)+AdrSiteMType(MolType)), iBond
            stop 141            
        endif
        
        if (df_type==3) then
            do j=1, NSiteGroupsInBond(iBond) ! Pairs stated in mcm-file
                if (AtomTriplets(i,1)+AdrSiteMType(MolType).eq.iSiteInBond(iBond,j).and.&
                    AtomTriplets(i,3)+AdrSiteMType(MolType).eq.jSiteInBond(iBond,j).and.&
                    AtomTriplets(i,2)+AdrSiteMType(MolType).eq.kSiteInBond(iBond,j)) &
                    then
                    flag=.true.
                    exit
                endif
            enddo
        endif
        if (df_type==2) then
            do j=1, NSiteGroupsInBond(iBond) ! Pairs stated in mcm-file
                if (AtomTriplets(i,1)+AdrSiteMType(MolType).eq.iSiteInBond(iBond,j).and.&
                    AtomTriplets(i,2)+AdrSiteMType(MolType).eq.jSiteInBond(iBond,j))&
                    then
                    flag=.true.
                    exit
                endif
            enddo
        endif
        if (.not.flag) then
            write(stdout,*) 'ERROR: Reading ',df_kind,' File: ',trim(iFilename),&
            ' Distribution for bond ', BondNumber, 'of MolType ', trim(NameMType(MolType))
            write(stdout,*) 'Can not find ',i,' group of sites/atoms defining the bond in the mcm file : ', AtomTriplets(i,1:3)
                    stop 142
        endif
    enddo
end subroutine
