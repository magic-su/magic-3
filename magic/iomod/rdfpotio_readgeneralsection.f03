subroutine ReadGeneralSection(text,iend, N_NB, N_B, N_A, NPoints,RMax, RMin, df_kind,&
        df_NAngleBondsExcludeMType, df_NPairBondsExcludeMType)
! Read the General section of RDF/pot-file and check that it is in agreement with what we got from mcm-file    
! Global variables
    integer(4), intent(in) :: iend
    character(rdfpot_text), allocatable, intent(in) :: text(:)
    character(*), intent(in) :: df_kind
    real(8), intent(out)::  RMax, RMin
    integer(4), intent(out) :: NPoints, N_NB, N_B, N_A
    integer(4), intent(inout):: df_NAngleBondsExcludeMType(:), df_NPairBondsExcludeMType(:)

! Local variables        
    integer i,j,k, ierr, iostt, bGeneral,eGeneral, cnt_bGeneral, cnt_eGeneral,&
            df_LengthNameSType, df_NSType, nrecs, irec, iMType, iMT
    character(rdfpot_text) l,str,field, mtype_name
    character(rdfpot_text), allocatable:: rec_mtype(:)
    character(atom_name_len), allocatable:: df_NameSType(:)
    logical flag 
    
    i=1
    bGeneral=0
    eGeneral=0
    cnt_bGeneral=0
    cnt_eGeneral=0
    do while (i.le.iend)
        call ReadLineFromText(text,i,l,ierr)
        ! Indicate presence of &General 
        if(str_cmp(l,'&GENERAL')) then
            bGeneral=i
            cnt_bGeneral=cnt_bGeneral+1
        endif
        if(str_cmp(l,'&ENDGENERAL')) then 
            eGeneral=i
            cnt_eGeneral=cnt_eGeneral+1
        endif
        i=i+1
    enddo
    ! Indicate presence of &EndGeneral and check consistency
    if ((cnt_bGeneral.gt.1) .or. (cnt_eGeneral.gt.1)) then
        write(stdout,*) 'ERROR: Reading ',df_kind,': more than one line with tag &General or &EndGeneral found '
        stop 101
    endif
    if (bGeneral.eq.0) then
        write(stdout,*) 'ERROR: Reading ', df_kind, ': Can not find begin of section &General'
        stop 102
    endif
    if (eGeneral.eq.0) then
        write(stdout,*) 'ERROR: Reading ',df_kind,': Can not find end of section &EndGeneral'
        stop 103
    endif
    if (bGeneral.gt.eGeneral) then
        write(stdout,*) 'ERROR: Reading ',df_kind,': The section &General starts at line ',bGeneral, 'and ends at line ',eGeneral
        stop 104
    endif
            
    ! Read fields
    ! Default values    
    N_NB=0
    N_B=0
    N_A=0
    RMax=0
    RMin=0
    NPoints=0
    df_NSType=0
    df_LengthNameSType=0
    write(stdout,*) ''
    write(stdout,*) 'Reading &General section of the ',df_kind,' file'
    do i=bGeneral,eGeneral
        call ReadLineFromText(text,i,str,ierr)
        l=str
        call split(l,'=',field)
        select case (str_cln(field))
        case ('N_NB')
            call str2value(trim(adjustl(l)),N_NB,ierr)
            if (ierr==0) then
                write(stdout,*) '   Number of Non-bonded ',df_kind,'s to read:', N_NB
            else
                write(stdout,*) 'ERROR: Reading ',df_kind,': line:,',i,' Can not read the actual value of N_NB:',l
                write(stdout,*) str
                stop 105
            endif
        case('N_B')
            call str2value(trim(adjustl(l)),N_B,ierr)
            if (ierr==0) then
                write(stdout,*) '   Number of pairwise bond ',df_kind,'s to read:', N_B
            else
                write(stdout,*) 'ERROR: Reading ',df_kind,': line:',i,' Can not read the actual value of N_B:',l
                write(stdout,*) str
                stop 106
            endif
        case('N_A')
            call str2value(trim(adjustl(l)),N_A,ierr)
            if (ierr==0) then
                write(stdout,*) '   Number of angle ',df_kind,'s to read:', N_A
            else
                write(stdout,*) 'ERROR: Reading ',df_kind,': line:',i,' Can not read the actual value of N_A:',l
                write(stdout,*) str
                stop 107
            endif
        case('NPOINTS')
            call str2value(trim(adjustl(l)),NPoints,ierr)
            if (ierr==0) then
                write(stdout,*) '   Number of points for every non-bonded ',df_kind,':', NPoints
            else
                write(stdout,*) 'ERROR: Reading ',df_kind,': line:',i,' Can not read the actual value of NPoints:',l
                write(stdout,*) str
                stop 108
            endif
        case('MAX')
            call str2value(trim(adjustl(l)),RMAX,ierr)
            if (ierr==0) then
                write(stdout,*) '   Cut off radius for non-bonded short range interactions:', RMAX
            else
                write(stdout,*) 'ERROR: Reading ',df_kind,': line:',i,' Can not read the actual value of MAX:',l
                write(stdout,*) str
                stop 109
            endif
        case('MIN')
            call str2value(trim(adjustl(l)),RMIN,ierr)
            if (ierr==0) then
                write(stdout,*) '   Minimum distance (hard repulsive wall) for non-bonded short range interactions:', RMIN
            else
                write(stdout,*) 'ERROR: Reading ',df_kind,': line:',i,' Can not read the actual value of MIN:',l
                write(stdout,*) str
                stop 110
            endif
        case('NTYPES')
            call str2value(trim(adjustl(l)),df_NSType,ierr)
            if (ierr==0) then
                write(stdout,*) '   Number of atomic (site) types:', df_NSType
            else
                write(stdout,*) 'ERROR: Reading ',df_kind,': line:',i,' Can not read the actual value of NTypes:',l
                write(stdout,*) str
                stop 111
            endif
        case('TYPES')
!            if (present(NSType) .and. present(NameSType)) then 
                call removesp(l)            
                call reshape(df_NameSType, NSType)            
                call parse(l,',',df_NameSType,df_LengthNameSType)
                if (df_LengthNameSType.eq.0) then
                    write(stdout,*) 'ERROR: Reading ',df_kind,': line:',i,' Can not read the list of atom/site types'
                    write(stdout,*) str
                    stop 112                
                endif
                if (df_LengthNameSType.ne.NSType) then
                    write(stdout,*) 'ERROR: Reading ',df_kind,': line:',i,&
                    ' Stated list of atom/site types differs from the declared number of site types',NSType
                    write(stdout,*) str
                    stop 113                
                endif
                ! compare types lists from mcmfile and from rdf/pot
                do j=1, NSType
                    flag=.false.
                    do k=1, NSType
                        if (df_NameSType(j).eq.NameSType(k)) then
                            flag=.true.
                            exit
                        endif
                    enddo
                    if (.not. flag) then
                        write(stdout,*) 'ERROR: Reading ',df_kind,': line:',i
                        write(stdout,*) 'Atom/site type ',df_NameSType(j), 'which is stated in the ',df_kind,'-file ',&
                        'does not exist in the list of atom/site types read from mcm-files'
                        stop 114
                    endif
                enddo                                       
                write(stdout,*) '   Atomic (site) types read:', (df_NameSType(j), j=1, df_LengthNameSType)
!            endif
        case('NANGLEBONDSEXCLUDE')
            call ReadExclusionHeader(l, 'Angle', df_kind, df_NAngleBondsExcludeMType)        
        case('NPAIRBONDSEXCLUDE')
            call ReadExclusionHeader(l, 'Pair', df_kind, df_NPairBondsExcludeMType)

        case default
                continue !write(stdout,*) 'Debug: Unknown line:',trim(adjustl(str))
        end select
    enddo
    ! Check if all the necessary values were read
    if (N_NB.eq.0) then
        write(stdout,*) 'ERROR: Reading ',df_kind,':'
        write(stdout,*) 'No non-bonded ',df_kind,'s are stated to read'       
        stop 115
    endif
    if (NPoints.eq.0) then
        write(stdout,*) 'ERROR: Reading ',df_kind,':'
        write(stdout,*) 'Number of points for every non-bonded ',df_kind,' (NPOINTS) is not defined.'       
        stop 116
    endif
    if (RMAX.le.0.0000001) then
        write(stdout,*) 'ERROR: Reading ',df_kind,':'
        write(stdout,*) 'Cut off radius for non-bonded short range ',df_kind,' ',& 
            '(RMAX) is not defined'       
        stop 117
    endif
    if (RMIN.le.0.0000001) then
        write(stdout,*) '   Minimum distance (hard repulsive wall) for non-bonded',&
            ' short range interactions (RMIN) is not defined or set to 0'       
        write(stdout,*) 'Default value of 0.0 will be used'
    endif
    if (df_kind=='RDF') then 
        if (df_NSType.eq.0) then
            write(stdout,*) 'ERROR: Reading ',df_kind,':'
            write(stdout,*) 'Number of atom/site types (NTypes) is not defined.'       
            stop 118
        endif
        if (df_LengthNameSType.eq.0) then
            write(stdout,*) 'ERROR: Reading ',df_kind,':'
            write(stdout,*) 'List of atom/site types (Types) is not defined'       
            stop 119
        endif
    endif
    if (N_B.eq.0) then
        write(stdout,*) 'WARNING: Reading ',df_kind,':'
        write(stdout,*) 'No pairwise bonded ',df_kind,'s are stated to read'
    endif
    if (N_A.eq.0) then
        write(stdout,*) 'WARNING: Reading ',df_kind,':'
        write(stdout,*) 'No angle-bending bonded ',df_kind,'s are stated to read'
    endif
end subroutine
