!    Set of utilities
!
!    RAND   - random number generator
!    RANQ   - generate random quaternion vector
!    ROTATE - rotate a molecule according to a quaternion vector
!    ROTATE2 - rotate a molecule over a given axis on a given angle
!    RandRotVect - generate random axis for random rotation
!    MakeSaveFileName(BaseOutName, extension, IREP, ID) - generate proper name for output file, based on base-name and file type


module utilsmod    
    use precision
    implicit none
    interface assertequal
        module procedure assertequal_real8
        module procedure assertequal_real8_vec       
    end interface
    
    real(8), private, parameter:: PI=3.14159256D0! PI=3.1415926535897932d0
contains
!==================== RAND =============================================
!$PRAGMA SUN OPT=0
! Random numbers generator
real(8) function RAND()
    integer(4):: ITWO=2, M2=0,IX=14371,HALFM,IA0,IC0,IA,IC,MIC,IY,M
    real(8) S
    save
    if(M2.eq.0) then
        M=1
        do while (M.gt.M2)
            M2=M
            M=ITWO*M2
        enddo
        HALFM=M2
        IA0=HALFM*datan(1.d0)/8.d0
        IC0=HALFM*(0.5d0-dsqrt(3.d0)/6.d0)
        IA=8*IA0+5
        IC=2*IC0+1
        MIC=(M2-IC)+M2
        S=sngl(0.5d0/HALFM)
    endif
    IY=IX*IA
    if(IY.gt.MIC)IY=(IY-M2)-M2
    IY=IY+IC
    if(IY/2.gt.M2)IY=(IY-M2)-M2
    if(IY.lt.0)IY=(IY+M2)+M2
    IX=IY
    RAND=IY*S
    return
end function rand
!============RanRotVec==========================================
!Generate random vector (to rotate molecule around)
function RanRotVec()
    real(8), dimension(3) :: RanRotVec

    real(8) CosTheta,SinTheta,Phi
        
    CosTheta=rand()*2.d0-1.d0
    SinTheta=Dsqrt(1.d0-CosTheta**2)
    Phi=rand()*2.d0*Pi
    RanRotVec = [Dcos(Phi)*sinTheta, Dsin(Phi)*sinTheta, cosTheta]
    
end function RanRotVec
!=============== ROTATE ==============================================
! Rotation of vector (X,Y,Z) using quaternion Q
function ROTATE(Q, vec)    
    real(8), dimension(3) :: ROTATE    
    real(8) Q(4), vec(3)
    real(8) SIGN,AA,BB,CC,DD,AB,AC,AD,BC,BD,CD,RX,RY,RZ
!  rotate using quaternion vector Q
    SIGN = -1.D0
    AA   = Q(1)*Q(1)
    BB   = Q(2)*Q(2)
    CC   = Q(3)*Q(3)
    DD   = Q(4)*Q(4)
    AB   = Q(1)*Q(2)
    AC   = Q(1)*Q(3)
    AD   = Q(1)*Q(4)*SIGN
    BC   = Q(2)*Q(3)
    BD   = Q(2)*Q(4)*SIGN
    CD   = Q(3)*Q(4)*SIGN
    RX   = vec(1)*(-AA+BB-CC+DD)+vec(2)*(+CD-AB)*2.D0 +vec(3)*(+BC+AD)*2.D0
    RY   = vec(1)*(-AB-CD)*2.D0 +vec(2)*(+AA-BB-CC+DD)+vec(3)*(+BD-AC)*2.D0
    RZ   = vec(1)*(+BC-AD)*2.D0 +vec(2)*(-AC-BD)*2.D0 +vec(3)*(-AA-BB+CC+DD)
    ROTATE = [RX, RY,RZ]
END function ROTATE

function RotMat(phi, a_vec)
    real(8), dimension(3,3):: RotMat
    real(8) phi, a_vec(3)
    
    real(8) CosPhi,OneMinCos,SinPhi,x,y,z,xy,xz,yz,xx,yy,zz
    
    CosPhi=Dcos(phi)
    OneMinCos=1-CosPhi
    SinPhi=Dsin(phi)
    RotMat(1,1)= CosPhi+OneMinCos*a_vec(1)*a_vec(1)
    RotMat(2,1)= OneMinCos*a_vec(1)*a_vec(2) + SinPhi*a_vec(3)
    RotMat(3,1)= OneMinCos*a_vec(1)*a_vec(3) - SinPhi*a_vec(2)

    RotMat(1,2)= OneMinCos*a_vec(1)*a_vec(2) - SinPhi*a_vec(3)
    RotMat(2,2)= CosPhi + OneMinCos*a_vec(2)*a_vec(2)
    RotMat(3,2)= OneMinCos*a_vec(2)*a_vec(3) + SinPhi*a_vec(1)

    RotMat(1,3)= OneMinCos*a_vec(1)*a_vec(3) + SinPhi*a_vec(2)
    RotMat(2,3)= OneMinCos*a_vec(2)*a_vec(3) - SinPhi*a_vec(1)
    RotMat(3,3)= CosPhi + OneMinCos*a_vec(3)*a_vec(3)
    
end function

!=============ROTATE2=============================================
!Rotate vector (rx,ry,rz) around vector (ax,ay,az) on angle phi using rotation matrix
function Rotate2(r_vec, phi, a_vec)
    real(8), dimension(3) :: Rotate2
    real(8), intent(in) :: r_vec(3), phi, a_vec(3)   

    Rotate2 = MATMUL(RotMat(phi, a_vec), r_vec)
end function Rotate2

!=============ROTATE2Mult=============================================
!Rotate array of vectors Rvec(3, N) around vector (ax,ay,az) on angle phi using rotation matrix
subroutine Rotate2Mult(Rvec, phi, a_vec)
    real(8), intent(inout):: Rvec(:,:)
    real(8), intent(in):: phi, a_vec(3)

    Rvec=MatMul(RotMat(phi, a_vec), Rvec)
    return
end subroutine

!=============== RANQ =========================================
!
!  Generate random quaternion vector
!$PRAGMA SUN OPT=0
function RANQ()
    real(8), dimension(4):: RANQ
    real(8) A,B,C
    
    A=PI*RAND()/2.D0
    B=PI*RAND()
    C=PI*RAND()
    RANQ = [DSIN(A)*DSIN(B-C), DSIN(A)*DCOS(B-C), DCOS(A)*DSIN(B+C), DCOS(A)*DCOS(B+C)]

END function RANQ
!

subroutine invInPlace(A)
  integer, parameter:: dp=8
  real(dp), dimension(:,:), intent(inout) :: A

  real(dp), dimension(size(A,1)) :: work  ! work array for LAPACK
  integer(4), dimension(size(A,1)) :: ipiv   ! pivot indices
  integer(4) :: n, info

  work=0.0
  ipiv=0
  n = size(A,1)

  ! DGETRF computes an LU factorization of a general M-by-N matrix A
  ! using partial pivoting with row interchanges.
  call DGETRF(n, n, A, n, ipiv, info)
  if (info /= 0) then
      write(*,*) "Inverting Matrix: DGETRF: Info=",info
      write(*,*) 'Matrix is numerically singular!'
      stop 1
  end if

  ! DGETRI computes the inverse of a matrix using the LU factorization
  ! computed by DGETRF.
  call DGETRI(n, A, n, ipiv, work, n, info)
  if (info /= 0) then
      write(*,*) "Inverting Matrix: DGETRI: Info=",info
      write(*,*)'Matrix inversion failed!'
      stop 1 
  end if
end subroutine

character(filename_text) function MakeSaveFileName(BaseOutName, extension, IREP, ID)
    use precision
    implicit none
    integer(4), intent(in), optional::IREP, ID
    character(*), intent(in):: BaseOutName,extension
       
    MakeSaveFileName=BaseOutName(:)    
    if (present(IREP)) then
        MakeSaveFileName(Len_Trim(MakeSaveFileName)+1:Len_Trim(MakeSaveFileName)+2)='.i'
        write(MakeSaveFileName(Len_Trim(MakeSaveFileName)+1:Len_Trim(MakeSaveFileName)+3),'(i0.3)') IREP
    endif
    if (present(ID)) then
        if (ID.ne.0) then
            MakeSaveFileName(Len_Trim(MakeSaveFileName)+1:Len_Trim(MakeSaveFileName)+2)='.p'
            write(MakeSaveFileName(Len_Trim(MakeSaveFileName)+1:Len_Trim(MakeSaveFileName)+4),'(i0.4)') ID
        endif
    endif
    MakeSaveFileName(Len_Trim(MakeSaveFileName)+1:Len_Trim(MakeSaveFileName)+Len_Trim(extension))=trim(extension)    
        
end function 

logical function isequal(one, another, threshold, l_relative)
    implicit none
    real(8), intent(in) :: one, another
    real(4), intent(in):: threshold
    logical, intent(in):: l_relative
    
!    character(*), intent(in) :: message    
    if (l_relative .and. one.ne.0.0) then
        isequal = (abs(1.0 - another/one).lt.threshold)
    else
        isequal = (abs(one - another).lt.threshold)
    endif
    
end function

subroutine assertequal_real8(message, one, another, threshold, l_relative)
    implicit none
    real(8), intent(in) :: one, another
    real(4), intent(in):: threshold
    character(*), intent(in) :: message
    logical, intent(in):: l_relative
!    character(120) error_message
    if (.not. isequal(one, another, threshold, l_relative)) then
        write(*,*) trim(message)," Not Equal:", one, another
        stop 1
    endif
    
end subroutine
subroutine assertequal_real8_vec(message, one, another, threshold, l_relative)
    implicit none
    real(8), intent(in) :: one(:), another(:)
    real(4), intent(in):: threshold
    character(*), intent(in) :: message
    logical, intent(in):: l_relative
    integer(4) i
!    character(120) error_message
    if (size(one).ne.size(another)) stop "AssertEqual_real_array error: arrays of different sizes"
    if (.not. all([(isequal(one(i), another(i), threshold, l_relative), i=1, size(one))])) then
        write(*,*) trim(message)," Not Equal:", one, another
        stop 1
    endif
    
end subroutine
end
