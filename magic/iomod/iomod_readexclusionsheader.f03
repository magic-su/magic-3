subroutine ReadExclusionHeader(line, type_, source_type, NBondExcludeMType_)
    character(*), intent(in) :: line, type_, source_type
    integer(4), intent(inout) :: NBondExcludeMType_(:)

    ! Local variables
    character(rdfpot_text) str, mtype_name
    character(rdfpot_text), allocatable:: rec_mtype(:)
    integer(4) nrecs, irec, iMType, j, ierr
    
    if (.not.(str_cmp(type_, 'ANGLE') .or. str_cmp(type_, 'PAIR'))) then
        write(*,*) 'Error: wrong value of parameter type_. It must be either ANGLE or PAIR'
        stop 633
    endif
    
    call removesp(line)  
    allocate(rec_MType(NMType))
    rec_MType(:) = ''
    if (StrCount(',', line).eq.0) then                
        nrecs = 1
        rec_MType(1) = line
    else
        call parse(line,',', rec_MType, nrecs)
    endif
    if (nrecs.ne.NMType) then
        write(stdout,*) 'ERROR: Reading ', source_type, ': line:',trim(line)
        write(stdout,*) ' Number of MolType:value records differs from number of molecular types in the system'
        write(stdout,*) 'N(MolTypes):', NMType, '!= N(records):', nrecs
        stop 638
    endif
! compare types lists from mcmfile and from the record
    do irec=1, nrecs
        str = rec_MType(irec)
        call split(str,':',mtype_name)
        iMType = GetMolTypeByName(mtype_name)
        if (iMType .eq. 0) then ! Nothing found
            write(stdout, *) 'ERROR: Reading ', source_type,': line:',trim(line),'record: ', trim(rec_MType(irec))
            write(stdout, *) 'Can not find Molecular Type with name:', trim(adjustl(mtype_name))
            stop 636        
        endif                
        call str2value(trim(adjustl(str)), j, ierr)
        if (ierr.ne.0) then
            write(stdout, *) 'ERROR: Reading ', source_type,': line:',trim(line),'record: ', trim(rec_MType(irec))
            write(stdout, *) ' Can not read the exclusion value. It has to be positive integer number'
            stop 637
        endif
        NBondExcludeMType_(iMType) = j
    enddo
    write(stdout,*) ' ', type_, 'BondExclusions read:'
    do iMType=1, NMType
      write(stdout,*) '    ',trim(NameMType(iMType)), NBondExcludeMType_(iMType)
    enddo    
    deallocate(rec_mtype)   
end subroutine
    
