subroutine ReadPropCommon(text, beg_text, end_text, PName, PRmin, PRmax, PNPoints, PType,&
                            cnt_N_NB, cnt_N_B, cnt_N_A, bTable, eTable, FlagFixed, &
                            FlagZero,FlagTableFound, FlagIncludeFileFound, IncludeFileName, InitPot, df_kind)
! Global Variables
    character(*), intent(in)    :: text(:), df_kind
    character(*), intent(out)   :: PName,PType
    character(*), intent(out)   :: IncludeFileName    
    integer(4), intent(in)      :: beg_text, end_text 
    integer(4), intent(out)     :: PNPoints, bTable, eTable
    integer(4), intent(inout)   :: cnt_N_NB, cnt_N_B, cnt_N_A
    integer(4), intent(out)     :: InitPot
    real(8), intent(out)        :: PRmin, PRmax
    logical, intent(out)        :: FlagTableFound, FlagIncludeFileFound,FlagZero, FlagFixed
! Local Variables
    integer  cnt_bTable, cnt_eTable, j,ierr
    character(rdfpot_text) str,l,field
    
    bTable=0
    eTable=0
    cnt_eTable=0
    cnt_bTable=0
    ! Default values
    FlagZero=.false.
    FlagTableFound=.false.
    FlagIncludeFileFound=.false.
    FlagFixed=.false. 
    InitPot =-1 ! Undefined
    do j=beg_text, end_text
        call ReadLineFromText(text,j,str,ierr)
        l=str
        call split(l,'=',field)        
        select case (str_cln(field))
            case ('NAME')
                PName=trim(adjustl(l))
            case ('MIN')
                call str2value(trim(adjustl(l)),PRMIN,ierr)
                if (ierr.ne.0) then
                    write(stdout,*) 'ERROR: Reading ',trim(df_kind),':  line:',j,' Can not read the actual value of MIN:',l
                    write(stdout,*) str
                    stop 143                                                                                                    
                endif
            case ('MAX')
                call str2value(trim(adjustl(l)),PRMAX,ierr)
                if (ierr.ne.0) then
                    write(stdout,*) 'ERROR: Reading ',trim(df_kind),':  line:',j,' Can not read the actual value of MAX:',l
                    write(stdout,*) str
                    stop 144                                                                      
                endif
            case ('NPOINTS')
                call str2value(trim(adjustl(l)),PNPoints,ierr)
                if (ierr.ne.0) then
                    write(stdout,*) 'ERROR: Reading ',trim(df_kind),':  line:',j,' Can not read the actual value of NPoints:',l
                    write(stdout,*) str
                    stop 145                
                endif
            case ('TYPE')
                PType=trim(adjustl(l))
                select case (trim(adjustl(PType)))
                    case ('NB')
                        cnt_N_NB=cnt_N_NB+1
                    case ('B')
                        cnt_N_B=cnt_N_B+1 
                    case ('A')
                        cnt_N_A=cnt_N_A+1
                    case default
                        write(stdout,*) 'Error: line ',j,': Wrong type of the ',trim(df_kind),': ',&
                                    trim(adjustl(PType)),'    It shall be NB, B or A'                                
                end select 
            case ('&TABLE')
                   FlagTableFound=.true.
                   bTable=j
                   cnt_bTable=cnt_bTable+1                   
            case ('&ENDTABLE')
                   eTable=j
                   cnt_eTable=cnt_eTable+1
            case ('&FIXED')
                   FlagFixed=.true.                   
            case ('&ZERO')  
                   FlagZero=.true.                   
            case ('&INITZERO')  
                   InitPot=0
            case ('&INITPMF')  
                   InitPot=1                   
            case ('&INCLUDERDF', '&INCLUDEPOTENTIAL')
                   FlagIncludeFileFound=.true.
                   IncludeFileName=trim(adjustl(l))
!                   write(stdout,*) 'Debug: IncludedFileFound:', IncludeFileName                   
            case ('INITPOT')
!                   write(stdout,*) 'Debug InitPot=',trim(adjustl(l))
                   select case (str_cln(adjustl(l)))
                       case ('PMF')
                           InitPot=1
                       case ('ZERO')
                           InitPot=0
                       case default
                           write(stdout,*) 'ERROR: Reading ',trim(df_kind),':  line:',j,&
                                   ' Unknown type of potential initialization:',trim(adjustl(l))
                           stop 146
                   end select
                    
            case default
                continue 
        end select                                
    enddo
    !    Set default behavior for initial trial potentials: Zero for NB, PMF for B/A
    if (InitPot.eq.-1) then        
        if (trim(adjustl(PType)).eq.'NB') then
            InitPot=0
        else
            InitPot=1
        endif
    endif
    ! Check consistency of &Table &EndTable tags
    if  ((cnt_bTable.gt.1) .or. (cnt_eTable.gt.1)) then
        write(stdout,*) 'ERROR: Reading ',trim(df_kind),':  more than one line with tag &Table or &EndTable found '
        stop 147
    endif
    if (FlagTableFound) then 
        if (bTable.eq.0) then
            write(stdout,*) 'ERROR: Reading ',trim(df_kind),':  Can not find begin of section &Table'
            stop 148
        endif
        if (eTable.eq.0) then
            write(stdout,*) 'ERROR: Reading ',trim(df_kind),':  Can not find end of section &EndTable'
            stop 149
        endif
        if (bTable.gt.eTable) then
            write(stdout,*) 'ERROR: Reading ',trim(df_kind),':  The section &Table starts at line ',bTable,&
                    'and ends at line ',eTable
            stop 150
        endif    
    endif 
    if (FlagTableFound .and. FlagIncludeFileFound) then
        write(stdout,*) 'ERROR: Reading ',trim(df_kind),&
                ':  Both &Table and &Includend_text are present in the ',trim(df_kind),' definition.'
        stop 151
    endif
    if (.not.(FlagTableFound .or. FlagIncludeFileFound.or.FlagZero)) then
        write(stdout,*) 'ERROR: Reading ',trim(df_kind),':  Neither &Table nor &Includend_text nor &Zero are found', &
                    'The ',trim(df_kind),' values are not defined.'
        stop 152        
    endif    
end subroutine
