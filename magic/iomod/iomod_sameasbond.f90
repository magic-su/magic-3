subroutine ProcessSameAsBondRecords(RDFSameAsBondNameMType, PotSameAsBondNameMType, RDFSameAsBondNumber, PotSameAsBondNumber)
    implicit none
    character(mol_name_len), allocatable, intent(in):: RDFSameAsBondNameMType(:), PotSameAsBondNameMType(:)
    integer(4), allocatable, intent(in):: RDFSameAsBondNumber(:), PotSameAsBondNumber(:)

    integer(4) iBond, iMT

    call reshape(BondSameBond, max(NBond,1))

    if (allocated(RDFSameAsBondNameMType) .and. allocated(PotSameAsBondNameMType)) then ! Both records are present
        ! Check that records in RDF and Potential files are coherent
        do iBond = 1, NBond
            ! Check the MolType Name
            if (RDFSameAsBondNameMType(iBond) .ne. PotSameAsBondNameMType(iBond)) then
                write(stdout,*) 'Error SameAsBond record MolType mismatch between RDF and Potential file.'
                write(stdout,*) 'RDF file:',RDFSameAsBondNameMType(iBond), ', Pot file:',PotSameAsBondNameMType(iBond)
                write(stdout,*) NameBond(iBond)
                stop 664
            endif
            ! Check the Bond Number
            if (RDFSameAsBondNumber(iBond) .ne. PotSameAsBondNumber(iBond)) then
                write(stdout,*) 'Error SameAsBond record BondNumber mismatch between RDF and Potential file.'
                write(stdout,*) 'RDF file:',RDFSameAsBondNumber(iBond), ', Pot file:',PotSameAsBondNumber(iBond)
                write(stdout,*) NameBond(iBond)
                stop 664
            endif
        enddo
    endif

    if (allocated(RDFSameAsBondNameMType)) then ! we already checked coherence with potential record, or no such records available
        do iBond = 1, NBond
            if (RDFSameAsBondNameMType(iBond).eq.'') then ! if no record for this bond
                BondSameBond(iBond) = iBond  ! No linked bond
            else
            ! Find the moltype:
                iMT = GetMolTypeByName(RDFSameAsBondNameMType(iBond))
                if (iMT .eq. 0) then ! Nothing found
                    write(stdout,*) 'Error SameAsBond record MolType:',RDFSameAsBondNameMType(iBond), &
                               ' can not find such MolType in the system'
                    write(stdout,*) NameBond(iBond)
                    stop 664
                endif
                BondSameBond(iBond) = sum(NBondMType(1:iMT-1)) + RDFSameAsBondNumber(iBond)
            endif
        enddo
    else ! No records
        do iBond = 1, NBond
            BondSameBond(iBond) = iBond
        enddo
    endif

end subroutine

integer(4) function GetMolTypeByName(name)
    implicit none
    character(*), intent(in):: name
    integer iMT

    GetMolTypeByName=0
    do iMT = 1, size(NameMType)
        if (trim(NameMType(iMT)) .eq. trim(name)) GetMolTypeByName = iMT
    enddo    
end function

subroutine CheckBondsAreSame(NPointsRDFB, RMinRDFB, RMaxRDFB, RDFref_B_riBond)
    implicit none

    integer(4), allocatable, intent(in):: NPointsRDFB(:)
    real(8), intent(in):: RMinRDFB(:), RMaxRDFB(:)
    real(8), allocatable, intent(in):: RDFref_B_riBond(:,:)
    integer(4) ierr

    integer(4) iBond, iR

    ierr = 0
    do iBond=1, NBond
        ! Check Npoints
        if (NPointsPotB(iBond) .ne. NPointsPotB(BondSameBond(iBond))) then
            write(stdout, *) 'Number of points '            
            ierr = 663
        endif
        if (RMinPotB(iBond) .ne. RMinPotB(BondSameBond(iBond))) then
            write(stdout, *) 'RMin value '
            ierr = 663
        endif
        if (RMaxPotB(iBond) .ne. RMaxPotB(BondSameBond(iBond))) then
            write(stdout, *) 'RMax value '
            ierr = 663
        endif
        do iR=1, size(PotB_riBond(:,iBond))
            if (PotB_riBond(iR,iBond).ne.PotB_riBond(iR,BondSameBond(iBond))) then
                write(stdout, *) 'Potential value at point', iR
                ierr = 663
            end if
        end do

        if (NPointsRDFB(iBond) .ne. NPointsRDFB(BondSameBond(iBond))) then
            write(stdout, *) 'Number of points '
            ierr = 663
        endif
        if (RMinRDFB(iBond) .ne. RMinRDFB(BondSameBond(iBond))) then
            write(stdout, *) 'RMin value '
            ierr = 663
        endif
        if (RMaxRDFB(iBond) .ne. RMaxRDFB(BondSameBond(iBond))) then
            write(stdout, *) 'RMax value '
            ierr = 663
        endif
        if (allocated(RDFref_B_riBond)) then  ! if there are any RDFref defined  
            do iR=1, size(RDFref_B_riBond(:,iBond))
                if (RDFref_B_riBond(iR,iBond).ne.RDFref_B_riBond(iR,BondSameBond(iBond))) then
                    write(stdout, *) 'RDFRef value at point', iR
                    ierr = 663
                end if
            end do
        endif 
        if (ierr.ne.0) then
            write(stdout, *) ' differs between bond ', trim(NameBond(iBond)),&
                ' and the linked bond ', trim(NameBond(BondSameBond(iBond)))
            stop 663
        endif
    end do
    
end subroutine
