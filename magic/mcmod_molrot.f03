!==================== MCRot =======================================
subroutine MCmodMolRot()
    use utilsmod
    implicit none
    
!-------Local Variables---------------------
    real(8) DEW, ran,DE
    real(8), allocatable:: S_new(:,:), W_New(:,:), deltaR(:,:)
    integer(4) J,JST,NAtomsInMol,JJ,nchatom,K,kk, iMol, iAtom, ibeg, iend
    
    integer(4),allocatable:: ichatomlist(:,:)
    real(8) phi, vec1(3)

!  Choice of trial Molecule
    iMol = MCStepMolMolecules(int(NMCStepMolMolecules*rand())+1)
    NAtomsInMol=NSiteMType(MTypeMol(iMol)) ! Number of atoms in the selected molecule
    
    AccCntMType(MTypeMol(iMol),2,3)=AccCntMType(MTypeMol(iMol),2,3)+1! Count trial step 
!  Calculate the COM of the molecule, keeping the bonds whole with respect to PBC
    call MCmodGetCOM(iMol)
! Trial rotation generation
    vec1 = RanRotVec()
    phi=rand()*2*3.141592653d0*MCRotStepMType(MTypeMol(iMol))
! Trial coordinates
    call TimerStart(T_MCRotdE)
    call MCmodMolRotCheckTrial(iMol, vec1, phi)
    call TimerStop(T_MCRotdE)
    allocate(S_new(3,NAtomsInMol),ichatomlist(NAtomsInMol,2)) ! Array of shifted coordinates of the selected molecule.
    S_new(:,:)=0.d0
    allocate(W_new(3,NAtomsInMol)); W_new=0.0
    allocate(deltaR(3,NAtomsInMol)); deltaR=0.0
    ichatomlist(:,:)=0
    nchatom=0
  ! Trial rotaton
    if (IPRINT.gt.7) write((stdout),*) 'MCRot: Ich,Q,    Xo,     Yo,     Zo,     Xn,    Yn,    Zn'
    do J=1,NAtomsInMol  ! Forming array of shifted coordinates
        JJ=AdrAtomMol(iMol)+J ! Global atom number of J-th atom in the molecule
        W_new(:,j)=Wvec(:,jj)
        W_new(1:3,j) = rotate2(w_new(1:3,J), phi, vec1) ! Rotating atoms around center of MassSite of their molecule       
        S_new(:,j)=XvecCOMMol(:,imol)+W_new(:,J) !Convert to system-wide coordinates (add COM-coordinates)
        deltaR(:,J)=W_new(:,j)-Wvec(:,jj)![WX(JJ), WY(JJ), WZ(JJ)]
        if(ChargeAtom(JJ).ne.0.d0) then
            nchatom=nchatom+1
            ichatomlist(nchatom,1)=J ! Atom number in the molecule
            ichatomlist(nchatom,2)=JJ! Global atom number
            if (IPRINT.gt.7) write((stdout),'(a,i5,7f9.4)')'MCRot:',nchatom,ChargeAtom(JJ),Svec(:,JJ),S_new(:,J)
        endif
    enddo 
   call PBC_vec(S_new(1:3,1:NAtomsInMol))  
! Energy contributions
    ibeg=AdrAtomMol(iMol)+1
    iend=AdrAtomMol(iMol+1)   
!  Corrections due to Ewald electrostatics
    call MCmodDIFEWM(iMol,S_new,DEW)                        
    DE = (ENB_new - ENB_old) + (EelNB_new - EelNB_old) + DEW ! Total energy difference
!  MC transition
    ran=rand()
    if (DE.gt.500) DE=500.0
    if ((DE.lt.0).or.(dexp(-DE).gt.ran)) then ! If accepted
        do J=1,NAtomsInMol
            JJ=AdrAtomMol(iMol)+J
            Wvec(:,JJ)=S_new(:,j)-XvecCOMMol(:,imol)
        enddo
        Svec(1:3,AdrAtomMol(iMol)+1:AdrAtomMol(iMol+1))=S_new(1:3,1:NAtomsInMol)

        AccCntMType(MTypeMol(iMol),1,3)=AccCntMType(MTypeMol(iMol),1,3)+1
        ENER=ENER+DE
        EINT=EINT
        EEL=EEL+(EelNB_new - EelNB_old) + DEW 
        call TimerStart(T_MCUpdArrRot)
        ! Update arrays        
        !Pairwise Non-bonded, electrostatics and Pair bonds
        forall (iAtom=1:NAtomsInMol)
            Rij(1:NAtom,AdrAtomMol(iMol)+iAtom)        = RijM_new(1:NAtom,iAtom) ! interparticle distance
            Rij(AdrAtomMol(iMol)+iAtom,1:NAtom)        = RijM_new(1:NAtom,iAtom)
            Ri_vec(:,AdrAtomMol(iMol)+iAtom)           = Ri_vec(:,AdrAtomMol(iMol)+iAtom)+deltaR(:,iAtom)
        end forall
#ifdef debug
        cnt_Ri_vec_buffer = cnt_Ri_vec_buffer + 1
        Ri_vec_buffer(:,:,cnt_Ri_vec_buffer) = Ri_vec(:,:)
#endif        
#ifdef pressure                
        VIRNB = VirNB + VirNB_new - VirNB_old
#endif
        ENB = ENB + (ENB_new - ENB_old)
        EelNB = EelNB + (EelNB_new - EelNB_old)

        if (IPRINT.ge.7) write((stdout),'(a,i5,5f10.7)')'MCRot:iMol,DE,DDE,DEE,DEW ',iMol,DE,(ENB_new - ENB_old),&
                        (EelNB_new - EelNB_old),DEW
        if (IPRINT.gt.7) write((stdout),*) "MCRot: ENER=",ENER," DE=",DE
        SSIN=SSIN+sigmasin
        SCOS=SCOS+sigmacos
        call TimerStop(T_MCUpdArrRot)
    endif
    deallocate(S_new,ichatomlist)
    return
end subroutine