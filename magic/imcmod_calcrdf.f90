!     
! This is the part of imcmod module, devoted to calculation of sampled RDFs
!
subroutine CalculateRDF( CORS, npairsrdf)
    use InOutUnit
    use magicmod, only : NAV => global_NAV
    implicit none
    !Global variables
   
    integer(8), intent(in):: npairsrdf(:,:), CORS(:)

    !Local variables
    logical, save:: FirstTime=.true.
    
    real(8) DRR
    integer(4) iSType, jSType,i, iBond, nr
       
    
    if (FirstTime) then
        call reshape(RAS,NA)
        RAS=0.0
        call reshape(SHELV,NA)        
        SHELV=0.0
        do NR=1,NA
            RAS(NR)=(dfloat(NR)-0.5)*RMAX/NA
            SHELV(NR)=4.d0*3.1415926535897932d0*RMAX/NA*(RAS(NR)**2+(RMAX/NA)**2/12.d0)
        end do                
    endif
        
    ! Count number of pairs for NB
    call reshape2(npairs,NSType,NSType)
    npairs=0       
    do iSType=1, NSType 
        if(Btest(StateNB(iSType,iSType),6)) &
            npairs(iSType,iSType)=npairsrdf(iSType,iSType)+sum(CORS(IndexGlobNB_riSjS(1:NA,iSType,iSType)))
        do jSType=iSType+1,NSType            
            if(Btest(StateNB(jSType,iSType),6)) then ! if calculating RDFs for this pair
                npairs(jSType,iSType)=npairsrdf(jSType,iSType)+npairsrdf(iSType,jSType)+&
                                                            sum(CORS(IndexGlobNB_riSjS(1:NA,jSType,iSType)))
                npairs(iSType,jSType)=npairs(jSType,iSType)
            endif
        enddo
    enddo
    npairs= npairs/NAV
    ! Count number of pairs for Bonds
    call reshape(NPairsTotalBond, max(NBond,1))
    do iBond = 1, NBond
        if (Btest(StateB(iBond),6)) then
            NPairsTotalBond(iBond) = (sum(CORS(IndexGlobB_riBond(1:NpointsPotB(iBond),iBond)))/NAV)
        else
            NPairsTotalBond(iBond) = 0
        endif
    enddo
           
    !Get reference <S> if exist
    if (FirstTime) then
        ! Allocate arrays
        if (.not. allocated(Sref_NB_riSjS)) allocate(Sref_NB_riSjS(Na, NSType,NSType))  ! In best case this shall be moved to Readio.Initiate
        Sref_NB_riSjS=0.0
        if (.not. allocated(RDFcalc_NB_riSjS)) allocate(RDFcalc_NB_riSjS(NA, NSType,NSType))
        RDFcalc_NB_riSjS=0.0
        if (.not. allocated(Scalc_NB_riSjS)) allocate(Scalc_NB_riSjS(NA, NSType,NSType))
        Scalc_NB_riSjS=0.0                
        call reshape2(Sref_B_riBond, maxval([1, NPointsPotB(:)]), max(1, NBond))
        Sref_b_riBond=0.0
        call reshape2(Scalc_B_riBond, maxval([1, NPointsPotB(:)]), max(1, NBond))
        Scalc_B_riBond=0.0
        call reshape2(RDFcalc_B_riBond, maxval([1, NPointsPotB(:)]), max(1, NBond))
        RDFcalc_B_riBond=0.0            

        ! Calculate S_NB reference
        do iSType=1, NSType        
            do jSType=iSType, NSType                   
              if (BTest(StateNB(jSType,iSType),4)) & ! If RDFref exists for this types pair
                    Sref_NB_riSjS(:,jSType,iSType)=RDFref_NB_riSjS(:,jSType,iSType)*NPairs(jSType,iSType)*SHELV(:)/VOL
            enddo
            Sref_NB_riSjS(:,iSType,:)=Sref_NB_riSjS(:,:,iSType)
        enddo    
        
        ! Calculate S_Bond reference
        do iBond=1, NBond
            if (Btest(StateB(iBond),4)) then ! If RDFref exists for this types pair
                DRR=(RMaxPotB(iBond)-RMinPotB(iBond))/NPointsPotB(iBond)
                Sref_B_riBond(1:NpointsPotB(iBond),iBond)=RDFref_B_riBond(1:NpointsPotB(iBond),iBond)*NPairsTotalBond(iBond)*DRR      
            endif
        enddo        
    endif
    
    !Get sampled <S> and RDF
    do iSType=1, NSType        
        do jSType=iSType, NSType            
            if(Btest(StateNB(jSType,iSType),6)) then ! if calculating RDFs for this pair
                !Get sampled <S>
                Scalc_NB_riSjS(1:NA,jSType,iSType)=CORS(IndexGlobNB_riSjS(1:NA,jSType,iSType))*FNR  ! <S> - sampled
                !Get sampled RDF             
                RDFcalc_NB_riSjS(1:NA,jSType, iSType)=Scalc_NB_riSjS(1:NA,jSType,iSType)*VOL/&
                                                        (NPairs(jSType,iSType)*SHELV(1:NA))   ! calculated RDF - sampled  
            endif
        enddo
    enddo

! calculate S and RDF sampled    
    do iBond=1, NBond
        if  (Btest(StateB(iBond),6)) then ! if calculate RDF for this bond
        ! calculate S
            Scalc_B_riBond(1:NpointsPotB(iBond),iBond)=CORS(IndexGlobB_riBond(1:NpointsPotB(iBond),iBond))*FNR
        ! Calculate RDF sampled    
            DRR=(RMaxPotB(iBond)-RMinPotB(iBond))/NPointsPotB(iBond)
            RDFcalc_B_riBond(1:NpointsPotB(iBond),iBond)=Scalc_B_riBond(1:NpointsPotB(iBond),iBond)/(NPairsTotalBond(iBond)*DRR)       ! calculated RDF                        
        endif
    enddo
!    FirstTime=.False.
end subroutine CalculateRDF
