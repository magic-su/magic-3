subroutine WritePot(oFilename, NPoints,irep)
    
    use utilsmod, only: MakeSaveFileName
    ! Global variables
    character(*), intent(in) :: oFilename
    integer(4), intent(in)   :: NPoints, irep        

    
    ! Local variables
    integer iSType, jSType, iBond, lBond, iostt,i, iGroup, iMType

    ! Opening file
    open(unit=stdout_pot, status='unknown', iostat=iostt, &
        file=trim(MakeSaveFileName(oFileName, '.pot', IREP=IREP)))
    ! Write General section        
    write(stdout_pot,*) '&General'
    write(stdout_pot,'(a,i0)') 'NTypes=',NSType
!    write(stdout_pot,*) 'Types=', (trim(NameSType(iSType))//', ', iSType=1,size(NameSType))
    write(stdout_pot,'(a,i0)') 'N_NB=',NactNB
    write(stdout_pot,'(a,i0)') 'N_B=',NBondB
    write(stdout_pot,'(a,i0)') 'N_A=',NBondA
    write(stdout_pot,'(a,f14.7)') 'Min=',RMin
    write(stdout_pot,'(a,f14.7)') 'Max=',RMax
    write(stdout_pot,'(a,i0)') 'NPoints=',NPoints

    call WriteExclusionsHeader(stdout_pot)

    write(stdout_pot,*) '&EndGeneral'    

    ! Write intermolecular (NB) potentials
    do iSType=1, NSType ! Loop over all posiible pairs of Site Types
        do jSType=iSType, NSType
            if (BTest(StateNB(jSType,iSType),3)) then ! Potential exists            
                write(stdout_pot,*) '&Potential'
                write(stdout_pot,*) 'Name=',trim(NameSType(iSType)),'-',trim(NameSType(jSType))
                write(stdout_pot,'(a,i0)') 'Type=NB'
                write(stdout_pot,'(a,f14.7)') 'Min=',RMin
                write(stdout_pot,'(a,f14.7)') 'Max=',RMax
                write(stdout_pot,'(a,i0)') 'NPoints=',NPoints
                write(stdout_pot,*) 'AtomTypes=',trim(NameSType(iSType)),',',trim(NameSType(jSType))
                if (.not.BTest(StateNB(jSType,iSType),2)) write(stdout_pot,*) '&FIXED'
                ! Write a table
                write(stdout_pot,*) '&Table'
                do i=1, size(PotNB_riSjS,1)
                    write(stdout_pot,'(f14.7,1x,f14.7)') RMin+(RMax-RMin)*(i-0.5)/NPoints, PotNB_riSjS(i,jSType,iSType)*FRT
                enddo
                write(stdout_pot,*) '&EndTable'
                
                write(stdout_pot,*) '&EndPotential'
            endif
        enddo
    enddo
    ! Write Intramolecular (Bond) potentials
    do iBond=1, NBond ! Over all bonds in the system
        iMType=MTypeBond(iBond)
        if (BTest(StateB(iBond),3)) then ! If potential is defined            
            write(stdout_pot,*) '&Potential'
            lBond=iBond-sum(NBondMType(1:iMType-1)) ! local number of the bond in the respective Molecular Type
            write(stdout_pot,*) 'Name=',trim(NameBond(iBond))
            if (BTypeBond(iBond).eq.1) then
                write(stdout_pot,*) 'Type=B'
            elseif (BTypeBond(iBond).eq.2) then
                write(stdout_pot,*) 'Type=A'
            endif                        
            write(stdout_pot,*) 'MolType=',trim(NameMType(iMType))
            write(stdout_pot,'(a,i0)') 'BondNumber=',lBond                                
            if (BondSameBond(iBond).ne.iBond) then! if the bond is linked to another bond
                write(stdout_pot,*) 'SameAsBond=', trim(NameBond(BondSameBond(iBond)))                                               
            endif
            write(stdout_pot,'(a,f14.7)') 'Min=',RMinPotB(iBond)
            write(stdout_pot,'(a,f14.7)') 'Max=',RMaxPotB(iBond)
            write(stdout_pot,'(a,i0)') 'NPoints=',NPointsPotB(iBond)
            if (.not.BTest(StateB(iBond),2)) write(stdout_pot,*) '&FIXED'
            ! Write NPairs/NTriplets and Pairs/Triplets
            if (BTypeBond(iBond).eq.1) then ! Pairwise Bond
                write(stdout_pot,'(a,i0)') 'NPairs=',NSiteGroupsInBond(iBond)
                write(stdout_pot,"(a6, i4, a1, i4)",advance='no') 'Pairs=',&
                   iSiteInBond(iBond,1)-AdrSiteMType(iMType),'-',&
                   jSiteInBond(iBond,1)-AdrSiteMType(iMType)
                do iGroup=2, NSiteGroupsInBond(iBond)
                    write(stdout_pot,"(a2,i4,a1,i4)", advance='no') &
                    ', ',iSiteInBond(iBond,iGroup)-AdrSiteMType(iMType),'-',&
                         jSiteInBond(iBond,iGroup)-AdrSiteMType(iMType)
                enddo
                write(stdout_pot,*) ''           !Endline          
            elseif (BTypeBond(iBond).eq.2) then ! Angle bending bond
                write(stdout_pot,'(a,i0)') 'NTriplets=',NSiteGroupsInBond(iBond)
                write(stdout_pot,"(a9,i4,a1,i4,a1,i4)",advance='no') 'Triplets=',&
                   iSiteInBond(iBond,1)-AdrSiteMType(iMType),'-',&
                   kSiteInBond(iBond,1)-AdrSiteMType(iMType),'-',&
                   jSiteInBond(iBond,1)-AdrSiteMType(iMType)
                do iGroup=2, NSiteGroupsInBond(iBond)
                    write(stdout_pot,"(a2,i4,a1,i4,a1,i4)", advance='no') &
                           ', ',iSiteInBond(iBond,iGroup)-AdrSiteMType(iMType),'-',&
                                kSiteInBond(iBond,iGroup)-AdrSiteMType(iMType),'-',&
                                jSiteInBond(iBond,iGroup)-AdrSiteMType(iMType)
                enddo
                write(stdout_pot,*) '' ! Endline
            endif
           ! Write a table
            write(stdout_pot,*) '&Table'
            do i=1, NPointsPotB(iBond)
                write(stdout_pot,'(f14.7, f14.7)') RMinPotB(iBond)+(RMaxPotB(iBond)-RMinPotB(iBond))*(i-0.5)/NPointsPotB(iBond),&
                    PotB_riBond(i,iBond)*FRT
            enddo
            write(stdout_pot,*) '&EndTable'
            write(stdout_pot,*) '&EndPotential'
        endif
    enddo    
    ! Close file    
    close(stdout_pot)
end subroutine 
