    !===================== READCRD ==================================
    ! read initial coordinates and eventually velocities from a "xmol" file s
    subroutine READCRD(FSTART,ID)        
        use precision
        use utilsmod
!        use iomod, only: NAtom
!        use mcmod, only: Ri_vec 
        implicit none
    ! Global Variables            
        integer(4), intent(in):: ID        
        
        character(filename_text) FSTART    
    ! Local Variables
        character(filename_text) LoadFileName ! Local variable to work with, without changing global FSTART
        character*80 CHR
        integer(4) i,iostat,IS,NPP, NConf

        LoadFileName=FSTART
        write((stdout),*) ' Read starting geometry from file ',LoadFileName
        if ((NFStart.eq.0).and.(ID.ne.0)) then ! If each process read only one configuration from individual file

            LoadFileName(Len_Trim(LoadFileName)+1:Len_Trim(LoadFileName)+2)='.p'
            write(LoadFileName(Len_Trim(LoadFileName)+1:Len_Trim(LoadFileName)+3),'(i0.3)') ID
        endif
        LoadFileName(Len_Trim(LoadFileName)+1:Len_Trim(LoadFileName)+11)='.start.xmol'
        open(unit=stdin_crd,file=LoadFileName,status='old',iostat=iostat)
        if (iostat.ne.0) then
            if (ID.gt.0) write(stdout,*) ' Problems reading file ',trim(LoadFileName),' on process ID:',ID
            write(6,*) ' Problems reading file ',trim(LoadFileName)
            stop 415
        else
            write((stdout),*) ' The coordinates file was opened for reading: ',LoadFileName
        endif
         ! Generate random number Nconf and skip first NConf*NAtom
        if (NFStart.gt.0) then
            NConf=RAND()*(NFStart)*0.999999
        else
            NConf=0
        endif
        write(stdout,*) 'Will use configuration ',NConf+1,' from file ',trim(LoadFileName)
        ! Skipping lines
        do i=1, (NAtom+2)*NConf
            read(stdin_crd,*) CHR
        enddo
        write(stdout,*) 'Reading ',NConf+1,' configuration from file ',trim(LoadFileName)
        ! Now we are reading it for real
        read(stdin_crd,*)NPP
        if(NPP.ne.NAtom)then        
            write(stdout,*) ' wrong N mol. in CRD file ',LoadFileName,' Sholud be:',NAtom,' In file:',NPP,' ID:',ID        
            stop 416
        end if
        read(stdin_crd,*)CHR
        do I=1,NAtom
            read(stdin_crd,*)CHR,Ri_vec(:,i)
        end do
        write(stdout,*)' Initial coord.  read from ',LoadFileName
        close(stdin_crd)
    end subroutine READCRD