subroutine ReadRDFPot(iFilename, &
    RDFref_B_riBond, rRDFB_riBond, RDFref_NB_riSjS, &    
    NPoints, RMaxRDFNB, RMinRDFNB,&
    NPointsRDFB,RMaxRDFB,RMinRDFB,&
    RDFSameAsBondNameMType, RDFSameAsBondNumber, &
     df_NAngleBondsExcludeMType, df_NPairBondsExcludeMType, &
    df_kind)
    ! Global variables
    character(*), intent(in) :: iFilename, df_kind    
    real(8), intent(out)     ::  RMaxRDFNB, RMinRDFNB, RMaxRDFB(:),RMinRDFB(:)    
    real(8), allocatable, intent(out) :: RDFref_NB_riSjS(:,:,:), RDFref_B_riBond(:,:), rRDFB_riBond(:,:)
    integer(4), intent(inout)  :: NPointsRDFB(:)
    integer(4), intent(out), allocatable ::  df_NAngleBondsExcludeMType(:), df_NPairBondsExcludeMType(:)                              
    integer(4), intent(out)    :: NPoints
    character(mol_name_len), allocatable, intent(out):: RDFSameAsBondNameMType(:)
    integer, allocatable, intent(out) :: RDFSameAsBondNumber(:)    
       
    ! Local variables
    integer N_NB, N_B, N_A
    real(8) PRmax,PRmin
    real(8), allocatable:: r(:), U(:)
    character(atom_name_len) AtomTypesPairStr(2)
    character(rdfpot_text) str,l, PName, PType,RDF_FileName
    character(rdfpot_text), allocatable :: text(:), RDFtext(:)
    integer, allocatable:: AtomPairs(:,:), AtomTriplets(:,:)
    integer iostt, i, ierr, iPoint, iR,&            
             iend, bRDF, eRDF, cnt_N_NB, cnt_N_B, cnt_N_A, iend_RDF_text, bRDF_text, eRDF_text, bTable_text, eTable_text,&
             PNPoints, bTable, eTable, PNPairs, PNTriplets, AtomTypesPair(2),&
             LocalBond, iBond, MolType, NBonds, InitPot, Overhead, ibit, n_atoms, stdout_temp
    logical FlagTableFound, FlagIncludeFileFound, FlagZero, FlagFixed
    
#ifdef mpi    
    include 'mpif.h'
#endif
    ! Preallocate arrays
    ! Allocate arrays for SameBond-records: MolecularTypeName, BondNumber
    allocate(RDFSameAsBondNameMType(size(StateB)))
    RDFSameAsBondNameMType = ''
    allocate(RDFSameAsBondNumber(size(StateB)))
    RDFSameAsBondNumber = 0
    call reshape(df_NAngleBondsExcludeMType, NMType); df_NAngleBondsExcludeMType(:) = 1   
    call reshape(df_NPairBondsExcludeMType, NMType); df_NPairBondsExcludeMType(:) = 1    
    NBonds=max(sum(NBondMType(:)), 1)
    if (ID.le.1) then ! If in single mode or master-process
        stdout_temp = stdout; stdout = 6 ! Redirect all output to the console
        if (str_cmp(df_kind, 'RDF')) then
            ibit = 4
        elseif (str_cmp(df_kind, 'potential')) then
            ibit = 3
        else
            write(stdout,*) 'Error: Read pot/rdf: wrong DF_KIND', df_kind
            stop 157
        endif
        ! Open the RDF file and scan it  for number of lines (variable iend)
        iend = GetFileLength(iFilename, trim(df_kind)//' input')
        ! Allocate array for the file    
        allocate(text(iend), RDFtext(iend))
        ! Read the file to the array text
        open(unit=stdin_rdf,file=iFilename,status='old',iostat=iostt)
        do i=1, iend
            read(stdin_rdf,'(a)') l
            text(i)=trim(adjustl(l))
        enddo
        close(stdin_rdf)    

        !Read &General section of the file    
        call ReadGeneralSection(text,iend, N_NB, N_B, N_A, NPoints,RMaxRDFNB, RMinRDFNB, df_kind,&
                 df_NAngleBondsExcludeMType, df_NPairBondsExcludeMType)
        if (.not. allocated(RDFref_NB_riSjS)) allocate(RDFref_NB_riSjS(NPoints,NSType,NSType))
        ! Read RDFs/potentials        
        ! Count the actual number of Non-bonded, bonded, and bending angle RDFs
        i=1 
        bRDF=0
        eRDF=0
        cnt_N_NB=0
        cnt_N_B=0
        cnt_N_A=0    
        do while (i.le.iend)
            call ReadLineFromText(text,i,l,ierr)
            ! Indicate presence of &RDF/&Potential
            if (str_cmp(l, '&'//str_cln(df_kind))) then ! Found begin of the RDF-section
                ! Find end of the RDF/Potential section
                call GetEndOfSection(text,i,iend,bRDF,eRDF, df_kind)
                ! Read common properties of the RDF/Potential
                call ReadPropCommon(text, bRDF, eRDF, PName, PRmin, PRmax, PNPoints, PType,&
                                cnt_N_NB, cnt_N_B, cnt_N_A, bTable, eTable, FlagFixed, &
                                FlagZero, FlagTableFound, FlagIncludeFileFound, RDF_FileName, InitPot, df_kind)
                ! Read specific properties of the RDF/Potential with respect to the type
                if (FlagIncludeFileFound) then
                   call ReadIncludedSection(RDF_FileName, RDFtext, &
                    iend_RDF_text, bRDF_text, eRDF_text, bTable_text, eTable_text, df_kind)                
                else
                    if (allocated(RDFtext)) deallocate(RDFtext)
                    allocate(RDFtext(iend))
                    RDFtext(:)=text(:)
                    RDF_FileName=iFileName
                    RDFtext=text
                    iend_RDF_text=iend
                    bRDF_text=bRDF
                    eRDF_text=eRDF
                    bTable_text=bTable
                    eTable_text=eTable
                endif            
                select case (str_cln(PType))
                    case ('NB')
                        call ReadPropNB(RDFtext, RDF_Filename, bRDF_text, eRDF_text, AtomTypesPairStr, AtomTypesPair,&
                             RMinRDFNB, RMaxRDFNB, PRmin, PRmax, Npoints, PNpoints, df_kind)                    
                        StateNB(AtomTypesPair(1),AtomTypesPair(2)) = &
                                        IBSet(StateNB(AtomTypesPair(1),AtomTypesPair(2)),ibit)
                        if (FlagFixed) &
                            StateNB(AtomTypesPair(1),AtomTypesPair(2))=IBClr(StateNB(AtomTypesPair(1),AtomTypesPair(2)),2)
                        if (InitPot.eq.1) then ! If init potential with PMF
                            StateNB(AtomTypesPair(1),AtomTypesPair(2))=IBSet(StateNB(AtomTypesPair(1),AtomTypesPair(2)),1)                         
                        elseif (InitPot.eq.0) then ! If init potential with zero
                            StateNB(AtomTypesPair(1),AtomTypesPair(2))=IBCLR(StateNB(AtomTypesPair(1),AtomTypesPair(2)),1)                         
                        endif
                        StateNB(AtomTypesPair(2),AtomTypesPair(1))=StateNB(AtomTypesPair(1),AtomTypesPair(2))
                        call ReadTableCommon(RDFtext, bTable_text, eTable_text, r, U, PNPoints, PRMAX, PRMIN, PType, df_kind)                        

                        if (str_cmp(df_kind, 'RDF')) then 
                            Overhead=NPoints-PNPoints ! Number of first lines where RDF==0, in the core region 
                            do iPoint=1, NPoints
                                if (iPoint.le.Overhead) then 
                                    RDFref_NB_riSjS(iPoint,AtomTypesPair(1),AtomTypesPair(2))=0.0                        
                                    RDFref_NB_riSjS(iPoint,AtomTypesPair(2),AtomTypesPair(1))=0.0                        
                                else
                                    iR=r(iPoint-Overhead)*NPoints/PRMAX+1.1
        !                            write(stdout,*) 'Debug: Read RDF: Detecting R,R: i=',iPoint,' iR=', iR 
                                    RDFref_NB_riSjS(iR,AtomTypesPair(1),AtomTypesPair(2))=U(iPoint-Overhead)
                                    RDFref_NB_riSjS(iR,AtomTypesPair(2),AtomTypesPair(1))=U(iPoint-Overhead)
                                endif
                            enddo                    
                        else ! Potential
                            do iPoint=1, PNPoints
                                if (abs(r(iPoint)-(dfloat(iPoint)-0.5)*PRMAX/PNPoints).gt.PRMAX/PNPoints*0.4) then
                                    write(stdout,*) 'ERROR: Reading Potentials:'
                                    write(stdout,*) 'Suspicious spacing in NB-potential ',&
                                        trim(AtomTypesPairStr(1)),'-',trim(AtomTypesPairStr(2)),&
                                        ' check around point ',iPoint, 'of the potential'
                                        stop 158
                                endif                        
                                RDFref_NB_riSjS(iPoint,AtomTypesPair(1),AtomTypesPair(2))=U(iPoint)                        
                                RDFref_NB_riSjS(iPoint,AtomTypesPair(2),AtomTypesPair(1))=U(iPoint)                        
                            enddo        
                        endif
                    case ('B','A')
                        if (str_cln(PType)=='B') then
                            n_atoms = 2
                        else
                            n_atoms = 3
                        endif
                        call ReadPropBA(RDFtext, RDF_Filename, bRDF_text, eRDF_text, RDFSameAsBondNameMType, RDFSameAsBondNumber,&
                            iBond,df_kind, n_atoms)
                        StateB(iBond)=IBSet(StateB(iBond),ibit)
                        if (FlagFixed) StateB(iBond)=IBClr(StateB(iBond),2) ! State potential for bond is fixed
                        if (InitPot.eq.1) then 
                            StateB(iBond)=IBSet(StateB(iBond),1) ! If init potential with PMF
                        elseif (InitPot.eq.0) then ! If init potential with zero
                            StateB(iBond)=IBCLR(StateB(iBond),1)
                        endif
                        NPointsRDFB(iBond)=PNPoints
                        RMaxRDFB(iBond)=PRmax
                        RMinRDFB(iBond)=PRmin
                        call reshape2(RDFref_B_riBond,PNPoints,NBonds)
                        call reshape2(rRDFB_riBond,PNPoints,NBonds)
                        call ReadTableCommon(RDFtext, bTable_text, eTable_text, r, U, PNPoints, PRMAX, PRMIN, PType, df_kind)                        
                        do iPoint=1, PNPoints
                            rRDFB_riBond(iPoint,iBond)=r(iPoint)
                            RDFref_B_riBond(iPoint,iBond)=U(iPoint)
                            if (abs(r(iPoint)-PRMin-(iPoint-0.5)*(PRMax-PRMin)/PNPoints).gt.0.4*(PRMax-PRMin)/PNPoints) then
                                write(stdout,*) 'ERROR: Reading ',trim(df_kind),':'
                                write(stdout,*) 'Suspicious spacing in ',trim(df_kind),' for Bond ',&
                                                iBond-sum(NBondMType(1:MolType-1)),&
                                                ' check around line ',iPoint, 'of the ',trim(df_kind)
                                write(stdout,*) 'Expected value: R(i)=',PRMin-(iPoint-0.5)*(PRMax-PRMin)/PNPoints,&
                                                'Actual read value R(i)=',r(iPoint) 
                                    stop 159
                            endif    
                        enddo  
                end select 
                if (iprint.ge.8) &
                    write(stdout,*) 'Debug: ',trim(df_kind),&
                            ' Name:', trim(PNAME),', MIN:',PRMIN,', MAX:', PRMAX, ', NPoints:', PNPoints,&
                            ', AtomTypes:', trim(AtomTypesPairStr(1)),',',trim(AtomTypesPairStr(2))              
                ! Check if a -table of included-DF is found
                ! Read the table
            endif        
            i=i+1
        enddo ! Loop over all the RDFs/potentials    
        if (cnt_N_NB.ne.N_NB) then
            write(stdout,*) 'ERROR: Reading ',trim(df_kind),':'
            write(stdout,*) 'Number of actual INTERmolecular (NB) ',trim(df_kind),'s read ', cnt_N_NB, 'differs from'
            write(stdout,*) 'number stated in General section of the ',trim(df_kind),' file : N_NB=', N_NB
            stop 160
        endif
            if (cnt_N_B.ne.N_B) then
            write(stdout,*) 'ERROR: Reading ',trim(df_kind),':'
            write(stdout,*) 'Number of actual INTRAmolecular pairwise bond ',trim(df_kind),' (B) read ', cnt_N_B, 'differs from'
            write(stdout,*) 'number stated in General section of the ',trim(df_kind),' file : N_B=', N_B
            stop 161
        endif
        if (cnt_N_A.ne.N_A) then
            write(stdout,*) 'ERROR: Reading RDF:'
            write(stdout,*) 'Number of actual INTRAmolecular angle ',trim(df_kind),'(A) read ', cnt_N_A, 'differs from'
            write(stdout,*) 'number stated in General section of the ',trim(df_kind),' file : N_A=', N_A
            stop 162
        endif
        cnt_N_B=0
        cnt_N_A=0    
        deallocate(text, RDFtext)
        stdout = stdout_temp ! Revert stdout value
    endif
    
#ifdef mpi
! Passing initialized arrays to all processes
    call MPI_BARRIER(MPI_COMM_WORLD,ierr)
    call MPI_Bcast(NPoints, 1,MPI_INTEGER,0,MPI_COMM_WORLD,ierr) 
    if (.not. allocated(RDFref_NB_riSjS)) allocate(RDFref_NB_riSjS(NPoints,NSType,NSType))

    call MPI_Bcast(RMaxRDFNB, 1,MPI_REAL8,0,MPI_COMM_WORLD,ierr) 
    call MPI_Bcast(RMinRDFNB, 1,MPI_REAL8,0,MPI_COMM_WORLD,ierr) 
    call MPI_Bcast(RMaxRDFB, size(RMaxRDFB),MPI_REAL8,0,MPI_COMM_WORLD,ierr) 
    call MPI_Bcast(RMinRDFB, size(RMinRDFB),MPI_REAL8,0,MPI_COMM_WORLD,ierr) 
    call MPI_Bcast(RDFref_NB_riSjS, size(RDFref_NB_riSjS),MPI_REAL8,0,MPI_COMM_WORLD,ierr)   
    call MPI_Bcast(NPointsRDFB, size(NPointsRDFB),MPI_INTEGER,0,MPI_COMM_WORLD,ierr)
    call reshape2(RDFref_B_riBond, max(maxval(NPointsRDFB(:)), 1), NBonds)
    call reshape2(rRDFB_riBond, max(maxval(NPointsRDFB(:)), 1), NBonds)
    call MPI_Bcast(RDFref_B_riBond, size(RDFref_B_riBond),MPI_REAL8,0,MPI_COMM_WORLD,ierr)
    call MPI_Bcast(rRDFB_riBond, size(rRDFB_riBond),MPI_REAL8,0,MPI_COMM_WORLD,ierr)

    call MPI_Bcast( df_NAngleBondsExcludeMType, size( df_NAngleBondsExcludeMType),MPI_INTEGER,0,MPI_COMM_WORLD,ierr)
    call MPI_Bcast(df_NPairBondsExcludeMType, size(df_NPairBondsExcludeMType),MPI_INTEGER,0,MPI_COMM_WORLD,ierr)

    call MPI_Bcast(RDFSameAsBondNumber, size(RDFSameAsBondNumber),MPI_INTEGER,0,MPI_COMM_WORLD,ierr)    
    call MPI_BARRIER(MPI_COMM_WORLD,ierr)
    call MPI_Bcast(RDFSameAsBondNameMType, size(RDFSameAsBondNameMType)*mol_name_len, MPI_CHARACTER,0,MPI_COMM_WORLD,ierr)
    call MPI_Bcast(StateB, size(StateB),MPI_INTEGER, 0,MPI_COMM_WORLD,ierr)
    call MPI_Bcast(StateNB, size(StateNB),MPI_INTEGER,0,MPI_COMM_WORLD,ierr)
#endif
end subroutine