!==================== MCTrans =======================================

subroutine MCmodMolTrans()
    implicit none

!-------Local Variables---------------------
    real(8) DE, DEW, deltarand(3)
    real(8), allocatable:: S_new(:,:)
    integer(4) NAtomsInMol, iMol, j, jj, nchatom, iAtom
    integer(4),allocatable:: ichatomlist(:,:)    

    !  Choice of trial Molecule

    iMol = MCStepMolMolecules(int(NMCStepMolMolecules*rand())+1)
    NAtomsInMol=NSiteMType(MTypeMol(iMol)) ! Number of atoms in the selected molecule

    AccCntMType(MTypeMol(iMol),2, 2)=AccCntMType(MTypeMol(iMol),2, 2)+1! Count trial step
    allocate(S_new(3,NAtomsInMol),ichatomlist(NAtomsInMol,2)) ! Array of shifted coordinates of the selected molecule.
    S_new(:,:)=0.d0
    
    nchatom=0
  ! Trial shift
    deltarand= [(rand()-0.5d0), (rand()-0.5d0), (rand()-0.5d0)]*MCTransStepMType(MTypeMol(iMol))
    if (IPRINT.gt.7) write((stdout),*) 'MCTrans trans: Ich,Q,    Xo,     Yo,     Zo,     Xn,    Yn,    Zn'
    do J=1,NAtomsInMol  ! Forming array of shifted coordinates
        JJ=AdrAtomMol(iMol)+J ! Global atom number of J-th atom in the molecule        
        S_new(:,J)=Svec(:,jj)+deltarand
        if(ChargeAtom(JJ).ne.0.d0) then
            nchatom=nchatom+1
            ichatomlist(nchatom,1)=J ! Atom number in the molecule
            ichatomlist(nchatom,2)=JJ ! Global atom number
            if (IPRINT.gt.7)write((stdout),'(a,i5,7f9.4)') 'MCTrans Trans:',nchatom,ChargeAtom(JJ),Svec(:,JJ),S_new(:,J)
        endif
    enddo   
    call PBC_vec(S_new(1:3,1:NAtomsInMol)) 
    
!  Energy difference
    call TimerStart(T_MCTransdE)
    call MCmodMolTransCheckTrial(iMol, deltaRand)
    call TimerStop(T_MCTransdE)
    ! Calculate energy component differences
    call MCmodDIFEWM(iMol,S_new, DEW)                        
    DE = (ENB_new - ENB_old) + (EelNB_new - EelNB_old) + DEW ! Total energy difference
                
    if (IPRINT.gt.7) write((stdout),*)' MCTrans trans: iMol, DE, DEW',iMol,DE,&
        (ENB_new - ENB_old), (EelNB_new - EelNB_old), DEW
    if(DE.gt.0)then
        if(DE.gt.18.d0) then
            deallocate(S_new,ichatomlist)
            return
        endif
        if(dexp(-DE).lt.rand())then ! TODO merge this two ifs
            deallocate(S_new,ichatomlist)
            return
        endif
    end if
    call TimerStart(T_MCUpdArrTrans)
!  New configuration saving
    Svec(1:3,AdrAtomMol(iMol)+1:AdrAtomMol(iMol+1))=S_new(1:3,1:NAtomsInMol)
    ! Update arrays
        !Pairwise Non-bonded, electrostatics and Pair bonds
    forall (iAtom=1:NAtomsInMol)
        Rij(:,AdrAtomMol(iMol)+iAtom)        = RijM_new(:,iAtom) ! interparticle distance
        Rij(AdrAtomMol(iMol)+iAtom,:)        = RijM_new(:,iAtom)
        Ri_vec(:,AdrAtomMol(iMol)+iAtom)     = Ri_vec(:,AdrAtomMol(iMol)+iAtom)+deltarand
        
    end forall
#ifdef debug
        cnt_Ri_vec_buffer = cnt_Ri_vec_buffer + 1
        Ri_vec_buffer(:,:,cnt_Ri_vec_buffer) = Ri_vec(:,:)
#endif            
#ifdef pressure            
    VIRNB = VirNB + VirNB_new - VirNB_old
#endif        

    ENB = ENB + (ENB_new - ENB_old)
    EelNB = EelNB + (EelNB_new - EelNB_old)        
    ENER=ENER+DE
    EEL = EEL+ DEW + (EelNB_new - EelNB_old) + (EelEXCL_new - EelEXCL_old) 
    AccCntMType(MTypeMol(iMol),1, 2)=AccCntMType(MTypeMol(iMol),1, 2)+1!
    if (IPRINT.gt.7) write((stdout),*) "MCTrans trans: ENER=",ENER," DE=",DE
    SSIN=SSIN+sigmasin
    SCOS=SCOS+sigmacos
    call TimerStop(T_MCUpdArrTrans)
    deallocate(s_new,ichatomlist)
    return
end subroutine
