subroutine SetParameters(SHELV, RAS                     )!,&

!  Physical constants and PI
    real(8),parameter:: EPS0=8.854d-12, ELCH=1.602d-19, AVAG=6.02252d23, BOLTZ=1.38054d-23, PI=3.1415926535897932d0
    
    real(8), allocatable:: SHELV(:), RAS(:)
    allocate(SHELV(NA),RAS(NA))
    VOL	= product(BOX)
    !VOL=BOXL*BOYL*BOZL
!    HBOXL=0.5*BOXL
	  
    FRT = 1.d-3*BOLTZ*TEMP*AVAG  !  En.: i.u -> kJ/M
    FKJM = FRT/NMol           
    FPR=1.d25*BOLTZ*TEMP/VOL                   !  Pr:  i.u -> atm
    !  electrostatic constant: transfers Uel(1/r A) -> KT
    COULF= 1.d10*ELCH**2/(4.d0*PI*EPS0*EPS*BOLTZ*TEMP)
    write(stdout,*)' COULF=',COULF,'  F(kt -> kJ) ',FRT,' P(N -> atm) ',FPR
    if (iAverage.le.0) iAverage=2*NAtom ! Set automatic value for averaging
#ifdef PBC_TO
    VOL=VOL*0.5
#endif
end subroutine SetParameters