subroutine ShiftIntraPotToZero(StateB)
! Subroutine shifts every intramolecular potential in such a way that it has zero value in minimum.
    use dynarray
    use utilsmod
    implicit none
!Global Variables
    integer(4), intent(in):: StateB(:) 
!Local variables
    integer(4) NMINP(1),I,iBond, NP
    real(8) POMIN

    call reshape(NMIN, max(NBond,1))
    do iBond=1,NBond ! Shifting intramolecular potential to touch zero-level
        if (BTest(StateB(iBond),9) .and. BTest(StateB(iBond),8)) then  ! actual bond allowed for correction
            NP=NPointsPotB(iBond)
            nminp=minloc(PotB_riBond(1:NP,iBond))
            PotB_riBond(1:NP,iBond)=PotB_riBond(1:NP,iBond)-PotB_riBond(nminp(1),iBond)
            NMIN(iBond)=NMINP(1)+5*rand()-2.
            if(NMIN(iBond).lt.1) NMIN(iBond)=1
            if(NMIN(iBond).gt.NP) NMIN(iBond)=NP
        endif
    end do
    ! Now when all actual bonds have been shifted, we can use their values for the linked bonds.
    do iBond=1,NBond
        if ( (.not. BTest(StateB(iBond),9)) .and. BTest(StateB(BondSameBond(iBond)),8) ) then  ! linked bond allowed for correction
            NP=NPointsPotB(iBond)
            PotB_riBond(1:NP,iBond)=PotB_riBond(1:NP, BondSameBond(iBond))
            NMIN(iBond)=NMIN(BondSameBond(iBond))
        endif
    enddo
end subroutine
