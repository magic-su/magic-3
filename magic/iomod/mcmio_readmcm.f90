subroutine ReadMCM( STypeSite)
    
    implicit none    

    integer, allocatable:: STypeSite(:)
    character(atom_name_len), allocatable:: NameSTypeSite(:)
    logical l_ReadSiteTypeNumbers
    
    ! Read list of sites from each mcm-file. Create global list of sites, Name, X,Y,Z, Mass, Charge, TypeIndex, TypeName
    call MCMioReadSites(STypeSite, NameSTypeSite, l_ReadSiteTypeNumbers)
    ! Analyze list of site-types based on their names
     write(stdout,*) ''     
    call MCMioSTypeDetect(NameSTypeSite,STypeSite, l_ReadSiteTypeNumbers, NSType, NameSType)
    call MCMioReadBond()
            
        write(stdout,*) 'MCM-files are read successfully.'
        write(stdout,*) ''
        write(stdout,*) 'Number of Site/Atom types detected:',NSType
        write(stdout,*) 'Number of Sites detected:',NSite
        write(stdout,*) 'Number of intramolecular bonds detected:',NBond
end subroutine ReadMCM        