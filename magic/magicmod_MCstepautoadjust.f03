subroutine MCStepAutoAdjust()
    use precision
    use iomod, only: NMType, NameMType, iTrans, iRot, &
            MCStepAtomMType,MCTransStepMType, MCRotStepMType, MCStepAtomARMType, MCTransStepARMType, MCRotStepARMType, BOX
    use mcmod, only: AccCntMType, iStep
    implicit none
    integer(8), allocatable:: AccCNT_accum(:,:,:)
    real(8), allocatable:: AccRat_sampl(:,:)
    integer(4)  MCSteps,i, ierror
    real(8), parameter:: a=0.4, b=0.4
    character(len=5) :: fmt !Auxilary variable for output format specification
#ifdef mpi
       include 'mpif.h'    
#endif        
! Collect Acceptance counters 
!    NMType=size(MCStepAtomMType)
    if (psize.eq.0) then 
        MCSteps=iStep
    else
        MCSteps=iStep*psize
    endif
    allocate(AccCNT_accum(size(AccCntMType,1),size(AccCntMType,2),size(AccCntMType,3)))
    allocate(AccRat_sampl(size(AccCntMType,1),size(AccCntMType,3)))
    AccCNT_accum=0; AccRat_sampl=0
#ifdef mpi
    call MPI_AllREDUCE(AccCntMType,AccCnt_accum,size(AccCntMType),MPI_INTEGER8,MPI_SUM,MPI_COMM_WORLD,ierror)
#else
    AccCnt_accum=AccCntMType
    
#endif
! Calculate Acceptance Ratios    
    AccRat_sampl(:,1)=dfloat(AccCnt_accum(:,1,1))/dfloat(AccCnt_accum(:,2,1))
    AccRat_sampl(:,2)=dfloat(AccCnt_accum(:,1,2))/dfloat(AccCnt_accum(:,2,2))
    AccRat_sampl(:,3)=dfloat(AccCnt_accum(:,1,3))/dfloat(AccCnt_accum(:,2,3))
    if (ID.le.1) then
        write(6,*) 'MC step size automatic adjustment. MCStep:',iStep, 'NSteps=',MCSteps
        write(fmt,'(i3)') NMType*3
        write(6,'(a,'//fmt//'a8)')    ' Molecule Type         ',(NameMType(:))
        write((6),'(a,'//fmt//'f8.3)')' MCStepAtom(MolType)   ',MCStepAtomMType(:)
        write((6),'(a,'//fmt//'f8.3)')' MCTranStep(MolType)   ',MCTransStepMType(:)
        write((6),'(a,'//fmt//'f8.3)')' MCRotStep(MolType)    ',MCRotStepMType(:)                     
        write(6,*) 'Sampled Acceptance Ratio' 
        write((6),'(a,'//fmt//'f8.3)')'Acceptance ratio atoms ',AccRat_sampl(:,1)
        write((6),'(a,'//fmt//'f8.3)')'Acceptance ratio trans ',AccRat_sampl(:,2)
        write((6),'(a,'//fmt//'f8.3)')'Acceptance ratio rot   ',AccRat_sampl(:,3)   
        write(6,*) 'Desired Acceptance Ratio' 
        write((6),'(a,'//fmt//'f8.3)')'Acceptance ratio atoms ',MCStepAtomARMType(:)
        write((6),'(a,'//fmt//'f8.3)')'Acceptance ratio trans ',MCTransStepARMType(:)
        write((6),'(a,'//fmt//'f8.3)')'Acceptance ratio rot   ',MCRotStepARMType(:)   
    endif
    ! Calculate update to MC step size    
    do i=1,NMType
        MCStepAtomMType(i)=MCStepAtomMType(i)*(log(a*MCStepAtomARMType(i) +b))/(log(a*AccRat_sampl(i,1) +b))
        if (AccRat_sampl(i,2).gt.0) & ! to avoid error for ions, which do not have rotation and translation steps.
            MCTransStepMType(i)=MCTransStepMType(i)*(log(a*MCTransStepARMType(i) +b))/(log(a*AccRat_sampl(i,2) +b))
        if (AccRat_sampl(i,3).gt.0) &
            MCRotStepMType(i)=MCRotStepMType(i)*(log(a*MCRotStepARMType(i) +b))/(log(a*AccRat_sampl(i,3) +b))
    enddo   

    ! Check that step-sizes are reasonable, i.e. less than 0.1*Box size
    if (max(maxval(MCStepAtomMType(:)), maxval(MCTransStepMType(:))).gt.0.1*maxval(BOX)) then        
        if (ID.le.1) then
            write(6,*) 'Warning: The adjusted MC-step is too big > 0.1*Box. It will be capped at the 0.1*Box value.'
            write(6,*) 'The self-adjustment of MC step seems disconverging.'
            write(6,*) 'Consider specifying the step values manually, and turning off autoadjustment'        
        endif
        where (MCStepAtomMType.gt. 0.1*maxval(BOX)) MCStepAtomMType = 0.1*maxval(BOX)
        where (MCTransStepMType.gt. 0.1*maxval(BOX)) MCTransStepMType = 0.1*maxval(BOX)        
    endif    

    
    if (ID.le.1) then
        write(6,*) 'Adjusted MC steps'     
        write(6,'(a,'//fmt//'a8)')    ' Molecule Type         ',(NameMType(:))
        write((6),'(a,'//fmt//'f8.3)')' MCStepAtom(MolType)   ',MCStepAtomMType(:)
        write((6),'(a,'//fmt//'f8.3)')' MCTranStep(MolType)   ',MCTransStepMType(:)
        write((6),'(a,'//fmt//'f8.3)')' MCRotStep(MolType)    ',MCRotStepMType(:)                     
    endif             
    deallocate(AccCNT_accum,AccRat_sampl)
end subroutine