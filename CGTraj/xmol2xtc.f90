!     Package tranal v.5.0
!     Transformation of trajectory from atomistic to CG-based
!subroutine written by Alexander Mirzoev
program xmol2xtc_converter
use TrajectoryIO
use strings, only: str_cmp
use precision
implicit none
!DEC$ FREEFORM
!DIR$ FREE
character*128 buffer,FMT
integer(4) numargs, ierr, natoms

real(8), allocatable :: R_vec(:,:)
character(atom_name_len), allocatable  :: Names(:)
character(4) frmt
logical l_exist
real(4) box(3), time

!  Reading command line arguments
    numargs=iargc()
    if (numargs.ne.1) then
        write(*,*) 'Too many or too few parameters in command line:',numargs,'! Just one needed: Input file name.'
        stop
    else 
        call GETARG(1,buffer)
        write((6),*) 'Input file name stated in command line: ',buffer
    endif
! Reading input file - detect number of atoms
    inquire(file=trim(buffer), exist=l_exist)
    if (.not. l_exist) stop 'input file does not exist'
    open(5,file=trim(buffer),IOSTAT=ierr, status='OLD', action='READ')
    if (ierr.ne.0) stop 'can not open input trajectory for reading'
    read(5,*) natoms
    close(5)
    allocate(R_vec(3, Natoms))
    R_vec = 0.0
    allocate(Names(Natoms))
    Names = ''
!    open(5,file=trim(buffer),IOSTAT=ierr, status='OLD', action='READ')
    call OpenTraj('xmol', buffer, stdout_=5, mode='r', ierr=ierr)
    call OpenTraj('xtc',buffer(1:index(buffer,'.', back=.True.))//'xtc' , stdout_=15, mode='w', ierr=ierr)
    write(*,*) 'Reached time (ns): ' 
    do 
        call ReadFrame('xmol', 5, NAtoms, BOX, Names, R_vec, time, ierr)
        if (is_iostat_end(ierr)) then
                write(*,*) 'End of the  file  reached.'
                exit
        else if (ierr.ne.0) then
            write(*,*)'ERROR: while reading from input file '
            stop 10
        endif    
        call SaveFrame('xtc', 15, Natoms, box, Names, R_vec, time)
        if (mod(int(time/1000),1000).eq.0) then
            write(*,'(f12.0)') time/1000000 
        endif         
    enddo     

    write(*,'(a, f12.0)') 'Last frame time (ns): ', time/1000000 
    call CloseTraj('xtc', 15, 'w')
    call CloseTraj('xmol',5,'r')
    call OpenTraj('xmol', 'last_frame.xmol', stdout_=15, mode='w', ierr=ierr)   
    call SaveFrame('xmol', 15, Natoms, box, Names, R_vec, time)
    call CloseTraj('xmol', 15, 'w')
    
end
	      
	

