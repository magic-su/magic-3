   subroutine InitGeometry(ID, iREP)

        use precision
        use utilsmod
!        use iomod, only: NAtom, NMType, NameMType, FStart, LCRD, LCRDPASS, NFStart, NSiteMType!, AdrAtomMType, AdrMolMType,AdrAtomMol
        implicit none
        ! Global
        
        integer(4), intent(in)::  ID, iREP
        
        !Local
        integer(4) i
        
        !  Preparing initial state geometry

        write(stdout,*) '' 
        write(stdout,*) 'Prepare starting geometry' 

        if (IREP.eq.1) then  ! The first iteration
            write(stdout,*) 'The first inverse iteration'
            if (LCRD) then
                write(stdout,*) 'Reading initial geometry from file' 
                call READCRD(FSTART,ID)
            else            
                write(stdout,*) 'Randomly generate initial geometry' 
                call GenerateGeometry()
            endif
        else ! Not the first iteration
            if (.not. LCRDPASS) then
                if (LCRD) then
                    write(stdout,*) 'Reading initial geometry from file' 
                    call READCRD(FSTART, ID)
                else            
                        write(stdout,*) 'Randomly generate initial geometry' 
                        call GenerateGeometry()
                endif                
            else
                write(stdout,*) 'Continue with the existing geometry of the system'             
            endif
        endif        

        Svec = Ri_vec ! Copy all generated coordinates to Svec

        ! Make sure COM of all molecules are inside the box
        do i=1, NMol
            call MCmodGetCOM(i)
        enddo                
        
        ! Put all Svec-coordinates inside the box, do not keep bonds whole
        do concurrent(i=1:NAtom)
            call MCPBC(Svec(:,i))
        enddo

    end subroutine
