program MAGIC
!*
!*  MagiC's Monte Carlo Engine - direct and inverse simulation
!*
!*  Construction of effective potentials for arbitrary molecular
!*  systems from given RDF
!*
!*  Units: distances A
!*         Energy - kJ/mol
!*
!*  Alexander Lyubartsev      alexander.lyubartsev@mmk.su.se
!*  Alexander Mirzoev         amirzoev@ntu.edu.sg or alexander.mirzoev@outlook.com
!*
!*  The main module
!*
!DEC$ FREEFORM
use precision
use dynarray
use utilsmod
use timer
use iomod 
use imcmod
use mcmod
use magicmod
implicit none

#ifdef mpi
include 'mpif.h'
#endif

! few Auxilary variables:
integer(4) ::   I,J, ierr, wrong_reads_cnt


! Initialization
call InitMagic()
! Here all input files are read, and default parameters are set      
call ReadAll(&
    RDFref_NB_riSjS,RDFref_B_riBond, &
    AdrGlobIndexAtomij, rank, ID, psize)
! Initialize Monte Carlo engine variables
! TODO Remove line below
if ((ID.eq.1).and.(IPRINT.ge.5)) then
    do i=1, NMType
        write(6,*) '   *** MOLECULE ',trim(adjustl(NameMType(i))),',  Type  No.',i
    enddo
endif
call MCmodInit()        
call TimerStamp(stdout,'Initialization finished:')        
if (ID.le.1) call ShiftIntraPotToZero(StateB)
#ifdef mpi
    ! Passing new potential to all processes
call TimerStart(T_MPI)
call MPI_Bcast(PotNB_riSjS,size(PotNB_riSjS),MPI_REAL8,0,MPI_COMM_WORLD,ierr) !Sending POT        
call MPI_Bcast(PotB_riBond,size(PotB_riBond),MPI_REAL8,0,MPI_COMM_WORLD,ierr) !Sending PotB_riBond
call TimerStop(T_MPI)
#endif

do ! IMC loop -----------------------------------------------------------------------------------------------
    !  Setting zero-th for intramolec. potential
    istep = 0    
    call ResetCounters()     
    !!  Preparing initial state geometry    
    if (mode .ne. INVERT) then
        call TimerStart(T_IMCinit)
        do i=1, 100
            call InitGeometry(ID, iREP)    
            call MCmodConstruct(ID, irep, istep.gt.NMCStepsEquil, ierr)
            if (ierr.eq.0) exit ! if no error in configuration, leave the loop
        enddo
    if (ierr .ne. 0) then
        write(stdout, *) 'Error: can not read/generate starting configuration compatible with provided bond distributions.'
        stop 702
    endif

        call MCmodInitEwald()        
        call MCmodFrozenEnergy(NBondsAtom,BondNum, jAtomBond)

        call MCmodEwald()                                        
        call MCmodConstruct(ID, irep, istep.gt.NMCStepsEquil, ierr)

        call MCmodPrintEnergy() ! Set iStep to 0 to suppress Error in Energy message    
        write((stdout),*)' Init. energy = ',ENER*FKJM,' fourier ',EFUR*FKJM         

        if(iWriteTraj.gt.0) then ! If writing trajectory: Open trajectory output file and write first snapshot
            call OpenTraj(ID,psize,IREP)
            call WriteTraj(psize,int(0,8),Ri_vec)
        endif        
        call TimerStop(T_IMCinit)
!Here we are done with all the initialization  !!!
    endif
    
!   Start simulation
    write((stdout),*) '' 
    write((stdout),*) '' 
    write((stdout),*)'==================================================='
    write((stdout),*)'About to start ',NMCSteps,' Monte Carlo steps; iteration',IREP
    write((stdout),*)'==================================================='
    write((stdout),*)' Total atoms      ',NAtom
    write((stdout),*)' Total molecules  ',NMol
    write((stdout),*)' Interaction sites',NSType
    write((stdout),*)' Atom sites       ',NSite
    write((stdout),*)' Molecule types   ',NMType
    write((stdout),*)'==================================================='
    call TimerStamp(stdout,'')
    if(IPRINT.ge.6)then
        write((stdout),*)' array Bond_iSjS'
        do I=1,NSite
            write((stdout),*)(Bond_iSjS(I,J),J=1,NSite)
        end do
    end if
    if (LExternalTraj) call OpenExternalTraj(ID)
    MC_loop: do iStep=1, NMCSteps ! ===================MC simulations Loop=======================================================================                           
        if (IPRINT.gt.7)   write ((stdout),*) 'AccRat:',AccCntMType(:,1,:),'ISTEP:',ISTEP
    ! Single atom displacement MC step
        call TimerStart(T_MCStep)
        if (LExternalTraj) then
            ierr = 0
            if (IS_IOSTAT_END(stdin_externaltraj)) exit
            call ReadFrame(Ri_vec)
            if (IS_IOSTAT_END(stdin_externaltraj)) exit
            if (ISTEP.gt.NMCStepsEquil) call MCmodConstructGeometry(ID, irep, istep.gt.NMCStepsEquil, ierr)
            if (ierr.ne.0) then 
                wrong_reads_cnt = wrong_reads_cnt + 1
            else
                wrong_reads_cnt = 0
            endif
            if (wrong_reads_cnt .ge. 1000) then                
                write(stdout, *) 'Error: 10 consequent reads from external trajectory has bonds beyond allowed range'
                stop 703
            endif
!            write(*,*) 'DEBUG ', iStep
            if (ierr.ne.0) cycle MC_loop ! if no error in configuration, cycle the loop
            ! if we read coordinates from external trajectory - use these coordinates for Svec also            
            Svec = Ri_vec                
        else
            call MCmodStepAtom()            
        endif
        call TimerStop(T_MCStep)
    ! Molecule translation MC step
        if ((ITRANS.gt.0).and.(mod(ISTEP,ITRANS).eq.0)) then
            call TimerStart(T_MCTrans)
            call MCmodMolTrans()
            call TimerStop(T_MCTrans)
        endif        
    ! Molecule rotation MC step
        if ((IROT.gt.0).and.mod(ISTEP,IROT).eq.0) then
            call TimerStart(T_MCRot)
            call MCmodMolRot()
            call TimerStop(T_MCRot)
        endif ! End of rotational MC step               
        ! Move centers of mass of all molecules inside the box once in a while (once all molecules moved). Not used with external trajectory
        if ((.not. lExternalTraj).and.(mod(iStep, nAtom).eq.0)) then
            do i=1, NMol
                call MCmodGetCOM(i)
            enddo                
        endif
    ! Averaging
        if((ISTEP.gt.NMCStepsEquil).and.(mod(ISTEP,iAverage).eq.0)) then            
            call MCmodCollectCorrel()                    
        end if ! end of Averaging
    ! Write coordinates to the trajectory file    
        if(iWriteTraj.gt.0.and.mod(ISTEP,iWriteTraj).eq.0)  call WriteTraj(psize,ISTEP,Ri_vec)                
    ! Write MC-simulation status to local output file
        if((iCalcEnergy.gt.0).and.mod(ISTEP,iCalcEnergy).eq.0) then 
            call TimerStart(T_reConstruct)
            ! Recalculate and Check possible deviations in the total energy. They can appear from errors in calculation of energy change during MC steps
            call MCmodConstruct(ID, irep, istep.gt.NMCStepsEquil, ierr)        
            call MCmodEwald()
            call MCmodPrintEnergy()    
            call TimerStop(T_reConstruct)
        end if
                
    ! Perform potential convergence check on master process (or in serial mode)
        if ((Mode.ne.SAMPLE) .and. LpotCorrCheck &
            .and. ((ISTEP-NMCStepsEquil).gt.0) &
            .and. (mod(ISTEP-NMCStepsEquil,IPotCorrCheck).eq.0)) then
            
            call CollectCrossCorr(.false.)
	    if (ID.le.1)  then 
                write((stdout),*)"Potential correction convergence check",(ISTEP+1-NMCStepsEquil)/IPotCorrCheck," of ",&
                                (NMCSteps-NMCStepsEquil+1)/IPotCorrCheck                
		call POTCORRMain(.true.,IREP,int((ISTEP+1-int(NMCStepsEquil,4))/IPotCorrCheck, 8), 0) !Here I sent ID=0, to write output in general output file
	    endif
        endif
        
    ! MCstep AutoAdjustment
        if (NMCAutoAdjust.gt.0) then
            if ((ISTEP.lt.NMCStepsEquil).and.(mod(ISTEP,NMCStepsEquil/NMCAutoAdjust).eq.0)) then
                call MCStepAutoAdjust()               
            endif            
        endif
        
    enddo MC_loop!==================== end of MC-simulation loop
    iStep=iStep-1 ! Make iStep=NMCSteps, to avoid any issues
!=======================
!   Write Monte Carlo simulation summary (first on each process and the on the MPI-master)
    if ((Mode .ne. INVERT).and. (.not. LExternalTraj)) then        
        call PrintMCSummary(stdout,NAV, NMCSteps,&
                NMCStepsEquil,AccCntMType,&
                EELS,EINTS,ENERS,VirNB_accum,VirEL_accum,VirB_accum)       

        if (ID.ge.1) call CollectMCsummary(int(iStep,kind=8))
        if (ID.eq.1) call PrintMCsummary(6,global_NAV, global_NMCSteps,NMCStepsEquil*psize,&
            global_AccCntMType,&
            global_EELS,&
            global_EINTS,global_ENERS,global_VirNB_accum,global_VirEL_accum,global_VirB_accum)              
    endif
    
    ! Perform the inverse procedure: Collect the data from all processes and run IMC      
    call CollectCrossCorr(.true.)        
    
    if ((Mode.ne.SAMPLE).and.(ID.le.1)) then ! If it is a serial run or Master-process of MPI run
        call POTCORRMain(.false., IREP,int(0,8),0) !Here I sent ID=0, to write output in general output file
        if (ID.le.1) call ShiftIntraPotToZero(StateB)
        call WritePot(BaseOutFileName, NA,irep)
    endif
    
    ! Write the final configuration of current IMC iteration to file
    if (LXMOL) call SaveLastConf(ID,IREP, Ri_vec)     
    ! Close the trajectory file for writing
    if (iWriteTraj.gt.0) close(stdout_trj)
#ifdef mpi
    call MPI_BARRIER(MPI_COMM_WORLD,ierr)        
    call TimerCollectMPI(psize)
#endif
    if (ID.le.1) then 
        call TimerStamp(6,'Inverse iteration completed:')
        write(6,*) ''
        write(6,*) 'Average timing (over all processes):'
        call TimerReport(6)
    endif
    
    if(IREP.ge.IREPT) exit ! If no more inverse iterations to perform then exit.
    IREP=IREP+1    
#ifdef mpi
    ! Passing new potential to all processes
    call TimerStart(T_MPI)
    call MPI_Bcast(PotNB_riSjS,size(PotNB_riSjS),MPI_REAL8,0,MPI_COMM_WORLD,ierr) !Sending POT        
    call MPI_Bcast(PotB_riBond,size(PotB_riBond),MPI_REAL8,0,MPI_COMM_WORLD,ierr) !Sending PotB_riBond
    call TimerStop(T_MPI)
#endif
enddo  ! end of IMC loop

! Deallocate arrays, close opened files, finalize MPI.
call TerminateMagiC()
end
