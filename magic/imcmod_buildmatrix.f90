subroutine IMC_build_matrix(CORS, CORP)
!Global
integer(8), intent(in):: CORS(:), CORP(:,:)

!Local
integer(4) IC,JC, I,J, NR, iBond, iSType, jSType
! Count array dimensions (number of equations in the linear system) IC
IC=0
do iSType=1, NSType
    do jSType=iSType, NSType
        if (btest(StateNB(jSType,iSType),8)) then ! if the potential is to be updated
            do NR=1, NA
              if ((IND(NR, jSType, iSType).eq.1).and.&
                (CORS(IndexGlobNB_riSjS(NR, jSType, iSType)).ne.0)) IC=IC+1
            enddo
        endif
    enddo
enddo
do iBond=1, NBond
    if (btest(StateB(iBond),8)) then
        do NR=1, NPointsPotB(iBond)
            if((INDI(NR,iBond).eq.1).and.(CORS(IndexGlobB_riBond(NR, iBond)).ne.0).and.(NR.ne.NMIN(iBond))) IC=IC+1
        enddo
    endif
enddo

NUR=IC ! Number of equations in the system
! Allocate arrays
allocate(DIFF(NUR)); allocate(CROSS(NUR,NUR)); allocate(IUCMP(NUR)); allocate(IICMP(size(CORS))); allocate(IPVT(NUR))
DIFF(:)=0.d0; CROSS(:,:)=0.d0; IPVT(:)=0; IICMP(:)=0; IUCMP(:)=0;


IC=0
do iSType=1, NSType
    do jSType=iSType, NSType
        if (btest(StateNB(jSType,iSType),8)) then ! if the potential is to be updated
            do NR=1, NA
              i=IndexGlobNB_riSjS(NR, jSType, iSType)
              if ((IND(NR, jSType, iSType).eq.1).and.(CORS(I).ne.0)) then
                IC=IC+1
                IUCMP(IC)=I            
                IICMP(I)=IC
                DIFF(IC)=COR(I)
              endif
            enddo
        endif
    enddo
enddo
write(stdout_imc,*) 'Number of equations due to intermolecular/nonbonded terms: ',IC

do iBond=1, NBond
    if (btest(StateB(iBond),8)) then ! if the potential is to be updated
        do NR=1, NPointsPotB(iBond)
            I=IndexGlobB_riBond(NR, iBond)
            if((INDI(NR,iBond).eq.1).and.(CORS(I).ne.0)) then
                if(NR.eq.NMIN(iBond))then
                   write((stdout_imc),*)' Point ',NR,' blocked for bond ', iBond
                else
                   IC=IC+1
                   IUCMP(IC)=I
                   IICMP(I)=IC
                   DIFF(IC)=COR(I)
                endif            
            endif
        enddo
    endif
enddo

NUR=IC
do IC=1,NUR
    I=IUCMP(IC)
    do JC=IC,NUR
        J=IUCMP(JC)
        if(I.gt.J)write(stdout_imc,*)'  !!!  violation in uncompressing '
        CROSS(JC,IC) = CORP(J,I)*FNR-CORS(I)*CORS(J)*FNR**2
        CROSS(IC,JC)=CROSS(JC,IC)
    end do
end do
if(IPRINT.ge.8)then
    do IC=1,NUR
        I=IUCMP(IC)
        write(stdout_imc,'(i4,10(200f10.6/4x))')I,(CROSS(IC,JC),JC=1,NUR)
    end do
end if

!write(stdout_imc,'(i4,10(200f10.6/4x))') NUR-2,(CROSS(NUR-2,J),J=1,NUR)
!write(stdout_imc,'(i4,10(200f10.6/4x))') NUR-1,(CROSS(NUR-1,J),J=1,NUR)
!write(stdout_imc,'(i4,10(200f10.6/4x))') NUR,(CROSS(NUR,J),J=1,NUR)

end subroutine IMC_build_matrix

subroutine NG_build_matrix(CORS, CORP,  PotRcut)
     
implicit none
!Global
integer(8), intent(in):: CORS(:), CORP(:,:)
real(8), intent(in)   :: PotRCUT
!Local
integer(4) IC,JC, I,J, NR, iBond, iSType, jSType
! Count array dimensions

!First count number of RDF points, which has nonzero reference and sampled values
Nrdf=0
do iSType=1, NSType
    do jSType=iSType, NSType
        if (btest(StateNB(jSType,iSType),6)) then ! if the RDF is sampled for the pair of atom types
            do NR=1, NA
              if ((Sref_NB_riSjS(NR,jSType,iSType).gt.0).and.(Scalc_NB_riSjS(NR, jSType, iSType).gt.0)) Nrdf=Nrdf+1
            enddo
        endif 
    enddo
enddo
write(stdout_imc,*) 'Number of available NB RDF points is:',Nrdf

! This check can be useful when all symmetric matrixes will be reduced to upper-diagonal (if it will ever happen)
!j=count(((reshape(IND(1:NA,1:NSType,1:NSType),[NA*NSType*NSType]).eq.1)).and.&
!    (CORS(reshape(IndexGlobNB_riSjS(1:NA, 1:NSType, 1:NSType),[NA*NSType*NSType])).ne.0))
!if ((nrdf.ne.I).or.(nrdf.ne.j)) then
!    write(*,*) 'NG_build_matrix error nrdf',nrdf,I,j
!    stop 1
!endif

do iBond=1, NBond
    do NR=1, NPointsPotB(iBond)
        if (btest(StateB(iBond),6)) then ! if the RDF is sampled for the bond
            if ((Sref_B_riBond(NR,iBond).gt.0).and.(Scalc_B_riBond(NR, iBond).gt.0).and.(NR.ne.NMIN(iBond))) Nrdf=Nrdf+1
        endif
    enddo
enddo

write(stdout_imc,*) 'Number of available total RDF points is:',Nrdf

!Than count number of available POTENTIAL points, which are defined and not fixed, and which have nonzero value of the respective RDF
Npot=0
do iSType=1, NSType
    do jSType=iSType, NSType
        if (btest(StateNB(jSType,iSType),8)) then ! if the potential is to be updated
            do NR=1, NA
              if ((IND(NR,jSType,iSType).eq.1) .and. & ! if the point has nonzero respective RDFref
                    (Scalc_NB_riSjS(NR, jSType, iSType).ne.0) .and. & ! ! if the point has nonzero respective RDFsampled
                    (RAS(NR).le.PotRcut)) & ! if the point is within PotRcut distance
                        Npot=Npot+1
            enddo
        endif
    enddo
enddo
do iBond=1, NBond
    if (btest(StateB(iBond),8)) then ! if the potential is to be updated
        do NR=1, NPointsPotB(iBond)
            if  ((INDI(NR,iBond).eq.1) .and. & ! if the point has nonzero respective RDFref
                (Scalc_B_riBond(NR, iBond).ne.0) .and. & ! if the point has nonzero respective RDFsampled
                (NR.ne.NMIN(iBond))) & ! if the point is not fixed due-to being randomly chosen close-to-minimum-point
                    Npot=Npot+1 ! count the point
        enddo
    endif
enddo

write(stdout_imc,*) 'Number of available potential points is:',Npot

if (Npot.gt.NRdf) then
    write(stdout_imc) 'NG_build_matrix: Error: Npot>NRDF, NPOT, NRDF:', NPot, NRdf
    stop 203
endif
! 

! Allocate arrays
allocate(DIFF(Nrdf)); Diff=0.d0
allocate(S_weight(Nrdf)); S_weight=1.d0
allocate(iRDF2iGlob(Nrdf)); iRDF2iGlob=0
allocate(iGlob2iRDF(size(CORS))); iGlob2iRDF=0
allocate(iPot2iGlob(Npot)); iPot2iGlob=0
allocate(iGlob2iPot(size(CORS))); iGlob2iPot=0
allocate(CROSS(NPot,NRDF)); CROSS=0.d0

! Calculate differences in Sref-Ssaml
IC=0
do iSType=1, NSType
    do jSType=iSType, NSType
        if (btest(StateNB(jSType,iSType),6)) then ! if the RDF is sampled for the pair of atom types
            do NR=1, NA
              i=IndexGlobNB_riSjS(NR, jSType, iSType)
          if ((Sref_NB_riSjS(NR,jSType,iSType).gt.0).and.(Scalc_NB_riSjS(NR, jSType, iSType).gt.0)) then
                IC=IC+1
                iRDF2iGlob(IC)=I ! iRDF-> Global index            
                iGlob2iRDF(I)=IC ! Global index -> iRDF
                if(factor_NG.eq.1.)then
                  S_weight(IC)=1.0/Sref_NB_riSjS(NR,jSType,iSType)
                else if(factor_NG.eq.0)then
                  S_weight(IC)=1.
                else 
                  S_weight(IC)=1.0/Sref_NB_riSjS(NR,jSType,iSType)**factor_NG
                end if
                DIFF(IC)=COR(I)*S_weight(IC) ! difference in Ssampl-Sref
              endif
            enddo
        endif
    enddo
enddo
!write(*,*) 'Debug: NG_BuildMatrix: NRdf_NB:',IC
do iBond=1, NBond
    if (btest(StateB(iBond),6)) then ! if the RDF is sampled for the bond
        do NR=1, NPointsPotB(iBond)
            I=IndexGlobB_riBond(NR, iBond)
            if((Sref_B_riBond(NR,iBond).gt.0).and.(Scalc_B_riBond(NR, iBond).ne.0)) then
                if(NR.eq.NMIN(iBond))then
                   write((stdout_imc),*)' Point ',NR,' choosen to be blocked for bond ', iBond
                else
                   IC=IC+1
                   iRDF2iGlob(IC)=I
                   iGlob2iRDF(I)=IC
                   if(factor_NG.eq.1.)then
                     S_weight(IC)=1.0/Sref_B_riBond(NR,iBond)
                   else if(factor_NG.eq.0)then
                     S_weight(IC)=1.
                   else 
                     S_weight(IC)=1.0/Sref_B_riBond(NR,iBond)**factor_NG
                   end if
                   DIFF(IC)=COR(I)*S_weight(IC)
                endif            
            endif
        enddo
    endif
enddo
!write(*,*) 'Debug: NG_BuildMatrix: NRdf:',IC
! Populating reference arrays for Potentials
IC=0
do iSType=1, NSType
    do jSType=iSType, NSType                                 
        if (btest(StateNB(jSType,iSType),8)) then ! if the potential is to be updated
            do NR=1, NA
              i=IndexGlobNB_riSjS(NR, jSType, iSType)
              if ((IND(NR,jSType,iSType).eq.1).and.(Scalc_NB_riSjS(NR, jSType, iSType).ne.0).and.RAS(NR).le.PotRcut) then
                IC=IC+1
                iPot2iGlob(IC)=I ! iPot-> Global index            
                iGlob2iPot(I)=IC ! Global index -> iPot
              endif
            enddo
        endif
    enddo
enddo
!write(*,*) 'Debug: NG_BuildMatrix: NPot_NB:',IC
do iBond=1, NBond
    if (btest(StateB(iBond),8)) then ! if the potential is to be updated
        do NR=1, NPointsPotB(iBond)
            I=IndexGlobB_riBond(NR, iBond)
            if((INDI(NR,iBond).eq.1).and.(Scalc_B_riBond(NR, iBond).gt.0)) then
                if(NR.eq.NMIN(iBond))then
                   write((stdout_imc),*)' Point ',NR,' blocked for bond ', iBond
                else
                   IC=IC+1
                   iPot2iGlob(IC)=I
                   iGlob2iPot(I)=IC
                endif            
            endif
        enddo
    endif
enddo
!write(*,*) 'Debug: NG_BuildMatrix: NPot:',IC
! Constructing the matrix Nrdf x Npot
do IC=1,NRDF
    I=iRDF2iGlob(IC)
!    write(*,*) 'Debug: NG_BuildMatrix: IC,I:',IC,I
    do JC=1,NPot
        J=iPot2iGlob(JC)
        if(J.ge.I)then
          CROSS(JC,IC) = CORP(J,I)*FNR-CORS(I)*CORS(J)*FNR**2
        else
          CROSS(JC,IC) = CORP(I,J)*FNR-CORS(I)*CORS(J)*FNR**2
        endif
    end do
end do
end subroutine NG_build_matrix
