#!/bin/bash
# Define location of MAGIC.
DIR=$( cd $( dirname ${BASH_SOURCE[0]} ) && pwd )
MAGICHOME=$DIR
XDRDIR=$DIR/lib/xdrfile-1.1.4
#echo "Location of MAGIC's home folder: " $MAGICHOME

export MAGICHOME
export PATH=$PATH:$MAGICHOME/bin
#export PATH=$MAGICHOME/bin:$MAGICHOME/magic:$PATH
export LD_LIBRARY_PATH=$XDRDIR/lib:$LD_LIBRARY_PATH
export PYTHONPATH=$MAGICHOME/MagicTools:$XDRDIR/src/python:$PYTHONPATH

