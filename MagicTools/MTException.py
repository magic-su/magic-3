class MT_Exception(Exception):
    '''Base class for exceptions in MagicTools'''
    pass
class GeneralError(MT_Exception):
    '''General non-specific Error in MagicTools'''
    # def __init__(self, msg):
    #     self.message = msg
    #     print(self.message)
    #     super(GeneralError, self).__init__(self, self.message)
    pass
class InputValueError(GeneralError):
    ''' Error in input file'''
    pass

class AtomError(GeneralError):
    pass
class MolTypeError(GeneralError):
    pass
class MCMError(GeneralError):
    """Class for reporting MCMfile-related errors"""
    pass
class TrajError(GeneralError):
    """Class for reporting Trajectory file-related errors"""
    pass
class RDFError(GeneralError):
    '''Class for reporting RDF-related errors'''
    pass
class RDFCalculatorError(GeneralError):
    pass
class RDFReadError(RDFError):
    '''Class for reporting errors while reading RDF input file'''
    pass

class MagicToolsError(GeneralError):
    pass

class DFsetError(GeneralError):
    pass
class DFError(GeneralError):
    pass
class DFsAccumulatorError(GeneralError):
    pass