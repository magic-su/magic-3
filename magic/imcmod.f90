module imcmod
    use precision
    use strings
    use utilsmod
    use dynarray
    use timer
    use InOutUnit
    use iomod
    implicit none

    real(8), allocatable:: RAS(:), SHELV(:)
    integer(8), allocatable:: npairs(:,:)
    real(8), allocatable:: Sref_NB_riSjS(:,:,:), RDFref_NB_riSjS(:,:,:), RDFcalc_NB_riSjS(:,:,:), Scalc_NB_riSjS(:,:,:),&
                           Sref_B_riBond(:,:), RDFref_B_riBond(:,:), Scalc_B_riBond(:,:), RDFcalc_B_riBond(:,:)
    real(8), allocatable:: COR(:)            ! Before calling potcorr, it keeps difference between <Sref> and <Scalc>, after calling potcorr - it keeps correction to potential.
    integer(4) :: NUR, Nrdf, NPot
    integer(4), allocatable:: NMIN(:) ! Array keeps point where ipp-th intramolecular potential has MTypeSite minimum.
    integer(4), allocatable, private:: IICMP(:),IUCMP(:),IPVT(:), iRDF2iGlob(:), iPot2iGlob(:), iGlob2iPot(:), iGlob2iRDF(:)
    real(8),allocatable, private::DIFF(:),CROSS(:,:),S_weight(:)
    integer(4), allocatable,private:: STypePairs(:,:), NPairsTotalBond(:)
    real(8), private :: FNR ! 1/NAV
    character*11 comment

    contains
    include "imcmod_applycorrections.f03"
    include "imcmod_buildmatrix.f90"
    include "imcmod_calcdeviation.f03"
    include "imcmod_calcrdf.f90"
    include "imcmod_IMC_deallocate.f03" 
    include "imcmod_IMC_setcorrections.f90"
    include "imcmod_printpotential.f90"
    include "imcmod_printrdf.f03"
    include "imcmod_regularization.f03"    
    include "imcmod_shiftintrapottozero.f03"

    subroutine PotCorrMain(isPotCorrCheck, IREP, IterCorr, ID)

    use magicmod, only : NAV => global_NAV, CORP => global_CORP, CORS => global_CORS, npairsRDF => global_npairsRDF

    implicit none
!-------Global Variables--------------------
    real(8),allocatable:: CORMF(:)
    integer(4) IREP,ID
    integer(8) IterCorr
    logical, intent(in):: isPotCorrCheck ! Is it just a check, or it is a real correction?

!-------Local Variables--------------------
    integer(4) INFO, IC, JC, iSType, jSType, i, ii, iBond, k,l, NURL, NUR3, EVcut
    real(8) DevS,DevRDF
    real(8) FELC, FELR, FACT
    real(8), allocatable:: J(:,:),Jt(:,:),JtJ(:,:),JtJinv(:,:),JtJinvJt(:,:),NG_CORR(:)
    real(8), allocatable:: EVE(:,:),EVA(:),WRK2(:),WRK4(:),SCL(:)
    real(8) RES

    call TimerStart(T_IMC)

    ! Check if there is no potential which shall be updated (i.e. bit 8 is 1) and exit.
    if (.not. any(&
             [ ((BTest(StateNB(jSType,iSType), 8), jSType=1,NSType), iSType=1,NSType),   &
                (BTest(StateB(iBond), 8), iBond=1,NBond)] &
             )) then
        write(stdout_imc,*) 'There is nothing to update: Leave IMC-procedure'
        call TimerStop(T_IMC)
        return
    endif

    ! Generating comment-string to mark output lines:
    if (isPotCorrCheck) then
        write(comment,'(a7,i2,i2)') ' Intermediate ',IREP,IterCorr
    else
        write(comment,'(a7,i2)') ' Final ',IREP
    endif
    
    if(NAV.gt.0)then
        FNR=1./dfloat(NAV)
    else
        write(stdout_imc,*) 'ERROR: Inverse Procedure: No averaging has been done during MC run! NAV=',NAV
        stop 201
    end if
! Calculate RDFs from sampled data:
    call CalculateRDF(CORS,npairsrdf)

! Print RDFs
    call PrintRDF(CORS)

! Calculate deviation with reference RDFs
    call CalcDeviation(DevS, DevRDF)
    DevS=sqrt(DevS/(NA*(NMType+1)*NMType))
    DevRDF=sqrt(DevRDF/(NA*(NMType+1)*NMType))
    write(stdout_imc,'(a7,a,f11.6,a7,f11.6)') comment, 'Deviation from references: S: ',DevS,' RDF: ',DevRDF

! Calculate and regularize differences between reference and sample data
    call reshape(COR,size(CORS))
    allocate(CORMF(size(CORS))); CORMF=0.d0
    call Regularization(CORMF, COR)

    write(stdout_imc,*)'InvMode = ',InvMode
    
    If (isPotCorrCheck) write (stdout_imc,*) "Potential correction convergence check ",IterCorr    
    select case(InvMode)
        case (NG) ! if NewtonGauss
            call NG_build_matrix(CORS, CORP, PotRcut)
            if (.not. allocated(JtJ)) then
    !!            allocate(J(Nrdf,Npot)); J=transpose(CROSS)
                allocate(JtJ(NPot,NPot)); JtJ=0.d0
    !!            allocate(Jt(NPot,NRDF)); Jt=0.d0
                allocate(JtJinv(NPot,NPot)); JtJinv=0.d0
                allocate(JtJinvJt(NPot,NRDF)); JtJinvJt=0.d0
                allocate(NG_CORR(NPot)); NG_CORR=0.d0
            endif
    !!        Jt=transpose(J)  !!   = CROSS
    !!        JtJ=matmul(Jt,J)
            do i=1,Npot
              do k=1,Npot
                do l=1,NRDF
                  JtJ(i,k)=JtJ(i,k)+CROSS(i,l)*CROSS(k,l)*S_weight(l)
                end do
              end do
            end do
            JtJinv=JtJ; call invInPlace(JtJinv)
            JtJinvJt=matmul(JtJinv,CROSS)
            NG_CORR=matmul(JtJinvJt,DIFF)
            COR(:)=0.d0; cor(iPot2iGlob(1:NPot))=NG_CORR(1:NPot)
            call IMC_SetCorrections(CORS)
        case (IMC) ! IMC
            !   Zeros elemination
            call IMC_build_matrix(CORS, CORP)
            write(stdout_imc,*)' Number of equations ',NUR
            write(stdout_imc,*)' regularization parameter  ',REGP
            write(stdout_imc,*)' max. relative change rdf  ',RTM
            write(stdout_imc,*)' max. change of potential ',DPOTM
            ! Matrix solution
    !#include "imcmod_debug_pre.f90"
            call DGESV(NUR,1,CROSS,NUR,IPVT,DIFF,NUR,INFO)
            !#include "imcmod_debug_post.f90"
            if(INFO.ne.0)then ! If no solution found
                write(stdout_imc,*)' IMC ERROR: singular correlation matrix '
                write(stdout_imc,*)' no solution found '
                write(stdout_imc,*)' Increase the number of Monte Carlo steps or switch to IBI'
                write(stdout_imc,*)' INFO = ', INFO
                stop 202
            else ! if solution found
                COR(:)=0.d0; cor(IUCMP(1:NUR))=DIFF(1:NUR)
                call IMC_SetCorrections(CORS)
            endif
        case (EVR) ! Eigen values reugularized IMC)
            !   Zeros elemination
           call IMC_build_matrix(CORS, CORP)
            write(stdout_imc,*)' Number of equations ',NUR
            write(stdout_imc,*)' regularization parameter  ',REGP
            write(stdout_imc,*)' max. relative change rdf  ',RTM
            write(stdout_imc,*)' max. change of potential ',DPOTM
            write(stdout_imc,*)' EVR parameters: ',XREG,YREG
            ! Eigen vector representation
            !#include "imcmod_debug_post.f90"
            NURL=(NUR+1)*NUR/2
            NUR3=3*NUR
            allocate(WRK2(NURL),EVE(NUR,NUR),EVA(NUR),WRK4(NUR3),SCL(NUR))
            WRK2(:)=0.d0
            EVA(:)=0.d0
            WRK4(:)=0.d0
            k=0
            do i=1,NUR
               do ii=i,NUR
                  k=k+1
                    WRK2(k)=CROSS(i,ii)
		enddo
            enddo
  !    EVA : eigen values
  !    EVE : eigen vectors
	    call DSPEV('V','L',NUR,WRK2,EVA,EVE,NUR,WRK4,INFO)
  !  computation   SCL = < Eve_j | DeltaS >   
            do i=1,NUR
               SCL(i)=0.
               do ii=1,NUR
                  SCL(I)=SCL(I)+EVE(II,I)*DIFF(II)
               end do
            end do
   ! regularized matrix inversion
            EVcut = NUR*(1.-YREG)
            if(EVcut < 1)EVcut=1
            if(EVcut > NUR)EVcut=NUR
            write(stdout_imc,'(a,2f11.4,a,2f10.6,a,f10.6)')&
            ' E.V. range: ',EVA(NUR),EVA(NUR-1),' ... ',EVA(2),EVA(1)
            write(stdout_imc,'(a,i6,f10.5)')' EVcut ',EVcut,EVA(EVcut)
            write(stdout_imc,'(a,f10.7)')' Max dump factor ',(EVA(1)/EVA(EVcut))**(1.-XREG)
            do II=1,NUR
               DIFF(II)=0.
               do I=1,NUR
                  if(I.ge.EVcut)then
                     FACT=1.
                  else   
                     FACT=(EVA(I)/EVA(EVcut))**(1.-XREG)
                  end if   
        DIFF(II)=DIFF(II)+FACT*EVE(II,I)*SCL(I)/EVA(I)
               end do   
             end do
             if(INFO.ne.0)then ! If no solution found
                write(stdout_imc,*)' IMC ERROR: singular correlation matrix '
                write(stdout_imc,*)' no solution found '
                write(stdout_imc,*)' Increase the number of Monte Carlo steps or switch to IBI'
                write(stdout_imc,*)' INFO = ', INFO
                stop 202
             else ! if solution found
                COR(:)=0.d0; cor(IUCMP(1:NUR))=DIFF(1:NUR)
                call IMC_SetCorrections(CORS)
             endif
             deallocate (WRK2,EVE,EVA,WRK4,SCL)
        case(IBI) ! IBI
             COR(1:size(CORS))=CORMF(1:size(CORS)) ! Using correction got within IB (Mean Force approximation)
    end select
    ! Limiting the change of the potential
    where (abs(COR).gt.DPOTM) COR=sign(DPOTM,COR)

        ! Print RDF, RDFref, Pot+Corr, Pot, Corr !
    call PrintPotential(irep, itercorr, isPotCorrCheck)

    if ((.not. isPotCorrCheck).and.(ID.ne.1)) & ! Do not apply corrections if Check-call or if it is the master-process in MPI run.
        call ApplyCorrections(COR)

    call IMC_deallocate(J, Jt, JtJ, JtJinv, JtJinvJt, NG_CORR, CORMF)
    call TimerStop(T_IMC)

end subroutine
end
