subroutine ReadCmdLine(rank, buffer)
    integer rank, iargc
    character(filename_text) buffer
    integer numargs
!  Reading command line arguments
    numargs=iargc()
    if (numargs.gt.1) then
        write(*,*) 'Too many parameters in command line:',numargs,'! Just one needed: Input file name.'
        stop 1
    elseif (numargs.eq.1) then
        call GETARG(1,buffer)
        if ((rank.eq.0)) write((6),*) 'Input file name stated in command line: ',buffer
    elseif(numargs.eq.0) then
        if ((rank.eq.0)) write((6),*) 'Input file name NOT stated in command line: Use default input.inp'
        buffer='input.inp'
    endif    
end subroutine ReadCmdLine