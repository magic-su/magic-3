subroutine PrepareReadInput(buffer, text)
    
    use utilsmod, only: MakeSaveFileName    
    
    character(input_text),intent(out), allocatable :: text(:)
          
    integer i, iend, iostt
    character(filename_text) buffer
    character(input_text) l
    
    call ReadCmdLine(rank, buffer) ! read main input file name from the command line
    call SetInOut(ID, 1, pSize)  
    if ((rank.eq.0)) then 
         write (*,*) ''
         write(*,*) '-----------------------------------------------------------------------'
         write(*,*) ' Reading main input file:',buffer
    endif
    iend = GetFileLength(buffer, 'main input')    ! Open the Input file and scan it  for number of lines (variable iend)
    call SetInOut(ID, 5, pSize)
    ! Allocate array for the content of the file    
    allocate(text(iend))
 ! Read the file to the array text
    open(unit=stdin, file=buffer, status='old', iostat=iostt)
    do i=1, iend
        read(stdin,'(a)') l
        text(i)=trim(adjustl(l))
    enddo
    close(stdin)
    call ReadParameter(iPrint, text, 'VerboseLevel', 'IPRINT',&
        'How much information to print',&
        'W',5)!,7,6)
    call ReadParameter(BaseOutFileName, text, 'Output', 'BASEOUTFILENAME',&
        'Base name for output files',&
        'W','output')!,iPrint,6)
    call SetInOut(ID, iPrint, pSize)

  ! Open separate output file for each process    
    if (ID.gt.0) then        
        open(unit=stdout, file=MakeSaveFileName(BaseOutFileName,'', ID=ID), status='unknown')
        write(stdout,*) 'Running in parallel mode: process ',ID,' of ',psize    
    endif
! Open dev/null output file
    open(unit=stdout_devnull, file='/dev/null', status='unknown')
    
! redirect slave-process output to dev/null and master-process to console
    if (ID.gt.1) then  
        stdout = stdout_devnull
    else 
        stdout = stdout_main
    endif    
      
end subroutine PrepareReadInput