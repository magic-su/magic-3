subroutine EnergyForce(RR,IT,JT, Energ, Force)
    implicit none
    real(8), intent(IN) ::  RR
    integer(4), intent(in):: IT,JT
    real(8),intent(out):: ENERG, Force
    integer(4) NR1
    real(8) Al!, AL1

    NR1=int(RR/RDFINC+0.5) ! NR1=round(RR/RDFINC+0.5)
    if (NR1.le.0) NR1=1
    if (NR1.ge.NA) NR1=NR1-1
    AL=(RR-(NR1-0.5)*RDFINC)/RDFINC
    ENERG=(1-AL)*PotNB_riSjS(NR1,IT,JT)+AL*PotNB_riSjS(NR1+1,IT, JT)
    FORCE=(PotNB_riSjS(NR1+1,IT, JT)-PotNB_riSjS(NR1,IT,JT))/RDFINC
    return
end subroutine

subroutine EneriForci(RR,iBond,Rmin,Rmax,PotB_riBond,NPointsPotB, Eneri, Forci)
    real(8), intent(in):: RR,Rmin,Rmax,PotB_riBond(:,:)
    integer(4), intent(in):: iBond,NPointsPotB
    real(8), intent(out):: Eneri, FORCI
    real(8) RDINC,AL,RR1
    integer(4) NR1

    Eneri=0.0; FORCI=0.0
    RDINC=(Rmax-Rmin)/NPointsPotB
    if(RR.le.Rmin)then
        ENERI=PotB_riBond(1,iBond)
        write(stdout,'(a,2f10.4,i4)')' too small argument in Bond Energy calculation ENERI:',RR,Rmin,iBond
        return
    end if
    if(RR.gt.Rmax)then
        ENERI=PotB_riBond(NPointsPotB,iBond)
        write(stdout,'(a,2f10.4,i4)') ' too large argument in Bond Energy calculation ENERI:',RR,Rmax,iBond
        return
    end if
    NR1=(RR-Rmin)/RDINC+0.5
    if(NR1.le.0)NR1=NR1+1
    if(NR1.ge.NPointsPotB) NR1=NR1-1
    RR1=Rmin+(NR1-0.5)*RDINC
    AL=(RR-RR1)/RDINC
    ENERI=(1-AL)*PotB_riBond(NR1,iBond)+AL*PotB_riBond(NR1+1,iBond)
    Forci=(PotB_riBond(NR1+1,iBond)-PotB_riBond(NR1,iBond))/RDINC
    return
end subroutine
