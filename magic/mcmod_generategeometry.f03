    subroutine GenerateGeometry()
        use utilsmod
        use InOutUnit
!        use iomod, only: BOX, NMType, NameMType, FCRD, LMoveMType, AdrAtomMType, AdrMolMType,AdrAtomMol, RSite, NSiteMType, &
!            NMolMType, SiteAtom
        use precision
        implicit none
        ! Global        
        !Local
        integer(4) iMType, iostt, IC, iMol, iAtom, im ,is, isp
        real(8) QP(4)        ! quaternion array
        character(atom_name_len) atomname

    ! If there are fixed molecules:
        if (.not. all(LMoveMType)) then
            write(stdout,*) 'There are frozen molecules in the system, their positions will be read from file: ', trim(FCRD)
            open (unit=stdin_frozencrd,file=FCRD,status='old',iostat=iostt)
            if (iostt.ne.0) stop 'The coordinate file not found'
            read(stdin_frozencrd,*)
            read(stdin_frozencrd,*)
        endif

        do iMType=1,NMType
            if(.not.LMoveMType(iMType)) then  !  fixed coordinates (e.g.DNA)        
                IC=AdrAtomMType(iMType)
                do iMol=1,NMolMType(iMType)
                    do iAtom=1,NSiteMType(iMType)
                        IC=IC+1    
                        read(stdin_frozencrd,*) atomname,Ri_vec(:,ic)
                    end do
                end do
            else
                write(stdout,*)' Generate random distribution of molecules ',trim(NameMType(iMType))
    !  random distribution for COM particles
                do iMol=1,NMolMType(iMType)
                    IM=AdrMolMType(iMType)+iMol                    
                    XvecCOMMol(:,im)=([rand(), rand(), rand()]-0.5d0)*BOX
                    if(IPRINT.ge.7)write(stdout,'(I6,3F12.6)')IM,XvecCOMMol(:,IM)!,Y(IM),Z(IM)
    ! random rotation - using quaternion
                    QP = RANQ()
                    if(IPRINT.ge.7)write(stdout,'(4F12.6)') QP(1),QP(2),QP(3),QP(4)                                
                    do iatom=1,NSiteMType(iMType)
                        ISP=AdrAtomMol(IM)+iAtom
                        IS=SiteAtom(ISP)
                        Wvec(1:3,isp)=Rsite(IS,1:3)
                        Wvec(1:3, isp) = ROTATE(QP,Wvec(1:3,ISP)) ! This one rotates molecule on quaternion QP                        
                        Ri_vec(:,isp)=XvecCOMMol(:,IM)+Wvec(:,isp)
                        if(IPRINT.ge.7)write(stdout,'(3I6,3F12.4)') IM,IS,ISP,Ri_vec(:,isp)
                    end do
                end do
            end if  ! LMoveMType
        end do  ! NMType
        if (.not. all(LMoveMType)) close(stdin_frozencrd)
    end subroutine