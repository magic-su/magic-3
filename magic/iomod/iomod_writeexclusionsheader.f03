subroutine WriteExclusionsHeader(stdout_)       
    implicit none
    integer(4), intent(in):: stdout_
    
    integer(4) iMType
    
    write(stdout_,"(a19, a,a1,i0)", advance='no') 'NAngleBondsExclude=', trim(NameMType(1)), ':', NAngleBondsExcludeMType(1)
    do iMType=2, NMType
        write(stdout_,"(a2,a,a1,i0)", advance='no') ', ', trim(NameMType(iMType)), ':', NAngleBondsExcludeMType(iMType)
    enddo
    write(stdout_,*) ''           !Endline              
    write(stdout_,"(a18,a,a1,i0)", advance='no') 'NPairBondsExclude=', trim(NameMType(1)), ':', NPairBondsExcludeMType(1)        
    do iMType=2, NMType
        write(stdout_,"(a2, a,a1,i0)", advance='no') ', ', trim(NameMType(iMType)), ':', NPairBondsExcludeMType(iMType)        
    enddo
    write(stdout_,*) ''           !Endline   

end subroutine

