subroutine MCmodMolTransCheckTrial(iMol, deltaR)

    use iomod, only: NAtom
    implicit none
    integer(4), intent(in):: iMol
    real(8), intent(in):: deltaR(3)
    !Local
    integer(4) NAtomsInMol, iAtom, jAtom,i, ibeg
    real(8) Force_, Energy_
    real(8), allocatable:: RijM_vec_new(:,:,:)

    call TimerStart(T_MCTransNewCoords)
    ENB_new = 0.0; EelNB_new=0.0
    ENB_old = 0.0; EelNB_old=0.0
    VirNB_new = 0.0; VirNB_old = 0.0
    ! New distances:    
    NAtomsInMol=NSiteMType(MTypeMol(iMol))

    ibeg = AdrAtomMol(iMol)
    call reshape3(RijM_vec_new,3,NAtom,NAtomsInMol)
!DIR$ SIMD    
    do concurrent(i=1:NAtomsInMol, jAtom=1:NAtom)
        RijM_vec_new(1:3,jAtom, i) = Svec(1:3, jAtom) - Svec(1:3, ibeg+i) - deltaR(1:3)
    end do
                                            
    RijM_vec_new(:,AdrAtomMol(iMol)+1:AdrAtomMol(iMol+1),1:NAtomsInMol)= 0.0
    ! Apply PBC for the new vector distances
    call PBC_vecM(RijM_vec_new(1:3,:,1:NAtomsInMol))                
    ! Calculate distance modulus   
    do concurrent(i=1:NAtomsInMol, jAtom=1:NAtom )
	RijM_new(jAtom, i) = sqrt(RijM_vec_new(1,jAtom,i)**2 + RijM_vec_new(2,jAtom,i)**2 + RijM_vec_new(3,jAtom,i)**2)
    enddo
    do concurrent(iAtom = 1:NAtomsInMol, jAtom=AdrAtomMol(iMol)+1:AdrAtomMol(iMol+1))
        RijM_new(jAtom, iAtom)= Rij(jAtom, iAtom+AdrAtomMol(iMol))
    end do
    
    call TimerStop(T_MCTransNewCoords)
    !Set all new energies and virial parts to zero
    call TimerStart(T_MCTransNewArr)
    call TimerStop(T_MCTransNewArr)            
    ! Calculate NB energy terms, where MaskNB and r<rcut
    call TimerStart(T_MCTransNBcount)
    do i=1, NAtomsInMol
        iAtom=AdrAtomMol(iMol)+i
        do jatom=1, AdrAtomMol(iMol)
            if (MaskNB_SR(jAtom,iAtom)) then
                if (RijM_new(jAtom,i)<RCUT) then                   
                   call EnergyForce(RijM_new(jAtom,i),STypeAtom(jAtom),STypeAtom(iAtom), Energy_, Force_)
                   ENB_new = ENB_new + Energy_
#ifdef pressure                   
                   VirNB_new = VirNB_new + Force_*RijM_new(jAtom,i)
#endif                   
                endif            
            endif
            if (Rij(jAtom,iAtom)<RCUT) then                   
                call EnergyForce(Rij(jAtom,iAtom),STypeAtom(jAtom),STypeAtom(iAtom), Energy_, Force_)                   
                ENB_old = ENB_old + Energy_
#ifdef pressure                   
                VirNB_old = VirNB_old + Force_*Rij(jAtom,iAtom)
#endif                   
            endif            

            if (MaskNB_EL(jAtom,iAtom)) then
                if ((RijM_new(jAtom,i)<RECUT)) then 
                    Energy_ = ChargeAtom(iAtom)*ChargeAtom(jAtom)*COULF*derfc(ALPHA*RijM_new(jAtom,i))/RijM_new(jAtom,i)
                    EelNB_new = EelNB_new + Energy_
                endif
                if ((Rij(jAtom,iAtom)<RECUT)) then 
                    Energy_ = ChargeAtom(iAtom)*ChargeAtom(jAtom)*COULF*derfc(ALPHA*Rij(jAtom,iAtom))/Rij(jAtom,iAtom)                        
                    EelNB_old = EelNB_old + Energy_
                endif
            endif
        enddo       
        do jatom=AdrAtomMol(iMol+1)+1, NAtom
            if (MaskNB_SR(jAtom,iAtom)) then
                if (RijM_new(jAtom,i)<RCUT) then                   
                   call EnergyForce(RijM_new(jAtom,i),STypeAtom(jAtom),STypeAtom(iAtom), Energy_, Force_)
                   ENB_new = ENB_new + Energy_
#ifdef pressure                   
                   VirNB_new = VirNB_new + Force_*RijM_new(jAtom,i)
#endif                   
                endif            
            endif
            if (Rij(jAtom,iAtom)<RCUT) then                   
                   call EnergyForce(Rij(jAtom,iAtom),STypeAtom(jAtom),STypeAtom(iAtom), Energy_, Force_)                   
                   ENB_old = ENB_old + Energy_
#ifdef pressure                   
                   VirNB_old = VirNB_old + Force_*Rij(jAtom,iAtom)
#endif                   
            endif            

            if (MaskNB_EL(jAtom,iAtom)) then
                if ((RijM_new(jAtom,i)<RECUT)) then 
                    Energy_ = ChargeAtom(iAtom)*ChargeAtom(jAtom)*COULF*derfc(ALPHA*RijM_new(jAtom,i))/RijM_new(jAtom,i)
                    EelNB_new = EelNB_new + Energy_
                endif
                if ((Rij(jAtom,iAtom)<RECUT)) then 
                    Energy_ = ChargeAtom(iAtom)*ChargeAtom(jAtom)*COULF*derfc(ALPHA*Rij(jAtom,iAtom))/Rij(jAtom,iAtom)                        
                    EelNB_old = EelNB_old + Energy_
                endif

            endif
        enddo       
    enddo
    call TimerStop(T_MCTransNBcount)
end subroutine
