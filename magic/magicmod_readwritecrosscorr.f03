subroutine WriteCrossCorr()
    use precision
    use utilsmod, only: MakeSaveFileName
    use InOutUnit
    use strings
    use iomod, only: CrossCorrFile, InvMode
    implicit none 
    
    logical l_exist
    
    if (.not. str_cmp(CrossCorrFile,'')) then 
        write(stdout,*) 'Write CrossCorr Matrix to file: ', CrossCorrFile        
        open(unit=stdout_crosscorr, file=CrossCorrFile, action='write', status='replace', form='unformatted')   
        write(stdout_crosscorr) global_NAV
        write(stdout_crosscorr) size(global_CORS)
        write(stdout_crosscorr) global_CORS
        write(stdout_crosscorr) size(global_npairsRDF, 1), size(global_npairsRDF, 2) 
        write(stdout_crosscorr) global_npairsRDF
        if (InvMode.ge.1) then
            write(stdout_crosscorr) size(global_CORP, 1), size(global_CORP, 2)
            write(stdout_crosscorr) global_corp 
        endif    
        close(stdout_crosscorr)    
    endif
end subroutine

subroutine ReadCrossCorr()
    use precision
    use dynarray
    use strings
    use InOutUnit
    use iomod, only: CrossCorrFile, InvMode
    implicit none
    
    logical l_exist    
    integer(4):: global_NAV_read=0
    integer(8), allocatable:: global_CORS_read(:), global_CORP_read(:,:), global_npairsrdf_read(:,:)
    integer(4) N, M
    
    l_exist = .false.            
    if (.not. str_cmp(CrossCorrFile, '')) inquire(file=CrossCorrFile, exist=l_exist)
    
    if (l_exist) then 
        ! Reading values
        write(stdout,*) 'Read CrossCorr Matrix from file: ', CrossCorrFile
        open(unit=stdin_crosscorr, file=CrossCorrFile, form='unformatted', status='OLD')
        read(stdin_crosscorr) global_NAV_read
        
        read(stdin_crosscorr) N
        call reshape(global_CORS_read, N)                
        read(stdin_crosscorr) global_CORS_read
        
        read(stdin_crosscorr) N, M
        call reshape2(global_npairsRDF_read, N, M)
        read(stdin_crosscorr) global_npairsRDF_read
        
        if (InvMode.ge.1) then
            read(stdin_crosscorr) N, M
            call reshape2(global_CORP_read, N, M)
            read(stdin_crosscorr) global_CORP_read 
        endif    
        close(stdin_crosscorr) 
        
        ! check that arrays are of same size
        if (size(global_CORS).ne.size(global_CORS_read)) then
            write(stdout, *) 'Error: sizes of global_CORS and global_CORS_read mismatch:', size(global_CORS), size(global_CORS_read)
            stop 662
        endif
        if (size(global_npairsRDF).ne.size(global_npairsRDF_read)) then
            write(stdout, *) 'Error: sizes of global_npairsRDF and global_npairsRDF_read mismatch:', &
                size(global_npairsRDF), size(global_npairsRDF_read)
            stop 662
        endif
        if (InvMode.ge.1) then
            if ((size(global_CORP,1).ne.size(global_CORP_read,1)).or.(size(global_CORP,2).ne.size(global_CORP_read,2))) then
                write(stdout, *) 'Error: sizes of accumulated CrossCorr matrix and the one in the file mismatch:',&
                      size(global_CORP,1), size(global_CORP,2), size(global_CORP_read,1), size(global_CORP_read,2)
                stop 662
            endif
        endif 
        ! Now we can update accumulated cross-correlation arrays
        global_NAV = global_NAV + global_NAV_read
        global_CORS = global_CORS + global_CORS_read
        global_npairsRDF = global_npairsRDF + global_npairsRDF_read
        if (InvMode.ge.1) global_CORP = global_CORP + global_CORP_read
        
        ! deallocate temporary arrays
        deallocate(global_CORS_read, global_npairsRDF_read)
        if (InvMode.ge.1) deallocate(global_CORP_read)
    else
        write(stdout,*) 'CrossCorr Matrix file does not exist: Nothing to read.'
    endif
end subroutine