    subroutine ResetCounters()
        implicit none
        !  New run, summators=0
            write((stdout),*) 'Set all counters to zero'
            NAV=0            
            AccCntMType(:,1,1:3)=0
            AccCntMType(:,2,1:3)=1
            ENERS=0.d0
            EELS=0.
            EINTS=0.
            VirNB_accum=0.
            VirB_accum=0.
            VirEL_accum=0.
            pressure_instant=0.
            CORS(:)=0.d0            
            if (allocated(CORP)) CORP(:,:)=0.d0
            npairsrdf(:,:)=0

    end subroutine