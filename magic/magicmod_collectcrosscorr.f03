!
!=================== CollectCrossCorr =====================================
!   Subroutine collecting data from different processes using MPI
subroutine CollectCrossCorr(isCrossCorr_RW)
    use dynarray
    use iomod, only: InvMode, LExternalTraj
    use mcmod, only: NAV, CORS, CORP, npairsrdf
    !-------Global Variables--------------------
    logical, intent(in):: isCrossCorr_RW
    !-------Local Variables--------------------
    integer(4) i,j,k, ierror
#ifdef mpi
    include 'mpif.h'
#endif
    call TimerStart(T_MPI)
! Allocate memory for the collected arrays global_*
    call reshape(global_CORS,size(CORS))        
    if (InvMode.ge.1) call reshape2(global_CORP,size(CORP,1),size(CORP,2))
    call reshape2(global_npairsrdf,size(npairsrdf,1),size(npairsrdf,2))

#ifdef mpi
    if (ID.gt.0) then
        call MPI_BARRIER(MPI_COMM_WORLD,ierror)
        
    ! Gathering CORS - self-correlated terms
        call MPI_REDUCE(CORS,global_CORS,size(CORS),MPI_INTEGER8,MPI_SUM,0,MPI_COMM_WORLD,ierror)
        call MPI_BARRIER(MPI_COMM_WORLD,ierror)
    ! Gathering CORP - cross-correlated terms
        if (InvMode.ge.1) then ! If IMC mode, collect cross correlated terms
            call MPI_REDUCE(CORP,global_CORP,size(CORP),MPI_INTEGER8,MPI_SUM,0,MPI_COMM_WORLD,ierror)
        endif
    ! Gathering number of counted pairs in RDF calc
        call MPI_REDUCE(npairsrdf,global_npairsrdf,size(npairsrdf),MPI_INTEGER8,MPI_SUM,0,MPI_COMM_WORLD,ierror)
        call MPI_REDUCE(NAV,global_NAV,1,MPI_INTEGER,MPI_SUM,0,MPI_COMM_WORLD,ierror)

        if ((.not. LExternalTraj) .and. (ID.eq.1) .and. (global_NAV.ne.(NAV*psize))) then
            write(*,*) 'Collect: ERROR in NAV - reduction', global_NAV,'!=',NAV*psize    
            stop 204
        endif    
    endif
#endif
    if (ID.eq.0) then 
        ! in case of serial execution
        global_CORS=CORS
        global_NAV=NAV
        global_npairsrdf=npairsrdf
        if (InvMode.ge.1) global_CORP=CORP
    endif
    
    if (ID.le.1) then ! if serial or master process
        if (isCrossCorr_RW) then ! Read Update and Write CrossCorr
            call ReadCrossCorr()
            call WriteCrossCorr()
        endif
    endif
    call TimerStop(T_MPI)
    
end subroutine