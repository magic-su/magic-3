subroutine ApplyCorrections(COR)

!Global
    real(8), intent(in)    :: COR(:)
    !Local
    integer(4) iSType, jSType, iBOnd, NP

    do iSType=1,NSType
        do jSType=iSType,NSType
            if (BTest(StateNB(jSType,iSType),8)) then  
                PotNB_riSjS(1:NA,jSType, iSType)=PotNB_riSjS(1:NA,jSType, iSType)+COR(IndexGlobNB_riSjS(1:NA,jSType,iSType))                  
                PotNB_riSjS(1:NA,iSType, jSType)=PotNB_riSjS(1:NA,jSType, iSType)                
            end if                                
        end do
    end do   

    do iBond=1, NBond
        if (Btest(StateB(iBond),8)) then 
            np=NPointsPotB(iBond)
            PotB_riBond(1:NP,iBond) = PotB_riBond(1:NP,iBond)+COR(IndexGlobB_riBond(1:NP,iBond))
        endif
    enddo
end subroutine ApplyCorrections
