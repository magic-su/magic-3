subroutine MCmodStepAtomCheckTrial(iAtom, deltaR, Lexit)
    
    use iomod, only: NAtom
    implicit none
    
    integer(4), intent(in):: iAtom
    real(8), intent(in):: deltaR(3)    

    !Local                                
    integer(4) jatom,i
    real(8) Force_, Energy_
    character(60) msg
    logical Lexit

    ! New distances:             
!DIR$ SIMD
    ! For atoms belonging to different molecules we use PBC-wrapped coordinates
    do concurrent  (jAtom = 1: NAtom, MolAtom(jAtom).ne. MolAtom(iAtom))       
        Rij_vec_new(1:3, jAtom) = Svec(1:3,jAtom) - Svec(1:3,iAtom) - DeltaR
    end do
    ! For atoms belonging to same molecule we use PBC-UNwrapped coordinates
    do concurrent  (jAtom = 1: NAtom, MolAtom(jAtom).eq. MolAtom(iAtom))       
        Rij_vec_new(1:3, jAtom) = Ri_vec(1:3,jAtom) - Ri_vec(1:3,iAtom) - DeltaR
    end do    
    Rij_vec_new(:,iAtom)=0.0 ! Self-distance = 0.0
    ! Apply PBC for the new vector distances
    call PBC_vec(Rij_vec_new)            
    ! Calculate distance modulus
    do concurrent (jAtom=1:NAtom, jAtom/=iAtom)
	Rij_new(jAtom) = sqrt(Rij_vec_new(1,jAtom)**2 + Rij_vec_new(2,jAtom)**2 + Rij_vec_new(3,jAtom)**2)
    enddo
    Rij_new(iAtom) = 0.0
    !Set all new energies to zero
    ENB_new = 0.0; EelNB_new=0.0; EelEXCL_new = 0.0
    ENB_old = 0.0; EelNB_old=0.0; EelEXCL_old = 0.0
    VirNB_old = 0.0; VirNB_new = 0.0
    ! Calculate Pairwise-Bond energy terms
    call MCmodStepAtomBCheckTrial(iAtom, &
                                    Rij_new(jAtomBond(1:NBondsAtom(iAtom), iAtom)), &
                                    Rij(jAtomBond(1:NBondsAtom(iAtom),iAtom), iAtom), &
                                    EB_new, EB_old, VirB_new, VirB_old, LExit)        
    if (lExit) return     
    ! Calculate Angle-bending energy terms
    call MCmodStepAtomACheckTrial(iAtom, LExit)
    if (lExit) return
    ! Calculate NB energy terms, where MaskNB and r<rcut
    call TimerStart(T_RealSpNBMC)
    do jatom=1, NAtom
        if (MaskNB_SR(jAtom,iAtom)) then            
            if (Rij_new(jAtom)<RCUT) then
                call EnergyForce(Rij_new(jAtom),STypeAtom(jAtom),STypeAtom(iAtom), Energy_, Force_)                
                ENB_new = ENB_new + Energy_
#ifdef pressure                    
                VIRNB_new = VirNB_new + Force_*Rij_new(jAtom)                
#endif
            endif        
            if (Rij(jAtom, iAtom)<RCUT) then
                call EnergyForce(Rij(jAtom, iAtom),STypeAtom(jAtom),STypeAtom(iAtom), Energy_, Force_)                
                ENB_old = ENB_old + Energy_
#ifdef pressure                    
                VIRNB_old = VirNB_old + Force_*Rij(jAtom, iAtom)                
#endif
            endif        

        endif
    enddo       
    call TimerStop(T_RealSpNBMC)
    ! Calculate electrostatic energy terms or corrections (for bonded atoms)
    call TimerStart(T_RealSpElMC)
    do jAtom=1, NAtom
        if (MaskNB_EL(jAtom,iAtom)) then 
            if ((Rij_new(jAtom)<RECUT)) then
                Energy_ = ChargeAtom(iAtom)*ChargeAtom(jAtom)*COULF*derfc(ALPHA*Rij_new(jAtom))/Rij_new(jAtom)
                EelNB_new = EelNB_new + Energy_
            endif
            if ((Rij(jAtom, iAtom)<RECUT)) then
                Energy_ = ChargeAtom(iAtom)*ChargeAtom(jAtom)*COULF*derfc(ALPHA*Rij(jAtom, iAtom))/Rij(jAtom, iAtom)                        
                EelNB_old = EelNB_old + Energy_
            endif
        endif
        if (MaskNB_EL_excl(jAtom, iAtom)) then                                    
            Energy_ = ChargeAtom(iAtom)*ChargeAtom(jAtom)*COULF*((derfc(ALPHA*Rij_new(jAtom))-1.)/Rij_new(jAtom))
            EelEXCL_new = EelEXCL_new + Energy_                                        
            EelEXCL_old = EelEXCL_old + &
                        ChargeAtom(iAtom)*ChargeAtom(jAtom)*COULF*((derfc(ALPHA*Rij(jAtom, iAtom))-1.)/Rij(jAtom, iAtom))
        endif
    enddo
    call TimerStop(T_RealSpElMC)
end subroutine
    
subroutine MCmodStepAtomBCheckTrial(iAtom, R_new, R_old, EB_new, EB_old, VirB_new, VirB_old, LExit)        
    !Global
    implicit none
    integer(4), intent(in)  :: iAtom   
    real(8), intent(in)     :: R_new(:), R_old(:)
    logical, intent(out)    :: LExit        
    real(8), intent(out)     :: EB_new, EB_old, VirB_new, VirB_old
    !Local
    integer(4) j, N
    real(8) Force_, Energy_

    lExit=.false.
    EB_new =0.0; EB_old=0.0;
    VirB_new = 0.0; VirB_old = 0.0
    N=NBondsAtom(iAtom)
    if (N.gt.0) then         
        if (ANY(R_new .gt. RMaxPotB(BondNum(1:N,iAtom)) .or. R_new .lt. RMinPotB(BondNum(1:N,iAtom)))) then
            LExit=.true.; return
        endif
        ! Calculate pairwise Bond energy terms
        do j=1,N 
            call EneriForci(R_new(j), BondNum(j,iAtom), RMinPotB(BondNum(j,iAtom)),&
                RMaxPotB(BondNum(j,iAtom)), PotB_riBond, NPointsPotB(BondNum(j,iAtom)), Energy_, Force_)            
            EB_new = EB_new + Energy_            
#ifdef pressure                                
            VIRB_new = VIRB_new + Force_*R_new(j)
#endif          
            call EneriForci(R_old(j), BondNum(j,iAtom), RMinPotB(BondNum(j,iAtom)),&
                RMaxPotB(BondNum(j,iAtom)), PotB_riBond, NPointsPotB(BondNum(j,iAtom)), Energy_, Force_)            
            EB_old = EB_old + Energy_            
#ifdef pressure                                
            VIRB_old = VIRB_old + Force_*R_old(j)
#endif                      
        enddo
    endif
end subroutine
    
subroutine MCmodStepAtomACheckTrial(iAtom, LExit)
    ! Implicitly updates VIRij, Phi_new
    !Global
    implicit none
    integer(4), intent(in):: iAtom
    logical, intent(out):: LExit        
    !Local
    integer(4) i,j, N
    type (angle) iAngle
    real(8) Energ, Force, cosphi

    LExit=.false.
    N=NAngAtom(iAtom) ! Number of A-bonds the atom involved in as a side atom
    Eang_new=0.0    
    ! Calculate new angles related to the shifted atom iAtom
    do i=1, NAngAtom(iAtom)
        iAngle=Angles(AngAtom(i,iAtom))
        if (iAtom.eq.iAngle%Atom1) then !First atom in the triplet
            cosphi=dot_product(Rij_vec_new(1:3,iAngle%Atom2), Ri_vec(1:3, iAngle%Atom2)-Ri_vec(1:3, iAngle%Atom3))/&
                                    (Rij_new(iAngle%Atom2)*Rij(iAngle%Atom2,iAngle%Atom3))
        elseif (iAtom.eq.iAngle%Atom2) then ! Second atom in the triplet
            cosphi=dot_product(Rij_vec_new(1:3,iAngle%Atom1),Rij_vec_new(1:3,iAngle%Atom3))/&
                                    (Rij_new(iAngle%Atom1)*Rij_new(iAngle%Atom3))
        else! third atom in the triplet 
            cosphi=dot_product(Rij_vec_new(1:3,iAngle%Atom2), Ri_vec(1:3, iAngle%Atom2)-Ri_vec(1:3, iAngle%Atom1))/&
                                    (Rij_new(iAngle%Atom2)*Rij(iAngle%Atom2,iAngle%Atom1))
        endif 
        if (abs(cosphi).gt.1.d0) cosphi=sign(1.d0,cosphi)                        
        Phi_new(i)=dacos(cosphi)*RAD2DEG

    enddo
    ! Check if the new values are within the allowed range of angles
    if (ANY(Phi_new(1:N).gt.RMaxPotB(Angles(AngAtom(1:N,iAtom))%Bond))) then
            LExit=.true.; return
    endif
    if (ANY(Phi_new(1:N).lt.RMinPotB(Angles(AngAtom(1:N,iAtom))%Bond))) then
        LExit=.true.; return
    endif
    ! Calculate new angle-bending energy terms
    do j=1,N 
        iAngle=Angles(AngAtom(j,iAtom))
        call EneriForci(Phi_new(j),iAngle%Bond, RMinPotB(iAngle%Bond),&
            RMaxPotB(iAngle%Bond),PotB_riBond,NPointsPotB(iAngle%Bond), Energ, Force)
        Eang_new(j)=Energ
    enddo    
end subroutine

