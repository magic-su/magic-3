module iomod   
    use precision
    use strings
    use utilsmod
    use dynarray
    use InOutUnit
 
    implicit none
    
    integer(4), protected::     NA, &               ! Number of points in one intermolecular RDF/potential (resolution)
                                NAtom,&               ! Total number of atoms in system
                                NSType,&               ! Total number of atom types in the system
                                NpointsGlobTotal,&    ! Dimention of Sj, SiSj arrays (total number of points in all RDFs)
                                NMCAutoAdjust, &    ! How often to perform MC step autoadjustment during equilibration phase
                                IPotCorrCheck,&       ! How often to perform Potential Corection Check.
                                InvMode,&           ! Inverse mode selector: -1 -not defiend, 0-IBI, 1-IMC, 2-NewtonGauss, 3 - EVR
                                ITRANS,&            ! How often to make MCtranslation step
                                IROT,&              ! How often to make MCrotation step
                                NBond,&              ! Number of intramolecular RDFs
                                NSite,&              ! Number of atomsites (total number of atoms in list of molecule types) in the system                
                        	NBondB=0,NBondA=0,&       ! Number of intramolecular pair potentials and covalent angle bending potentials
                                NactNB,&                ! Number of Intermolecular potentials
                                NFStart,&             ! Number of first configurations to be randomly picked from starting configuration file
                                NMol,&               ! Total number of molecules in system
                                IREPT,&            ! Total number of IMC/IBI iterations to perform
                                iWriteTraj,&               ! How often to wtire trajectory to a file     	
                                iCalcEnergy,&              !How often to recalculate total energy/write energies in output
                                iAverage,&               !How often to compute averages
                                RandomSeed,&               ! Random number generator starting seed
                                NMCStepMolMolecules ! Total number of molecules subject to rotation/translation

                            
    integer(4), allocatable, protected:: MTypeSite(:),& ! ! MTypeSite(iS) number of molecular type that site with index iS belongs to
                                         StateNB(:,:), StateB(:), & ! Bit-mask for every NB, B interaction
                                         NMolMType(:),&          ! Number of molecules of each type        
                                         NSiteMType(:),&          ! Number of atoms in molecule of each type
                                         AdrAtomMType(:),&         ! Address of first molecule of i-th type in atom list (number first atom of first molecule of type i-th) //index in the global atomlist of (the first atom of the molecular type iMolType) -1 (preceding atom)
                                         AdrMolMType(:),&           ! Address of i-th molecular type in row of all molecules (first molecule of i-th type in molecule list) //index in the global moleculelist of the last molecule with type (iMol-1) (preceding moleculetype)
                                         AdrBondMType(:),&           ! number of bond after which bonds belonging to molecular type NMType are located
                                         AdrAtomMol(:),&          ! Address of i-th molecule in general atom list (given by number of first atom of molecule - 1)
                                         MTypeMol(:),&            ! Type of i-th molecule in general molecule list
                                         MTypeSType(:),&          ! Molecular type of molecule that atomic type iNSN belongs to
                                         STypeAtom(:),&      ! global atomic type of atom with global index i
                                        SiteAtom(:),&       ! global site's number which refers to atom with global index iat
                                         MolAtom(:),&           ! global number of the molecule, that atom with index i belongs to
                                        BTypeBond(:),&      ! Bond type (0-pairwise, 1-angle bending)
                                        MTypeBond(:),&      ! Molecule type the bond belongs to
                                        BondSameBond(:), &     ! number of the bond which is equivalent to this one (but belongs to another molecular type)
                                        IndexGlobNB_riSjS(:,:,:),&       ! Point in a global RDF/potential array which refers to NA-th point of potenial/RDF between iSG-th and jSG-th global atom types        
                                        IND(:,:,:),&        ! Array of marks for each point of IndexGlobNB_riSjS: -3, potential is known, but protected for corrections, -2 no data about this RDF/potential (whole potential is zero), -1 some points are missing, 0 RDF has zero value in this point and potential has high (prohibiting) value; 1 - RDF is nonzero and poential has normal value.
                                        IndexGlobB_riBond(:,:),&        ! IPOTI(iNST,IPP) Point in a global RDF/potential array which refers to iNst-th point of IPP-th potenial/RDF
                                        INDI(:,:),&         ! Array of marks for each point of IndexGlobNB_riSjS                                        
                                        MCStepMolMolecules(:) ! Array of molecules which are accessible for Translation/Rotation (boosted molecules will be listed more than once))



    character(atom_name_len), allocatable:: &
                                NameSite(:),&   ! Names of atoms in all atoms list
                                NameSType(:)    ! Names of atomic types, in same order as they appear in RDF-file
     
    character(filename_text), protected:: CrossCorrFile,&
                                          FileExternalTraj, & ! External trajectory file
                                          FileExclusionSR, FileExclusionEL,& ! Exclusion list files
                                          FILPOT,&         ! file with potentials
                                          FILRDF,&         ! file with RDFs/ADFs
                                          FSTART,&          ! name of file with initial coordinates (velocities)
                                          BaseOutFileName,& ! Filename template to use for writing output files                                          
                                          FCRD           !File with starting coordinates of frozen molecules.
    character(5), private :: ExternalTrajFormat

                                          
    integer(4), protected:: Mode
    integer(4), parameter, public:: INVERT=1, SAMPLE=-1, NATIVE=0, IBI=0, IMC=1, NG=2, VIMC=2, EVR=3
    integer(8), protected :: NMCSteps,&              ! Total number of MK/MD steps do be performed in each MC/MD process.
                             NMCStepsEquil ! Number of equilibration MC steps

    logical, protected :: LExternalTraj, & ! Read frames from external trajectory
                          LExclusionSR,  LExclusionEL, & ! Read exclusion lists
                          LReadPot,& ! Should we read potential from file? If no LReadRDF must be "true"
                            LReadRDF,& ! Perform inverse simulation (and read target RDF from file)   	
                            LXMOL,& ! If to dump last configuration to the file
                            LPotCorrCheck,& ! If the Potential Correction check is enabled ?
                            LCRD,&  ! Start from coordinates in FSTART file, otherwise start from random configuration
                            LCRDPass!Pass last MC configuration as starting on following IMC iteration.

    logical, allocatable, protected ::  LMoveMType(:)      ! Should we move this type of molecules                          
    logical, allocatable, protected :: MaskExclSR(:,:), MaskExclEL(:,:)                                
    real(8), protected :: BOX(3), &!          ! Periodic cell size         
                          COULF,&             ! Coefficient to convert electrostatic energy to internal units. 1.d10*ELCH**2/(4.d0*PI*EPS0*EPS*BOLTZ*TEMP)
                          EPS,&               ! Dielectric permittivity                          
                          factor_NG,&         ! SashaL's weight factor for optimization
                          FKJM, &             ! Energy conversion factor. Internal units -> KJ/mol
                          FQ,AF,&             ! Electrostatic interaction parameters        
                          FRT,&               ! Force conversion factor. Internal units -> KJ/mol/A
                          FPR,&               ! Pressure conversion factor. Internal units -> KJ/mol/A^3
                          POTCUT,&            ! Higher border for potential value.
                          PotRcut,&           ! Cutoff for correction of potential in NG optimization
                          RCUT,&              ! Real-space cutoff radius for short-range LJ interactions
                          RECUT,&             ! Real space cutoff radius for direct electrostatics part
                          RMIN,RMAX,&         ! Min and Max distances in intermolecular RDFs. RMAX defines Short-Range cutoff radius.
                          TEMP,&              ! Desired temperature of the system
                          VOL,&               ! Periodic cell volume
                          REGP,&              ! Regularization parameter
                          XREG,&              ! EVR  power parameter  (XREG=1 makes IMC)
                          YREG,&              ! EVR  e.v. parameter  (YREG=1 makes IMC)
                          DPOTM,&             ! Max change of the potential
                          RTM!,&               ! Max relative difference in potential correction


    real(8), protected, allocatable:: 	ChargeAtom(:),& ! Charge of iat-th atom in the global atomlist                          
                                	RMinPotB(:),& ! Minimal radius for i-th intramolecular potentinal
                                        RMaxPotB(:),&  ! Maximal radius for i-th intramolecular potentinal
                                        MCStepAtomARMType(:),MCTransStepARMType(:), MCRotStepARMType(:), & ! Desired Acceptance ratios for MC step autoajdustment
                                        SumMassMType(:),& !Mass of the molecule of type ITYP
                                        RSite(:,:),& ! Coordinates of all sites of each molecular type, readed from MCM-file
                                        ChargeSite(:),& ! Charges of each site of system
                                        MassSite(:)!,& ! Mass of each atom of each molecular type

                                        
    real(8), public, allocatable::      PotB_riBond(:,:),&         ! POTI(iNST,IPP) - iNST-th point of IPP-th intramolecular potential/bond
                                        PotNB_riSjS(:,:,:),&        ! POT(iNA,isg,jsg)	value of intermolecular potential between isg-jsg atomic types on distance iNA
                                        MCStepAtomMType(:),&! Max displacement in MC step for atomes of each MolType(single atom shift)
                                        MCTransStepMType(:),MCRotStepMType(:)!,& ! Size of MC translation/rotation of the whole molecule in Angstroms/degrees
                                        

        
    integer(4), protected:: NMType, &              ! Number of molecular types
                            NAtomChrgAtom             ! Number of charged atoms in the system
    integer(4), protected, allocatable:: &
                            AtomChrgAtom(:),&   ! AtomChrgAtom(NAtomChrgAtom) Global index of NAtomChrgAtom-atom  in list of charged atoms                            
                            NPointsPotB(:),&            ! Number of points in i-th intramolecular potential
                            NSiteGroupsInBond(:),&           ! NPTT(NBond) Number of site pairs/triplets which refers to NBond-th RDF/potential  ADF/angle-potentials
                            iSiteinBond(:,:),jSiteinBond(:,:),kSiteinBond(:,:),& ! Sites (i,NBond) which refer to i-th pair(triplet) of NBond intramolec bond
                            NBondMType(:),&            ! number of bonds in molecule of type iTYPE
                            Bond_iSjS(:,:),&        ! rdf/potential which iX-th and iY-th pair of sites refers to
                            AdrSiteMType(:)!,&          ! Address of molecule type in site list // index in the global sitelist after which sites of the molecular type NMTypee will stand

    character(mol_name_len), protected, allocatable:: NameMType(:)      ! Names of molecular types 
    character(bond_name_len), protected, allocatable:: NameBond(:)       ! Names of each bond (bond-type)    
    integer(4), private, allocatable:: NAngleBondsExcludeMType(:), NPairBondsExcludeMType(:) ! aggregated and checked values
    integer(4), private, allocatable:: RDFNAngleBondsExcludeMType(:), RDFNPairBondsExcludeMType(:), & ! read from RDF file
                                      PotNAngleBondsExcludeMType(:), PotNPairBondsExcludeMType(:),& ! read from potential-file
                                      ExclSRNAngleBondsExcludeMType(:), ExclSRNPairBondsExcludeMType(:),& ! read from exclusions_SR-file
                                      ExclELNAngleBondsExcludeMType(:), ExclELNPairBondsExcludeMType(:),& ! read from exclusions_EL-file
                                      MCStepMolBoostChance(:) ! Array of integer multipliers boosting probability of molecular types to be selected for molecular-MC-steps
    integer(4), private :: ID, rank, psize ! internal values of ID, rank, psize to be used in iomod.                                     
   
    interface ReadParameter  ! Generic operator for reading a parameter from input file
        module procedure ReadParameterInt
        module procedure ReadParameterInt8
        module procedure ReadParameterReal
        module procedure ReadParameterString
        module procedure ReadParameterLogical
    end interface    
    
    contains
    include "iomod_preparereadinput.f90"
    include "iomod_readall.f90"
#include "iomod_readinput.f90"
    include "iomod_readcmdline.f90"
    include "iomod_readexclusionsheader.f03"
    include "iomod_readexclusions.f03"    
    include "iomod_sameasbond.f90"            
#include "iomod_setparameters.f90"
    include "iomod_writetraj.f03"    
    include "iomod_writeexclusionsheader.f03"    
    include "iomod_readexternaltraj.f03"
        
    include "initiate.f90"    
    
    include "mcmio_readmcm.f90"
    include "mcmio_readbond.f90"
    include "mcmio_readsites.f90"
    include "mcmio_setcomzero.f90"    
    include "mcmio_setreferences.f90"
    include "mcmio_stypedetect.f90"
    
    include "rdfpotio_getendofsection.f03"
    include "rdfpotio_getfilelength.f03"
    include "rdfpotio_readgeneralsection.f03"
    include "rdfpotio_readincludedsection.f03"
    include "rdfpotio_readpropba.f03"
    include "rdfpotio_readpropcommon.f03"
    include "rdfpotio_readpropnb.f03"
#include "rdfpotio_readrdfpot.f03"
    include "rdfpotio_readtablecommon.f03"
    include "rdfpotio_writepot.f03"
    
end
