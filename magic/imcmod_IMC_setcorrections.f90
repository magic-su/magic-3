subroutine IMC_SetCorrections(CORS)
    
! Global
integer(8), intent(in):: CORS(:)

! Local     
integer(4) IC, I, Npoints, iBond, iSType, jSType, NR    
real(8) CORMin, CORMax

! Imputing correction to the excluded points of bonded potentials (near minimum of intramolecular potential)
do iBond=1,NBond
    if (BTEST(StateB(iBond),8)) then ! If potential is subject for correction
        if (NMIN(iBond).eq.1) then ! Left boundary case
            COR(IndexGlobB_riBond(NMIN(iBond),iBond))=COR(IndexGlobB_riBond(NMIN(iBond)+1,iBond))
        elseif (NMIN(iBond).eq.NPointsPotB(iBond)) then ! Right boundary case
            COR(IndexGlobB_riBond(NMIN(iBond),iBond))=COR(IndexGlobB_riBond(NMIN(iBond)-1,iBond))
        else
            COR(IndexGlobB_riBond(NMIN(iBond),iBond))=0.5*(COR(IndexGlobB_riBond(NMIN(iBond)-1,iBond))&
                                                  +COR(IndexGlobB_riBond(NMIN(iBond)+1,iBond)))
        endif
    endif
enddo

! Shifting correction to a zero level
do iBond=1,NBond ! Over all bonds
    NPoints=NPointsPotB(iBond)
    if (BTEST(StateB(iBond),8)) then ! If potential is subject for correction
        CORMin=minval(COR(IndexGlobB_riBond(1:NPoints,iBond)))
        CORMax=maxval(COR(IndexGlobB_riBond(1:NPoints,iBond)))
        if (CORMin*CORMax.gt.0) then ! Both min and max has same sign
            if (CORMax.lt.0) then ! Potential correction values are below zero
               COR(IndexGlobB_riBond(1:NPoints,iBond))=COR(IndexGlobB_riBond(1:NPoints,iBond))-CORMax
            elseif (CORMin.gt.0) then ! Potential correction values are below zero
               COR(IndexGlobB_riBond(1:NPoints,iBond))=COR(IndexGlobB_riBond(1:NPoints,iBond))-CORMin
            endif
        endif
    endif
enddo

! If potential is not prohibiting, but no pairs on this distance were found we lower the potential value in this point                
do iSType=1, NSType
    do jSType=iSType, NSType                                 
        if (btest(StateNB(jSType,iSType),8)) then 
            do NR=1, NA
              i=IndexGlobNB_riSjS(NR, jSType, iSType)
              if((IND(NR, jSType, iSType).eq.1).and.(CORS(I).eq.0)) COR(I)=-DPOTM          
            enddo
        endif
    enddo
enddo
do iBond=1, NBond
    if (BTEST(StateB(iBond),8)) then ! If potential is subject for correction
        do NR=1, NPointsPotB(iBond)
            I=IndexGlobB_riBond(NR, iBond)
            if((INDI(NR,iBond).eq.1).and.(CORS(I).eq.0)) COR(I)=-DPOTM
        enddo
    endif
enddo

end subroutine IMC_setCorrections

