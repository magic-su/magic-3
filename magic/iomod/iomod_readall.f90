subroutine ReadAll(RDFref_NB_riSjS, RDFref_B_riBond,&
                   AdrGlobIndexAtomij, &
                   rank_, ID_, psize_)   


    integer(4), intent(in):: rank_, ID_, psize_

    real(8), allocatable:: SHELV(:), RAS(:)        
    integer, allocatable:: STypeSite(:)
    character(atom_name_len), allocatable:: NameSTypeSite(:)
    
!---------------------------------------------------------------------------------            
    character(filename_text) iFilename       
!---SetReferences--------------------------------------------------------------
    integer  NPointsNBGpot
    integer, allocatable :: indexGPotNB_riSjS(:,:,:), NPointsRDFB(:), &
    iRGPot(:), iSTypeNBGpot(:), jSTypeNBGpot(:)
        
!---ReadRDF---------------------------------------------------------
    real(8), allocatable::RDFref_NB_riSjS(:,:,:), RDFref_B_riBond(:,:), rRDFB_riBond(:,:)
    real(8), allocatable:: rPotB_riBond(:,:)
    real(8), allocatable::RMaxRDFB(:),RMinRDFB(:)
    integer NPointsRDFNB, NPointsPotNB
    real(8) RMaxRDFNB, RMinRDFNB, RMaxPotNB, RMinPotNB
    character(mol_name_len), allocatable:: RDFSameAsBondNameMType(:), PotSameAsBondNameMType(:)
    integer(4), allocatable :: RDFSameAsBondNumber(:), PotSameAsBondNumber(:)
!----Initiate-----        
    integer(4), allocatable:: AdrGlobIndexAtomij(:,:)

!----ReadInput----------------          
    character(filename_text) oFilename    
    integer i,j,k

 ! ------PrepareReadInput()
    character(input_text), allocatable :: text(:)
    ID = ID_;  rank = rank_; psize = psize_ ! pass values to the module's variables
    
    call PrepareReadInput(iFilename, text)
    call ReadInput(iFilename)
    write(stdout,*) ''            
    write(stdout,*) '-----------------------------------------------------------------------'    
    write(stdout,*) ''
    write(stdout,*) 'Reading mcm-files'
    write(stdout,*) ''
    call ReadMCM(STypeSite)
    write(stdout,*) ''
    write(stdout,*) '-----------------------------------------------------------------------'    
    write(stdout,*) ''
    write(stdout,*) 'Setting reference data structures:'    
    call SetReferences(STypeSite, &        
        RDFref_NB_riSjS,& 
        NPointsRDFB, RMaxRDFB, RMinRDFB)
    write(stdout,*) 'Reference data structures set successfully'    
    write(stdout,*) ''    
    write(stdout,*) '-----------------------------------------------------------------------'        
    
    if (LExclusionSR.or.LExclusionEL) then 
        write(stdout,*) "Reading Exclusions"    
        if (LExclusionSR) call readexclusions(FileExclusionSR, MaskExclSR, &
                                              ExclSRNAngleBondsExcludeMType, ExclSRNPairBondsExcludeMType) 
        if (LExclusionEL) call readexclusions(FileExclusionEL, MaskExclEL, &
                                              ExclELNAngleBondsExcludeMType, ExclELNPairBondsExcludeMType)         
        write(stdout,*) '-----------------------------------------------------------------------'        
    else
        write(stdout,*) "Using default exclusions: 1-Bond and 1-Angle"    
        call reshape(ExclSRNAngleBondsExcludeMType, NMType); ExclSRNAngleBondsExcludeMType = 1
        call reshape(ExclSRNPairBondsExcludeMType, NMType); ExclSRNPairBondsExcludeMType = 1
        call reshape(ExclELNAngleBondsExcludeMType, NMType); ExclELNAngleBondsExcludeMType = 1 
        call reshape(ExclELNPairBondsExcludeMType, NMType); ExclELNPairBondsExcludeMType = 1
    endif
    if (LReadRDF) then                 
        write(stdout,*) ''    
        write(stdout,'(a)',advance='no') 'Reading RDFs: '    
        call ReadRDFPot(FilRDF, &
           RDFref_B_riBond, rRDFB_riBond, RDFref_NB_riSjS,  &             
            NPointsRDFNB, RMaxRDFNB, RMinRDFNB,&
            NPointsRDFB, RMaxRDFB, RMinRDFB, &
            RDFSameAsBondNameMType, RDFSameAsBondNumber,&
            RDFNAngleBondsExcludeMType, RDFNPairBondsExcludeMType,&
            'RDF')
        write(stdout,*) 'RDFs are read successfully'        
        write(stdout,*) ''    
        write(stdout,*) '-----------------------------------------------------------------------'        
    endif       

    if (LReadPOT) then       
        write(stdout,*) ''    
        write(stdout,*) 'Reading potentials'    
        call ReadRDFPot(FilPot, &
        PotB_riBond, rPotB_riBond, PotNB_riSjS, &
        NPointsPotNB, RMaxPotNB, RMinPotNB,&
        NPointsPotB, RMaxPotB, RMinPotB, &
        PotSameAsBondNameMType, PotSameAsBondNumber,&
        PotNAngleBondsExcludeMType, PotNPairBondsExcludeMType,&
        'potential')
        write(stdout,*) 'Potentials are read successfully'        
        write(stdout,*) ''    
        write(stdout,*) '-----------------------------------------------------------------------'        
    endif
    
    ! Check and compare SameAsBond records and initialize BondSameAsBond-array
    if (LReadRDF.and.LReadPot) then
        if (NPointsPotNB.ne.NPointsRDFNB) then
            write(stdout,*) 'ERROR: Max number of points in NB RDF and potential differs:'
            write(stdout,*) 'NPointsPotNB=',NPointsPotNB, ' and  NPointsRDFNB=',NPointsRDFNB
            stop 624
        else
            NA=NPointsPotNB
        endif
    elseif (LReadRDF) then
        NA=NPointsRDFNB
    elseif (LReadPot) then
        NA=NPointsPotNB
    else
        write(stdout,*) "Error: Neither RDF nor potential file were stated to read"
        stop 647
    endif
    write(stdout,*) ''
    call SetParameters(SHELV, RAS)
    write(stdout,*) ''
    write(stdout,*) '-----------------------------------------------------------------------'
    write(stdout,*) ''
    write(stdout,*) "Summarizing all RDF/potentials data which was read"

    call Initiate(RDFref_NB_riSjS,RDFref_B_riBond, rPotB_riBond,&            
        NPointsRDFNB,NPointsPotNB, RMaxRDFNB, RMaxPotNB, RMinRDFNB, RMinPotNB,&
        NPointsRDFB, RMaxRDFB, RMinRDFB, &        
        RDFSameAsBondNameMType, PotSameAsBondNameMType, RDFSameAsBondNumber, PotSameAsBondNumber, &
        AdrGlobIndexAtomij)
    write(stdout,*) ''
    write(stdout,*) '-----------------------------------------------------------------------'
    write(stdout,*) ''
    if (ID.le.1) call WriteExclusionsHeader(6)
    call SetInOut(ID, iPrint, pSize) ! Recover output units for all the processes
end subroutine ReadAll