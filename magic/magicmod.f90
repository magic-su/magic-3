! CollectMCsummary - subroutine collects statistic data from all processes with MPI, and the calculates final averaged values.
! PrintMCsummary - subroutine prints final output in Molsim (priorly collected )

module magicmod
    use InOutUnit
    use timer    
    implicit none
!    private
     !-------Global Variables--------------------
    integer(4), protected :: global_NAV  ! general number of averaged configurations
    integer(8), protected :: global_NMCSteps ! general number of MK-steps performed in one IMC interation (total number over all processes)
    ! Arrays for MPI-based gathering and averaging of correlators
    integer(8), allocatable, protected :: &
        global_CORS(:),&
        global_CORP(:,:),&
        global_npairsrdf(:,:) ,&
        global_AccCntMType(:,:,:) !Total Acceptance counter over all processes for MC steps: 1-single atom, 2-molecule translation, 3 - molecule rotation
                        
    real(8), public::   &
        global_EELS, &
        global_EINTS, &
        global_ENERS, & ! MPI accumulators                        
        global_VirNB_accum, global_VirEL_accum, global_VirB_accum
                        
    integer(4), public :: IREP=1              ! Number of current IMC/IBI iteration
                        
    integer(4), protected:: ID=0,& ! Id of the process: 0-serial run, 1-parallel run master process, >1 parallel run slave-process.
                            psize=0,& ! Number of processes in parallel run
                            rank=0 ! Number of current MPI process                            
                    
    contains
#include "magicmod_collectcrosscorr.f03"
#include "magicmod_collectMCsummary.f03"
#include "magicmod_initmagic.f03"        
#include "magicmod_MCstepautoadjust.f03"
#include "magicmod_printMCsummary.f03"
#include "magicmod_terminatemagic.f03"
    include "magicmod_readwritecrosscorr.f03"

end
