#!/bin/tcsh -f
set sourced=($_)
# Define location of MAGIC.
if ("$sourced" != "") then
    set script_fn=`readlink -f $sourced[2]`
    echo "$script_fn"
    set dir = `dirname $sourced[2]`
endif
set MAGICHOME=`cd $dir && pwd`
echo "Location of MAGIC's home folder: " $MAGICHOME

if ( ! -d $MAGICHOME/bin ) then 
   mkdir $MAGICHOME/bin
endif

set path = ( $path $MAGICHOME/bin $MAGICHOME/magic)

if ($?LD_LIBRARY_PATH )then
setenv LD_LIBRARY_PATH "$MAGICHOME/lib/xdrfile-1.1.4/lib":"$LD_LIBRARY_PATH"
else
setenv LD_LIBRARY_PATH "$MAGICHOME/lib/xdrfile-1.1.4/lib"
endif

if($?PYTHONPATH)then
setenv PYTHONPATH "$MAGICHOME/MagicTools":"$PYTHONPATH"
else
setenv PYTHONPATH "$MAGICHOME/MagicTools"
endif
setenv PYTHONPATH "$MAGICHOME/lib/xdrfile-1.1.4/src/python":"$PYTHONPATH"
setenv MAGICHOME "$MAGICHOME"
