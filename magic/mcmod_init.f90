subroutine MCmodInit()

!    use iomod, only: NAtom, MaskExclSR, MaskExclEL, LExclusionEL, LExclusionSR
    implicit none

    integer(4) iAtom, jAtom , jBond, MAxBonds, i, iNBond, iMol, iMType, iBond, igrp, mol,NAtomsInMol
    type(angle) iAngle

    ! Set default values, allocate arrays
    RDFINC=RMAX/NA
    ! And now we can allocate memory for structural average arrays        
    call reshape(CORS, max(NPointsGlobTotal,1))
    call reshape2(npairsrdf,NSType,NSType)
    if (InvMode.ge.1) then
        call reshape2(CORP, max(NPointsGlobTotal,1), max(NPointsGlobTotal,1))
    endif

    call reshape2(XvecCOMMol,3,NMol)
    call reshape2(Svec,3,NAtom)
    call reshape2(Wvec,3,NAtom)
    
    call reshape2(Rij,NAtom, NAtom);
    call reshape(Rij_new,NAtom); Rij_new=0.0
    call reshape2(Ri_vec,3,NAtom)
    call reshape2(Rij_vec_new,3,NAtom); Rij_vec_new=0.d0    
#ifdef debug
    call reshape3(Ri_vec_buffer, 3, NAtom, iCalcEnergy)    
#endif
    NAtomsInMol=MaxVal(NSiteMType(1:NMType))
    call reshape2(RijM_new, NAtom, NAtomsInMol)
    call reshape3(AccCntMType,NMType,2,3)
    AccCntMType(1:NMType,2,1:3)=1 ! initialize with 1-s to prevent division by zero error
!  Pairwise Bond part ------------------
    MaxBonds=maxval(count(Bond_isjs(:,:).ne.0,1)) ! Returns maximum number of bonds a single atom in involved in
    if (iPrint.ge.8) write(stdout,*) 'MCmodInit MaxBonds:', MaxBonds
    if (MaxBonds.eq.0)  MaxBonds=1 ! 1 if no bond are present. Allocate dummy arrays
    call reshape(NBondsAtom, NAtom)
    call reshape2(BondNum, MaxBonds, NAtom)
    call reshape2(jAtomBond, MaxBonds, NAtom)
    do iAtom=1,NAtom
        NBondsAtom(iAtom)=count(BTypeBond(pack(Bond_iSjS(:,SiteAtom(iAtom)),Bond_iSjS(:,SiteAtom(iAtom)).ne.0)).eq.1)
        BondNum(1:NBondsAtom(iAtom),iAtom)=pack(pack(Bond_iSjS(:,SiteAtom(iAtom)),Bond_iSjS(:,SiteAtom(iAtom)).ne.0),&
                                BTypeBond(pack(Bond_iSjS(:,SiteAtom(iAtom)),Bond_iSjS(:,SiteAtom(iAtom)).ne.0)).eq.1)
    enddo
    do iAtom=1,NAtom
        jBond=0
        do jAtom=1,NAtom
            if ((MolAtom(iAtom).eq.MolAtom(jAtom)).and.(Bond_iSjS(SiteAtom(jAtom),SiteAtom(iAtom)).ne.0)) then
                if (BTypeBond(Bond_iSjS(SiteAtom(jAtom),SiteAtom(iAtom))).eq.1) then
                    jBond=jBond+1
                    jAtomBond(jBond,iAtom)=jAtom
                endif
            endif
        enddo
    enddo

! Angle-bond part-----------------------------------------------------
    ! Count total number of angle-bonds present in the system: i
    NAngles=0
    do iBond=1, NBond
        if (BTypeBond(iBond).eq.2) NAngles=NAngles+NSiteGroupsInBond(iBond)*NMolMType(MTypeBond(iBond))
    enddo
    if (iPrint.ge.8) write(stdout,*) 'MC init: Total number of angle-bonds:',NAngles

    allocate(angles(max(NAngles,1))) ! Array of a-bonds
    angles(:)%Eang = 0.d0
    ! Assign atoms and bonds to the allocated angles
    i=0
    Mol=0
    do iMType=1,NMType
        if (iPrint.ge.8) write(stdout,*) 'iMType',iMType
        do iNBond=1, NBondMType(iMType)
            iBond=AdrBondMType(iMType)+iNBond
            if (iPrint.ge.8) write(stdout,*) 'iNBond',iNBond, iBond
            if (BTypeBond(iBond).eq.2) then
                do iGrp=1, NSiteGroupsInBond(iBond)
                    if (iPrint.ge.8) write(stdout,*) 'igrp',igrp
                    do iMol=1, NMolMType(iMType)
                        if (iPrint.ge.8) write(stdout,*) 'iMol',iMol+Mol
                        if (iPrint.ge.8) write(stdout,*) 'AdrAtomMol',AdrAtomMol(iMol+Mol)
                        i=i+1
                        angles(i)%Atom1=AdrAtomMol(iMol+Mol)+iSiteInBond(iBond,iGrp)-AdrSiteMType(iMType)
                        angles(i)%Atom2=AdrAtomMol(iMol+Mol)+kSiteInBond(iBond,iGrp)-AdrSiteMType(iMType)
                        angles(i)%Atom3=AdrAtomMol(iMol+Mol)+jSiteInBond(iBond,iGrp)-AdrSiteMType(iMType)
                        angles(i)%Bond=iBond
                        angles(i)%LMove=BTest(StateB(iBond),5)
                        if (iPrint.ge.8) write(stdout,*) &
                                i, angles(i)%Atom1,angles(i)%Atom2,angles(i)%Atom3,angles(i)%Bond, angles(i)%LMove
                    enddo
                enddo
            endif
        enddo
        Mol=Mol+NMolMType(iMType)
    enddo
    ! Construct arrays of angles specified for each atom:
    call reshape(NAngAtom, NAtom)
        ! Count in how many a-bonds each atom is involved
    NAngAtom=0
    do i=1, NAngles
        NAngAtom(Angles(i)%Atom1)=NAngAtom(Angles(i)%Atom1)+1
        NAngAtom(Angles(i)%Atom2)=NAngAtom(Angles(i)%Atom2)+1
        NAngAtom(Angles(i)%Atom3)=NAngAtom(Angles(i)%Atom3)+1
    enddo
    ! Allocate real sized or 1-sized dummy arrays, depending on presence of angle-bonds
    call reshape2(AngAtom,max(Maxval(NAngAtom),1),NAtom)
    call reshape(Phi_new, max(Maxval(NAngAtom),1))
    call reshape(Eang_new,max(Maxval(NAngAtom),1))

    NAngAtom=0
        ! Populate lists of a-bond for every atom
    if (iPrint.ge.8) write(stdout,*) 'NAngles:',NAngles
    do i=1, NAngles
        NAngAtom(Angles(i)%Atom1)=NAngAtom(Angles(i)%Atom1)+1
        NAngAtom(Angles(i)%Atom2)=NAngAtom(Angles(i)%Atom2)+1
        NAngAtom(Angles(i)%Atom3)=NAngAtom(Angles(i)%Atom3)+1
        AngAtom(NAngAtom(Angles(i)%Atom1),Angles(i)%Atom1)=i
        AngAtom(NAngAtom(Angles(i)%Atom2),Angles(i)%Atom2)=i
        AngAtom(NAngAtom(Angles(i)%Atom3),Angles(i)%Atom3)=i
    enddo

    if (iPrint.ge.8) then
        do i=1,NAtom
            write(stdout,*) 'Atom:', i, 'angles:', AngAtom(1:NAngAtom(i),i)
        enddo
    endif

!   NB----------------------------------------------

    !Construct mask array - mask=true if pair is in NB, false elsewhere
    ! Creating NB-masks specific to exclusions: short range, electrostatic and bonded electrostatic.
    
    allocate(MaskNB_SR(NAtom,NAtom)); MaskNB_SR = .true.
    allocate(MaskNB_EL(NAtom,NAtom)); MaskNB_EL = .true.
    allocate(MaskNB_EL_excl(NAtom,NAtom)); MaskNB_EL_excl = .false.
    
    ! Set mask for Non-bonded short-range exclusions:
    do iAtom=1,NAtom
        do jAtom=iAtom+1,NAtom
            if (MolAtom(iAtom).eq.MolAtom(jAtom)) then 
                if (LExclusionSR) then                    
                    MaskNB_SR(jAtom,iAtom) = .not. MaskExclSR(SiteAtom(jAtom),SiteAtom(iAtom))
                else
                    MaskNB_SR(jAtom,iAtom)=(Bond_iSjS(SiteAtom(jAtom),SiteAtom(iAtom)).eq.0) ! By default if not bonded, then MaskNB=True
                endif 
            else
                MaskNB_SR(jAtom,iAtom) = .true.
            endif 
        enddo
        MaskNB_SR(iAtom,iAtom) = .false.
    enddo

    ! Set mask for Non-bonded electrostatic exclusions:
    ! Define Mask for atom-pairs to be excluded from Ewald-summation, such as bonded atoms
    do iAtom=1,NAtom
        do jAtom=iAtom+1,NAtom
            if (MolAtom(iAtom).eq.MolAtom(jAtom)) then
                if (LExclusionEL) then ! Use user-provided exclusion rules
                    MaskNB_EL(jAtom,iAtom) = .not. MaskExclEL(SiteAtom(jAtom),SiteAtom(iAtom))
                    if (MaskExclEL(SiteAtom(jAtom),SiteAtom(iAtom))) MaskNB_EL_excl(jAtom,iAtom) = .true.
                else ! Use default exclusions from electrostatics
                    MaskNB_EL_excl(jAtom, iAtom) = (Bond_iSjS(SiteAtom(jAtom),SiteAtom(iAtom)).ne.0)
                    MaskNB_EL(jAtom, iAtom) = (Bond_iSjS(SiteAtom(jAtom),SiteAtom(iAtom)).eq.0)
                endif
            endif
        enddo
        MaskNB_EL(iAtom,iAtom) = .false.
    enddo    

    ! Set MaskNB_ false to frozen pairs
    do iAtom=1,NAtom
        do jAtom=iAtom+1,NAtom
            if (.not.Btest(StateNB(STypeAtom(jAtom),STypeAtom(iAtom)),5)) then ! Frozen pair
                MaskNB_SR(jAtom,iAtom)=.false.
                MaskNB_EL(jAtom,iAtom)=.false.
                MaskNB_EL_excl(iAtom, jAtom) = .true. ! Mark pairs of frozen atoms as explicitly excluded from electrostatics
            endif
        enddo
    enddo

    ! Set Masks for Electrostatic interactions to false if atoms not charged
    do iAtom=1, NAtom
        do jAtom=iAtom+1, NAtom
            if ((ChargeAtom(iAtom).eq.0.0).or.(ChargeAtom(jAtom).eq.0.0)) then
                MaskNB_EL(jAtom, iAtom) = .false.
                MaskNB_EL_excl(jAtom, iAtom) =.false.
            endif                
        enddo
    enddo
            
    ! Make matrixes symmetric    
    do iAtom=1,NAtom
        do jAtom=iAtom+1,NAtom
                MaskNB_EL(iAtom,jAtom)=MaskNB_EL(jAtom,iAtom)
                MaskNB_SR(iAtom,jAtom)=MaskNB_SR(jAtom,iAtom)
                MaskNB_EL_excl(iAtom,jAtom)=MaskNB_EL_excl(jAtom,iAtom)
        enddo
    enddo
    
end subroutine
