subroutine MCmodStepAtom()
    use utilsmod, only: isequal
    use timer
!    use iomod, only: NAtom
    implicit none

!-------Local Variables---------------------    
    integer(4) iAtom, N, ikv, iMType
    logical lExit
    real(8) R(3), DDE, DE, DEE, DEW, DDI, DDI_ANG, deltaR(3), DES
    
    !  Choice of trial particle
    do
        iAtom=NAtom*rand()+1
        iMType=MTypeMol(MolAtom(iAtom))
        if(LMoveMType(iMType)) exit
    enddo
    AccCntMType(iMType,2, 1)=AccCntMType(iMType,2, 1)+1 ! Count trial step for the molecular type
    ! Calculate random displacement vector and new coordinates of the atom
    deltar=[MCStepAtomMType(iMType)*(rand()-0.5d0),MCStepAtomMType(iMType)*(rand()-0.5d0),MCStepAtomMType(iMType)*(rand()-0.5d0)]
    R=Svec(:,iAtom)+deltaR
    call MCPBC(R)
    call TimerStart(T_MCStepdE)
    call MCmodStepAtomCheckTrial(iAtom, deltaR, Lexit)
    call TimerStop(T_MCStepdE)
    if (.not. LEXIT) then
        ! Calculate energy component differences
        DDI_ang=sum(Eang_new)-sum(Angles(AngAtom(1:NAngAtom(iAtom),iAtom))%Eang,mask=Angles(AngAtom(1:NAngAtom(iAtom),iAtom))%LMove)
        !  Corrections due to electrostatics
        if(ChargeAtom(iAtom).ne.0.d0) then                        
            call MCmodDifEw(R,ChargeAtom(iAtom),DEW, Svec(:,iAtom))                        
        else
            DEW=0.0
        endif
        DE = (ENB_new - ENB_old) + (EelNB_new - EelNB_old) + (EB_new - EB_old) + (EelEXCL_new - EelEXCL_old) + DDI_ang + DEW
!  MC transition
        if(DE.gt.0.0)then
            if(DE.gt.18.d0)return
            if(dexp(-DE).lt.rand())return
        end if
!  New configuration
        Svec(:,iAtom)=R
        ENER = ENER + DE
        EEL = EEL+ DEW + (EelNB_new - EelNB_old) + (EelEXCL_new - EelEXCL_old) 
        EINT = EINT + (EB_new - EB_old)
        EINT_ang = EINT_ang + DDI_ang
        AccCntMType(iMType,1, 1)=AccCntMType(iMType,1,1)+1
        if(ChargeAtom(iAtom).ne.0.d0) then
            SSIN=SSIN+D_SIN
            SCOS=SCOS+D_COS
        endif
        
        call TimerStart(T_MCUpdArr)
        ! Update arrays        
        !Pairwise Non-bonded, electrostatics and Pair bonds
        Rij(:,iAtom)        = Rij_new ! interparticle distance
        Rij(iAtom,:)        = Rij_new
        Ri_vec(:,iAtom)     = Ri_vec(:,iAtom)+deltaR
#ifdef debug
        cnt_Ri_vec_buffer = cnt_Ri_vec_buffer + 1
        Ri_vec_buffer(:,:,cnt_Ri_vec_buffer) = Ri_vec(:,:)
#endif        
        ENB = ENB + (ENB_new - ENB_old)
        EelNB = EelNB + (EelNB_new - EelNB_old)
        EBond = EBond + (EB_new - EB_old)
        EelEXCL = EelEXCL + (EelEXCL_new - EelEXCL_old)
        EAngle = EAngle + DDI_ang
#ifdef pressure
        VIRNB = VirNB + VirNB_new - VirNB_old
        VIRB = VirB + VirB_new - VirB_old
#endif        
        ! Angle-bending, side
        N=NAngAtom(iAtom)
        if (N.gt.0) then
            Angles(AngAtom(1:N,iAtom))%Eang=Eang_new(1:N)
            Angles(AngAtom(1:N,iAtom))%Phi=Phi_new(1:N)
        endif        
        call TimerStop(T_MCUpdArr)
    endif
    return
end subroutine!MCStep