        if (ID.le.1) then
            open(7+ID+100,file='IMC.CORS.dat')
            do IC=1,size(CORS)
		  write (7+ID+100,*) CORS(IC)
            end do                        
            close(7+ID+100)
            
            open(7+ID+100,file='IMC.Sref.dat')
            do iSType=1, NSType        
                do jSType=iSType, NSType
                    do i=1, NA
                        write (7+ID+100,*) Sref_NB_riSjS(i,jSType,iSType)
                    enddo
                end do  
            enddo
            do iBond=1, NBond
                do i=1,NpointsPotB(iBond)
                    write (7+ID+100,*) Sref_B_riBond(i,iBond)
                enddo
            enddo
            close(7+ID+100)
            
            open(7+ID+100,file='IMC.Scalc.dat')
            do iSType=1, NSType        
                do jSType=iSType, NSType
                    do i=1, NA
                        write (7+ID+100,*) Scalc_NB_riSjS(i,jSType,iSType)
                    enddo
                enddo
            end do                        
            do iBond=1, NBond
                do i=1,NpointsPotB(iBond)
                    write (7+ID+100,*) Scalc_B_riBond(i,iBond)
                enddo
            enddo
            close(7+ID+100)
            
            open(7+ID+100,file='IMC.COR.dat')
            do iSType=1, NSType        
                do jSType=iSType, NSType
                    do i=1, NA
                        write (7+ID+100,*) COR(IndexGlobNB_riSjS(i,jSType, iSType))
                    enddo
                enddo
            end do                        
            do iBond=1, NBond
                do i=1,NpointsPotB(iBond)
                    write (7+ID+100,*) COR(IndexGlobB_riBond(i,iBond))
                enddo
            enddo
            close(7+ID+100)
            
            open(7+ID+100,file='IMC.CORP.dat')
            do IC=1,size(CORS)            
		  write (7+ID+100,*) (CORP(jC,iC),JC=1,SIZE(CORS))
            end do
            close(7+ID+100)
            
            open(7+ID+100,file='IMC.CROSS.dat')            
            do IC=1,NUR
		  write (7+ID+100,*) (CROSS(jC,iC),JC=1,NUR)
            end do
            close(7+ID+100)
            
            open(7+ID+100,file='IMC.DIFF.dat')            
            do IC=1,NUR
		  write (7+ID+100,*) DIFF(IC)
            end do        
            close(7+ID+100)
        endif    
        