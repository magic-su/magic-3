!
! Monte-Carlo Engine
! 
MODULE mcmod
    use precision
    use strings
    use utilsmod
    use dynarray
    use InOutUnit
    use Timer
    use iomod
    implicit none

    Type Angle
        integer(4):: Atom1, Atom2, Atom3 ! 1-2-3
        integer(4):: Bond
        real(8) :: phi, Eang
        logical:: LMove
    end type
    
    integer(4), private:: NKV               ! Number of reciprocal space vectors
    real(8), allocatable, private :: &
                            D_SIN(:), D_COS(:), & ! Sin, cos for calculation of energy difference in reciprocal part of Ewald sum in single MC step
                            sigmasin(:), sigmacos(:), & !Sin, cos for calculation of energy difference in reciprocal part of Ewald sum in Molecule translation/rotation
                            Recvec(:,:),& ! Reciprocal space vectors for Ewald
                            RKV(:),&            ! Coefficient in Fourier part of Ewald method, which is a function of K**2
                            SSIN(:),&           ! SSIN(1:NKV)   array of sum(q(i)*sin(K*r(i))) over all possible vectors K(1:NKV)
                            SCOS(:),&           ! SCOS(1:NKV)	array of sum(q(i)*cos(K*r(i))) over all possible vectors K(1:NKV)                            
                            XvecCOMMol(:,:),&! Coordinates of Center of Mass (COM) of each molecule                            
                            Wvec(:,:)!,& ! Coordinates of atom relative to MTypeSite molecule's COM


    real(8), allocatable, public :: Svec(:,:) ,& ! Coordinates of all atoms of system,
                                    Ri_vec(:,:) ! Coordinates of the atoms not disturbed by PBC
    real(8), protected::  &
                        ENER=0.d0,&              ! Current (running) total energy of the system
                        EINT=0.d0,&              ! Current internal (intramolecular pairwise bonds) energy
                        EINT_ang=0.d0,&          ! Current internal angular (intramolecular bemding angle bonds) energy
                        EEL=0.d0,&               ! Electrostatic energy of the system
                        EFUR=0.d0,&              ! Fourier part of electrostatic energy in Ewald sum
                        ESF=0.d0, &              ! Fourier part of electrostatic self-interaction energy
                        pressure_instant         ! Instant pressure
    
    real(8), protected :: ENERS,&             ! Accumulator for averaging Total Energy
                          EELS,&              ! Accumulator for averaging Electrostatic Energy
                          EINTS,&              ! Accumulator for averaging Internal Energy
                          VirNB_accum, VirEL_accum, VirB_accum ! Accumulators for Virial components: Non-bonded, Electrostatic, Bonded
                                        
    integer(4), protected:: 	NAV!,&               ! Number of sampled (averaged) configurations                          
    integer(8),allocatable, protected ::   AccCntMType(:,:,:),& ! total steps/accepted steps counter for MC steps:  first index - MolType; second index = 1:accepted steps, 2:total steps; third index  1-single atom, 2-molecule translation, 3 - molecule rotation;
                                            CORS(:),&           ! <S_i> - local value for each process (in MPI case)
                                            CORP(:,:),&         ! <S_i*S_j>    local value for each process (in MPI case)
                                            npairsrdf(:,:) ! Arrays for counting number of pairs for intermolecular RDFs for normalization.                

    integer(8), public :: ISTEP=0!,&           ! Current (running) MC/MD step


    type (Angle), allocatable :: Angles(:) ! Array of angle-bonds present in the system
    integer(4):: NAngles ! How many angles are present in the system

    real(8), private, parameter :: RAD2DEG=180.d0/3.1415926535897932d0
    real(8), private:: ALPHA    ! Electrostatic interaction parameters
    real(8), allocatable :: Rij(:,:), &    ! Cross-atom distance matrix
                            Rij_new(:), & ! Trial interatom distances in MC step
                            Rij_vec_new(:,:), & ! vector of interparticle distances
                            Phi_new(:),&  ! Trial angles in MC-step 
                            RijM_new(:,:) ! Trial Interatom distances for Translation/Rotation of the molecule in MC
                            
    real(8), allocatable :: Eang_new(:) ! Trial energy terms associated with each trial angle in MC step                           
    real(8) ::              ENB_new, ENB_old, EelNB_new, EelNB_old, EB_new, EB_old, EelEXCL_new, EelEXCL_old, &
                            ENB, EelNB, EBond, EAngle, EelEXCL
    real(8)::               ENB_frozen, EB_frozen, EA_frozen, EelNB_frozen, EelBA_frozen, &
                            VirNB_new, VirB_new, VirNB_old, VirB_old, VirNB, VirB, VirNB_frozen, VirB_frozen
    integer(4), allocatable:: NAngAtom(:), AngAtom(:,:) ! Arrays specifying angle-bonds (indexes in array angles) for each atom of the system.
    integer(4), allocatable:: NBondsAtom(:), BondNum(:,:), jAtomBond(:,:) ! How many bonds the atom is involved, the bond number, second atom involved in the bond
    integer(4), allocatable:: AdrGlobIndexAtomij(:,:)
    real(8), private:: RDFINC            ! Auxilary variable - dR in RDF
    logical(kl1), allocatable:: MaskNB_SR(:,:), MaskNB_EL(:,:),&  ! Masks for the non-bonded interactions: Short-range and Electrostatics
                           MaskNB_EL_excl(:,:)              ! Mask for atoms to explicitly exclude from electrostatics interactions
#ifdef debug                           
    real(8), private, allocatable:: Ri_vec_buffer(:,:,:)
    integer(4), private:: cnt_Ri_vec_buffer
#endif

    contains
    include "mcmod_calcpres.f03"
#include "mcmod_collectcorrel.f03"
#include "mcmod_construct.f03"
    include "mcmod_constructgeometry.f03"
#include "mcmod_debug.f03"
    include "mcmod_difew.f03"
    include "mcmod_difewm.f03"
    include "mcmod_energyforce.f03"
#include "mcmod_printenergy.f03"
    include "mcmod_ewald.f90"        
#include "mcmod_frozenenergy.f03"    
    include "mcmod_generategeometry.f03"        
    include "mcmod_getCOM.f03"
#include "mcmod_init.f90"
    include "mcmod_initgeometry.f03"    
#include "mcmod_initEwald.f03"    
#include "mcmod_molrot.f03"
#include "mcmod_molrotchecktrial.f03"
#include "mcmod_moltrans.f03"    
#include "mcmod_moltranschecktrial.f03"    
    include "mcmod_readcrd.f03"
    include "mcmod_resetcounters.f03"
#include "mcmod_stepatom.f03"
#include "mcmod_stepatomchecktrial.f03" 
#include "mcmod_PBC.f03"
    
END
