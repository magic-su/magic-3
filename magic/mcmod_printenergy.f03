subroutine MCmodPrintEnergy()
    use InOutUnit
    implicit none
    real(8) ENER_old, EINT_old, EINT_ang_old, EEL_old, EFUR_old, EFCOR, DE, DEE, DES
    character(len=5) :: fmt !Auxilary variable for output format specification
    
    ENER_old=ENER; EINT_old=EINT; EINT_ang_old=EINT_ang;  EEL_old=EEL; EFUR_old=EFUR
    
    DE = ENB + ENB_frozen
    EINT = EBond + EB_frozen
    EINT_ang = EAngle + EA_frozen
    DEE = EelNB + EelNB_frozen
    DES = EelEXCL + EelBA_frozen
                
    EEL=ESF+EFUR+DEE+DES
    ENER=EEL+DE+EINT+EINT_ang
    EFCOR=ESF+EFUR+DES
    
#ifdef pressure           
        pressure_instant=(NAtom-(VirNB+VirNB_frozen+VirB+VirB_frozen-EEL)/3.)*FPR  
#endif
    ! Output thermodynamical variables                        
    write(fmt,'(i3)') NMType*3
    write(stdout,"(' Step:',i0, ', Etotal:',f11.3, ', Eelstat:',f11.3, ', Ebond:',f11.3, ', Pressure:',f12.2)") &
                           ISTEP, ENER*FKJM, EEL*FKJM, EINT*FKJM, pressure_instant            
    write(stdout,'(a,'//fmt//'a8)')    ' Molecule Type          ',(NameMType(:))            
    write(stdout,'(a,'//fmt//'f8.3)')' Acceptance ratio atoms ',AccCntMType(:,1,1)*1./AccCntMType(:,2,1)
    write(stdout,'(a,'//fmt//'f8.3)')' Acceptance ratio trans ',AccCntMType(:,1,2)*1./AccCntMType(:,2,2)
    write(stdout,'(a,'//fmt//'f8.3)')' Acceptance ratio rot   ',AccCntMType(:,1,3)*1./AccCntMType(:,2,3)   

    if(IPRINT.ge.6) write(stdout,'(4f12.3)')-VirNB*FPR/3.,EEL*FPR/3.,-VirB*FPR/3,NAtom*FPR               
    if(IPRINT.ge.6) write(stdout,'(a,4f14.2)')' Pr.comp:',-VirNB*FPR,-VirB*FPR,-EFUR*FPR/3.,NAtom*FPR                
    
      
    if((dabs(ENER_old-ENER).gt.0.01).and.(iPrint.ge.5).and.(iStep.gt.0 .and. .not.lExternalTraj)) then
        write(stdout,'(2(a,f14.5))') 'MCENERGY: Error in energy: was ',ENER_old*FKJM,' now ',ENER*FKJM
        write(stdout,'(2a)') '                  ', &
            'Etotal          Eel         Ebond           Eang           Efurier        Eelself      '
        write(stdout,'(a15,6f14.6)') 'Before Energy:',ENER_old*FKJM,EEL_old*FKJM,EINT_old*FKJM,EINT_ang_old*FKJM,&
                                        EFUR_old*FKJM,ESF*FKJM
        write(stdout,'(a15,6f14.6)') 'After Energy:',ENER*FKJM,EEL*FKJM,EINT*FKJM,EINT_ang*FKJM,EFUR*FKJM,ESF*FKJM
    endif
            
    if(IPRINT.ge.7)then
        write(stdout,*)'*** ENERGY components and (non-frozen part)***'
        write(stdout,*)' total:         ',ENER*FKJM,'(',(ENER-ENB_frozen-EB_frozen-EA_frozen-EelNB_Frozen-EelBA_frozen)*FKJM,')'
        write(stdout,*)' short range:   ',DE*FKJM, '(',(DE-ENB_frozen)*FKJM,')'   
        write(stdout,*)' electrostatic: ',EEL*FKJM,'(',(EEL-EelNB_Frozen-EelBA_frozen)*FKJM,')'
        write(stdout,*)' pairwise bonds:',EINT*FKJM,'(',(EINT-EB_frozen)*FKJM,')'
        write(stdout,*)' angle bonds:    ',EINT_ang*FKJM,'(',(EINT_ang-EA_frozen)*FKJM,')'
        write(stdout,*)' real Ew:       ',DEE*FKJM,'(',(DEE-EelNB_Frozen)*FKJM,')'
        write(stdout,*)' Fourier:       ',EFUR*FKJM
        write(stdout,*)' self-mol:      ',DES*FKJM,'(',(DES-EelBA_Frozen)*FKJM,')'
        write(stdout,*)' Ewald self-int:',ESF*FKJM
        write(stdout,*)' Ewald self-mol:',(ESF+DES)*FKJM
        write(stdout,*)' tot Ew.corr:   ',EFCOR*FKJM
    end if          
end subroutine