subroutine PBC_vec(R)
    use iomod, only: BOX
    implicit none

    real(8) :: HBOX(3)
    real(8), intent(inout) :: R(:,:)
    integer(4) i, j, size_

    HBOX=Box*0.5
    size_ = size(R,2)
!DIR$ SIMD    
    do concurrent (i=1:size_, j=1:3, (abs(R(j,i))>HBOX(j)))
            R(j,i)=R(j,i)-sign(BOX(j),R(j,i))
    enddo


end subroutine PBC_vec

subroutine PBC_vecM(R)
    use iomod, only: BOX
    implicit none
    real(8) :: HBOX(3)
    real(8), intent(inout) :: R(:,:,:)
    integer(8) i, k, j, size3_, size2_

    HBOX=Box*0.5
    size3_ = size(R,3)
    size2_ = size(R,2)    
    do concurrent( k=1:size3_, i=1:size2_, j=1:3, abs(R(j,i,k))>HBOX(j))
	R(j,i,k) = R(j,i,k) - sign(BOX(j), R(j,i,k))
    enddo
end subroutine PBC_vecM

pure subroutine MCPBC(R)
    use iomod, only: BOX
    implicit none
    real(8), intent(inout):: R(3)
    real(8) HBOX(3),CORSS
    integer(4) i
!Multiple PBC realization
    HBOX=Box*0.5
#ifdef PBC_MULT
    do i = 1,3
        do
            if(R(i).gt.-HBOX(i)) exit
            R(i)=R(i)+BOX(i)
        enddo
        do
            if(R(i).lt.HBOX(i)) exit
            R(i)=R(i)-BOX(i)
        enddo
    enddo
#else
    do i = 1, 3
        if(R(i).gt. HBOX(i)) R(i) = R(i) - BOX(i)
        if(R(i).lt.-HBOX(i)) R(i) = R(i) + BOX(i)
    enddo
#ifdef PBC_TO

    CORSS=HBOX(1)*int((4.0/3.0)*(sum(abs(R))/BOX(1))
    R(1)=R(1)-sign(CORSS,R(1))
    R(2)=R(2)-sign(CORSS,R(2))
    R(3)=R(3)-sign(CORSS,R(3))

#endif
#endif
    return
end subroutine
