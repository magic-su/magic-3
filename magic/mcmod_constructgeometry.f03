subroutine MCmodConstructGeometry(ID, irep, lprint, ierr)
!    use iomod, only: NAtom
    implicit none
    
    logical, intent(in) :: lprint
    integer, intent(out) :: ierr
    integer(4), intent(in) :: ID, irep
    
    integer(4) iAtom, jAtom, jBond, iBond, MaxBonds , i, NR, iMol
    type(Angle) :: iAngle
    real(8) cosphi, phi
    real(8), allocatable :: Rij_vec(:,:), Rij_temp(:)
    
    
    ! Construct matrix of interparticle distances                 
!    Rij=0.0
    do iAtom=1, NAtom
        call reshape2(Rij_vec,3,NAtom)
        call reshape(Rij_temp, NAtom)
        Rij_vec=0.0; 
  
        do jAtom=iAtom+1,NAtom     
            Rij_vec(:,jAtom)=Ri_vec(:,jAtom)-Ri_vec(:,iAtom)
!            Rij_vec(:,iAtom,jAtom)=-Rij_vec(:,jAtom,iAtom)                
        enddo
!    enddo                            
    ! Apply PBC for the new vector distances
!    do iAtom=1,NAtom
        call PBC_vec(Rij_vec(:,:))         
        call PBC_vec(Rij_vec(:,:))         
    
    ! Calculate distance modulus            
!        do jAtom=iAtom+1, NAtom
!          do i=1, 3
!            if(abs(Rij_vec(i, jAtom)) .gt. RCUT) then
!!          if (max(abs(Rij_vec(1,jAtom)), abs(Rij_vec(1,jAtom)), abs(Rij_vec(3,jAtom))).gt.RCUT) then  !assuming bonded interactions are within NB cutoff
!              Rij_temp(jAtom) = BOX(1)  ! to avoid sqrt()
!              exit
!            endif
!            if(i.eq.3) Rij_temp(jAtom) = sqrt(dot_product(Rij_vec(:,jAtom), Rij_vec(:,jAtom)))
!          enddo
!        enddo
        Rij_temp(iAtom+1:NAtom) = sqrt(sum(Rij_vec(:,iAtom+1:NAtom)**2,dim=1))        
        ! Consistency check
        if ((istep.gt.0).and.(.not.lExternalTraj)) then
            do jAtom=iAtom+1,NAtom
                if (.not. isequal(Rij(jAtom,iAtom), Rij_temp(jAtom), 1e-5,l_relative=.false.)) then
                    write(stdout,*) &
                        'Error: MCmod Construct: reConstructed distance Rij differs from the original value.',&
                        'i,j=',iAtom, jAtom, ' RijOld:', Rij(jAtom,iAtom), ' RijNew:', Rij_temp(jAtom),&
                        ' delta=',Rij(jAtom,iAtom)- Rij_temp(jAtom), ' step:',istep
                 call SaveLastConf(ID,IREP, Ri_vec)
                 call mcmodDebugDumpTrj(ID, iRep)
                 stop 404
                endif
            enddo
        endif 
        ! Consistency test passed - we can write the new coordinates into Rij
        Rij(iAtom+1:NAtom,iAtom) = Rij_temp(iAtom+1:NAtom)
        ! Check if no states with g(r)=0 are occupied.
        ! TODO: Is it actually needed?
! Disabled by Terry, since it's never triggered while using MC, and not useful
! while using MD
!        do jAtom=iAtom+1,NAtom     
!            if (MaskNB_SR(jAtom,iAtom)) then                  
!                if (Rij(jAtom, iAtom).le.RCUT) then
!		    NR=Rij(jAtom,iAtom)/RDFINC+1
!                    if((IND(NR,STypeAtom(iAtom),STypeAtom(jAtom)).eq.0).and.(lprint)) then
!                        write((stdout),'(a, 3(Xi0), f12.4, Xi0)') &
!                        'MCmodConstructGeometry: Wrong configuration: iAtom, jAtom, NR, Rij(jAtom,iAtom), step', &
!                                                              iAtom, jAtom, NR, Rij(jAtom,iAtom), iStep
!                        write((stdout),*) Rij_vec(:,jAtom)                            
!                    end if   !  if(IND...
!                endif
!            endif
!        enddo
    enddo
    ! Make Rii - very high, to exclude self-interaction from estimation
    do iAtom=1,NAtom 
        Rij(iAtom,iAtom)=RCUT*10.0+1
    enddo       
    ! Set values for lower diagonal part of the matrix Rij
    do iAtom=1, NAtom
        do jAtom = iAtom+1, NAtom
            Rij(iAtom, jAtom) = Rij(jAtom, iAtom)
        enddo
    enddo
    ! Check if no states with g(r)=0 are occupied.
    ! TODO: Is it actually needed?
!    do iAtom=1,NAtom
!        do jAtom=iAtom+1,NAtom
!            if (MaskNB_SR(jAtom,iAtom)) then                  
!                if (Rij(jAtom, iAtom).le.RCUT) then
!		    NR=Rij(jAtom,iAtom)/RDFINC+1
!                    if((IND(NR,STypeAtom(iAtom),STypeAtom(jAtom)).eq.0).and.(lprint)) then
!                        write((stdout),'(a, 3(Xi0), f12.4, Xi0)') &
!                        'MCmodConstructGeometry: Wrong configuration: iAtom, jAtom, NR, Rij(jAtom,iAtom), step', &
!                                                              iAtom, jAtom, NR, Rij(jAtom,iAtom), iStep
!                        write((stdout),*) Rij_vec(:,jAtom,iAtom)                            
!                    end if   !  if(IND...
!                endif
!            endif
!        enddo            
!    enddo
    ! Calculate pairwise Bond energy terms  where r<rcut    
    ierr = 0
!   Terry: the following check is useless when reading trajectory from MD
!   simulation
    if (.not. LExternalTraj) then
    do iAtom=1, NAtom
        do jBond=1,NBondsAtom(iAtom)
            jAtom=jAtomBond(jBond,iAtom)
            iBond=BondNum(jBond,iAtom)
            if ((Rij(jAtom,iAtom).lt.RMinPotB(iBond)).or.((Rij(jAtom,iAtom).gt.RMaxPotB(iBond)))) then
                write(stdout,*) 'Error: MCMod Construct. Bond length beyond allowed range.'
                write(stdout,*) 'MCStep:',iStep
                write(stdout,*) 'Atoms:',iAtom, jAtom
                write(stdout,*) 'BondNumber, RMin, RMax, R:',iBond,RMinPotB(iBond),RMaxPotB(iBond),Rij(jAtom,iAtom)
                write(stdout,'(a7,3f12.4,a7,3f12.4)') 'iAtom:',Ri_vec(:,iAtom), 'jAtom:', Ri_vec(:, jAtom)
                ierr = 1
                if (iStep.eq.0) then !if this is a starting configuration, which can be read from file, and subject to minor issues in bond length.
                    write(stdout,*) 'Need to reinitialize starting configuration!'
                    return
                elseif (.not. LExternalTraj) then
                    stop 413
                endif
            endif
        enddo
    enddo
    endif
    ! Calculate angles
    do i=1, NAngles
!        cosphi=dot_product(Rij_vec(:,Angles(i)%Atom1,Angles(i)%Atom2),&
!                Rij_vec(:,Angles(i)%Atom3,Angles(i)%Atom2))&
!                        /(Rij(Angles(i)%Atom1,Angles(i)%Atom2)*Rij(Angles(i)%Atom3,Angles(i)%Atom2))
        cosphi = (Rij(Angles(i)%Atom1,Angles(i)%Atom2)**2 + Rij(Angles(i)%Atom2,Angles(i)%Atom3)**2 &
                  -Rij(Angles(i)%Atom1,Angles(i)%Atom3)**2)&
                  /(2.d0*Rij(Angles(i)%Atom1,Angles(i)%Atom2)*Rij(Angles(i)%Atom3,Angles(i)%Atom2))
        if (abs(cosphi).gt.1.d0) cosphi=sign(1.d0,cosphi)                        
        phi=dacos(cosphi)*RAD2DEG                                               
        Angles(i)%Phi=phi                                            
    enddo
    ! Check ABond ranges
! Same as bonds, not useful when reading external trajectory
    if (.not.LExternalTraj) then
    do i=1, NAngles
        iAngle=Angles(i)
        if ((iAngle%Phi.gt.RMaxPotB(iAngle%Bond).or.(iAngle%Phi.lt.RMinPotB(iAngle%Bond)))) then 
            write(stdout,*) 'Error: MCMod Construct. Bond angle beyond allowed range.'
            write(stdout,*) 'Atoms:',iAngle%Atom1,iAngle%Atom2,iAngle%Atom3
            write(stdout,*) 'BondNumber, PhiMin, PhiMax, Phi:',iAngle%Bond,RMinPotB(iAngle%Bond),RMaxPotB(iAngle%Bond),iAngle%Phi
            ierr=1
            if (iStep.eq.0) then !if this is a starting configuration, which can be read from file, and subject to minor issues in bond length.
                write(stdout,*) 'Need to reinitialize starting configuration!'                
                return
            else if (.not. LExternalTraj) then
                stop 414
            endif
        endif
    enddo    
    endif
    deallocate(Rij_vec)
end subroutine
