subroutine MCmodConstruct(ID, irep, lprint, ierr)

    implicit none

    logical, intent(in) :: lprint
    integer, intent(out) :: ierr
    integer(4), intent(in) :: ID, irep

    integer(4) iAtom, jAtom, jBond,iBond , i, NR, iMol
    type(Angle) :: iAngle
    type (Angle), allocatable :: oldAngles(:) ! Array of angle-bonds present in the system
    real(8):: Force, Energ, dr(3),  phi, cosphi
    real(4):: threshold=1e-5
!    real(8), allocatable::RijOld(:,:)
    real(8) EBond_old, EAngle_old, ENB_old, EelNB_old, EelEXCL_old, DE, VirNB_old, VirB_old
!   Debugging part - allocate temporary arrays to keep old Rij and Eij values: TODO: Get rid of Eij-components
    ierr=0 ! Default value = 0, i.e. no error
!    allocate(RijOld(size(Rij,1),size(Rij,2)))

    oldAngles = Angles
!    RijOld=Rij

    
    ENB_old = ENB; ENB = 0.0
    EBond_old = EBond; EBond = 0.0
    EAngle_old = EAngle; EAngle = 0.0
    EelNB_old = EelNB; EelNB = 0.0
    EelEXCL_old = EelEXCL; EelEXCL = 0.0
    
#ifdef pressure    
    VirNB_old = VirNB; VirNB = 0.0
    VirB_old = VirB; VirB = 0.0
#endif
    
!   NB----------------------------------------------
! Recalculate COM and move COM of every molecule to the origin periodic box
    do iMol=1, NMol
        call MCmodgetCOM(iMol)
    enddo    
! Check agreement between Svec and Ri_vec
    do iAtom=1, NAtom
        dR=(Ri_vec(:,iAtom)-Svec(:,iAtom))
        call MCPBC(dR)
        if (sum(dR**2).gt.1e-8) then
            write(stdout,'(a,/,28X i0, 9f9.4,X i0)') 'MCmodConstruct: Ri_vec!=SX:iAtom, Ri_vec(:,iAtom), Svec(:,iAtom), dR, iStep',&
                                                                     iAtom, Ri_vec(:,iAtom), Svec(:,iAtom), dR, iStep
            call SaveLastConf(ID,IREP, Ri_vec)
            stop  401
        endif
        if (any([abs(Svec(:, iAtom)).gt.Box*0.5])) then
            write(stdout,'(a,/,6f9.4,X i0,X i0)') 'MCmodConstruct: Svec beyond the periodical box:1/2*BOX, Svec, iAtom, iStep',&
                                                                     Box*0.5, Svec(:,iAtom), iAtom, iStep
            call SaveLastConf(ID,IREP, Ri_vec)
            stop  401
            
        endif
    enddo
    call MCmodConstructGeometry(ID, irep, lprint, ierr)
    if (ierr .ne. 0) return
    ! Calculate short-range NB energy terms  where r<rcut
    call TimerStart(T_RealSpNB)
    do iAtom=1,NAtom
        do jAtom=iAtom+1,NAtom
            if (MaskNB_SR(jAtom,iAtom)) then
                if (Rij(jAtom, iAtom).le.RCUT) then
                    call EnergyForce(Rij(jAtom,iAtom),STypeAtom(jAtom),STypeAtom(iAtom), Energ, Force)
                    ENB = ENB + Energ
#ifdef pressure                        
                    VirNB = VirNB + Force*Rij(jAtom,iAtom)
#endif                    
                endif
            endif
        enddo
    enddo
    call TimerStop(T_RealSpNB)
    ! Calculate pairwise Bond energy terms  where r<rcut
    do iAtom=1, NAtom
        do jBond=1,NBondsAtom(iAtom)
            jAtom=jAtomBond(jBond,iAtom)            
            if (iAtom.lt.jAtom) then
                iBond=BondNum(jBond,iAtom)
                if (Btest(StateB(iBond),5)) then 
                    call EneriForci(Rij(jAtom,iAtom),iBOnd,RMinPotB(iBond),&
                        RMaxPotB(iBond),PotB_riBond,NPointsPotB(iBond),Energ, Force)
                    EBond = EBond + Energ
#ifdef pressure                                
                    VIRB = VIRB + Force*Rij(jAtom,iAtom)
#endif            
                endif
            endif
        enddo
    enddo

    ! Calculate electrostatic terms or corrections
    call TimerStart(T_RealSpEl)
    do iAtom=1,NAtom
        do jAtom=iAtom+1, NAtom
!            if ((ChargeAtom(jAtom)*ChargeAtom(iAtom).ne.0.0)) then
                if (MaskNB_EL(jAtom,iAtom).and.(Rij(jAtom,iAtom).le.RECUT)) then
                    Energ = ChargeAtom(iAtom)*ChargeAtom(jAtom)*COULF*derfc(ALPHA*Rij(jAtom,iAtom))/Rij(jAtom,iAtom)                
                    EelNB = EelNB + Energ
                endif
                
                if (MaskNB_EL_excl(jAtom, iAtom)) then
                    Energ = ChargeAtom(iAtom)*ChargeAtom(jAtom)*COULF*((derfc(ALPHA*Rij(jAtom,iAtom))-1.)/Rij(jAtom,iAtom))
                    EelEXCL = EelEXCL + Energ
                endif
!            endif
        enddo
    enddo
    call TimerStop(T_RealSpEl)

    ! Check distances deviation from old values
    do i=1, NAngles
        if ((iStep.gt.0).and.(abs(Angles(i)%Phi-oldAngles(i)%Phi).gt.threshold)) then
             write(stdout,*) &
                 'Error: MCmod Construct: Deviation in the angle value: Angle ',i,&
                 'Atoms(i,j,k)=(',Angles(i)%Atom1,Angles(i)%Atom2,Angles(i)%Atom3,')',&
                 'OldValue:',oldAngles(i)%Phi,&
                 'NewValue:',Angles(i)%Phi,&
                 ' delta phi =',oldAngles(i)%Phi-Angles(i)%Phi,&
                 'R_12 :',Rij(Angles(i)%Atom1,Angles(i)%Atom2),&
                 'R_13 :',Rij(Angles(i)%Atom1,Angles(i)%Atom3),&
                 'R_32 :',Rij(Angles(i)%Atom3,Angles(i)%Atom2),&
                 ' step:',istep
             call SaveLastConf(ID,IREP, Ri_vec)
             call mcmodDebugDumpTrj(ID, iRep)
             stop 402
         endif
    enddo

    ! Calculate Angle-Bond energies
    do i=1, NAngles
        if (Angles(i)%LMove) then 
            call EneriForci(Angles(i)%Phi,Angles(i)%Bond,RMinPotB(Angles(i)%Bond),RMaxPotB(Angles(i)%Bond),&
                        PotB_riBond,NPointsPotB(Angles(i)%Bond), Energ, Force)
            if ((iStep.gt.0).and.(abs(Angles(i)%Eang-Energ).gt.threshold).and.(.not.LExternalTraj)) then  ! TODO Remove check for LEXTERNALTRAJ
                 write(stdout,*) &
                     'Error: MCmod Construct: Deviation in the angle energy: Angle ',i,&
                     'Atoms(i,j,k)=(',Angles(i)%Atom1,Angles(i)%Atom2,Angles(i)%Atom3,')',&
                     'OldValue:',Angles(i)%Eang,&
                     'NewValue:',Energ,&
                     ' delta Energ=',Angles(i)%Eang-Energ, ' step:',istep
                 call SaveLastConf(ID,IREP, Ri_vec)
                 call mcmodDebugDumpTrj(ID, iRep)
                 stop 403
             endif
             Angles(i)%Eang=Energ
             EAngle = EAngle + Energ
        endif
    enddo
    !Summary
    if (iprint.ge.8) then
        write(stdout,*) 'MCmod Construct: Short-Range Non-Bonded energy:', ENB*FKJM
        write(stdout,*) 'MCmod Construct: Real space electrostatic Non-Bonded energy:', EelNB*FKJM
        write(stdout,*) 'MCmod Construct: Pairwise Bonds energy:', EBond*FKJM
        write(stdout,*) 'MCmod Construct: Angle Bonds energy:', EAngle*FKJM
    endif

    ! Check differences between initial values and recalculated values:
    if ((istep.gt.0).and.(.not.lExternalTraj)) then ! TODO Remove check for LEXTERNALTRAJ
!        do iAtom=1,NAtom
!            do jAtom=iAtom+1,NAtom
!                if (.not. isequal(RijOld(jAtom,iAtom), Rij(jAtom,iAtom), 1e-5,l_relative=.false.)) then
!                    write(stdout,*) &
!                        'Error: MCmod Construct: reConstructed distance Rij differs from the original value.',&
!                        'i,j=',iAtom, jAtom, ' RijOld:', RijOld(jAtom,iAtom), ' Rij:', Rij(jAtom,iAtom),&
!                        ' delta=',RijOld(jAtom,iAtom)- Rij(jAtom,iAtom), ' step:',istep
!                 call SaveLastConf(ID,IREP, Ri_vec)
!                 call mcmodDebugDumpTrj(ID, iRep)
!			stop 404
!                endif
!            enddo
!        enddo
	dE= ENB_old - ENB +&
	    EBond_old - EBond +&
	    EAngle_old - EAngle +&
	    EelNB_old -  EelNB +&
	    EelEXCL_old - EelEXCL

        if((dabs(dE).gt.threshold).and.(iStep.gt.0)) then
            write((stdout),'(2(a,f14.5))') 'MCmodConstruct: Error in energy after complete recalculation of the distances'
            write((stdout),'(a,4(a7,e14.6),a7,i8)') 'Deviations in Energy:',&
                            ' Enb:',ENB_old - ENB,&
                            ' Eelnb:',EelNB_old -  EelNB ,&
                            ' Eb:',EBond_old - EBond,&
                            ' Ea:',EAngle_old - EAngle,&
                            ' Step:',iStep

        endif
!        write(*,*) 'MCMOD Construct Energy'
#ifdef pressure        
        call assertequal('Deviation in Virial_NB after complete recalculation:',&
                            VirNB, VirNB_old, 1.e-8, l_relative=.true.)        
        call assertequal('Deviation in Virial_B after complete recalculation:',&
                            VirB, VirB_old, 1.e-8, l_relative=.true.)        
#endif                
    endif
#ifdef debug
    Ri_vec_buffer = 0.0
    cnt_Ri_vec_buffer = 0
#endif
    
end subroutine
