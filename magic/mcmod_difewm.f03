!========================= DIFEWM ================================
!
!  Change of reciprocal Ewald energy if "I" molecule, represented by list of charged atoms ichatomlist change
!  position
!
subroutine MCmodDIFEWM(iMol, RN, DEW)
!Global variables
    implicit none    
    integer(4), intent(in):: iMol
    real(8), intent(out):: DEW ! Change in reciprocal Ewald energy because of shift
    real(8),intent(in) :: RN(:,:) ! new trial coordinates of charged shifted atoms    
! local variables
    integer i,inkv,II, ibeg, iend, iatom_
    real(8):: dp_old, dp_new,sin_dp_old,cos_dp_old,sin_dp_new,cos_dp_new

    call TimerStart(T_DifEwM)
    ibeg = AdrAtomMol(iMol)
    iend = AdrAtomMol(iMol+1)

    do inkv=1,NKV ! over all K-vectors
        sigmasin(inkv)=0.d0 ! Check if this line can be removed later
        sigmacos(inkv)=0.d0 ! Check if this line can be removed later
	do concurrent (iAtom_=ibeg+1:iend, ChargeAtom(iAtom_).ne.0.0)
                dp_new = Recvec(1,inkv)*RN(1,iAtom_-ibeg) + Recvec(2,inkv)*RN(2,iAtom_-ibeg) + Recvec(3,inkv)*RN(3,iAtom_-ibeg)
                dp_old = Recvec(1,inkv)*Svec(1,iAtom_) + Recvec(2,inkv)*Svec(2,iAtom_) + Recvec(3,inkv)*Svec(3,iAtom_)                
                sin_dp_old = sin(dp_old)
                cos_dp_old = cos(dp_old)
                sin_dp_new = sin(dp_new)
                cos_dp_new = cos(dp_new)
                sigmacos(inkv)=sigmacos(inkv)+ChargeAtom(iAtom_)*(cos_dp_new-cos_dp_old)
                sigmasin(inkv)=sigmasin(inkv)+ChargeAtom(iAtom_)*(sin_dp_new-sin_dp_old)
        enddo
    enddo
    DEW=sum((sigmacos*(2.0*SCOS+sigmacos)+sigmasin*(2.0*SSIN+sigmasin))*RKV)
    call TimerStop(T_DifEwM)
end subroutine MCmodDIFEWM