subroutine TerminateMagiC()
        use precision
        use timer        
        implicit none
#ifdef mpi
        include 'mpif.h'
#endif                                   
        integer ierror
        
        call TimerReport(stdout) ! local report on every parallel process
        if (ID.ne.0) then
            close(stdout)
        endif
#ifdef mpi
        call MPI_BARRIER(MPI_COMM_WORLD,ierror)        
        call TimerCollectMPI(psize)
        call MPI_FINALIZE(ierror)
#endif
    end subroutine TerminateMagiC