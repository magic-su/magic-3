subroutine PrintPotential(irep, itercorr, &
    isPotCorrCheck)

    use InOutUnit
    ! Global Variables
    integer(4), intent(in):: IREP
    integer(8), intent(in):: itercorr
    logical, intent(in)   :: isPotCorrCheck

!    
! Local Variables    
    integer(4) iSType, jSType, NR, iBond, IAT, JAT, KAT
    real(8) DRR
    character*64 str,fmt ! Comment about potential line - either    
    character(128) str2
    
    if (.not.isPotCorrCheck) then
        write(stdout_imc,*)'------------------------------------'
	write(stdout_imc,*) 'Final Corrected effective potential', IREP
    else
        write(stdout_imc,*)'Intermediate Corrected effective potential', IterCorr
    endif
        
    select case (InvMode)
        case (NG)
            write(stdout_imc,*)' NG - optimization approach '
        case (IMC)
            write(stdout_imc,*)' IMC - crosscorrelation approach '
        case (IBI)
            write(stdout_imc,*)'  Mean-field approach'       
    end select
    write(stdout_imc,*)' regularization parameter  ',REGP
    do iSType=1,NSType
        do jSType=iSType,NSType
            if (Btest(StateNB(jSType,iSType),7)) then ! if the potential is to be printed
                write(stdout_imc,*)' This pair:',NameSType(iSType),' - ',NameSType(jSType),&
                    ' intermolecular pairs: ',npairs(iSType,jSType)
                write(stdout_imc,*)'   R        RDF        RDFref     Potential  Init.Pot.             REP NCheck'          
                if (Btest(StateNB(jSType,iSType),4)) then  ! if RDFref exist               
                    do NR=1,NA
                        if((IND(NR,jSType,iSType).eq.1).or.& 
                            ((IND(NR,jSType,iSType).eq.-3).and.(RDFref_NB_riSjS(NR,jSType, iSType).ne.0.0))) then
                            write(stdout_imc,'(f9.4,5f11.4,3x,a4,2a5,a11)') &
                                RAS(NR),&
                                RDFcalc_NB_riSjS(NR,jSType,iSType),&
                                RDFref_NB_riSjS(NR,jSType,iSType),&
                                (PotNB_riSjS(NR,jSType,iSType)+COR(IndexGlobNB_riSjS(NR,jSType,iSType)))*FRT,&                            
                                PotNB_riSjS(NR,jSType,iSType)*FRT,&
                                COR(IndexGlobNB_riSjS(NR,jSType,iSType)),&                            
                                'pot:',NameSType(iSType),NameSType(jSType),comment                    
                        endif
                    end do
                else ! No RDFref
                    do NR=1,NA
                        if((IND(NR,jSType,iSType).eq.1).or.& 
                            ((IND(NR,jSType,iSType).eq.-3).and.(RDFref_NB_riSjS(NR,jSType, iSType).ne.0.0))) then
                            write(stdout_imc,'(f9.4,5f11.4,3x,a4,2a5,a11)') &
                                RAS(NR),&
                                0.0,&
                                0.0,&
                                PotNB_riSjS(NR,jSType,iSType),&                            
                                PotNB_riSjS(NR,jSType,iSType)*FRT,&
                                0.0,&                            
                                'pot:',NameSType(iSType),NameSType(jSType),comment                    
                        endif
                    end do                    
                endif                
            end if                    
        end do
    end do
    
!    Intramolecular RDF-s    
    write(stdout_imc,*)'   Intramolecular Potentials'    
    do iBond=1, NBond
        ! if the molecule is allowed to move, and either RDF is defined 
        if (Btest(StateB(iBond),7)) then
            write(fmt,*) atom_name_len+1 ! length of atom-name field
            write(str2,*) 'Bond '//trim(NameBond(iBond))
            if (.not. BTest(StateB(iBond),9)) str2 = trim(str2)//' SameAs:'//trim(NameBond(BondSameBond(iBond)))
            if (BTypeBond(iBond).eq.1) then                
                write(str,'(2a'//trim(adjustl(fmt))//')') NameSite(iSiteInBond(iBond,1)), NameSite(jSiteInBond(iBond,1))
                write(stdout_imc,'(a11,a,a3,a,a16,a)')' This pair:',NameSite(iSiteInBond(iBond,1)),' - ',&
                    NameSite(jSiteInBond(iBond,1)),' intramolecular ', trim(str2)
                write(stdout_imc,*)'   R        RDF     RDFref    Potential      Init.Pot.'
            elseif (BTypeBond(iBond).eq.2) then
                write(str,'(3a'//trim(adjustl(fmt))//')') NameSite(iSiteInBond(iBond,1)),NameSite(kSiteInBond(iBond,1)),&
                             NameSite(jSiteInBond(iBond,1)) 
                write(stdout_imc,'(a14,a,a3,a,a3,a,a)')' This triplet:',NameSite(iSiteInBond(iBond,1)),&
                        ' - ',NameSite(kSiteInBond(iBond,1)),&
                        ' - ',NameSite(jSiteInBond(iBond,1)), trim(str2)
                write(stdout_imc,*)'   Phi        ADF     ADFref    Potential     Init.Pot.'
            endif

            if (Btest(StateB(iBond),4)) then ! if RDFref exists
                if((IPRINT.ge.6.and.NpointsPotB(iBond).gt.0))then
                    write(stdout_imc,*) ' NpairsAccumulated ', NPairsTotalBond(iBond)
                endif
                do NR=1,NpointsPotB(iBond)                
                    DRR=(RMaxPotB(iBond)-RMinPotB(iBond))/NPointsPotB(iBond)   
                    write(fmt,*) 3*(atom_name_len+1) ! triple length of atom-name field
                    write(stdout_imc,'(f10.4,5f10.5,5x,a4,a'//trim(adjustl(fmt))//', I4,a11)')&
                        RMinPotB(iBond)+DRR*(NR-0.5) ,&
                        RDFcalc_B_riBond(NR, iBond),&                    
                        RDFref_B_riBond(NR,iBond),&                    
                        (PotB_riBond(NR,iBond)+COR(IndexGlobB_riBond(NR,iBond)))*FRT,&
                        PotB_riBond(NR,iBond)*FRT,&
                        COR(IndexGlobB_riBond(NR,iBond)),&                    
                        'pot:',trim(str), INDI(NR,iBond), comment
                enddo
            else ! NO RDFref
                if((IPRINT.ge.6.and.NpointsPotB(iBond).gt.0))then
                    write(stdout_imc,*) ' NpairsAccumulated ',NPairsTotalBond(iBond)
                endif
                do NR=1,NpointsPotB(iBond)                
                    DRR=(RMaxPotB(iBond)-RMinPotB(iBond))/NPointsPotB(iBond)   
                    write(fmt,*) 3*(atom_name_len+1) ! triple length of atom-name field
                    write(stdout_imc,'(f10.4,5f10.5,5x,a4,a'//trim(adjustl(fmt))//', I4,a11)')&
                        RMinPotB(iBond)+DRR*(NR-0.5) ,&
                        0.0,&                    
                        0.0,&                    
                        PotB_riBond(NR,iBond),&
                        PotB_riBond(NR,iBond)*FRT,&
                        0.0,&                    
                        'pot:',trim(str), INDI(NR,iBond), comment
                enddo                
            endif
        endif
    end do    
call flush(stdout_imc)
  end subroutine PrintPotential
