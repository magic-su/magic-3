!====================== SETREFS =================================
subroutine SetReferences( STypeSite, &
    RDFref_NB_riSjS,& 
    NPointsRDFB, RMaxRDFB, RMinRDFB )
    
    implicit none 
! Set up reference arrays, addresses and units
! Global Variables
    integer  NPointsNBGpot
    integer, allocatable :: STypeSite(:), &
    NPointsRDFB(:)       

    real(8), allocatable:: RDFref_NB_riSjS(:,:,:) ,RMaxRDFB(:), RMinRDFB(:)
    ! Local variables        
    
    integer(4), allocatable :: SitesInGroupBondMType(:,:,:,:),& ! I-th triplet of sites involved in covalent-angle bond of type IBB-NBondB (numbering excluding pair bonds) in molecular type ITYP
        SideSiteABond(:,:,:),& !I-th dublet of sites involved in same covalent angle bond as site isite. Given site are sides of angle and isite is its edge
        NABondSite(:),& !  number of covalent angle bonds which site isite involved in
        SiteABondMType(:,:),& !  List of angle bond types in molecular type NMType. Last cell (NBondA+1,NMType) keeps number of bond types, and first cells in a row keep numbers ov these types.
        EdgeSiteABond_iSjS(:,:) !  Site which stands on edge of covalent angle bond between sites isite,jsite

    integer I,J,K, gbond, iNMType, IMOL, IAT, IS, IST, IM, JS, iBond, iSType, jSType

    call reshape(AdrMolMType,NMType+1)
    call reshape(AdrSiteMType,NMType+1)
    call reshape(AdrAtomMType,NMType+1)
    call reshape(AdrBondMType,NMType+1)
! Obtaining total number of  molecules in the system NMol
    NMol = sum(NMolMType(1:NMType))
! Allocate arrays having size of NMol
    call reshape(AdrAtomMol, NMol+1)
    call reshape(MTypeMol, NMol+1)    
    call reshape(MTypeSite, NSite)

    !Allocating arrays that depend on NSType
    call reshape2(StateNB,NSType,NSType)
    do iSType=1, NSType
        do jSType=1, NSType
            StateNB(iSType,jSType)=IBSet(StateNB(iSType,jSType),2) ! Default - free for corrections                                             
        enddo
    enddo
    ! Allocating arrays that depend on NBond or 1-element dummy arrays if no bonds are present
    iBond = Max(NBond,1)
    call reshape(StateB, iBond)
    do i=1,iBond 
        StateB(i)=IBSet(StateB(i),2) ! Default - free for correction    
    enddo             
    call reshape(RMaxRDFB,iBond)
    call reshape(RMinRDFB,iBond)
    call reshape(NPointsRDFB,iBond)
    call reshape(RMaxPotB,iBond)
    call reshape(RMinPotB,iBond)
    call reshape(NPointsPotB,iBond)    
        
    ! Counting total number of atoms NAtom
    NAtom=sum(NMolMType(1:NMType)*NSiteMType(1:NMType))
    ! Allocating arrays depend on NAtom
    call reshape(MolAtom,NAtom)
    call reshape(STypeAtom,NAtom)
    call reshape(SiteAtom,NAtom)
    call reshape(AtomChrgatom,NAtom)
    call reshape(ChargeAtom,NAtom)
    !  addresses
    AdrMolMType(1)=0
    AdrSiteMType(1)=0
    AdrAtomMType(1)=0
    AdrAtomMol(1)=0
    AdrBondMType(1)=0
    I=0 
    do iNMType=1,NMType	  
        I=I+NMolMType(iNMType)*NSiteMType(iNMType)
        AdrAtomMType(iNMType+1)=I                            !  atom addr
        AdrMolMType(iNMType+1)=AdrMolMType(iNMType)+NMolMType(iNMType)         !  molec adr
        AdrSiteMType(iNMType+1)=AdrSiteMType(iNMType)+NSiteMType(iNMType)       !  site addr
        AdrBondMType(iNMType+1)=AdrBondMType(iNMType)+NBondMType(iNMType)           !  bond addr
    end do
    if(IPRINT.ge.7)then
       write(stdout,*)' Addresses:'
       write(stdout,*)'          Typ     N    AdrMolMType'
       do I=1,NMType
            write(stdout,'(a,3I6)')' mol: ',I,NMolMType(I),AdrMolMType(I)
            write(stdout,'(a,3I6)')'  at: ',I,NMolMType(I)*NSiteMType(I),AdrAtomMType(I)
            write(stdout,'(a,3I6)')' sit: ',I,NSiteMType(I),AdrSiteMType(I)
       end do
    end if
! All references
IMOL=0
IAT=0
IS=0
IST=0
NAtomChrgatom = 0
if(IPRINT.ge.8)write(stdout,*)' References:'
if(IPRINT.ge.8)write(stdout,*) '  at    mol   site   typ  at_typ chrg'
do I=1,NMType
    do IM=1,NMolMType(I)
        IMOL=IMOL+1
        MTypeMol(IMOL)=I                       ! mol -> typ
        AdrAtomMol(IMOL)=IAT                   ! atom addr. in mol
        do IS=AdrSiteMType(I)+1,AdrSiteMType(I+1)
	    IAT=IAT+1
	    MolAtom(IAT)=IMOL                 ! atom -> mol
	    STypeAtom(IAT)=STypeSite(IS)      ! atom -> site_type
	    SiteAtom(IAT)=IS                  ! atom -> site
            ChargeAtom(IAT)=ChargeSite(IS)
            if(ChargeAtom(IAT).ne.0.d0)then         ! registering charged atoms
                NAtomChrgatom = NAtomChrgatom+1
                AtomChrgatom(NAtomChrgatom)=IAT
            end if
	    if(IPRINT.ge.8)write(stdout,'(5i6,f7.4)') IAT,IMOL,IS,I,STypeAtom(IAT),ChargeAtom(IAT)
	end do
    end do
    do IS=AdrSiteMType(I)+1,AdrSiteMType(I+1)
        MTypeSite(IS)=I                        ! site -> typ
    end do
end do
AdrAtomMol(IMOL+1)=IAT
write(stdout,*) 'Number of  charged atoms present in the system:',NAtomChrgatom
if (abs(sum(ChargeAtom)).gt.1e-11) then
    write(stdout,*) 'Error: Read MCM: Total charge of the system is not zero: ',sum(ChargeAtom)
    stop 502
endif

! Add content of setanglebonds_inc
! This section initialize lists of bonded atoms. This should make search of bonded atoms in MCstep and Energy subroutines much faster by extra cost of memory.

!Define maximum number of triplets in one bond type
if (NBond.gt.0) then
    j=max(MAXVAL(NSiteGroupsInBond(1:NBond),mask=BTypeBond(1:NBond).eq.2),0) ! Maximal number of atom groups
    write(stdout,*) 'Maximum number of triplets in a single bond:',j
    !Allocating memory for the arrays
    if (allocated(SitesInGroupBondMType)) deallocate(SitesInGroupBondMType)
    if (allocated(SideSiteABond)) deallocate(SideSiteABond)
    if (allocated(NABondSite)) deallocate(NABondSite)
    if (allocated(SiteABondMType)) deallocate(SiteABondMType)
    if (allocated(EdgeSiteABond_iSjS)) deallocate(EdgeSiteABond_iSjS)
    allocate(SitesInGroupBondMType(3,j,NBond,NMType))
    allocate(SideSiteABond(2,j,NSite))
    allocate(NABondSite(NSite))
    allocate(SiteABondMType(NBond+1,NMType))
    allocate(EdgeSiteABond_iSjS(NSite,NSite))
    SitesInGroupBondMType(:,:,:,:)=0
    SideSiteABond(:,:,:)=0
    NABondSite(:)=0
    SiteABondMType(:,:)=0
    EdgeSiteABond_iSjS(:,:)=0
    do iNMType=1, NMType ! Over all molecular types of the system
            write(stdout,*) 'Molecular type:',iNMType
            do iBond=1,NBond ! Loop over all bonds in the system            
                if ((MTypeSite(iSiteInBond(iBond,1)).eq.iNMType).and.(BTypeBond(iBond).eq.2)) then ! atom sites in the bond belongs to the chosen moltype
                    write(stdout,*) 'Bond:',iBond
                    SiteABondMType(NBond+1,iNMType)=SiteABondMType(NBond+1,iNMType)+1 !Counter for number of bond types filled
                    SiteABondMType(SiteABondMType(NBond+1,iNMType),iNMType)=iBond !Save bond type to the list
                    do i=1,NSiteGroupsInBond(iBond)
                        if (iPrint.gt.5) then
                            write(stdout,*) 'Site triplet:',i,' of ',NSiteGroupsInBond(iBond)
                            write(stdout,*) ISiteInBond(iBond,i),KSiteInBond(iBond,i),JSiteInBond(iBond,i)
                        endif
                        SitesInGroupBondMType(1,i,iBond,iNMType)=ISiteInBond(iBond,i)-AdrSiteMType(iNMType)
                        SitesInGroupBondMType(2,i,iBond,iNMType)=JSiteInBond(iBond,i)-AdrSiteMType(iNMType)
                        SitesInGroupBondMType(3,i,iBond,iNMType)=KSiteInBond(iBond,i)-AdrSiteMType(iNMType)
                        NABondSite(KSiteInBond(iBond,i))=NABondSite(KSiteInBond(iBond,i))+1
                        SideSiteABond(1,NABondSite(KSiteInBond(iBond,i)),KSiteInBond(iBond,i))=&
                                    ISiteInBond(iBond,i)-AdrSiteMType(iNMType)
                        SideSiteABond(2,NABondSite(KSiteInBond(iBond,i)),KSiteInBond(iBond,i))=&
                                    JSiteInBond(iBond,i)-AdrSiteMType(iNMType)
                        if ((EdgeSiteABond_iSjS(ISiteInBond(iBond,i),JSiteInBond(iBond,i)).eq.0).and.&
                            (EdgeSiteABond_iSjS(JSiteInBond(iBond,i),ISiteInBond(iBond,i)).eq.0)) then
                            EdgeSiteABond_iSjS(ISiteInBond(iBond,i),JSiteInBond(iBond,i))=&
                                KSiteInBond(iBond,i)-AdrSiteMType(iNMType)
                            EdgeSiteABond_iSjS(JSiteInBond(iBond,i),ISiteInBond(iBond,i))=&
                                KSiteInBond(iBond,i)-AdrSiteMType(iNMType)
                        else
                            write(stdout,*) 'Pair of sites ',ISiteInBond(iBond,i),JSiteInBond(iBond,i),&
                                            ' already bonded with covalent angle bond. iBond=',iBond
                            stop 514
                        endif
                    enddo
                endif
            enddo
    enddo
    !Writing output for self-check
    do i=1,NSite ! Loop over all sites of the system
        if (iPrint .gt. 5) write(stdout,*) 'Site:',I,' is involved in ',NABondSite(i),' covalent angle bonds ',&
            (SideSiteABond(1,j,i),SideSiteABond(2,j,i),':', j=1,NABondSite(i))
    enddo

    do iNMType=1,NMType ! over all molecular types
        write(stdout,*) 'Molecular type ',iNMType,' has ',SiteABondMType(NBond+1,iNMType),'covalent angle bonds types'
        do i=1,SiteABondMType(NBond+1,iNMType)
            iBond=SiteABondMType(i,iNMType)
            write(stdout,*) 'Bond type ',iBond,' has ',NSiteGroupsInBond(iBond),'covalent angle bonds'
            do j=1,NSiteGroupsInBond(iBond)
                if (iPrint .gt. 5) write(stdout,*) (SitesInGroupBondMType(k,j,iBond,iNMType), k=1,3)
            enddo
        enddo
    enddo
endif    
! ---------------------------------

J=0
do iNMType=1, NMType ! over all molecular types
    if (LMoveMType(iNMType).and.(NSiteMType(iNMType).gt.1)) then 
        j=j+1
    endif
enddo
if (J.eq.0) then
    write(stdout,*) 'WARNING! There are no multiatom molecules allowed to move in the system.',&
' Molecular Rotation/Translation MC steps are disabled'
    IROT=0
    ITRANS=0
endif


end subroutine SetReferences
