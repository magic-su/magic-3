!-------------------------------------------------------------------------------------------------------------------------
subroutine MCmodEwald()
    use timer                            
    implicit none                                

    ! Local variables  
    integer(4) :: IKV,IA,I
    real(8) SCS,SSN
    real(8) sin_SC,cos_SC, SC

    call TimerStart(T_Ewald)
!   reciprocal space Evald
    do IKV=1,NKV
        SCS=0.
        SSN=0.
        do IA=1,NAtomChrgAtom
            I = AtomChrgAtom(IA)
            SC = dot_product(Recvec(:,IKV),Svec(:,I))
            sin_SC = sin(SC)
            cos_SC = cos(SC)
            SSN=SSN+ChargeAtom(I)*sin_SC
            SCS=SCS+ChargeAtom(I)*cos_SC
        end do       
	SSIN(IKV)=SSN        
	SCOS(IKV)=SCS
    end do
    EFUR=sum(RKV*(SSIN**2+SCOS**2))
    call TimerStop(T_Ewald)
    return
end subroutine

