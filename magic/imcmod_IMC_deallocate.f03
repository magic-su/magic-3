subroutine IMC_deallocate(J, Jt, JtJ, JtJinv, JtJinvJt, NG_CORR, CORMF)    
    real(8), allocatable:: J(:,:),Jt(:,:),JtJ(:,:),JtJinv(:,:),JtJinvJt(:,:),NG_CORR(:), CORMF(:)

    select case (InvMode)
        case(0) !IBI

        case(1)
            deallocate(CROSS,IUCMP,IICMP,IPVT)
        case(2)
            deallocate(S_weight, JtJ, JtJinv, JtJinvJt, NG_CORR)
            deallocate(DIFF, iRDF2iGlob, iGlob2iRDF, iPot2iGlob, iGlob2iPot, CROSS)
        case(3)   !  EVR 
            deallocate(CROSS,IUCMP,IICMP,IPVT)
    end select

    deallocate(CORMF);
    if (allocated(DIFF)) deallocate(DIFF)
end subroutine
