subroutine readexclusions(filename, MaskExcl, NAngleBondsExcludeMType_, NPairBondsExcludeMType_)    
    use InOutUnit
    use strings
    use precision
!    use io_rdf_pot, only: GetFileLength

    implicit none
    
    logical, allocatable, intent(out):: MaskExcl(:,:)
    integer(4), allocatable, intent(out):: NAngleBondsExcludeMType_(:), NPairBondsExcludeMType_(:)
    
    character(*), intent(in):: filename    
   
    ! LOCAL VARIABLES
    character(input_text) line, field
    character(10), allocatable :: str_exclusions(:)
    logical l_exist
    integer(4) ierr, n_lines, i, i_site, j_site, n_excl, i_excl    
    
    n_lines = GetFileLength(filename, '') - 2  ! First two lines are for the header
    
    ! Assert we have exactly NSite-lines
    if (n_lines.ne.NSite) then
        write(*,*) 'ERROR: number of lines in the exclusion file does not match number of sites in the system:', filename
        write(*,*) 'n_lines=', n_lines, ';   NSites=', NSite
        stop 643
    endif
    
    open(unit=stdin_exclusions, file=filename, status='old', iostat=ierr)

    call reshape(NAngleBondsExcludeMType_, NMType) 
    call reshape(NPairBondsExcludeMType_, NMType)

    ! Read the header
    do i=1, 2
        read(stdin_exclusions,'(a)', iostat=ierr) line                
        call split(line,'=',field)
        select case (str_cln(field))
            case('NANGLEBONDSEXCLUDE')
                call ReadExclusionHeader(line, 'Angle', filename, NAngleBondsExcludeMType_)        
            case('NPAIRBONDSEXCLUDE')
                call ReadExclusionHeader(line, 'Pair', filename, NPairBondsExcludeMType_)
            case default
                write(*,*) 'Error: Unknown record in the header. Expected NANGLEBONDSEXCLUDE or NPAIRBONDSEXCLUDE, but got:',&
                        str_cln(field)
                stop 632
        end select
    enddo
    
    allocate(str_exclusions(NSite))
    allocate(MaskExcl(NSite, NSite))
    MaskExcl = .false.
    
    do i = 1, n_lines
        read(stdin_exclusions,'(a)', iostat=ierr) line                
        call split(line,':',field)
        call str2value(field, i_site, ierr)
        if (ierr.ne.0) then
            write(stdout,*) 'ERROR: Reading Exception file: ',trim(filename),' line:',i+2,&
                ' Can not read i_site number: ',trim(field)                               
            stop 639                                
        endif   
        if (i_site.ne.i) then
            write(*,*) 'ERROR: line:', i+2, ' i_site number ', i_site, ' differs from the line number'
            stop 642            
        endif        
        if (len(trim(line)).gt.0) then  ! if there are exclusions
            n_excl = StrCount(',', line) + 1 
            str_exclusions = ''  ! clear the array
            call parse(line, ',', str_exclusions, n_excl)
            do i_excl = 1, n_excl ! go over every record in the line
                call str2value(str_exclusions(i_excl), j_site, ierr)
                if (ierr.ne.0) then
                    write(stdout,*) 'ERROR: Reading Exception file: ',trim(filename),' line:',i+2,&
                                ' Can not read j_site number: ',trim(str_exclusions(i_excl))                               
                    stop 638
                endif   

                if (j_site.le.0) then ! Check for non-positive value
                    write(*,*) 'ERROR: in exclusion line', i+2, ' exclusion site number must be greater than zero'
                    stop 641
                endif
                if (MTypeSite(i_site).ne.MTypeSite(j_site)) then ! Check if sites belong to same molecular type
                    write(*,*) 'ERROR: line:', i+2, ' i_site: ', i_site,' j_site: ', j_site
                    write(*,*) 'both atoms in the exclusion must belong to the same molecular type' 
                    write(*,*) 'MolType(1):', MTypeSite(i_site), 'MolType(2):', MTypeSite(j_site)
                    stop 640
                endif
                MaskExcl(j_site, i_site) = .true.
                MaskExcl(i_site, j_site) = .true. ! make symmetrical
                MaskExcl(i_site, i_site) = .true. ! self exclusion
            enddo
        endif                
    enddo        
    
    if (iPrint.ge.7) then
        do i_site = 1, NSite
            write(stdout,*) i_site, ':', MaskExcl(:,i_site)
        enddo
    endif 
    close(stdin_exclusions)           
    deallocate(str_exclusions)
end subroutine
