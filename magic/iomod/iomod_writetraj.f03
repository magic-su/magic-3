!   This file contains subroutines responsible for output of MOLSIM
!       OpenTraj - Open/Create a file to write trajectory and connect to it. The file has name <BaseOutFileName>.i<iteration>.p<process>.traj.xmol
!       WriteTraj - Writes system's configuration to the trajectory file.
!       SaveLastConf - Writes resulting configuration of the system to a file. The file has name <BaseOutFileName>.i<iteration>.p<process>.start.xmol
!DEC$ FREEFORM

subroutine OpenTraj(ID,psize,IREP)
    use precision
    use InOutUnit
    use utilsmod, only: MakeSaveFileName    
    implicit none
    !Global variables
    integer(4) ID, psize,IREP
    !Local variables
    character(filename_text) SaveFileName 
    
    SaveFileName = MakeSaveFileName(BaseOutFileName, '.trj.xmol', IREP=IREP, ID=ID)
    open(unit=stdout_trj,file=SaveFileName)    
end subroutine

subroutine WriteTraj(psize,ISTEP,Ri_vec)
    !Global variables
    use precision
    use InOutUnit
    use TrajectoryIO, only: SaveFrame
    implicit none
    integer(4), intent(in)::  psize
    integer(8), intent(in)::  ISTEP
    real(8), intent(in) :: Ri_vec(:,:)
    ! Local variables
    integer(4) I
!    1.3  Dumping a configuration:
    call SaveFrame('xmol', stdout_trj, NAtom, real(BOX,4), NameSite(SiteAtom(1:NAtom)), Ri_vec, iStep*1.0)
end subroutine

subroutine SaveLastConf(ID,IREP,Ri_vec)
! Global Variables
    use precision
    use InOutUnit
    use utilsmod, only: MakeSaveFileName
    use TrajectoryIO, only: SaveFrame
    implicit none
    integer(4) IREP,ID
    real(8) Ri_vec(:,:)
! Local Variables
    character(filename_text) SaveFileName ! Local variable to work with, without changing global variable BaseOutFileName
    integer(4) i
    SaveFileName = MakeSaveFileName(BaseOutFileName, '.start.xmol', IREP=IREP, ID=ID)
    open(unit=stdout_snapshot,file=SaveFileName,status='unknown')
    call SaveFrame('xmol', stdout_snapshot, NAtom, real(BOX,4), NameSite(SiteAtom(1:NAtom)), Ri_vec, 0.0)
    close(stdout_snapshot)
    write((stdout),*)'Configuration snapshot file written ',SaveFilename
end subroutine
