subroutine OpenExternalTraj(ID)
!    use strings
!    use InOutUnit
!    use utilsmod, only: MakeSaveFileName
    use TrajectoryIO, only: OpenTraj
    implicit none    
    integer(4), intent(in) :: ID
    integer(4) ierr
    character(filename_text) :: FileTraj_head, FileTrajFull
    logical l_exist    
    ierr = 0
    
    ExternalTrajFormat = trim(FileExternalTraj(index(FileExternalTraj,'.', back=.true.)+1:))    
    FileTraj_head = trim(FileExternalTraj(1:index(FileExternalTraj,'.', back=.true.)-1))    
    FileTrajFull = MakeSaveFileName(FileTraj_head, '.'//trim(ExternalTrajFormat), ID=ID)
    inquire(file=FileTrajFull, exist=l_exist)
    write(stdout,*) 'OpenExternalTraj: Accessing file=',trim(FileTrajFull), '  exist=', l_exist
    if (.not.l_exist) then 
        FileTrajFull = MakeSaveFileName(FileTraj_head, '.'//trim(ExternalTrajFormat))
        inquire(file=FileTrajFull, exist=l_exist)
        write(stdout,*) 'OpenExternalTraj Accessing file=',trim(FileTrajFull), '  exist=', l_exist
        if (.not.l_exist) then 
            write(stdout,*) 'Error: OpenExternalTraj: File does not exist'
            stop 655
        endif
    endif   
        
    call OpenTraj(ExternalTrajFormat, FileTrajFull, stdin_externaltraj, 'r', ierr)
        
    if (ierr .ne. 0 ) then
        write(stdout, *) 'Error OpenExternalTraj: Can not open file:', FileExternalTraj, 'iostat=',ierr
        stop 660
    endif       
end subroutine

subroutine ReadFrame(Ri_vec)
    use TrajectoryIO, only: TrajectoryIO_ReadFrame=>ReadFrame
!    use InOutUnit
!    use strings
    implicit none 
    real(8), intent(inout) :: Ri_vec(:,:)
    integer(4) NAtom_, iAtom, pos_box, ierr
    real(4) box_(3), time
    character(atom_name_len), save, allocatable :: dummyNames(:)
    
    if (.not. allocated(dummyNames)) allocate(dummyNames(NAtom))
    
    box_ = -1.0 ! Default value for non-read box
    NAtom_ = -1 ! Default value for non-read number of atoms
    call TrajectoryIO_ReadFrame(ExternalTrajFormat, stdin_externaltraj, NAtom_, BOX_, dummyNames, Ri_vec, time, ierr)
    
    if ((NAtom_.ne.NAtom).and.(NAtom_ .ne.-1)) then
            write(stdout,*) 'Error Iomod_ReadExternalTraj_ReadFrame: Incorrect number of atoms in the frame:', NAtom_, &
                ' Shall be:', NAtom
            stop 659
        endif
    
    if (box_(1).gt.0) then  ! If box size read from trajectory
        if (sum(abs(BOX_ - BOX)).gt.3e-5) then
                write(stdout, *) 'Error: Periodic box dimensions of the external trajectory', BOX_,&
                    ' differ from the system specifications', BOX
                stop 658
            endif
    endif
    
end subroutine