subroutine MCmodFrozenEnergy(NBondsAtom,BondNum, jAtomBond)
!    use iomod, only: NAtom
    implicit none
    integer, intent (in):: NBondsAtom(:),BondNum(:,:), jAtomBond(:,:)



    real(8) Rij_frozen_vec(3), Rij_frozen,Phi_frozen ,R12(3), R13(3), R32(3), energ, force, cosphi
    integer(4) iAtom, jAtom, iBOnd, jBond, i
        ! Calculate Total energy contribution from the frozen pairs
    ENB_frozen=0.0; EB_frozen=0.0; EA_frozen=0.0;   EelNB_frozen=0.0; EelBA_frozen=0.0
    VirNB_frozen=0.0; VirB_frozen=0.0
    do iAtom=1,NAtom
        do jAtom=iAtom+1,NAtom
            if (.not.Btest(StateNB(STypeAtom(jAtom),STypeAtom(iAtom)),5)) then ! Frozen pair
                Rij_frozen_vec=Ri_vec(:,jAtom)-Ri_vec(:,iAtom)
                call MCPBC(Rij_frozen_vec)
                Rij_frozen=sqrt(sum(Rij_frozen_vec**2))
                if ((MolAtom(iAtom).ne.MolAtom(jAtom)).or.& !Different molecules
                    ((MolAtom(iAtom).eq.MolAtom(jAtom)).and.Bond_iSjS(SiteAtom(jAtom),SiteAtom(iAtom)).eq.0)) then !Not a bonded pair
                    if (Rij_frozen.le.RCUT) then
                        call EnergyForce(Rij_frozen,STypeAtom(jAtom),STypeAtom(iAtom), Energ, Force)
                        ENB_frozen=ENB_frozen+Energ
#ifdef pressure                            
                        VirNB_frozen=VirNB_frozen+Force*Rij_frozen
#endif
                        ! Check if no states with g(r)=0 are occupied.
                    endif
                    if (Rij_frozen.le.RECUT) then
                        EelNB_frozen=EelNB_frozen+ChargeAtom(iAtom)*ChargeAtom(jAtom)*COULF*derfc(ALPHA*Rij_frozen)/Rij_frozen
!                        write(*,*)'Energy_frozen:', iatom,jatom,Rij_frozen, COULF, ALPHA,&
!                        ChargeAtom(iAtom)*ChargeAtom(jAtom)*COULF*derfc(ALPHA*Rij_frozen)/Rij_frozen
                    endif

                endif
            endif
        enddo
    enddo

    do iAtom=1, NAtom
        do jBond=1,NBondsAtom(iAtom)
            iBond=BondNum(jBond,iAtom)
            jAtom=jAtomBond(jBond,iAtom)
            if (.not.Btest(StateB(iBond),5)) then ! Frozen pair
                if (jAtom.gt.iAtom) then
                    Rij_frozen_vec=Ri_vec(:,jAtom)-Ri_vec(:,iAtom)
                    call MCPBC(Rij_frozen_vec)
                    Rij_frozen=sqrt(sum(Rij_frozen_vec**2))
                    if ((Rij_frozen.lt.RMinPotB(iBond)).or.((Rij_frozen.gt.RMaxPotB(iBond)))) then
                        write(stdout,*) 'Error: MCMod Init: Frozen Pairs Energy calculation . Bond length beyond allowed range.'
                        write(stdout,*) 'Atoms:',iAtom, jAtom
                        write(stdout,*) 'BondNumber, RMin, RMax, R:',iBond,RMinPotB(iBond),RMaxPotB(iBond),Rij(jAtom,iAtom)
                        stop 410
                    endif
                    call EneriForci(Rij_frozen,BondNum(jBond,iAtom),RMinPotB(BondNum(jBond,iAtom)),&
                            RMaxPotB(BondNum(jBond,iAtom)),PotB_riBond,NPointsPotB(BondNum(jBond,iAtom)),Energ, Force)
!                    write(stdout,*) 'Frozen EB, EelB, i,j:',iAtom, jAtom,  Energ,&
!                        ChargeAtom(iAtom)*ChargeAtom(jAtom)*COULF*((derfc(ALPHA*Rij_frozen)-1.)/Rij_frozen)
                    EB_frozen=EB_frozen+Energ
                    VirB_frozen=VirB_frozen+Force*Rij_frozen
                    EelBA_frozen=EelBA_frozen+ChargeAtom(iAtom)*ChargeAtom(jAtom)*COULF*((derfc(ALPHA*Rij_frozen)-1.)/Rij_frozen)
                endif
            endif
        enddo
    enddo

    ! Calculate frozen Angle-Bond energies
    do i=1, NAngles
        if (.not.BTest(StateB(Angles(i)%Bond),5)) then ! if frozen
            R12=Ri_vec(:,Angles(i)%Atom2)-Ri_vec(:,Angles(i)%Atom1)
            R32=Ri_vec(:,Angles(i)%Atom2)-Ri_vec(:,Angles(i)%Atom3)
            R13=Ri_vec(:,Angles(i)%Atom3)-Ri_vec(:,Angles(i)%Atom1)
            cosphi=dot_product(R12,R32)/(sqrt(sum(R12**2))*sqrt(sum(R32**2)))
            if (abs(cosphi).gt.1.d0) cosphi=sign(1.d0,cosphi)
            Phi_frozen=dacos(cosphi)*RAD2DEG
            if (Phi_frozen.gt.RMaxPotB(Angles(i)%Bond).or.(Phi_frozen.lt.RMinPotB(Angles(i)%Bond))) then
                write(stdout,*) 'Error: MCMod Init: Frozen Energy. Bond angle beyond allowed range.'
                write(stdout,*) 'Atoms:',Angles(i)%Atom1,Angles(i)%Atom2,Angles(i)%Atom3
                write(stdout,*)'BondNumber, PhiMin, PhiMax, Phi:',&
                    Angles(i)%Bond,RMinPotB(Angles(i)%Bond),RMaxPotB(Angles(i)%Bond),Phi_frozen
                stop 411
            endif
            call EneriForci(Phi_frozen,Angles(i)%Bond,RMinPotB(Angles(i)%Bond),RMaxPotB(Angles(i)%Bond),&
                    PotB_riBond,NPointsPotB(Angles(i)%Bond), energ, force)
            EA_frozen=EA_frozen+energ
            EelBA_frozen=EelBA_frozen+ChargeAtom(Angles(i)%Atom1)*ChargeAtom(Angles(i)%Atom3)*COULF*&
                            ((derfc(ALPHA*sqrt(sum(R13**2)))-1.)/sqrt(sum(R13**2)))
        endif
    enddo
    write(stdout,*) 'MCmod: FrozenEnergy: Etotal:',(ENB_frozen+EB_frozen+EA_frozen+EelNB_Frozen+EelBA_frozen)*FKJM,&
                 ' ENB_frozen:',ENB_frozen*FKJM,&
                ' EB_frozen:', EB_frozen*FKJM,' EA_frozen:',EA_frozen*FKJM,'  EelNB:', EelNB_frozen*FKJM,&
                '  Eel(BandA):', EelBA_frozen*FKJM
#ifdef pressure                    
    write(stdout,*) 'MCmod: FrozenEnergy: Virial NB:',VirNB_frozen, '  Virial B:', VirB_frozen
#endif    
end subroutine
