!Make local coordinate system COM at zero
subroutine MCMioSetCOMZero(BeginSite,EndSite)
    implicit none
    ! GLobal variables
    integer(4), intent(in):: BeginSite,EndSite
    ! Local variables
    integer k, is, js
    real(8) COM(3),rr, rr2, summas
    
!  -------------------------------------------- 
!  This is total mass of the molecule in atomic units
    SUMMAS=SUM(MassSite(BeginSite:EndSite))
    do K = 1,3
        COM(K)=sum(RSite(BeginSite:EndSite, k) * MassSite(BeginSite:EndSite)) / SUMMAS
        RSite(BeginSite:EndSite, k) = RSite(BeginSite:EndSite, k) - COM(k)
    enddo! OF K
!  Print extra information
    if(IPRINT.ge.8)then
        write(stdout,*)'*** Molecular GEOMETRY (C.O.M. IN ORIGIN): '
        do is = BeginSite,EndSite
            write(stdout,'(I4,3x,A4,3X,3(1X,F7.3))') IS,NameSite(IS),RSite(IS,1),RSite(IS,2),RSite(IS,3)
        enddo! OF IS
    end if
    if(IPRINT.ge.10)then
        write(stdout,*)
        write(stdout,*)'*** INTER ATOMIC DISTANCES '
        do IS=BeginSite,EndSite
            do JS=IS+1,EndSite
                RR2=0.
	    	do K=1,3
                    RR2=RR2+(RSite(IS,K)-RSite(JS,K))**2
	    	end do
 	    	RR=sqrt(RR2)
                write(stdout,'(3a4,2x,f9.4)') NameSite(IS),' -> ',NameSite(JS),RR
            end do
	end do
    end if
end subroutine
