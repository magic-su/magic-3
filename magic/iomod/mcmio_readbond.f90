subroutine MCMioReadBond()
    
    implicit none

! Local variables
    character(filename_text) iFilename
    character(mcm_text) str
    integer iostt, iSite, iMType, IE,i,j, nargs, iNBond,IAT,JAT,KAT,NPOTS
    logical flagnewmcm

    NBond=0
    iNBond=0
    call reshape(NBondMType,NMType)
    NBondMType(:)=0    
    call reshape2(Bond_iSjS,sum(NSiteMType(1:NMType)),sum(NSiteMType(1:NMType)))
    write(stdout,*) ''
    write(stdout,*) 'Reading bonds present in the system:'
    do iMType=1, NMType ! Main Loop over all molecular types
        !  1 Open the .mcm file
        iFilename=(trim(adjustl(NameMType(iMType)))//'.mcm')		
        open(unit=stdin_mcm,file=iFilename,status='old',iostat=iostt)
        if (iostt .ne. 0) then
            write(stdout,*) 'ERROR: Reading MCM-file: mcm-file ',iFilename,' is not found!'
            stop 503
        endif    
        if(IPRINT.ge.5) write(stdout,*) '   *** MOLECULE ',trim(adjustl(NameMType(iMType))),',  Type  No.',iMType       
        STR=TAKESTR(stdin_mcm,Iostt)
    !  Number of atoms
        read(STR,*,iostat=iostt) iSite
        if (iostt .ne. 0) then
            write(stdout,*) 'ERROR: Reading MCM-file ',iFilename,' line 1'
            stop 504
        endif
    ! Skip lines with sites - we have read them before        
        do i=1, iSite
            STR=TAKESTR(stdin_mcm,Iostt)
        enddo
    ! Now we begin reading bonds
    !  Intramolecular information ("bonds")
        STR=TAKESTR(stdin_mcm,IE)        
!  Number of different "bonds"
        read(STR,*,iostat=iostt) iNBond
        if (iostt .ne. 0) then
            write(stdout,*)'ERROR: Reading MCM-file: Can not read number of pairwise-bonds ',iFilename
            stop 505
        endif
        NBondMType(iMtype)=iNBond       
        if (iNBond.gt.0) then                        
            call reshape(NSiteGroupsInBond,NBond+iNBond)        
            call reshape(BTypeBond,NBond+iNBond)        
            BTypeBond(NBond+1:NBond+iNBond)=1
            call reshape(MTypeBond,NBond+iNBond)        
            MTypeBond(NBond+1:NBond+iNBond)=iMType
            do i=1,iNBond ! Over all bonds in mcm file
                STR=TAKESTR(stdin_mcm,IE)
                read(STR,*,iostat=iostt)NPOTS ! Number of pairs in the bond
                if (iostt .ne. 0) then
                    write(stdout,*)'Reading MCM-file:', trim(iFilename)
                    write(stdout,*)'ERROR while reading number of site pairs in pairwise bond:', iNBond
                    stop 506
                endif
                NSiteGroupsInBond(NBond+i)=NPOTS
                call reshape2(iSiteInBond,NBond+iNBond, NSiteGroupsInBond(NBond+i))
                call reshape2(jSiteInBond,NBond+iNBond, NSiteGroupsInBond(NBond+i))   	            
                call reshape2(kSiteInBond,NBond+iNBond, NSiteGroupsInBond(NBond+i))
                do j=1,NPOTS ! Over all site pairs in the bond
                    STR=TAKESTR(stdin_mcm,IE)
                    read(STR,*,iostat=iostt) IAT,JAT
                    if (iostt.ne.0) then
                        write(stdout,*) 'Error: Reading MCM file: Reading bonded atom pairs: Bond:',NBond+i,&
                        'buffer string:',trim(str)
                        stop 507
                    endif
                    
                    IAT=IAT+sum(NSiteMType(1:iMType-1))
                    JAT=JAT+sum(NSiteMType(1:iMType-1))
                    iSiteInBond(NBond+i,j)=IAT
                    jSiteInBond(NBond+i,j)=JAT 
                    Bond_iSjS(IAT,JAT)=NBond+i
                    Bond_iSjS(JAT,IAT)=NBond+i
                    if (IAT.eq.JAT) then
                        write(stdout,*)'Reading MCM-file:', trim(iFilename)
                        write(stdout,*)'ERROR while reading site pairs in pairwise bond:', iNBond
                        write(stdout,*)'Same site is stated twice within the same bond:', IAT,'-',JAT
                        stop 508
                    endif
                end do
            end do
            NBond=NBond+iNBond
        else
            call reshape(MTypeBond, max(NBond+iNBond, 1))                
            call reshape(BTypeBond, max(NBond+iNBond, 1))        
            call reshape(NSiteGroupsInBond, max(NBond+iNBond, 1))                    
            call reshape2(iSiteInBond, max(NBond+iNBond, 1), 1)
            call reshape2(jSiteInBond, max(NBond+iNBond, 1), 1)   	            
            call reshape2(kSiteInBond, max(NBond+iNBond, 1), 1)
        endif
    ! Reading angle-bonds
    !  Intramolecular information ("bending angles")
        STR=TAKESTR(stdin_mcm,IE)
        ! Here the format of angle bond triplets shall be analyzed:
        ! If string Order=1-2-3 is present
        if (index(uppercase(STR),'ORDER=1-2-3').gt.0) then
            write(stdout,*) '   Reading MCM-file: New (direct) order of atom triplets 1-2-3 in bending angle bonds'
            flagnewmcm=.True.
        else
            write(stdout,*) '   Reading MCM-file: Old (non-direct) order of atoms triplets 1-3-2 in bending angle bonds'
            flagnewmcm=.false.
        endif
    !  Number of different "angles"
        read(STR,*,iostat=iostt)iNBond  !angle-bonds now
        if (iostt .ne. 0) then
            write(stdout,*)'   Reading MCM-file: Angle-bond section in absent in file ', trim(iFilename)	
        elseif (iNBond.gt.0) then ! There are angle bonds
            NBondMType(iMtype)=NBondMType(iMtype)+iNBond
            call reshape(NSiteGroupsInBond,NBond+iNBond)
            call reshape(BTypeBond,NBond+iNBond)
            BTypeBond(NBond+1:NBond+iNBond)=2
            call reshape(MTypeBond,NBond+iNBond)        
            MTypeBond(NBond+1:NBond+iNBond)=iMType
            do I=1,iNBond ! Over all angle-bonds in mcm file
                STR=TAKESTR(stdin_mcm,IE)
                read(STR,*,iostat=iostt)NPOTS ! Number of triplets in the anglebond
                if (iostt .ne. 0) then
                    write(stdout,*)'Reading MCM-file:', trim(iFilename)
                    write(stdout,*)'ERROR while reading number of site triplets in angle bending bond:', iNBond
                    stop 509
                endif
                NSiteGroupsInBond(NBond+i)=NPOTS
                call reshape2(iSiteInBond,NBond+iNBond, NSiteGroupsInBond(NBond+i))
                call reshape2(jSiteInBond,NBond+iNBond, NSiteGroupsInBond(NBond+i))
                call reshape2(kSiteInBond,NBond+iNBond, NSiteGroupsInBond(NBond+i))
                do j=1,NPOTS ! Over all atom triplets in the bond
                    STR=TAKESTR(stdin_mcm,IE)
                    if (flagnewmcm) then
                        read(STR,*) IAT,KAT,JAT !New 1-2-3 style Local site numbers
                    else
                        read(STR,*) IAT,JAT,KAT !Old 1-3-2 style Local site numbers
                    endif
                    IAT=IAT+sum(NSiteMType(1:iMType-1)) !Global site number
                    JAT=JAT+sum(NSiteMType(1:iMType-1))
                    KAT=KAT+sum(NSiteMType(1:iMType-1))
                    iSiteInBond(NBond+i,j)=IAT
                    jSiteInBond(NBond+i,j)=JAT
                    kSiteInBond(NBond+i,j)=KAT      
                    Bond_iSjS(IAT,JAT)=NBond+i
                    Bond_iSjS(JAT,IAT)=NBond+i
		    if ((IAT.eq.JAT).or.(IAT.eq.KAT).or.(JAT.eq.KAT)) then
			write(stdout,*)'Reading MCM-file:', trim(iFilename)
	        	write(stdout,*)'ERROR while reading site triplets in bond:', iNBond
                	write(stdout,*)'Same site is stated twice within the same bond:', IAT,'-',JAT,'-',KAT
			stop 510
		    endif
                end do
            end do
            NBond=NBond+iNBond
        endif
        close(stdin_mcm)        
    enddo ! over NMType
    NBondA=0
    NBondB=0
    do iNBond=1,NBond
        if (BTypeBond(iNBond).eq.1) NBondB=NBondB+1
        if (BTypeBond(iNBond).eq.2) NBondA=NBondA+1
    enddo
    write(stdout,*) 'Bonds are read successfully.'
    write(stdout,*) ''

    ! Defining names for Bonds
    allocate(NameBond(NBond))    
    do iNBond=1, NBond ! Over all bonds in the system
        iMType=MTypeBond(iNBond)        
        write(str,'(a,a1,i0)') trim(NameMType(iMType)),':',iNBond-sum(NBondMType(1:iMType-1))    ! local number of the bond in the respective Molecular Type
        NameBond(iNBond) = trim(str)
    enddo
        
end subroutine MCMioReadBond

