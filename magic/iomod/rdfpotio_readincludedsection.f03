subroutine ReadIncludedSection(IncludeFileName, text, iend, bInclude, eInclude, bTable, eTable, df_kind)
!! Global variables
     character(*), intent(in)   :: IncludeFileName, df_kind
     integer(4), intent(out)    :: iend, bInclude, eInclude, bTable, eTable
     
     character(*),allocatable, intent(inout)   :: text(:)

     ! Local variables   
     integer i,j, cnt_bInclude, cnt_eInclude, ierr, cnt_eTable, cnt_bTable

    character(rdfpot_text) l,str,field
    logical FlagTableFound
    
    ! Open the RDF/Potential file and scan it  for number of lines (variable iend)    
    iend = GetFileLength(IncludeFileName, str_cln(df_kind)//' include input')
! Allocate array for the file    
    if (allocated(text)) deallocate(text)
    allocate(text(iend))
! Read the file to the array text
    open(unit=stdin_rdfpot_inc,file=IncludeFileName,status='old',iostat=ierr)
    if (ierr.ne.0) then
        write(stdout, *) 'ERROR: Iomod.ReadIncluded: can not open file:', trim(IncludeFileName)
        stop 120
    endif
    do i=1,iend
        read(stdin_rdfpot_inc,'(a)') l
        text(i)=trim(adjustl(l))
    enddo
    close(stdin_rdfpot_inc)
    i=1
    bInclude=0
    eInclude=0
    cnt_bInclude=0
    cnt_eInclude=0
    cnt_eTable=0
    cnt_bTable=0
    do while (i.le.iend)
        call ReadLineFromText(text,i,l,ierr)
        ! Indicate presence of &Include 
        if(str_cmp(l,'&INCLUDED'//str_cln(df_kind))) then
            bInclude=i
            cnt_bInclude=cnt_bInclude+1
        endif
        if (str_cmp(l,'&ENDINCLUDED'//str_cln(df_kind))) then 
            eInclude=i
            cnt_eInclude=cnt_eInclude+1
        endif
        i=i+1
    enddo
    ! Indicate presence of &EndInclude and check consistency
    if ((cnt_bInclude.gt.1) .or. (cnt_eInclude.gt.1)) then
        write(stdout,*) 'ERROR: Reading ',trim(df_kind),': file ',trim(IncludeFileName), &
                        ' more than one line with tag &Included',trim(df_kind),&
                        ' or &EndIncluded',trim(df_kind),'  found '
        stop 121
    endif
    if (bInclude.eq.0) then
        write(stdout,*) 'ERROR: Reading ',trim(df_kind),': file ',trim(IncludeFileName), &
                    ' Can not find begin of section &Included',trim(df_kind),' '
        stop 122
    endif
    if (eInclude.eq.0) then
        write(stdout,*) 'ERROR: Reading ',trim(df_kind),': file ',trim(IncludeFileName), &
                    'Can not find end of section &EndIncluded',trim(df_kind),' '
        stop 123
    endif
    if (bInclude.gt.eInclude) then
        write(stdout,*) 'ERROR: Reading ',trim(df_kind),': file ',trim(IncludeFileName), &
                    ' The section &Included',trim(df_kind),' starts at line ',bInclude,&
                    ' and ends at line ',eInclude
        stop 124
    endif
    
    do j=bInclude, eInclude
        call ReadLineFromText(text,j,str,ierr)
        l=str
        call split(l,'=',field)        
        select case (str_cln(field))
             case ('&TABLE')
                   FlagTableFound=.true.
                   bTable=j
                   cnt_bTable=cnt_bTable+1
            case ('&ENDTABLE')
                   eTable=j
                   cnt_eTable=cnt_eTable+1                        
            case default
                !write(stdout,*) 'Debug: Unknown line:',trim(adjustl(str))
                continue
        end select                                
    enddo
    ! Check consistency of &Table &EndTable tags
    if  ((cnt_bTable.gt.1) .or. (cnt_eTable.gt.1)) then
        write(stdout,*) 'ERROR: Reading Included ',trim(df_kind),': ',trim(IncludeFileName),&
                ': more than one line with tag &Table or &EndTable found '
        stop 125
    endif
    if (bTable.eq.0) then
        write(stdout,*) 'ERROR: Reading ',trim(df_kind),': File: ',trim(IncludeFileName),&
                        ' Can not find begin of section &Table'
        stop 126
    endif
    if (eTable.eq.0) then
        write(stdout,*) 'ERROR: Reading ',trim(df_kind),': File: ',trim(IncludeFileName),&
                        ' Can not find end of section &EndTable'
        stop 127 
    endif
    if (bTable.gt.eTable) then
        write(stdout,*) 'ERROR: Reading ',trim(df_kind),': File: ',trim(IncludeFileName),&
                    ' The section &Table starts at line ',bTable, 'and ends at line ',eTable
        stop 128
    endif         
end subroutine

