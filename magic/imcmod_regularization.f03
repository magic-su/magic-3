subroutine Regularization(CORMF,COR)
    !Global    
    real(8), intent(out):: CORMF(:), COR(:)            
    !Local
    real(8), allocatable:: DIFRC(:), DIFMP(:)
    integer iSType, jSType, iBond, NP
    

    allocate(DIFRC(size(Sref_NB_riSjS,1)))
    allocate(DIFMP(size(Sref_NB_riSjS,1)))
    
    do iSType=1, NSType
        do jSType=iSType, NSType
            if (Btest(StateNB(jSType,iSType),6)) then  ! if RDF was sampled
                call Regularize(Sref_NB_riSjS(:,jSType,iSType), Scalc_NB_riSjS(:,jSType,iSType), REGP, DPOTM, RTM,DIFRC,DIFMP)                
                COR(IndexGlobNB_riSjS(:,jSType, iSType))=DIFRC
                if (Btest(StateNB(jSType,iSType),8)) CORMF(IndexGlobNB_riSjS(:,jSType, iSType))=DIFMP
            endif
        enddo
    enddo

    call reshape(DIFRC,size(Sref_B_riBond,1))
    call reshape(DIFMP,size(Sref_B_riBond,1))
    do iBond=1, NBond
        if (Btest(StateB(iBond),6)) then ! if RDF was sampled
            NP=NpointsPotB(iBond)
            call Regularize(Sref_B_riBond(1:NP,iBond), Scalc_B_riBond(1:NP,iBond),REGP, DPOTM, RTM,DIFRC(1:NP),DIFMP(1:NP))
            COR(IndexGlobB_riBond(1:NP,iBond))=DIFRC(1:NP)
            if (Btest(StateB(iBond),8)) CORMF(IndexGlobB_riBond(1:NP,iBond))=DIFMP(1:NP)            
        endif
    enddo
end subroutine Regularization

subroutine Regularize(Ref, Calc, REGP, DPOTM, RTM, DIFRC,DIFMP)
    real(8),intent(out):: DIFRC(:), DIFMP(:)
    real(8),intent(in):: Ref(:), Calc(:)
    real(8),intent(in):: REGP, DPOTM, RTM
    
    ! Calculate and scale down differences
    DIFRC=(Calc-Ref)*REGP    
    DIFMP=0.0
    ! This is only needed for IBI. and can be transferred to the relevant subroutine
    where ((Calc.gt.1.d-50 ).and.(Ref.gt.1.d-50 )) DIFMP=dlog(Calc/Ref)*RegP        
    where((Calc.lt.1.d-50 ).and.(Ref.gt.1.d-50 ))  DIFMP=-DPOTM
    where ((Calc.gt.1.d-50 ).and.(Ref.le.1.d-50 )) DIFMP=DPOTM
    
    ! Scaling of large relative changes
    where ((Calc.ne.0.0) .and. (abs(DIFRC).gt.Calc*RTM)) DIFRC=sign(Calc*RTM,DIFRC)
            
end subroutine Regularize
