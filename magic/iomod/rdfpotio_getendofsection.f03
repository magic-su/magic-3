subroutine GetEndOfSection(text,i,iend,bRDF,eRDF, df_kind)
! Global variables
    character(*), intent(in)         :: text(:), df_kind    
    integer(4), intent(in) :: i, iend
    integer(4), intent(out) :: bRDF, eRDF
    
! Local variables
    integer j, ierr
    character(rdfpot_text) l
    
    bRDF=i
    j=bRDF+1
    do j=bRDF+1,iend ! Search for &EndRDF/Potential record
        call ReadLineFromText(text,j,l,ierr)
        if(str_cmp(l,'&END'//str_cln(df_kind))) then 
            eRDF=j
            exit
        elseif ((str_cmp(l,'&'//str_cln(df_kind))).or.(j.eq.iend)) then
            write(stdout,*) 'ERROR: Reading ',trim(df_kind),&
            ': Can not find end of the section &',trim(df_kind),' started at line ',bRDF 
            stop 517 
        endif
    enddo
end subroutine