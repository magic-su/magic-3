\babel@toc {english}{}
\contentsline {section}{\numberline {1}What's new}{4}{section.1}% 
\contentsline {subsection}{\numberline {1.1}Version 3}{4}{subsection.1.1}% 
\contentsline {subparagraph}{CGTraj}{4}{section*.2}% 
\contentsline {subparagraph}{RDF}{4}{section*.3}% 
\contentsline {subparagraph}{Magic.Core}{4}{section*.4}% 
\contentsline {subparagraph}{MagicTools}{5}{section*.5}% 
\contentsline {subsection}{\numberline {1.2}Version 2.2}{5}{subsection.1.2}% 
\contentsline {subsection}{\numberline {1.3}Version 2}{5}{subsection.1.3}% 
\contentsline {section}{\numberline {2}Install}{6}{section.2}% 
\contentsline {subsection}{\numberline {2.1}Getting started}{6}{subsection.2.1}% 
\contentsline {subsection}{\numberline {2.2}Fast Installation}{6}{subsection.2.2}% 
\contentsline {subsection}{\numberline {2.3}Environment variables}{7}{subsection.2.3}% 
\contentsline {subsection}{\numberline {2.4}Manual Compilation}{7}{subsection.2.4}% 
\contentsline {subsubsection}{\numberline {2.4.1}\emph {CGTraj}}{7}{subsubsection.2.4.1}% 
\contentsline {subsubsection}{\numberline {2.4.2}\emph {Magic-Core}}{7}{subsubsection.2.4.2}% 
\contentsline {subsubsection}{\numberline {2.4.3}\emph {RDF and MagicTools}}{7}{subsubsection.2.4.3}% 
\contentsline {subsubsection}{\numberline {2.4.4}\emph {XTC trajectory file format support}}{7}{subsubsection.2.4.4}% 
\contentsline {section}{\numberline {3}Using MagiC}{9}{section.3}% 
\contentsline {subsection}{\numberline {3.1}Basic principles of the coarse-graining procedure}{9}{subsection.3.1}% 
\contentsline {subsection}{\numberline {3.2}\emph {CGTRAJ}: Bead Mapping}{11}{subsection.3.2}% 
\contentsline {subsubsection}{\numberline {3.2.1}Input/output files:}{11}{subsubsection.3.2.1}% 
\contentsline {subsubsection}{\numberline {3.2.2}cgtraj.inp: main input file}{12}{subsubsection.3.2.2}% 
\contentsline {subsubsection}{\numberline {3.2.3}Example: cgtraj.inp}{14}{subsubsection.3.2.3}% 
\contentsline {subsubsection}{\numberline {3.2.4}xmol2xtc Conversion Tool}{15}{subsubsection.3.2.4}% 
\contentsline {subsection}{\numberline {3.3}\emph {rdf.py}: Reference Distribution Functions calculation}{16}{subsection.3.3}% 
\contentsline {subsubsection}{\numberline {3.3.1}rdf.inp: main input file}{16}{subsubsection.3.3.1}% 
\contentsline {subsubsection}{\numberline {3.3.2}Example: rdf.inp}{19}{subsubsection.3.3.2}% 
\contentsline {subsection}{\numberline {3.4}\emph {MagiC Core}: Inverse Solver IMC/IBI}{21}{subsection.3.4}% 
\contentsline {subsubsection}{\numberline {3.4.1}General description}{21}{subsubsection.3.4.1}% 
\contentsline {subsubsection}{\numberline {3.4.2}Input/Output files}{21}{subsubsection.3.4.2}% 
\contentsline {subsubsection}{\numberline {3.4.3}magic.inp: main input file}{22}{subsubsection.3.4.3}% 
\contentsline {subsubsection}{\numberline {3.4.4}Example: magic.inp}{27}{subsubsection.3.4.4}% 
\contentsline {subsubsection}{\numberline {3.4.5}Conditional compilation: Increase the performance}{28}{subsubsection.3.4.5}% 
\contentsline {subsection}{\numberline {3.5}\emph {MagicTools}: Juggle with MagiC's data}{29}{subsection.3.5}% 
\contentsline {subsubsection}{\numberline {3.5.1}MagicTools data structures introduction}{29}{subsubsection.3.5.1}% 
\contentsline {paragraph}{Distribution functions and potentials:}{29}{section*.6}% 
\contentsline {paragraph}{Topology-related structures}{29}{section*.7}% 
\contentsline {subsubsection}{\numberline {3.5.2}Reading the data}{30}{subsubsection.3.5.2}% 
\contentsline {subsubsection}{\numberline {3.5.3}Plotting and Inspecting the data}{30}{subsubsection.3.5.3}% 
\contentsline {subsubsection}{\numberline {3.5.4}Numerical analysis and processing of the potentials}{31}{subsubsection.3.5.4}% 
\contentsline {subsubsection}{\numberline {3.5.5}Exporting topologies and potentials}{31}{subsubsection.3.5.5}% 
\contentsline {subsubsection}{\numberline {3.5.6}File conversion utilities}{32}{subsubsection.3.5.6}% 
\contentsline {section}{\numberline {4}\textit {MagicTools} procedures reference:}{33}{section.4}% 
\contentsline {subsection}{\numberline {4.1}Reading MagiC data files}{33}{subsection.4.1}% 
\contentsline {subsubsection}{\numberline {4.1.1}ReadRDF}{33}{subsubsection.4.1.1}% 
\contentsline {subsubsection}{\numberline {4.1.2}ReadPot}{33}{subsubsection.4.1.2}% 
\contentsline {subsubsection}{\numberline {4.1.3}ReadMagiC}{33}{subsubsection.4.1.3}% 
\contentsline {subsection}{\numberline {4.2}Visualization and inspection of the data}{34}{subsection.4.2}% 
\contentsline {subsubsection}{\numberline {4.2.1}OnePlot}{34}{subsubsection.4.2.1}% 
\contentsline {subsubsection}{\numberline {4.2.2}MultPlot}{35}{subsubsection.4.2.2}% 
\contentsline {subsubsection}{\numberline {4.2.3}HeatMap}{36}{subsubsection.4.2.3}% 
\contentsline {subsubsection}{\numberline {4.2.4}Deviation}{36}{subsubsection.4.2.4}% 
\contentsline {subsubsection}{\numberline {4.2.5}AnalyzeIMCOuput}{37}{subsubsection.4.2.5}% 
\contentsline {subsection}{\numberline {4.3}Analysis, processing and saving the data}{37}{subsection.4.3}% 
\contentsline {subsubsection}{\numberline {4.3.1}TotalPots}{37}{subsubsection.4.3.1}% 
\contentsline {subsubsection}{\numberline {4.3.2}GetOptEpsilon}{38}{subsubsection.4.3.2}% 
\contentsline {subsubsection}{\numberline {4.3.3}PotsEpsCorrection}{39}{subsubsection.4.3.3}% 
\contentsline {subsubsection}{\numberline {4.3.4}PotsPressCorr}{39}{subsubsection.4.3.4}% 
\contentsline {subsubsection}{\numberline {4.3.5}PotsExtendTailRange}{39}{subsubsection.4.3.5}% 
\contentsline {subsubsection}{\numberline {4.3.6}Average}{40}{subsubsection.4.3.6}% 
\contentsline {subsubsection}{\numberline {4.3.7}SaveDFsAsText}{40}{subsubsection.4.3.7}% 
\contentsline {subsubsection}{\numberline {4.3.8}DFset.Write}{40}{subsubsection.4.3.8}% 
\contentsline {subsection}{\numberline {4.4}Exporting topologies and potentials}{41}{subsection.4.4}% 
\contentsline {subsubsection}{\numberline {4.4.1}GromacsTopology}{41}{subsubsection.4.4.1}% 
\contentsline {subsubsection}{\numberline {4.4.2}LAMMPSTopology}{41}{subsubsection.4.4.2}% 
\contentsline {subsubsection}{\numberline {4.4.3}GALAMOSTTopology}{42}{subsubsection.4.4.3}% 
\contentsline {subsubsection}{\numberline {4.4.4}PotsExport}{43}{subsubsection.4.4.4}% 
\contentsline {subsubsection}{\numberline {4.4.5}Potential export procedure details}{44}{subsubsection.4.4.5}% 
\contentsline {subsubsection}{\numberline {4.4.6}xmol2gro}{45}{subsubsection.4.4.6}% 
\contentsline {subsubsection}{\numberline {4.4.7}gro2xmol}{46}{subsubsection.4.4.7}% 
\contentsline {subsubsection}{\numberline {4.4.8}tpr2mmol}{46}{subsubsection.4.4.8}% 
\contentsline {subsubsection}{\numberline {4.4.9}dcd2xtc}{46}{subsubsection.4.4.9}% 
\contentsline {subsubsection}{\numberline {4.4.10}GetStartConfs}{46}{subsubsection.4.4.10}% 
\contentsline {section}{\numberline {5}MagicTools object-classes reference}{47}{section.5}% 
\contentsline {subsection}{\numberline {5.1} Tabulated functions: RDFs, potentials, etc.}{47}{subsection.5.1}% 
\contentsline {subsubsection}{\numberline {5.1.1}DF: Distribution Function }{47}{subsubsection.5.1.1}% 
\contentsline {subsubsection}{\numberline {5.1.2}DFset - set of Distribution Functions}{48}{subsubsection.5.1.2}% 
\contentsline {subsection}{\numberline {5.2}Topology}{49}{subsection.5.2}% 
\contentsline {subsubsection}{\numberline {5.2.1}System: Top level object representing the whole molecular system }{49}{subsubsection.5.2.1}% 
\contentsline {subsubsection}{\numberline {5.2.2}MolType: Topology of a single molecular type }{50}{subsubsection.5.2.2}% 
\contentsline {subsubsection}{\numberline {5.2.3}Molecule }{51}{subsubsection.5.2.3}% 
\contentsline {subsubsection}{\numberline {5.2.4}BondType: }{51}{subsubsection.5.2.4}% 
\contentsline {subsubsection}{\numberline {5.2.5}Bond: }{52}{subsubsection.5.2.5}% 
\contentsline {subsubsection}{\numberline {5.2.6}AtomType }{52}{subsubsection.5.2.6}% 
\contentsline {subsubsection}{\numberline {5.2.7}Atom }{53}{subsubsection.5.2.7}% 
\contentsline {section}{\numberline {6}File formats}{54}{section.6}% 
\contentsline {subsection}{\numberline {6.1}.xmol}{54}{subsection.6.1}% 
\contentsline {subsection}{\numberline {6.2}.mmol}{54}{subsection.6.2}% 
\contentsline {subsubsection}{\numberline {6.2.1}MMOL Example: H2O.mmol}{55}{subsubsection.6.2.1}% 
\contentsline {subsection}{\numberline {6.3}.mcm}{55}{subsection.6.3}% 
\contentsline {subsubsection}{\numberline {6.3.1}.mcm file example: DMPC.CG.mcm}{56}{subsubsection.6.3.1}% 
\contentsline {subsection}{\numberline {6.4}.rdf and .pot file formats}{58}{subsection.6.4}% 
\contentsline {subsubsection}{\numberline {6.4.1}Header}{58}{subsubsection.6.4.1}% 
\contentsline {subsubsection}{\numberline {6.4.2}RDF/potential record: }{59}{subsubsection.6.4.2}% 
\contentsline {subsubsection}{\numberline {6.4.3}Included Potential/RDF}{60}{subsubsection.6.4.3}% 
\contentsline {subsection}{\numberline {6.5}exclusions.dat}{60}{subsection.6.5}% 
