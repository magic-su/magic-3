! Module for benchmarking of the code execution
!File : timingmod.f90
!Author : sasha
!
!Created on December 23, 2015, 2 : 07 PM
!

MODULE timer    
    
implicit none
! Named constants for accessing timing for different subroutines
integer(4), parameter:: T_N=34  ,& ! How many subroutines to benchmark
                        T_MCStep  = 1,  &
                        T_MCRot   = 2, T_MCRotNewCoords=21, T_MCRotNewArr=22, T_MCRotNBcount=23,&
                                      T_MCRotNBEnFor=27, T_MCRotNBEel=28, T_RealSPElRot=31, &
                        T_MCTrans = 3, T_MCTransNewCoords=24, T_MCTransNewArr=25, T_MCTransNBcount=26,  &
                                      T_MCTransNBEnFor=29, T_MCTransNBEel=30, &
                        T_RealSpNBMC = 34, T_RealSpElMC = 32, &
                        T_Average = 4,  &
                        T_reConstruct= 5,&
                        T_IMC     = 6,  &
                        T_Ewald   = 7,  &
                        T_DifEw   = 8, T_DifEWM  = 17, &
                        T_MCStepdE= 9, T_MCRotdE = 18, T_MCTransdE = 19,  &
                        T_MPI     = 10, &
                        T_IMCinit = 11, &
                        T_CrossCorrel = 12,&
                        T_RealSpEl = 13, T_RealSpNB = 33, &
                        T_MCUpdArr = 14, T_MCUpdArrRot = 15, T_MCUpdArrTrans = 16,&
                        T_Total = 20
real(8),private:: T_cpu(T_N), T_wall(T_N), T_wall_average(T_N), T_cpu_average(T_N)
real(8),private::  cnt_cpu_beg(T_N), cnt_cpu_end(T_N)
integer(8),private:: cnt_wall_beg(T_N), cnt_wall_end(T_N)
real(8), private :: t_wall_rate, t_cpu_rate, t_cpu_bigbang
integer(8), private:: cnt_rate,cnt_wall_bigbang

    
contains
subroutine TimerInit() ! Initiates the timer    
    call system_clock(cnt_wall_bigbang, cnt_rate); t_wall_rate = real(cnt_rate)
    call cpu_time(t_cpu_bigbang)
end subroutine

subroutine TimerStart(ID) ! Start/resume timer for the specified routine   
    integer(4),intent(in):: ID
#ifdef timer_on    
    if ((ID.gt.T_N).or.(ID.le.0)) then
        write(*,*) 'Error: TimerStart: Wrong TaskID specified'
        stop 1
    endif
    call system_clock(cnt_wall_beg(ID))
    call cpu_time(cnt_cpu_beg(ID))
#endif    
end subroutine

subroutine TimerStop(ID) !  Stop/pause timer for the specified routine
    integer(4),intent(in):: ID
#ifdef timer_on
    if ((ID.gt.T_N).or.(ID.le.0)) then
        write(*,*) 'Error: TimerStop: Wrong TaskID specified'
        stop 1
    endif
    call system_clock(cnt_wall_end(ID))
    call cpu_time(cnt_cpu_end(ID))
    T_wall(ID)=T_wall(ID)+(cnt_wall_end(ID)-cnt_wall_beg(ID))/t_wall_rate
    T_cpu(ID)=T_cpu(ID)+(cnt_cpu_end(ID)-cnt_cpu_beg(ID))
#endif    
end subroutine

subroutine TimerCollectMPI(psize) ! Collect local processes timer data
integer(4), intent(in):: psize    
#ifdef mpi
integer(4) ierror
include 'mpif.h'
    call MPI_REDUCE(T_wall,T_wall_average,T_N,MPI_REAL8,MPI_SUM,0,MPI_COMM_WORLD,ierror)
    call MPI_REDUCE(T_cpu,T_cpu_average,T_N,MPI_REAL8,MPI_SUM,0,MPI_COMM_WORLD,ierror)
!    T_wall_average=T_wall_average/psize; T_cpu_average=T_cpu_average/psize
!    write(*,*) 'T_wall_average:',T_wall_average
!    write(*,*) 'T_cpu_average:',T_cpu_average
    T_wall=T_wall_average/psize; T_cpu=T_cpu_average/psize 
#endif    
end subroutine

subroutine TimerReport(stdout) ! Report timing measurements and statistics
    integer(4),intent(in):: stdout
#ifdef timer_on
#ifndef test    
    call TimerStop(T_Total)
    write(stdout,*)               'Timing          T_wall(s)  Twall(%)    Tcpu(s)  Tcpu(%)'

    call TimerReportIt(T_Total,        'MagiC - total: ','',stdout)
    call TimerReportIt(T_MCStep,       'MCStep         ','',stdout)       
    call TimerReportIt(T_MCStepdE,     '  MCStepdE   : ','',stdout)    
    call TimerReportIt(T_DifEw,        '    Ewald    : ','',stdout)          
    call TimerReportIt(T_RealSpElMC,   '    RealSpEl : ','',stdout)    
    call TimerReportIT(T_RealSpNBMC,   '    RealSpNB : ','',stdout)
    call TimerReportIt(T_MCUpdArr,     '  MCStepUpd  : ','',stdout)
                                                    
    call TimerReportIt(T_MCRot,        'MCRot        : ','',stdout)
    call TimerReportIt(T_MCTrans,      'MCTrans      : ','',stdout)
    call TimerReportIt(T_DifEwM,       'MCDifEwMolec : ','',stdout)
     
    call TimerReportIt(T_Average,      'Averaging    : ','',stdout)
    call TimerReportIt(T_CrossCorrel,  '  CrossCorrel: ','',stdout)
    call TimerReportIt(T_reConstruct,  'TotalEnergy  : ','',stdout)    
    call TimerReportIt(T_Ewald,        '  Ewald      : ','',stdout)                                           
    call TimerReportIt(T_RealSpEl,     '  RealSpEl   : ','',stdout)
    call TimerReportIt(T_RealSpNB,     '  RealSpNB   : ','',stdout)    
    call TimerReportIt(T_IMC,          'IMC          : ','',stdout)
    call TimerReportIt(T_IMCinit,      'IMC Init     : ','',stdout)   
    call TimerReportIt(T_MPI,          'MPI          : ','',stdout)        

    call TimerStart(T_Total)    
#endif
#endif        
end subroutine

subroutine TimerReportIt(T_ID, title, comment,stdout)
    integer, intent(in) :: T_ID
    integer(4),intent(in):: stdout
    character(*), intent(in):: title, comment
    write(stdout,'(a14,a1,f12.2, f8.2, f12.2, f8.2 ,a)') title,':',&
            T_wall(T_ID), T_wall(T_ID)/T_wall(T_Total)*100.0,&
            T_cpu(T_ID), T_cpu(T_ID)/T_cpu(T_Total)*100, comment    
end subroutine

subroutine TimerStamp(stdout,text)
    character(*), intent(in):: text
    integer(4),intent(in):: stdout
    real(8) c
    integer(8) w 

    call system_clock(w); call cpu_time(c)
#ifndef test    
    write(stdout,*) text,' WallTime(sec): ',(w-cnt_wall_bigbang)/t_wall_rate, ' CPU Time(sec): ',c-t_cpu_bigbang     
#endif    
end subroutine
       
END
