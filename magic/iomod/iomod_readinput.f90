!DEC$ FREEFORM
subroutine ReadInput(iFilename)
! Global Variables
    character(*), intent(in):: iFilename
!Local variables
    integer(4) iend, iostt, i, ierr,j, iNMType, seed_
    real(8) trand, W, MCStepAtom, MCRotStep, MCTransStep
    character(input_text) field, l, str, strbox(3)
    character(input_text), allocatable :: text(:)
    character(mol_name_len), allocatable:: NMolMTypestr(:), LMoveMTypeStr(:), MCStepMolBoostChanceStr(:)
    logical l_exist, LIMC_
                
! Open the Input file and scan it  for number of lines (variable iend)
     write (stdout,*) ''
     write(stdout,*) '-----------------------------------------------------------------------'
     write (stdout,'(a)',advance='no') ' Reading main input file: '
    iend = GetFileLength(iFilename, 'main input')
    write (stdout,*) ''
    write (stdout,*) 'Parsing main input file'
    
! Allocate array for the content of the file    
    allocate(text(iend))
! Read the file to the array text
    open(unit=stdin,file=iFilename,status='old',iostat=iostt)
    do i=1, iend
        read(stdin,'(a)') l
        text(i)=trim(adjustl(l))
    enddo
    close(stdin)    

    call ReadParameter(iPrint, text, 'VerboseLevel', 'IPRINT', 'How much information to print', 'W',5)

    call ReadParameterInt(NMType, text, 'NMType', 'NTYP','Number of molecular types in the system', 'E',1)
! Set impossible defaults:
    BOX=-1.d0
    invMode=-1
! Loop over all lines of the array
    i=1        
    do while (i.le.iend)
        call ReadLineFromText(text,i,str,ierr)
        l=str
        call split(l,'=',field)
! NameMType  (NAMOL)    
        if (str_cmp(field,'NAMEMTYPE').or.str_cmp(field,'NAMOL')) then 
            allocate(NameMType(NMType))
        ! Clean l from possible begin and end symbols like ' and "
           call delall(l,'"')
           call delall(l,"'")

            call parse(l,',',NameMType, iNMType)
                if (iNMType.ne.NMType) then
                    write(stdout,*) 'ERROR: Reading Input: File: ',trim(iFilename),' line:',i,&
                               ' Not all of molecular types names were read: NameNMol: ',l                               
                    stop 648                    
                endif                
                do j=1, NMType
                    NameMType(j)=trim(adjustl(NameMType(j)))                    
                enddo
                write(stdout,*) 'MolecularType Names:'
                write(stdout,*) (trim(NameMType(j))//' ', j=1,NMType)
                write(stdout,*) ''
            continue
!NMOLMTYPE (NSPEC)
        elseif (str_cmp(field,'NMOLMTYPE').or.str_cmp(field,'NSPEC')) then 
            allocate(NMolMType(NMType))
            allocate(NMolMTypeStr(NMType))
            call parse(l,',',NMolMTypeStr, iNMType)
                if (iNMType.ne.NMType) then
                    write(stdout,*) 'ERROR: Reading Input: File: ',trim(iFilename),' line:',i, NEW_LINE('a'),&
                               ' Can not read Number of molecules: NMolMType: ',l                               
                        stop 649                    
                endif   
                do j=1, NMType
                    call str2value(trim(adjustl(NMolMTypestr(j))),NMolMType(j),ierr)
                    if (ierr.ne.0) then
                        write(stdout,*) 'ERROR: Reading Input: File: ',trim(iFilename),' line:',i,&
                               ' Can not read Number of molecules: NMolMType: ',l                               
                        stop 649                    
                    endif   
                enddo                
                write(stdout,*) 'Number of Molecules:',(NMolMType(1:NMType))
                write(stdout,*) ''
            continue
! MCStepMolBoostChance            
        elseif (str_cmp(field,'MCStepMolBoostChance')) then 
            allocate(MCStepMolBoostChance(NMType))
            allocate(MCStepMolBoostChanceStr(StrCount(',', l) + 1))
            call parse(l,',',MCStepMolBoostChanceStr, iNMType)
                if (iNMType.ne.NMType) then
                    write(stdout,*) 'ERROR: Reading Input: File: ',trim(iFilename),' line:',i, NEW_LINE('a'),&
                               ' Wrong number of the values provided for MCStepMolBoostChance: ', iNMType, 'instead of', NMType, &
                               NEW_LINE('a') ,l                               
                        stop 627                    
                endif   
                do j=1, NMType
                    call str2value(strip(MCStepMolBoostChanceStr(j)),MCStepMolBoostChance(j),ierr)
                    if (ierr.ne.0) then
                        write(stdout,*) 'ERROR: Reading Input: File: ',trim(iFilename),' line:',i,&
                               ' Can not read value for MCStepMolBoostChance: ',l                               
                        stop 626                    
                    endif   
                enddo                
                write(stdout,*) 'MCStepMolBoostChance: ',(MCStepMolBoostChance(1:NMType))
                write(stdout,*) ''
            continue

!LMoveMTYPE (LMOVE)
        elseif (str_cmp(field,'LMOVEMTYPE').or.str_cmp(field,'LMOVE')) then 
            allocate(LMoveMType(NMType))
            allocate(LMoveMTypeStr(NMType))
            call parse(l,',',LMoveMTypeStr, iNMType)
            if ((iNMType.eq.1).or.((iNMType.eq.NMType))) then  ! Either 1 or NMType records               
                do j=1, iNMType
                    if (str_cmp(LMoveMTypeStr(j),'.T.').or.str_cmp(LMoveMTypeStr(j),'T') &
                    .or.str_cmp(LMoveMTypeStr(j),'TRUE') .or.str_cmp(LMoveMTypeStr(j),'.TRUE.')) then                    
                        LMoveMType(j)=.true.
                    elseif (str_cmp(LMoveMTypeStr(j), '.F.')&
                    .or.    str_cmp(LMoveMTypeStr(j),'F') &
                    .or.    str_cmp(LMoveMTypeStr(j),'FALSE') &
                    .or.    str_cmp(LMoveMTypeStr(j),'.FALSE.')) then                    
                        LMoveMType(j)=.false.
                    else                    
                        write(stdout,*) 'ERROR: Reading Input: File: ',trim(iFilename),' line:',i,&
                               ' Can not read LMoveMType: :',l,':'                               
                        stop 650                    
                    endif   
                enddo          
                if (iNMType.eq.1) LMoveMType(:)=LMoveMType(1)
                write(stdout,*) 'Molecules are free to move: ', LMOVEMType(1:NMType)
                write(stdout,*) ''
            else
                    write(stdout,*) 'ERROR: Reading Input: File: ',trim(iFilename),' line:',i,&
                    ' Can not read LMoveMType: ',l
                    stop 650 
            endif                
            continue

! Box (BOXL, BOYL, BOZL)            
        elseif (str_cmp(field,'BOX').or.(INDEX(str_cln(field),'BO')==1)) then             
                write(stdout,*) 'BOX (A):',trim(l),':'
                write(stdout,*) ''
        box_select: select case (str_cln(field))                
                case ('BOX')
                    ierr=size(strbox)
                    call parse(l,',',strbox, ierr)
                    if (ierr.ne.3) write(stdout,*) 'Error while reading Box size: line: ',l
                    do j=1,3                        
                        call str2value(strbox(j),BOX(j),ierr)
                        if (ierr.ne.0) exit
                    enddo                        
                case ('BOXL')
                    call str2value(l,BOX(1),ierr)
                case ('BOYL')
                    call str2value(l,BOX(2),ierr)
                case ('BOZL')
                    call str2value(l,BOX(3),ierr)
                end select box_select
            if (ierr.ne.0) then
               write(stdout,*) 'ERROR: Reading Input: File: ',trim(iFilename),' line:',i,&
                       ' Can not read periodic box size: BOX ',l                               
                stop 651                    
            endif 
            continue                        
        endif
        i=i+1
    enddo
    
! Check if important values were set:
    if (.not. allocated(NMolMType)) then
        write(stdout,*) 'ERROR: Reading Input: File: ',trim(iFilename), 'Number of molecules of each kind NMolMType are not set'
        stop 653
    elseif (iPrint.ge.7) then 
        write(stdout,*) 'NMolMType=',NMolMType
    endif
    if (.not. allocated(NameMType)) then
        write(stdout,*) 'ERROR: Reading Input: File: ',trim(iFilename), 'Names of molecular types NameMType are not set'
        stop 652                       
    endif
    if (.not. allocated(LMoveMType)) then
        write(stdout,*) 'NOTE: Reading Input: File: ',trim(iFilename), &
        'All molecular types are allowed to move, since LMoveMType is not set. This is a default behaviour.'        
        allocate(LMoveMType(NMType))
        LMoveMType(:)=.true.
    endif
    ! Check if there is anything allowed to move in the system:
    if (.not. any(LMoveMType)) then
        write(stdout,*) 'ERROR: All Molecular types are fixed (not allowed to move). No point to proceed further, so program stops.'
        stop 665
    endif
    if (ANY(BOX.le.0)) then
        write(stdout,*) 'ERROR: Reading Input: File: ',trim(iFilename), 'Periodic box size BOX is not set'
        stop 654                                          
    endif    
    ! Read MCStepAtom per Molecule type or a single value, which will be applied for all.            
    allocate(MCStepAtomMType(NMType))
    call ReadParameterMCVec(NMType, NameMType, MCStepAtomMType, 1.d0, iFilename,&
                'MCStepAtom','DR','MC single atom displacement step', 'E', text)

    ! Read MCTransStep per Molecule type or a single value, which will be applied for all.                
    allocate(MCTransStepMType(NMType))
    call ReadParameterMCVec(NMType, NameMType, MCTransStepMType,0.d0, iFilename,&
                'MCStepTransMol','MCTransStep','MC molecule translation step', 'N', text)
                
    ! Read MCRotStep per Molecule type or a single value, which will be applied for all.            
    allocate(MCRotStepMType(NMType))
    call ReadParameterMCVec(NMType,NameMType, MCRotStepMType,0.d0, iFilename,&
                'MCStepRotMol','MCRotStep','MC molecule rotation step', 'N', text)

    ! Read Desired acceptance ratios:                 
    allocate(MCStepAtomARMType(NMType))
    call ReadParameterMCVec(NMType,NameMType, MCStepAtomARMType,0.5d0, iFilename,&
                'MCStepAtomAR','MCStepAtomAR','MC single atom desired acceptance ratio', ' ', text)
    allocate(MCTransStepARMType(NMType))
    call ReadParameterMCVec(NMType,NameMType,MCTransStepARMType,0.5d0, iFilename,&
                'MCStepTransMolAR','MCStepTransMolAR','MC translation step desired acceptance ratio', ' ', text)
    allocate(MCRotStepARMType(NMType))
    call ReadParameterMCVec(NMType,NameMType, MCRotStepARMType,0.5d0, iFilename,&
                'MCStepRotMolAR','MCStepRotMolAR','MC rotation step desired acceptance ratio', ' ', text)      

    call ReadParameter(EPS, text, 'Epsilon', 'EPS', 'Dielectric permittivity value', 'W',1.d0)        

    call ReadParameter(TEMP, text, 'Temperature', 'TEMP', 'Temperature', 'E',0.d0)

    call ReadParameter(NMCSteps, text, 'MCSteps', 'NMKS', 'Number of MC steps', 'E',0)

    call ReadParameter(NMCStepsEquil, text, 'MCStepsEquil', 'NMKS0', 'Number of MC equilibration steps', 'E',0)            
        
    call ReadParameter(ITRANS, text, 'iMCStepTransMol', 'ITRANS', 'MC molecule displacement frequency', 'N',0)            
    if ((maxval(MCTransStepMType).le.0).or.((ITRANS.le.0))) then
        write(stdout,*) 'WARNING: Reading Input: File: ',trim(iFilename), &
        'MC molecule displacement step (MCStepTransMol) or frequency (iMCStepTransMol) is not set.',&
         ' Molecule translation steps will not be performed in the simulation '
    endif    
         
        ! Check if rotation is enabled and if MCROTSTEP is not too big
    if (((maxval(MCRotStepMType)*2*3.1416).gt.0.3)) &
        write((stdout),*) 'Warning: MC rotation angle is greater than 0.3.  SIN(PHI)!= phi'              

    call ReadParameter(IROT, text, 'iMCStepRotMol', 'IROT', 'MC molecule rotation frequency', 'N',0)              
    if ((maxval(MCRotStepMType).le.0).or.((IROT.le.0))) then
        write(stdout,*) 'WARNING: Reading Input: File: ',trim(iFilename), &
        'MC molecule rotation step (MCStepRotMol) or frequency (iMCStepRotMol) is not set.',&
         ' Molecule rotation steps will not be performed in the simulation '
    endif    

    call ReadParameter(iCalcEnergy, text, 'iCalcEnergy', 'IOUT', 'How often to recalculate total energy in MC', 'E',0)          

    call ReadParameter(PotRcut, text, 'PotRcut', 'PotRcut', 'Cutoff distance for corrections of NB potentials', 'N',0.d0)          

    call ReadParameter(RECUT, text, 'RCutEl', 'RECUT', 'Cutoff distance for real space part of electrostatic interactions','N',0.d0)          

    call ReadParameter(AF, text, 'AF', 'AF', 'Ewald electrostatics parameter AF', 'N',3.d0)          
            
    call ReadParameter(FQ, text, 'FQ', 'FQ', 'Ewald electrostatics parameter FQ', 'N',9.d0)          

    call random_number(trand)
    call ReadParameter(RandomSeed, text, 'RandomSeed', 'NRS', ' Random seed for the random number generator','W',int(trand*100))          

 ! Initializing random numbers generator
    if (ID.ne.0) then 
        seed_ =  102+1+ID - 6 + 1
    else
        seed_ = 1
    endif
    do I=1,RandomSeed+13*(seed_) ! Different initialization for each process
      W=rand()
!      if(IPRINT.ge.10) write(stdout,*) I,W
    end do    
    if(IPRINT.ge.7)write(stdout,*) "Random seed: ",W
    
    call ReadParameter(POTCUT, text, 'ProhibPotLevel', 'POTCUT', ' Prohibiting potential level','N',1000.d0)   

    call ReadParameter(iAverage, text, 'iAverage', 'IAV',&
        ' Averaging frequency Automatic value iAverage=NAtoms*2 will be used:','W',-1)   
            
    call ReadParameter(REGP, text, 'REGP', 'REGP',' Regularization parameter (REGP) is not specified:','W',1.d0)          

    call ReadParameter(XREG, text, 'XREG', 'xreg',' EVR Regularization parameter (XREG) is not specified:','W',0.5d0)          

    call ReadParameter(YREG, text, 'YREG', 'yreg',' EVR Regularization parameter (YREG) is not specified:','W',0.5d0)          

    call ReadParameter(DPOTM, text, 'MaxPotCor', 'DPOTM','Maximum absolute correction of the potential','W',2.d0)              
    
    call ReadParameter(LCRDPass, text, 'KeepStructure', 'LCRDPASS',&
        'Trigger to use final structure of previous iteration as starting structure for the next iteration',&
        'W',.False.)  

    call ReadParameterReal(RTM, text, 'MaxRelDif', 'RTM',&
        'Parameter limiting maximum relative difference between reference and resulting averages',&
        'N',10.d0)  
        
    call ReadParameter(IREPT, text, 'NIter', 'IREPT', 'Numer of inverse iterations to perform', 'E',1)  
        
    
    call ReadParameter(str, text, 'InverseMode', 'InverseMethod', 'Inverse Method: IBI or IMC or NG','W','')          
    select case (str_cln(str))
        case ('IBI')
            InvMode = IBI
        case ('IMC')
            InvMode= IMC
        case ('NG', 'VIMC')
           InvMode= NG
        case ('EVR')
           InvMode= EVR
           if(XREG < 0.) XREG=0.
           if(XREG > 1.) XREG=1.
           if(YREG < 0.) YREG=0.
           if(YREG > 1.) YREG=1.
        case default ! InverseMode is not read
            write(stdout, *) 'Got unknown value for InverseMode: ',strip(str)
            write(stdout,*) 'InverseMode value is not set, checking obsolete LIMC-value '
            call ReadParameter(LIMC_, text, 'UseIMC', 'LIMC','Trigger to use Inverse Monte Carlo (or IBI instead)','W',.True.)              
            if (.not.LIMC_) InvMode= IBI
            if (LIMC_) InvMode= IMC
    end select
    call ReadParameter(IPotCorrCheck, text, 'iPotCorCheck', 'iPotCorrCheck',&
        'How often to perform intermediate potential correction checks?', 'N',0)  
    LPotCorrCheck=.false.
    if (IPotCorrCheck.gt.0) then 
        LPotCorrCheck=.true. ! If potential correction convergence test is enabled           
    endif 
                    
    call ReadParameter(FilRDF, text, 'InputRDF', 'FILRDF','Input RDF file', 'W','')  
        if (FilRDF=='') then
            write(stdout,*) ''
            write(stdout,*) 'ATTENTION!!! No RDF-file provided -> Inverse calculation can not be performed'
            write(stdout,*) ''
            LReadRDF=.false.
        else 
            LReadRDF=.true.
        endif
        
    call ReadParameter(FilPot, text, 'InputPot', 'FILPOT','Input Potential file','W','')  
    if (FilPOT=='') then   
        write(stdout,*) ''
        write(stdout,*) 'ATTENTION!!! No potential-file provided -> Potentials will be initialized from RDF-data'
        write(stdout,*) ''
        LReadPot=.false.
    else
        LReadPot=.true.
    endif
    
    call ReadParameter(FSTART, text, 'InputStartCoords', 'FSTART','Input file with starting coordinates','W','')  
    if (FSTART=='') then            
        write(stdout,*) 'NOTE! No starting coordinate-files are provided -> The starting structure will be generated randomly'
        LCRD=.false.
    else
    ! Here we check if there is an optional parameter stating how many configurations to use        
        l=FSTART
        call split(l,',',FSTART)
        if (trim(adjustl(l)).ne.'') then 
            call str2value(trim(adjustl(l)),NFStart,ierr)
            write(stdout,*) 'Starting coordinates will be read from file ',trim(FSTART)
            if (ierr.ne.0) then
                write(stdout,*) 'Can not understand how many configurations to read. Will read just the first configuration' 
                NFStart=0
            else
                write(stdout,*) 'random configuration among first ',NFStart,' will be read'             
            endif            
        else
            write(stdout,*) 'just first configuration of the file will be read' 
            NFStart=0
        endif

        LCRD=.true.

    endif
        
    call ReadParameter(FCRD, text, 'InputFrozenCoords', 'FCRD','File with coordinates of frozen molecules','N','')  
       
    call ReadParameter(CrossCorrFile, text, 'CrossCorrFile', 'CrossCorrFile',&
        'File to read/write CrossCorrelation matrix', 'N','')  
    
    LExternalTraj = .false.
    call ReadParameter(FileExternalTraj, text, 'ExternalTrajFile', 'ExternalTraj',&
        'File to read external trajectory', '','')  
        
    call ReadParameter(str, text, 'Mode', 'MagicMode', 'MagiC workflow mode: Native(0)/Master(1)/Slave(-1)', 'N','')
    if (trim(uppercase(str))=='NATIVE') then           
        Mode = NATIVE
        write(stdout,*) 'Mode: NATIVE' 
    elseif ((trim(uppercase(str))=='MASTER').or.(trim(uppercase(str))=='INVERT')) then
        write(stdout,*) 'Mode: INVERT:'         
        Mode = INVERT        
    elseif ((trim(uppercase(str))=='SLAVE').or.(trim(uppercase(str))=='SAMPLE')) then
        write(stdout,*) 'Mode: SAMPLE' 
        Mode = -1
    else ! Default
        write(stdout,*) 'Mode (default): NATIVE' 
        Mode = 0
    endif            
    if ( Mode.ne.NATIVE) then ! Collect or Invert mode (not native)
        iREPT = 1
        if (str_cmp(CrossCorrFile,'')) then
            write(stdout,*) 'ERROR: CrossCorrFile name is not provided.'
            stop 661
        endif
        if (Mode.eq.INVERT) then
            NMCSteps = 0 ! Do not perform MC-sampling
            inquire(file=CrossCorrFile, exist=l_exist)
            if (.not. l_exist) then
                write(stdout,*) 'ERROR: CrossCorrFile not found', CrossCorrFile
                stop 660
            endif
        endif
    endif
        
    call ReadParameter(factor_NG, text, 'factor_NG', 'FactorNG',&
        'Power for the weights in the NG optimization (0. - 1.)',&
        'N',0.d0)
        if (factor_NG<0.or.factor_NG>1) then
            write(stdout,*) 'NOTE! Crazy value of NG-factor (between 0 and 1 recommended)'
        endif
    call ReadParameter(LXMOL, text, 'DumpLastConf', 'LXMOL', 'Trigger to save last configuration of each MC-simulation','N',.false.)  
        
    call ReadParameter(iWriteTraj, text, 'WriteTraj', 'iWriteTraj', 'How often to write configurations to the trajectory', 'N',0)
        if (iWriteTraj==0) then
            write(stdout,*) 'NOTE! The trajectory will not be written'
        endif
                
! How often do auto adjustment of MC step size. Default - 0, do not do it.        
    call ReadParameter(NMCAutoAdjust, text, 'NMCAutoAdjust', 'iMCAutoAdjust',&
        'How many auto adjustments of MCstep-size to perform during equilibration phase?', 'N',0)  
        if (NMCAutoAdjust.gt.0) then
            if (.not. allocated(MCStepAtomARMType)) then
                allocate(MCStepAtomARMType(NMType)); MCStepAtomARMType=0.5;
                allocate(MCTransStepARMType(NMType)); MCTransStepARMType=0.5
                allocate(MCRotStepARMType(NMType)); MCRotStepARMType=0.5
            endif
        endif
    ! Read Exclusion file names
    LExclusionSR = .false.  ! Default - no exclusion list
    LExclusionEL = .false.  
    call ReadParameter(FileExclusionSR, text, 'ExclusionSR', 'ExclusionsSR', &
    'File to read list of short-range pair exclusions', '','')
    call ReadParameter(FileExclusionEL, text, 'ExclusionEL', 'ExclusionsEL', &
    'File to read list of electrostatics pair exclusions', '','')
    if (FileExclusionSR.ne.'') then
        LExclusionSR = .true.
        inquire(file=FileExclusionSR, exist=l_exist)
        if (.not. l_exist) then
            write(stdout,*) 'ERROR: ExclusionSR file not found', FileExclusionSR
            stop 645
        endif
    endif
    if (FileExclusionEL.ne.'') then
        LExclusionEL = .true.
        inquire(file=FileExclusionEL, exist=l_exist)
        if (.not. l_exist) then
            write(stdout,*) 'ERROR: ExclusionEL file not found', FileExclusionEL
            stop 644
        endif
    endif
    
    if (FileExternalTraj.ne.'') then
        write(stdout,*) 'External trajectory file is provided: No MC sampling will be performed, frames will be read from the file'       
        LXMOL = .false.
        iTrans = 0
        iRot = 0
        iAverage = 1
        NMCAutoAdjust = 0
        iWriteTraj = 0
        LExternalTraj = .true.
    endif
#ifdef PBC_TO
    if (RECUT.gt.minval(BOX)*sqrt(3.0)/4.0) then
#else
    if (RECUT.gt.minval(BOX)*0.5) then
#endif           
       write(stdout,*)' Real space electrostatics cutoff radius is larger than half of the smallest box size!'
       stop 646
    endif    

 write(stdout,*) 'Main input file parsed successfully.'     
end subroutine ReadInput

subroutine ShowDefaultMessage(readed, etype, message, name, str_defval)
    implicit none    
    character(len=*), intent(in):: message, name, str_defval   
    character(len=*), intent(in):: etype
    character(len=256):: msg
    logical, intent(in):: readed
        
    if (.not.readed) then
        write(msg,*) trim(message),' (',trim(adjustl(name)),') is not specified:'
        select case (uppercase(etype))
            case('E')
                write(stdout,*) 'ERROR: Reading Input: ', trim(msg)                
                stop 1
            case('W')
                write(stdout,*) 'WARNING: Reading Input: ',trim(msg)
                write(stdout,*) 'Default value of ',trim(str_defval), ' will be used'
            case('N')
                write(stdout,*) 'NOTE: Reading Input: ',trim(msg)
                write(stdout,*) 'Default value of ',trim(str_defval), ' will be used'
        end select
    endif

end subroutine
subroutine ReadParameterInt(param,text, name, oldname, message, etype, defval)
    integer(4) param, defval
    character(input_text) :: text(:)    
    character(len=*) name, oldname
    character(len=*) message
    character(len=*) etype    
    ! Local variables    
    integer i, ierr
    character(input_text) field, l, str    
    logical readed
    
    readed=.false.     ! Set not readed:
    !loop over the text 
    i=1
    do while (i.le. size(text))
        call ReadLineFromText(text,i,str,ierr)
        if (ierr.ne.0) exit
        l=str
        call split(l,'=',field)
        if (str_cmp(field,name).or.str_cmp(field, oldname)) then
            call str2value(trim(adjustl(l)),param,ierr)
            if (ierr.ne.0) then
                write(stdout,*) 'ERROR: Reading Input: line:',i,&
                ' Can not read the ',trim(message),' : (',trim(adjustl(name)),'):',l
                write(stdout,*) str
                stop 1                
            endif
            readed=.true.
            exit
            
        endif
        i=i+1
    enddo
    str=''; write(str,*) defval
    call ShowDefaultMessage(readed, etype, message, name, str)
    if (.not. readed) param=defval  
    if (iPrint .ge.7) write(stdout,*) trim(adjustl(name)),'=',param
end subroutine ReadParameterInt
    
subroutine ReadParameterInt8(param,text, name, oldname, message, etype, defval)
    integer(8) param
    integer(4) defval
    character(input_text):: text(:)    
    character(len=*) name, oldname
    character(len=*) message
    character(len=*) etype    
    ! Local variables    
    integer i, ierr
    character(input_text) field, l, str    
    logical readed
    character(len=256) msg
    
    readed=.false.     ! Set not readed:
    !loop over the text 
    i=1
    do while (i.le.size(text))
        call ReadLineFromText(text,i,str,ierr)
        if (ierr.ne.0) exit
        l=str
        call split(l,'=',field)
        
        if (str_cmp(field,name).or.str_cmp(field, oldname)) then
            call str2value(trim(adjustl(l)),param,ierr)
            if (ierr.ne.0) then
                write(stdout,*) 'ERROR: Reading Input: line:',i,&
                ' Can not read the ',trim(message),' : (',trim(adjustl(name)),'):',l
                write(stdout,*) str
                stop 1                
            endif
            readed=.true.
            exit
            
        endif
        i=i+1
    enddo
    ! Check if value was set:

    str=''; write(str,*) defval
    call ShowDefaultMessage(readed, etype, message, name, str)
    if (.not. readed) param=defval    
    if (iPrint .ge.7) write(stdout,*) trim(adjustl(name)),'=',param
end subroutine ReadParameterInt8
    
subroutine ReadParameterReal(param,text, name, oldname, message, etype, defval)
    real(8) param, defval
    character(input_text):: text(:)    
    character(len=*) name, oldname
    character(len=*) message
    character(len=*) etype    
    ! Local variables    
    integer i, ierr
    character(input_text) field, l, str   
    character(len=256) msg
    logical readed
    
    readed=.false.     ! Set not readed:
!loop over the text 
    i=1
    do while (i.le.size(text))
        call ReadLineFromText(text,i,str,ierr)
        if (ierr.ne.0) exit
        l=str
        call split(l,'=',field)
        
        if (str_cmp(field,name).or.str_cmp(field, oldname)) then
            call str2value(trim(adjustl(l)),param,ierr)
            if (ierr.ne.0) then
                write(stdout,*) 'ERROR: Reading Input: line:',i,&
                ' Can not read the ',trim(message),' : (',trim(adjustl(name)),'):',l
                write(stdout,*) str
                stop 1                
            endif
            readed=.true.
            exit
        endif
        i=i+1
    enddo
    ! Check if value was set:
    str=''; write(str,*) defval
    call ShowDefaultMessage(readed, etype, message, name, str)
    if (.not. readed) param=defval    
    if (iPrint .ge.7) write(stdout,*) trim(adjustl(name)),'=',param
end subroutine ReadParameterReal

subroutine ReadParameterString(param,text, name, oldname, message, etype, defval)
    character(len=*) param, defval
    character(input_text):: text(:)    
    character(len=*) name, oldname
    character(len=*) message    
    character(len=*) etype    
   ! Local variables    
    integer i, ierr
    character(input_text) field, l, str    
    logical readed
    character(len=256) msg
    
    readed=.false.     ! Set not readed:   
    !loop over the text 
    i=1
    do while (i.le.size(text))
        call ReadLineFromText(text,i,str,ierr)
        if (ierr.ne.0) exit
        l=str
        call split(l,'=',field)
        
        if (str_cmp(field,name).or.str_cmp(field, oldname)) then
            ! Clean l from possible begin and end symbols like ' and "
                call delall(l,'"')
                call delall(l,"'")
            param=trim(adjustl(l))
            readed=.true.
            exit
        endif
        i=i+1
    enddo
    ! Check if value was set:
    str=''; write(str,*) defval
    call ShowDefaultMessage(readed, etype, message, name, str)
    if (.not. readed) param=defval    
    if (iPrint .ge.7) write(stdout,*) trim(adjustl(name)),'=',trim(adjustl(param))
end subroutine ReadParameterString
    
subroutine ReadParameterLogical(param,text, name, oldname, message, etype, defval)
    logical param, defval
    character(input_text):: text(:)    
    character(len=*) name, oldname
    character(len=*) message    
    character(len=*) etype    
    ! Local variables
    character(len=256) msg
    logical readed
    integer i, ierr
    character(input_text) field, l, str    
    ! Set impossible defaults:
    readed=.false.
    !loop over the text 
    i=1
    do while (i.le.size(text))
        call ReadLineFromText(text,i,str,ierr)
        if (ierr.ne.0) exit
        l=str
        call split(l,'=',field)
        if (str_cmp(field,name).or.str_cmp(field, oldname)) then
        
            if (str_cmp(l,'.T.').or.str_cmp(l,'TRUE').or.str_cmp(l,'.TRUE.')) then
                param=.true.
            elseif (str_cmp(l,'.F.').or.str_cmp(l,'FALSE').or.str_cmp(l,'.FALSE.')) then
                param=.false.
            else
                write(stdout,*) 'ERROR: Reading Input:',&
                    ' Can not read the ',trim(message),' : (',trim(adjustl(name)),'):'
                write(stdout,*) 'line:"',str,'"'
                stop 1                
            endif
            readed=.true.
            exit
         endif  
         i=i+1
    enddo  
    ! Check if value was set:
    str=''; write(str,*) defval
    call ShowDefaultMessage(readed, etype, message, name, str)
    if (.not. readed) param=defval    
    if (iPrint .ge.7) write(stdout,*) trim(adjustl(name)),'=',param
end subroutine ReadParameterLogical

subroutine ReadParameterMCVec(NMType,NameMType,MCTransStepMType,DefValue,iFilename,&
            ParamName1,ParamName2, Title, Importance, text)    
    integer(4), intent(in):: NMType
    real(8),intent(out):: MCTransStepMType(:)
    character(*), intent(in):: NameMType(:)
    real(8),intent(in)    :: DefValue
!    character(input_text), intent(in):: l   !, str, strbox(3)
    character(filename_text), intent(in):: iFilename
    character(*), intent(in):: ParamName1,ParamName2, Title
    character, intent(in)::Importance
    character(input_text), intent(in):: text(:)
    
    character(input_text) field, str, l     
    character(mol_name_len), allocatable:: MCTransStepMTypeStr(:)
    integer(4) iNMType, j, ierr, i
    logical readed
    real(8) pvalue
    character(len=5) :: fmt

    readed=.false.     ! Set not readed:
    i=1        
    do while (i.le.size(text))
        call ReadLineFromText(text,i,str,ierr)
        l=str
        call split(l,'=',field)
        if (str_cmp(field,uppercase(ParamName1)).or.str_cmp(field, uppercase(ParamName2))) then 
            allocate(MCTransStepMTypeStr(NMType))
            call parse(l,',',MCTransStepMTypeStr, iNMType)
            if ((iNMType.ne.NMType).and.(iNMType.ne.1)) then
                write(stdout,*) 'ERROR: Reading Input: File: ',trim(iFilename),' line:',i,&
                ' Can not read ',trim(ParamName1),': ',trim(l)
                stop 1 
            endif                
            if (iNMType.eq.1) then !single record
                call ReadParameter(pvalue, text, ParamName1, ParamName2,&
                    Title, Importance, DefValue)
                    MCTransStepMType=pvalue
                write(stdout,*) 'Uniform ',title,' for all molecules of the system:'
            else   ! Multiple records
                do j=1, NMType
                    call str2value(MCTransStepMTypeStr(j),MCTransStepMType(j),ierr)
                    if (ierr.ne.0) then
                        write(stdout,*) 'Error while reading ',ParamName1,': line: ',l,' substring:',MCTransStepMTypeStr(j)
                        stop 1
                    endif
                enddo     
                write(stdout,*) 'NonUniform ',title,' - it is subject to Molecular Type:'
            endif
            write(fmt,'(i3)') NMType*3
            write(stdout,'(a14,'//fmt//'(a8,a1))') '    MolType: ',(trim(NameMType(j)),' ', j=1, NMType)
            write(stdout,'(a4,a10,a2,'//fmt//'f9.3)') '    ',ParamName1,': ', MCTransStepMType

            deallocate(MCTransStepMTypeStr)
            readed=.true.            
            exit
        endif
        i = i+1
    enddo
    if (.not. readed) MCTransStepMType=DefValue
end subroutine ReadParameterMCvec
