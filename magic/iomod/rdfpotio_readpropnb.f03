subroutine ReadPropNB(text,iFilename, beg_text, end_text, AtomTypesPairStr, &
    AtomTypesPair, RMinNB, RMaxNB, PRmin, PRmax, Npoints,&
    PNpoints, df_kind)
    ! Global Variables
    character(*), intent(in)    :: text(:), df_kind, iFilename
    integer(4), intent(in)      :: beg_text, end_text, Npoints, PNPoints
    integer(4), intent(out)     :: AtomTypesPair(2)
    real(8), intent(in)        :: RMaxNB, RMinNB, PRmin, PRmax
    character(*), intent(inout) :: AtomTypesPairStr(2)
! Local Variables
    integer  i,j,ierr
    character(rdfpot_text) field, l, str
    logical flag
    
    do j=beg_text, end_text ! Read properties which are specific to RDF/potential-type 
        call ReadLineFromText(text,j,str,ierr)
        l=str
        call split(l,'=',field)
        select case (str_cln(field))
            case ('ATOMTYPES')   
                call removesp(l)
                ierr=2
                call parse(l,',',AtomTypesPairStr,ierr)            
                if (ierr.ne.2) then
                    write(stdout,*) 'ERROR: Reading ',trim(df_kind),': File: ',trim(iFilename),' line:',j,&
                               ' Can not read the numbers of atomic types described by the ',trim(df_kind),'.',l
                    write(stdout,*) '"',trim(adjustl(AtomTypesPairStr(1))),'" ',' "',trim(adjustl(AtomTypesPairStr(2))),'"'           
                    stop 153
                endif
                flag=.false.
                do i=1, size(NameSType)
                    if (AtomTypesPairStr(1).eq.NameStype(i)) then 
                        flag=.true.
                        AtomTypesPair(1)=i
                    endif
                enddo
                if (.not. flag) then
                    write(stdout,*) 'ERROR: Reading ',trim(df_kind),':  File: ',trim(iFilename),&
                    ' line:',j, ' unknown atom/site type ',AtomTypesPairStr(1)
                    stop 21
                endif
                flag=.false.
                do i=1, size(NameSType)
                    if (AtomTypesPairStr(2).eq.NameStype(i)) then 
                        flag=.true.
                        AtomTypesPair(2)=i
                    endif
                enddo
                if (.not. flag) then
                    write(stdout,*) 'ERROR: Reading ',trim(df_kind),':  File: ',trim(iFilename),&
                    ' line:',j, ' unknown atom/site type ',AtomTypesPairStr(2)
                    stop 21
                endif               
            case default
                continue !write(stdout,*) 'Debug: Unknown line:',trim(adjustl(str))
        end select
    enddo
    
    if (str_cmp(df_kind,'POTENTIAL')) then
    !   Check consistency with Common properties
        if (PRMAX.ne.RMaxNB) then
            write(stdout,*) 'ERROR: Reading ',trim(df_kind),': File: ',trim(iFilename),&
                        ' MAX value for the NB-',trim(df_kind),':',PRMAX,&
                        ' differs from general MAX: ', RMaxNB
            stop 154                                           
        endif
        if (PRMIN.ne.RMinNB) then
            write(stdout,*) 'ERROR: Reading ',trim(df_kind),': File: ',trim(iFilename),&
                        ' MIN value for the NB-',trim(df_kind),':',PRMIN,&
                        ' differs from general MIN: ', RMinNB
            stop 155                                           
        endif
        if (PNpoints.ne.Npoints) then
            write(stdout,*) 'ERROR: Reading ',trim(df_kind),': File: ',trim(iFilename),&
                        ' Npoints value for the NB-',trim(df_kind),' :',PNpoints,&
                        ' differs from general Npoints: ', Npoints
            stop 156                  
        endif
    endif
end subroutine
