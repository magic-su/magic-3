subroutine PrintRDF(CORS)
    
    implicit none
    !Global variables
    integer(8), intent(in):: CORS(:)
    
    !Local variables
    real(4) DRR
    integer(4) iSType, jSType,i, iBond, nr
    
    character(12) str
    character(128) str2
    
! - Printing all the RDFs----------------------------------------------------------

    if ((.not. LReadRDF).or.(iPrint.ge.6)) then
        write(stdout_imc,*) '----------- Sampled distribution functions -------------'
    !   ! Print NB RDFs - considering the case:
        do iSType=1,NSType
            do jSType=iSType,NSType           
                if (Btest(StateNB(jSType,iSType),6)) then  ! if this RDF was sampled
                    write(stdout_imc,*)' This pair:',NameSType(iSType),' - ',NameSType(jSType),'   pairs: ',npairs(jSType,iSType)
                    write(stdout_imc,*)'   R       RDF   RDFref     <S(R)> ', '   <S(R)ref>    delta           typ '
                    do NR=1,NA
                        if (RDFref_NB_riSjS(NR, jSType, iSType).ne.0) &
                        write(stdout_imc,'(f10.4,2f10.5,2f10.3,5x,3a4)') &
                           RAS(NR),&
                           RDFcalc_NB_riSjS(NR,jSType,iSType),&
                           RDFref_NB_riSjS(NR,jSType,iSType),&
                           Scalc_NB_riSjS(NR,jSType,iSType),&
                           Sref_NB_riSjS(NR,jSType,iSType),&
                           'rdf:',NameSType(iSType),NameSType(jSType)
                    end do
                end if                    
            end do
        end do

    !    Intramolecular RDF-s    
        write(stdout_imc,*)'   Intramolecular RDF-s'
        do iBond=1, NBond
            ! if the molecule is allowed to move, and either RDF is defined or potential is fixed
            if (btest(StateB(iBond),6)) then ! if this RDF was sampled
                write(str2,*) 'Bond '//trim(NameBond(iBond))
                if (.not. BTest(StateB(iBond),9)) str2 = trim(str2)//' SameAs:'//trim(NameBond(BondSameBond(iBond)))
                if (BTypeBond(iBond).eq.1) then
                    write(str,'(2a4)') NameSite(iSiteInBond(iBond,1)), NameSite(jSiteInBond(iBond,1))
                    write(stdout_imc,'(a11,a,a3,a,a16,a)')' This pair:',NameSite(iSiteInBond(iBond,1)),' - ',&
                        NameSite(jSiteInBond(iBond,1)),' intramolecular ', trim(str2)
                    write(stdout_imc,*)'   R       RDF   RDFref     <S(R)>   <S(R)ref>    delta           typ '
                elseif (BTypeBond(iBond).eq.2) then
                    write(str,'(3a4)') NameSite(iSiteInBond(iBond,1)),NameSite(kSiteInBond(iBond,1)), NameSite(jSiteInBond(iBond,1)) 
                    write(stdout_imc,'(a14,a,a3,a,a3,a,a)')' This triplet:',NameSite(iSiteInBond(iBond,1)),&
                        ' - ',NameSite(kSiteInBond(iBond,1)),&
                        ' - ',NameSite(jSiteInBond(iBond,1)), trim(str2)
                    write(stdout_imc,*)'   Phi       ADF   ADFref     <S(phi)> ', '   <S(phi)ref>    delta           typ '
                endif

                if((IPRINT.ge.6.and.NpointsPotB(iBond).gt.0))then
                    write(stdout_imc,*) ' NpairAccumulated ',NPairsTotalBond(iBond)
                endif
                do I=1,NpointsPotB(iBond)
                    if((RDFref_B_riBond(I,iBond).ne.0)) then
                        DRR=(RMaxPotB(iBond)-RMinPotB(iBond))/NPointsPotB(iBond)                    
                        write(stdout_imc,'(f10.4,2f10.5,2f10.3,5x,a4,a12)')&
                           RMinPotB(iBond)+DRR*(I-0.5) ,&
                           RDFcalc_B_riBond(i, iBond),&
                           RDFref_B_riBond(i,iBond),&
                           Scalc_B_riBond(i,iBond),&
                           Sref_B_riBond(i,iBond),&
                           'rdf:',trim(str)
                     endif
                enddo
            endif
        end do
    endif
end subroutine PrintRDF
