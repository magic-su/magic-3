!     Package tranal v.5.0
!     Transformation of trajectory from atomistic to CG-based
!subroutine written by Alexander Mirzoev
program cgtrajconverter
use CGTrajMod
use TrajectoryIO
use strings, only: str_cmp
include "tranal.h"
!DEC$ FREEFORM
!DIR$ FREE
character*128 FFILTR,FNAME,cdum*8,chr4*5 !From Sasha's rdf function
integer iNTypes,NTypes ! Number of molecule types
character*12, allocatable:: LMolTypes(:) ! List of molecular type names
character*12,allocatable:: NameCGforMolType(:,:) ! Name of CG_bead
integer, allocatable:: NCGforMolType(:),NAtomsInCG(:,:),CGAtoms(:,:,:) !List of atoms belong to CG_bead
character(128) CGTrajOutFile /'cgtrajout.xmol'/ !Name of CG-trajectory file
integer Nat,iNCGforMolType ! Total number of atoms
integer posit2,posit
character(128) filenametemp /''/,dumpstring /''/
integer IEND,err, NCG !NCG- total number of CG_beads
real massa,charg,imass, RCOM(3) ! Mass and charge, COM for CG_bead
parameter (debug = 4)
character*128 buffer,FMT
integer(4) numargs

real(8), allocatable :: R_vec(:,:)
character*12,allocatable:: Names_CG_beads(:)
character(4) frmt
real(4) box(3)
!  Reading command line arguments
    numargs=iargc()
    if (numargs.gt.1) then
        write(*,*) 'Too many parameters in command line:',numargs,'! Just one needed: Input file name.'
        stop
    elseif (numargs.eq.1) then
        call GETARG(1,buffer)
        write((6),*) 'Input file name stated in command line: ',buffer
    elseif(numargs.eq.0) then
        write((6),*) 'Input file name NOT stated in command line: Use default cgtraj.inp'
        buffer='cgtraj.inp'
    endif
    call readbminput(buffer,NCGforMolType,NameCGforMolType,NAtomsInCG,CGAtoms,CGTrajOutFile, LMolTypes)
    frmt = CGTrajOutFile(index(CGTrajOutFile,'.', back=.True.)+1:len(CGTrajOutFile))
    write(*,*) 'Output trajectory format: ', frmt
! Reading input file
    open(unit=2,file=buffer,status='unknown')
   call SETTRAJ ! read and setup trajectory parameters
   iend=0
   if (debug .ge. 6) then
	write (*,*) "Output Main Parameters"
	write (*,*) 'NTypes:',NTypes
	write (*,*) 'NSPEC()',NSPEC(:)
	write (*,*) 'NCGforMolType()',NCGforMolType(:)
   endif
!-------Calculation of Total Number of CG in the system -------------
!   NCG = sum(NSPEC(1:NTypes)*NCGforMolType(1:NTypes))
   NCG = dot_product(NSPEC, NCGforMolType)
   allocate(R_vec(3, NCG))
   R_vec = 0.0
   allocate(Names_CG_beads(NCG))
   Names_CG_beads = ''
!   do iNTypes=1, NTypes
!      NCG=NCG+NSPEC(iNTypes)*NCGforMolType(iNTypes)
!   enddo
   if(debug .ge. 6) write(*,*) 'Total Number of CG: ',NCG
!-----------------------------------------------------------------
!   open(5,file=CGTrajOutFile,IOSTAT=err) !Open output file for rewriting
   call OpenTraj(frmt, CGTrajOutFile, stdout_=5, mode='w', ierr=err)
   do ! loop over all configurations
      call READCONF(iend)
      if (iend.ne.0) exit
      if(debug.ge.6) write(*,*) 'End of the trajectory file reached',iend
!      write (5,*) NCG ! Write first line with atoms number in CGtraj xmol
      if(debug.ge.6) write(*,*) 'NCG:',NCG
!      write (5,'(a6,f15.2,a10,f8.3,a1,f8.3,a1,f8.3)') 'after ',&
!     	FULTIM*1.0E3,' fs, BOX: ',BOXL,' ',BOYL,' ',BOZL !Write second line in CGtraj xmol
      if(debug.ge.6) write(*,*)'after ',FULTIM*1.0E3,' fs, BOX:',BOXL,' ',BOYL,' ',BOZL
      posit=1
      posit2=1
      if ((debug.ge.6)) write(*,*) 'posit iNCGforMolType iNTypes iAtomsInCG CGAtoms SX xcm'
      i_cg_bead_counter = 0
      do iNTypes=1, NTypes ! loop over all molecular types
!			   write(*,*)' going through type ',iNTypes
          do iMol=1,NSPEC(iNTypes) ! loop over all molecules of current type
!				   write(*,*)' molecule ',iMol,' of type ',iNTypes,' sites ',NCGforMolType(iNTypes)
	     do iNCGforMolType=1, NCGforMolType(iNTypes) ! loop over all CG for  this molecule type
!		write(*,*)' CG site no ',iNCGforMolType, NAtomsInCG(iNCGforMolType,iNTypes), ' posit ',posit1,posit2
	        xcm=0.0
		ycm=0.0
		zcm=0.0
		massa=0.0 !total mass
		imass=0.0 !current atoms mass						
		do iNAtomsInCG=1,NAtomsInCG(iNCGforMolType,iNTypes)  !loop over all atoms of current CG
!	write(*,*)' site loop: ',iNCGforMolType,iNTypes,iNAtomsInCG,
!     +         CGAtoms(iNCGforMolType,iNTypes,iNAtomsInCG),posit2  !
		   imass=mass(posit2-1+CGAtoms(iNCGforMolType,iNTypes,iNAtomsInCG))
		   massa=massa+imass
		   xcm=SX(posit-1+CGAtoms(iNCGforMolType,iNTypes,iNAtomsInCG))*imass+xcm
		   ycm=SY(posit-1+CGAtoms(iNCGforMolType,iNTypes,iNAtomsInCG))*imass+ycm
		   zcm=SZ(posit-1+CGAtoms(iNCGforMolType,iNTypes,iNAtomsInCG))*imass+zcm
 		   if ((debug.ge.6)) then
		      write(*,*) 'posit iNCGforMolType iNTypes iAtomsInCG CGAtoms SX xcm'
		      write(*,*)' debug:', posit,iNCGforMolType,iNTypes,iNAtomsInCG,&
     			CGAtoms(iNCGforMolType,iNTypes,iNAtomsInCG),SX(posit-1+CGAtoms(iNCGforMolType,iNTypes,iNAtomsInCG)),xcm
		   endif
		enddo
		xcm=xcm/massa
		ycm=ycm/massa
		zcm=zcm/massa
                i_cg_bead_counter = i_cg_bead_counter + 1 
                R_vec(1:3,i_cg_bead_counter) = [xcm, ycm, zcm]
                Names_CG_beads(i_cg_Bead_counter) = NameCGForMolType(iNCGforMolType,iNTypes)
!		write(5,'(a12,a1,f12.4,a1,f12.4,a1,f12.4)') NameCGForMolType(iNCGforMolType,iNTypes),' ', xcm ,' ',ycm,' ',zcm
	     enddo
	     posit=posit+NSITS(iNTypes) !Pointer on first atom of current molecule in full list
	  enddo
	  posit2=posit2+NSITS(iNTypes) !Pointer on first atom of current molecule in type list
       enddo
     box = [real(BOXL,4), real(BOYL,4), real(BOZL,4)]
!    write(*,*) box 
     call SaveFrame(frmt, 5, NCG, box, Names_CG_beads, R_vec, real(FULTIM*1.0E3, 4))
   enddo
!   close(5)
   call CloseTraj(frmt, 5, 'w')
   if(debug.ge.6) write(*,*) 'Trajectory generating finished'
!  write last frame in xmol format, to have names of the atoms
   if (.not. str_cmp(frmt, 'xmol')) then
       call OpenTraj('xmol', 'last_frame.xmol', stdout_=5, mode='w', ierr=err)   
       call SaveFrame('xmol', 5, NCG, box, Names_CG_beads, R_vec, real(FULTIM*1.0E3, 4))
       call CloseTraj('xmol',5,'w')
    endif

   if(debug.ge.6) write(*,*) 'MMOL files genetaring started'
! Genarating MMOL file for all molecular types
   posit=1
   do iNTypes=1, NTypes ! loop over all molecular types
      NX=0
      if(debug.ge.6) write(*,*) 'NAMOL:',NAMOL(iNTypes),'#'
      filenametemp=trim(LMolTypes(iNTypes))//'.mmol' 
      if(debug.ge.6) write(*,*) 'FilenameTemp:',trim(filenametemp),'#'
      open(5,file=trim(filenametemp),IOSTAT=err)
      write(5,'(i5)') NCGforMolType(iNTypes)
      write(5,'(a)') '# Atom, X, Y, Z, Mass, Charge'
      do iNCGforMolType=1,NCGforMolType(iNTypes) ! loop over all CG for  this molecule type
	 massa=0.0 ! Mass of CG
	 charg=0.0 !Charge of CG
	 RCOM=0.0
	 do iNAtomsInCG=1,NAtomsInCG(iNCGforMolType,iNTypes)!loop over all atoms of current CG
	    i=posit-1+CGAtoms(iNCGforMolType,iNTypes,iNAtomsInCG)
	    RCOM(1:3)=R(i,1:3)*mass(i)+RCOM(1:3)
	    massa=massa+mass(i)
	    charg=charg+charge(i)
	    if ((debug.ge.6)) then
		write(*,*) 'iNCGforMolType:',iNCGforMolType
		write(*,*) 'iNTypes:',iNTypes
		write(*,*) 'iAtomsInCG:',iNAtomsInCG
		write(*,*) 'iMass:',mass(i),'i:',i
		write(*,*) 'iCharge:',charge(i),'i:',i
		write(*,*) 'CGAtoms:',CGAtoms(iNCGforMolType,iNTypes,iNAtomsInCG)
		write(*,*)'R:',R(i,1:3)
	    endif
	 enddo
	 RCOM(1:3)=RCOM(1:3)/massa
	 if ((debug.ge.6)) write(*,*) 'RCOM: ',RCOM
         write(FMT,*) floor((LOG10(1.0*iNTypes))+1)
	 write(dumpstring,'(i'//FMT//')') iNTypes
	 if ((debug.ge.6)) write(*,*)'iNTypes-string:',trim(filenametemp),'#'
	 write(filenametemp,*)trim(NameCGForMolType(iNCGforMolType,iNTypes))!,
	 if ((debug.ge.6)) write(*,*) trim(filenametemp)
	 write(5,'(a12,a3,f12.4,a1,f12.4,a1,f12.4,a1,f10.4,a1,f8.4)') trim(filenametemp),'   ',&
     		  RCOM(1) ,' ',RCOM(2),' ',RCOM(3),' ',massa,' ',charg
      enddo
      close(5)
      posit=posit+NSITS(iNTypes)
   enddo
end
	      
	

