\section{File formats}
\subsection{.xmol} \label{XMOL}
This is plain text trajectory format, which can be produced by many molecular 
modeling packages, including MagiC. 
It consists of a number of consequent frames, with each frame having the following structure:

\begin{description}
 \item [line 1:] Number of atoms in the frame (N)
\item[line 2:] A commentary line
\item[line 3:] Name(atom1)  X(atom1)  Y(atom1)  Z(atom1)
\item[line 4:] Name(atom2)  X(atom2)  Y(atom2)  Z(atom2)
\item[lines 5,6...,N+2:] Names and coordinates of atoms 3 - N.
\end{description}

In case of trajectory, configuration files from each time frame are written consequently one after another.

There is no common requirements for the commentary (second) line. For CGtraj module of MagiC it is
assumed that the second line of each configuration follows this format (accepted in MDynaMix):

\verb| (char) <time> (char-s) BOX: <box_x> <box_y> <box_z>| 
\newline
where \verb|(char)| is any character word, \verb|<time>| is time in $fs$, \verb|<box_x> <box_y> <box_z>| 
(following after keyword \verb|BOX|). The length unit is {\AA}ngstr{\"o}ms and time unit is femtoseconds. 
The time step information is not needed for CGTraj but the box size information is essential.
If no box size information is present in the trajectory, the box size information can be supplied in the input file, but the later has a sense only in constant-volume simulations.

In xmol-files produced by MagiC the second line may have no time-stamp and box sizes.


\subsection{.mmol} \label{MMOL}
MMOL is a molecular topology file format, which is inherited from MDynaMix MD software. 
It consists of two parts: first part describes atomic composition, geometry, charges, masses and 
non-bonded interactions; the second part defines bonds, angles and torsions in the molecule. MMOL-topolygy 
files are used by cgtraj (\autoref{cgtraj}) for converting high-resolution trajectory to a coarse 
grained one and for calculation of the reference distribution functions rdf (\autoref{rdf}). 
In both cases only information from the first section of .mmol file (information about atomic composition) is used, and the bonding part may be omitted.

The first part of the file, which is part of interest has the following structure:
The first non-commentary line of a .mmol file is the number of atoms in the molecule. After it the 
corresponding number of lines follows, one line per atom. Each line contains 8 compulsory parameters. 
They are: 1) atom name in the program; 2),3) and 4) are the initial X,Y,Z coordinates of the atom in the molecular coordinate system, 5) mass in atom units, 6) charge, 7) Lennard-Jones parameter $\sigma$  in Å, 8) Lennard-Jones parameter $\varepsilon$ in kJ/M. Two optional columns may present.

For the correct work of CGTraj utility, the only important information is
the number of atoms in the molecule and masses of atoms. CGTraj utility may work without 
supplying .mmol files, in such case the masses of atoms are set to 1 and charges to 0. 


\subsubsection{MMOL Example: H2O.mmol}
\begin{Verbatim}[fontsize=\small]
#==================================================I
#       Molecular Dynamics Data Base               I
#       Configuration and interaction potential    I
#==================================================I
#           SPC H2O model                          I
#==================================================I 
#       Number of sites
 3
#       X        Y         Z         M          Q         sigma     epsilon
#               (A)                                                  (kJ/M)
O       0.       0.    -0.064609  15.9994      -0.82       3.1656    0.6502
H1      0.    -0.81649  0.51275    1.008        0.41       0.        0.
H2      0.     0.81649  0.51275    1.008        0.41       0.        0.
# We care only up to this line! The rest of this file can be skipped.
#    Num. of strings for the reference
4
       SPC water model
       Parameters from:          
       K TOUKAN AND A.RAHMAN,
       PHYS. REV. B Vol. 31(2) 2643 (1985)
#   Num. of bonds
 3
#ID(typ)    N1   N2         Reqv      Force        D        RHO (A**-1)    
 1           1    2          1.       2811.      420.      2.566
 1           1    3          1.       2811.      420.      2.566
 0           2    3         1.633      687.       0.        0.
#   Num. of angles
 0                                        
#   Num of dihedrals
 0
#   Additional options
#   flexible SPC water
fSPC
\end{Verbatim}

\subsection{.mcm} \label{MCM}
Coarse-grain topology file, which is similar to CG.mmol, but includes information about 
bead/CG-atom types and intramolecular bonds. In general it consists of three parts: the first 
part describes atoms involved in the molecule, the second part contains list of covalent-like 
bonds and the third part lists angle-bending bonds.  An individual .mcm file  should be 
provided for each molecular type present in the system. These files are automatically generated 
by rdf.py utility during computation of the reference distribution functions, but they can be manually edited if necessary. For example, partial charges of the CG sites can be corrected.

\textbf{Format of .mcm file:}\\
NB! Lines beginning with '\#' or '!' are commentaries, they are dropped while parsing the file.
\\
First the atom description block is specified:\\
\textbf{1 line:} Number of bead/atoms in the molecule (Natoms)\\
\textbf{Natoms lines:} Atom records. One record per line.\\
Each record contains 8 parameters. Atom name; X,Y,Z coordinates (\AA) of the atom in a local
coordinate system, mass (au.); charge (el.); index of the atom type; name of the bead/atom type. The short-range non-bonded interaction between a pair of beads/atoms will be defined by the bead/atom types defined in this file. Atoms of the same atom type interact by the same non-bonded potential.
\\
Then the pairwise bond block is specified:\\
\textbf{1 line:} The total number of pairwise bond types present in the molecule (Nbonds). \\
Then for every individual bond type (of Nbonds) one need to specify:  \\
\textbf{1 line:} Number of atom pairs which are involved in the bond of this type (NPairs); \\
\textbf{NPairs lines:} List of such atom pairs, one pair per line.\\
Then the angle bending bond block is specified:\\
\textbf{1 line:} Total number of angle bending bond types present in the molecule. 
For every individual bond type (NAngles) one need to specify:  \\
\textbf{1 line:} Number of atom triplets which are involved in the bond of this type (NAngles); \\
\textbf{NAngles lines:} List of such atom triplets, one triplet per line.

NB! In the first version of MagiC the triplet used to had unusual order: central atom stands 
last in the triple, e.g. triplet 1 3 2, defines angle between 1-2 and 2-3. Now it is changed 
to the regular 1-2-3 order, and to avoid misunderstanding \verb|rdf.py| utility  automatically
writes  \verb|Order=1-2-3| after the number of total angle bending bonds.

\subsubsection{.mcm file example: DMPC.CG.mcm}
The mcm-file listed below defines 10-beads model of DMPC-lipid, as shown on figure \ref{DMPC_interactions}. 

\begin{figure}[!h]\center
	\includegraphics[scale=0.3]{./files/DMPC_interactions.pdf}
	\caption{Example: 10-beads CG model of DMPC-lipid. Beads and bonds of same color have same type; solid lines denote covalent bonds; dashed arrows denote angle bending bonds. }
	\label{DMPC_interactions}
\end{figure}

\begin{Verbatim}[fontsize=\small]
#Number of atoms
10
# Name   X     Y       Z      Mass   Q  NumofType NameofType    
N    -6.1804 -17.2679 17.2781 73.139 0.76 1 N
P    -9.6995 -17.2121 14.915 123.0256 -0.89 2 P
C2   -18.1023 -13.2828 10.2371 56.108 -0.0 3 CH
C3   -21.9375 -11.5562 7.3382 56.108 -0.0 3 CH
C4   -24.5552 -9.7683 3.2938 57.116 -0.0 3 CH
C6   -15.3122 -17.1232 8.1441 56.108 -0.0 3 CH
C7   -18.7644 -15.0198 5.1392 56.108 -0.0 3 CH
C8   -21.6201 -13.2988 1.0155 57.116 -0.0 3 CH
C1   -14.7182 -15.6667 12.7078 72.0638 -0.09 4 CO
C5   -13.3132 -18.7418 11.2877 71.0558 0.22 4 CO
#
# Here we define covalent like bonds
# Total number of covalent bond types: 5
5
# Covalent bond N-P
# One atom pair belongs to covalent bond type 1
1
# Define pair of atoms by their numbers in the list above: 1 (N) 2 (P),
# the third number should always be 1 for covalent bond.
1  2  
# Covalent bond P-CO
# Two atom pairs belong to covalent bond type 2
2
# Define 2 pairs of atoms by their numbers in the list above: 2 (P) and 9,10 (C1,C5)
2  9  
2  10  
# Covalent bond CH-CH
# Four atom pairs belong to covalent bond type 3
4
# Define 4 pairs of atoms by their numbers in the list above: 3-4, 4-5, 6-7, 7-8
3  4  
4  5  
6  7  
7  8  
# Covalent bond type 4
2
9  3  
10  6  
# Covalent bond type 5
1
9  10  
#
# Here we define angle bending bonds
# Total number of angle bending bonds:5
5
# Two atom triplets belong to angle bond type 1: 
2
1  2  9  
1  2  10  
# Two atom triplets belong to angle bond type 2: 
2
2  9   3  
2  10  6  
# angle bond type 3: 
2
9  3  4  
10  6  7  
# angle bond type 4: 
2
3  4  5 
6  7  8 
# angle bond type 3: 
2
3  9  10  
6  10  9   
\end{Verbatim}


\subsection{.rdf and .pot file formats} \label{RDFnPOT}

The described here file formats for .rdf and .pot files are valid for MagiC v. 2.0 and higher. The format of files for .rdf and for potentials is similar and we give here
a common description of it refering as "RDF/potential" file format.

The RDF/potential file consists of a header section, marked by tags \verb|&General| 
and \verb|&EndGeneral| describing general properties of the RDFs/Potentials set for a
specific system, and independent RDF/potential records, marked 
\verb|&Potential| ... \verb|&EndPotential| or \verb|&RDF| ...  \verb|&EndRDF| respectively,
which specify RDF/potentials for each individual interaction. Each individual RDF/Potential 
can be included to the RDF/potential file from a separate file by \verb|include| statement.

Non-Bonded (NB) RDF/Potentials are identified by atom types involved in the RDF/Potential. 
Pairwise (B) and Angle-bending (A) bonds are identified by the molecular type they belongs 
to and the relative bond number in the molecular type.
Each Potential can be protected from correction (Fixed) by specifying the flag Fixed=True 
in the corresponding section of the potential file. Such potentials do not change during the inverse procedure.


\subsubsection{Header}
\verb|&General - &EndGeneral|

The header defines common properties of the RDFs/potentials provided in the file. Since 
individual RDF/potential records can be included from external files, the header section provides 
important information which helps to provide consistency between all records.
\begin{description}
\item [NTypes] - Number of bead/atom types present in the system.
\item [N\_NB, N\_B, N\_A] - Number of Non-Bonded, pairwise Bonded, and Angle-bending records, respectively
\item [NPoints] - Number of points in each NB-record.
\item [Min, Max] - Range of distance (in \AA) where Non-Bonded RDF/Potentials are defined
\end{description}

\subsubsection{RDF/potential record: } \label{InitZero}
\verb|&RDF| ...  \verb|&EndRDF| \\
or \\ 
\verb|&Potential| ... \verb|&EndPotential| 

Each individual record specifies one RDF/potential, providing information the CG atom types, range, 
number of points and the data-table with actual values. A record can be also included from a separate 
file. Every record consists of specifications, data table (\verb|&Table...&EndTable|) and optional 
include section (\verb|&IncludePotential...&EndIncludePotential|).

\begin{description}
\item[Name] - Name of the RDF/potential record. Is used as a comment line
\item[Type] - Type of the record. Can be NB, B, A, i.e. non-bonded, pairwise bond, angle-bending bond, respectively
\item[Min, Max] - Range (in \AA) where the record is defined
\item[NPoints] - Number of points in the record. Note, that for the core region of RDF, which has zero values
and often omitted, the Min value is typically not zero 
\item[AtomTypes] - For NB-record, specifies the pair of bead/atom types which are involved in the interaction.
\item[MolType] - For B- or A-record, specifies molecular type the bond belongs to.
\item[BondNumber] - For B- or A-record, specifies a relative number of bond to which this record belongs. 
Note that bonds are indexed locally, with respect to the molecular type, the same way as in the corresponding mcm-file for the given molecular type
\item[NPairs or NTriplets] - Number of atom pairs (triplets) involved in the bond (B- or A-bond)
\item[Pairs or Triplets] - List of atom pairs (triplets) involved in the bond. Pairs/Triplets are comma 
separated, and atom numbers are separated by dash symbol (-) withing each pair/triplet. 
For triplets, atom numbers are specified in a direct way, so the central atom of the triplet is a central 
atom of the angle. Atoms are numbered locally, with respect to the molecular type, same way as in the
corresponding mcm-file for the given molecular type).\\
\item[\&Fixed] - Potential file only. If stated, the potential will be excluded from the inverse procedure 
(e.g. it will be fixed in potential update/refinement)
%\item[&Zero] - RDF-file only. If stated, the given 
\item[\&InitZero] - RDF-file only. If the corresponding potential is not provided, initiate it with zero. 
Default for NB-potentials.
\item[\&InitPMF] - RDF-file only. If the corresponding potential is not provided, initiate it with 
Potential of Mean Force. Default for bond-potentials (both A- and B-).
\item[\&Table...\&EndTable] - Actual table defining the RDF/potential. The first column specifies distance 
(\AA) or angle (deg), the second column specifies value, which is unitless for RDF and kJ/mol for potential. 
The table shall be uniformly spaced, and the same grid resolution should be used in all  RDFs and potentials
of the same kind (NB, B or A) for the whole system
\\
\item[\&IncludePotential=IncludeFileName] 
or 
\item[\&IncludeRDF=IncludeFileName]

- Instead of providing data table in the record, one can import 
it from an external file \verb|<filename>|. This feature allows to 
incorporate easily potentials previously  obtained for other systems into the current one.
\end{description}

\subsubsection{Included Potential/RDF} \label{included-potential/rdf}
 \verb|&IncludedPotential| ... \verb|&EndIncludedPotential| \\
 \verb|&IncludedRDF| ...  \verb|&EndIncludedRDF|
The included record has nearly identical format as the parental RDF/potential section. The record 
specification values shall be also in agreement with the corresponding values of the parental section, 
to provide consistency of the whole set of RDF/potentials for the studied system.

\begin{description}
\item[Name] 
\item[Type] 
\item[Min, Max]
\item[NPoints] 
\item[AtomTypes] - NB interactions only
\item[MolType] - For bonds only
\item[BondNumber] - For bonds only
\item[NPairs or NTriplets] - For bonds only
\item[Pairs or Triplets] - For bonds only
\item[\&Table...\&EndTable]
\end{description}
Note that \&Fixed, \&InitZero and \&InitPMF keywords are not applicable in the included record, but 
shall be used in the main potential/RDF file instead.

\subsection{exclusions.dat} \label{exclusions.dat}
The file format for describing exclusions between all interaction sites of the system. This file is created by \hyperref[rdf]{rdf.py} and used a input to \hyperref[magic]{MagiC core}.
It can be edited manually if necessary.

The first two lines of the file state exclusion rules for each molecular type of the system, i.e. define the maximum number of bonds between atoms which is enough to exclude them from the non-bonded interactions.
All remaining lines specify site-site exclusions: \\
<site number> : <list of sites which do not have NB interactions to the given site> 
Example of exclusion file for DMPC-Cholesterol mixture:
\begin{verbatim}
NAngleBondsExclude=Chol.CG:1,DMPC.CG:1
NPairBondsExclude=Chol.CG:-1,DMPC.CG:1
1:2,3,4
2:1,3,4,5,8
3:1,2,4,5,6,8
4:1,2,3,5,8,9
5:2,3,4,6,7
6:3,5,7
7:5,6
8:2,3,4,9,10
9:4,8,10
10:8,9
11:12,13,14,15
12:11,13,14,15
13:11,12,14,15
14:11,12,13,15
15:11,12,13,14
\end{verbatim}

The last five lines of this example imply that therer are no non-bonded interaction within coarse-grained cholesterol molecule defined by sites 11-15.
%\subsection{ Magic core log file: .out} \label{.out}
