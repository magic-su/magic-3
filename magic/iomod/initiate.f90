subroutine Initiate(  RDFref_NB_riSjS,RDFref_B_riBond, rPotB_riBond,&    
    NPointsRDFNB,NPointsPotNB, RMaxRDFNB, RMaxPotNB, RMinRDFNB, RMinPotNB,&
    NPointsRDFB, RMaxRDFB, RMinRDFB, &    
    RDFSameAsBondNameMType, PotSameAsBondNameMType, RDFSameAsBondNumber, PotSameAsBondNumber, &
    AdrGlobIndexAtomij)
    ! Global Variables
    integer, allocatable:: NPointsRDFB(:)
    real(8), allocatable:: RDFref_NB_riSjS(:,:,:),RDFref_B_riBond(:,:),rPotB_riBond(:,:)
    real(8), allocatable::RMaxRDFB(:),RMinRDFB(:)
    integer NPointsRDFNB, NPointsPotNB
    real(8) RMaxRDFNB, RMinRDFNB, RMaxPotNB, RMinPotNB
    integer(4), allocatable:: AdrGlobIndexAtomij(:,:)
    integer NPointsGlobNB, NPointsGlobB
    character(mol_name_len), allocatable, intent(in):: RDFSameAsBondNameMType(:), PotSameAsBondNameMType(:)
    integer(4), allocatable, intent(in):: RDFSameAsBondNumber(:), PotSameAsBondNumber(:)
    ! Local Variables
    integer iSType, jSType, iBond, iR, i, j, np, iAtom, jAtom, iMol, iMType
    real(8), allocatable:: tmpU(:)
    real(8) dr

    ! Check if Potential file was not read
    if (.not.(LReadPot)) then
        write(stdout,*) "Potential file was not stated to read. Initiate parameters from the RDFs"
        RMaxPotNB=RMaxRDFNB
        RMinPotNB=RMinRDFNB
        NPointsPotNB=NPointsRDFNB
        NAngleBondsExcludeMType = RDFNAngleBondsExcludeMType
        NPairBondsExcludeMType = RDFNPairBondsExcludeMType        
    endif
    ! Check if RDF file was not read
    if (.not.(LReadRDF)) then
        write(stdout,*) "RDF file was not stated to read. Are you sure that you want to continue doing a single Monte Carlo run?"
        RMaxRDFNB=RMaxPotNB
        RMinRDFNB=RMinPotNB
        NPointsRDFNB=NPointsPotNB
        NAngleBondsExcludeMType = PotNAngleBondsExcludeMType
        NPairBondsExcludeMType = PotNPairBondsExcludeMType        
    endif
    ! Check that exclusions in RDF and Potential are consistent
    if (LReadRDF .and. LReadPot) then 
        if ( any(PotNAngleBondsExcludeMType.ne.RDFNAngleBondsExcludeMType))  then
            write(stdout,*) "Error: AngleBondExclusions in Potential and RDF are inconsistent"
            stop 635
        endif
        if (any(PotNPairBondsExcludeMType .ne. RDFNPairBondsExcludeMType)) then
            write(stdout,*) "Error: PairBondExclusions in Potential and RDF are inconsistent"
            stop 634
        endif
        NAngleBondsExcludeMType = PotNAngleBondsExcludeMType
        NPairBondsExcludeMType = PotNPairBondsExcludeMType                
    endif
    ! Check that exclusions in exclusions_SR.dat are consistent with ones from RDF/potential
    if ( any(NAngleBondsExcludeMType.ne.ExclSRNAngleBondsExcludeMType))  then
        write(stdout,*) "Error: AngleBondExclusions in Potential/RDF are inconsistent with the exclusions_SR-file or default values"
        stop 631
    endif
    if (any(NPairBondsExcludeMType .ne. ExclSRNPairBondsExcludeMType)) then
        write(stdout,*) "Error: PairBondExclusions in Potential/RDF are inconsistent with the exclusions_SR-file or default values"
        stop 630
    endif        
    ! Check if RMin/RMax and Npoints for NB RDF and potentials are the same
    if ((RMaxRDFNB.ne.RMaxPotNB).or.(RMinPotNB.ne.RMinRDFNB).or.(NPointsRDFNB.ne. NPointsPotNB)) then
        write(stdout,*) "Error: Parameters (RMin,RMax,NPoints) stated for NB-RDFs differs from parameters stated for NB-potentials"
        stop 629
    endif
    RMax = RMaxRDFNB
    RMin = RMinRDFNB
    if (.not. allocated(RDFref_NB_riSjS)) allocate(RDFref_NB_riSjS(NPointsRDFNB,NSType,NSType))
    if (.not. allocated(PotNB_riSjS)) allocate(PotNB_riSjS(NPointsPotNB,NSType,NSType))
    ! loop over all pairs of site types
    write(stdout,*) ''
    write(stdout,*) "Intermolecular interactions: ", NSType, ' Site Types, gives ',&
       NSType*(NSType+1)/2,' NB - pair potentials to define.'
    if (.not. allocated(IND)) allocate(IND(NA,NSType,NSType))
    IND(:,:,:)=-1
    NactNB=0
    do iSType=1, NSType
        do jSType=iSType, NSType
            NactNB=NactNB+1 ! By default count NB-interactions which are non-zero
            write(stdout,*) "Site Types pair: ", trim(NameSType(iSType)),'-',trim(NameSType(jSType))
            ! Case one - NoRDF no Potential -> No interaction
!            if (StateNB(jSType,iSType).lt.4) then
            if (.not.Btest(StateNB(jSType,iSType),3) .and. .not.Btest(StateNB(jSType,iSType),4)) then
                write(stdout,*)'  Neither RDF nor Potential are defined: No short-range interaction between the site types.'
                NactNB=NactNB-1 ! take away this pair from the NB interaction counter
                IND(1:NA,jSType,iSType)=-2
                PotNB_RiSjS(1:NA,jSType,iSType)=0.d0
                continue
            endif

            ! Case two - RDF stated, but Potential is absent -> Initialize potential
            if (BTest(StateNB(jSType,iSType),4).and.(.not.BTest(StateNB(jSType,iSType),3))) then
                write(stdout,'(a)',advance='no')'  RDF is defined, but Potential is not defined for this pair:'
                if (BTest(StateNB(jSType,iSType),1)) then
                    write(stdout,*) 'Initialize the potential with PMF'
                else ! If initialize with zero
                    write(stdout,*) 'Initialize the potential with zero'
                endif
                ! Loop over all points of the potential
                do iR=1, NA
                    if (RDFref_NB_riSjS(iR,jSType,iSType).gt.0.d0) then ! RDF has a nonzero value at this point
                        ! Mark the point as allowed for inverse
                        IND(iR,jSType,iSType)=1
                        if (BTest(StateNB(jSType,iSType),1)) then ! If initialize with PMF
                            PotNB_RiSjS(iR,jSType,iSType)=-dlog(RDFref_NB_riSjS(iR,jSType,iSType))
                        else ! If initialize with zero
                            PotNB_RiSjS(iR,jSType,iSType)=0.0
                        endif
                    else !RDF has a zero value at this point
                        PotNB_RiSjS(iR,jSType,iSType)=POTCUT+100.0*(NA-iR) ! Some large prohibiting value, which decays
                        IND(iR,jSType,iSType)=0 ! Excluded from inverse
                    endif
                enddo
                StateNB(jSType,iSType)=ibset(StateNB(jSType,iSType),3) ! Mark that potential is defined

                if(.not. BTest(StateNB(jSType,iSType),2)) then ! Fixed potential
                    write(stdout,*)' The Potential is stated to be fixed and will be excluded from the inverse procedure'
                    IND(1:NA,jSType,iSType)=-3
                else ! Non fixed potential
                    write(stdout,*)' The potential is not fixed. Inverse mode is enabled for it.'
                endif
                continue

            ! Case three - No RDF, but Potential is stated -> Fixed potential, no inverse
            elseif (BTest(StateNB(jSType,iSType),3).and.(.not.BTest(StateNB(jSType,iSType),4))) then
                write(stdout,'(a)',advance='no')'  RDF is not defined, but Potential is defined for this pair:'
                write(stdout,*)' The points will be excluded from the inverse procedure'
                IND(1:NA,jSType,iSType)=-4
                PotNB_RiSjS(1:NA,jSType,iSType)=PotNB_RiSjS(1:NA,jSType,iSType)/FRT
                continue

            ! Case four - RDF and Potential are stated
            elseif (BTest(StateNB(jSType,iSType),3).and.(BTest(StateNB(jSType,iSType),4))) then
                write(stdout,'(a)',advance='no')'  RDF and Potential are defined for this pair:'
                PotNB_RiSjS(1:NA,jSType,iSType)=PotNB_RiSjS(1:NA,jSType,iSType)/FRT
                if(.not.BTest(StateNB(jSType,iSType),2)) then ! Fixed potential
                    write(stdout,*)' The Potential is stated to be fixed. The points will be excluded from the inverse procedure'
                    IND(1:NA,jSType,iSType)=-3
                else ! Non fixed potential
                    write(stdout,*)' The potential is not fixed. Inverse mode is enabled'
                    do iR=1, NA
                        if (RDFref_NB_riSjS(iR,jSType,iSType).gt.0.d0) then ! RDF has a nonzero value at this point
                        ! Mark the point as allowed for inverse
                            IND(iR,jSType,iSType)=1
                        else ! Block point for inverse
                            IND(iR,jSType,iSType)=0
                        endif
                    enddo
                endif
            endif

            ! Keep the symmetry of the matrixes
            PotNB_RiSjS(1:NA,iSType,jSType)=PotNB_RiSjS(1:NA,jSType,iSType)
            IND(1:NA,iSType,jSType)=IND(1:NA,jSType,iSType)
            StateNB(iSType,jSType)=StateNB(jSType,iSType)
            write(stdout,*) ''
        enddo
    enddo

    write(stdout,*) ''
    write(stdout,*) "Intramolecular bonds: ",NBond,' in total'
    write(stdout,*) NBondB,' pairwise and ',NBondA,' angle-bending bonds to be defined'
!    write(stdout,*) ''
    if (NBond.gt.0) then
!        call reshape2(INDI,size(RDFref_B_riBond,1),size(RDFref_B_riBond,2))
        call reshape2(INDI,max(maxval(NPointsPotB),1),NBond)
        INDI(:,:)=-1
        ! Loop over all bonds
        do iBond=1, NBond
            write(stdout,'(a,i3,a,a,a1)',advance='no') "Bond ", iBond, ' belongs to Molecular Type ',&
                trim(NameMType(MTypeBond(iBond))),':'
            select case (BTypeBond(iBond))
                case(1)
                    write(stdout,*) "Pairwise Bond "
                case(2)
                    write(stdout,*) "Angle-bending (angular) Bond "
                case default
                    stop 'unknown bond type'
            end select

            ! Case one - NoRDF no Potential -> No interaction
            if (.not.BTest(StateB(iBond),4) .and. .not.BTest(StateB(iBond),3)) then
                    write(stdout,*)'ERROR: Neither RDF nor Potential are defined for this bond.'
                    stop 1

            ! Case two - RDF stated, but Potential is absent -> Initialize potential
            elseif (BTest(StateB(iBond),4).and.(.not.BTest(StateB(iBond),3))) then
                write(stdout,'(a)',advance='no')'   RDF is defined, but Potential is not defined for this bond:'
                if (BTest(StateB(iBond),1)) then
                    write(stdout,*) ' Initialize the potential with PMF'
                else ! If initialize with zero
                    write(stdout,*) ' Initialize the potential with zero'
                endif

                ! Get parameters from the corresponding RDF
                NPointsPotB(iBond)=NPointsRDFB(iBond)
                RMaxPotB(iBond)=RMaxRDFB(iBond)
                RMinPotB(iBond)=RMinRDFB(iBond)
                ! Reshape arrays for potential/distance
                call reshape2(PotB_riBond,NPointsPotB(iBond),NBond)
                call reshape2(rPotB_riBond,NPointsPotB(iBond),NBond)
                call reshape2(INDI,max(maxval(NPointsPotB),1),NBond)
                ! Loop over all points
                do iR=1,  NPointsPotB(iBond)
                    if (RDFref_B_riBond(iR,iBond).gt.0.d0) then ! RDF has a nonzero value at this point
                        ! Mark the point as allowed for inverse
                        INDI(iR,iBond)=1
                        if (BTest(StateB(iBond),1)) then ! If initialize with PMF
                            PotB_riBond(iR,iBond)=-dlog(RDFref_B_riBond(iR,iBond))
                        else ! If initialize with zero
                            PotB_riBond(iR,iBond)=0.0
                        endif
                    else !RDF has a zero value at this point
                        ! PotB_iBond(iR,iBond)=FCUT*abs(NPointsPotB(iBond)/2.0-)+100.0*(NA-iR) ! Some large prohibiting value, which decays
                        INDI(iR,iBond)=0 ! Excluded from inverse
                        write(stdout,*) 'ERROR: RDF for the bond has a zero value at point r=',&
                                    RMinRDFB(iBond)+(iR-0.5)*(RMaxRDFB(iBond)-RMaxRDFB(iBond))/NPointsRDFB(iBond)
                        stop 501
                    endif
                enddo
                StateB(iBond)=iBset(StateB(iBond),3)
                if(.not.BTest(StateB(iBond),2)) then ! Fixed potential
                    write(stdout,*)'The Potential is stated to be fixed and will be excluded from the inverse procedure.'
                   INDI(1:NPointsPotB(iBond),iBond)=-3
                else ! Non fixed potential
                    write(stdout,*)' The potential is not fixed. Inverse mode is enabled for it.'
                    do iR=1, NPointsPotB(iBond)
                        if (RDFref_B_riBond(iR,iBond).gt.0.d0) then ! RDF has a nonzero value at this point
                        ! Mark the point as allowed for inverse
                            INDI(iR,iBond)=1
                        else ! Block point for inverse
                            INDI(iR,iBond)=0
                        endif
                    enddo
                endif

                continue

        ! Case three - No RDF, but Potential is stated -> Fixed potential, no inverse
            elseif (BTest(StateB(iBond),3).and.(.not.BTest(StateB(iBond),4))) then
                write(stdout,'(a)',advance='no')'  RDF is not defined, but Potential is defined for this bond: '
                write(stdout,*)'The points will be excluded from the inverse procedure'
                INDI(1:NPointsPotB(iBond),iBond)=-4
                PotB_riBond(1:NPointsPotB(iBond),iBond)=PotB_riBond(1:NPointsPotB(iBond),iBond)/FRT
                StateB(iBond)=iBclr(StateB(iBond),2)
                NPointsRDFB(iBond)=NPointsPotB(iBond)
                RMinRDFB(iBond)=RMinPotB(iBond)
                RMaxRDFB(iBond)=RMaxPotB(iBond)
                continue

        ! Case four - RDF and Potential are stated
            elseif (BTest(StateB(iBond),3).and.(BTest(StateB(iBond),4))) then
                write(stdout,*)'RDF and Potential are defined for this pair:'
                PotB_riBond(1:NPointsPotB(iBond),iBond)=PotB_riBond(1:NPointsPotB(iBond),iBond)/FRT
                if(.not.BTest(StateB(iBond),2)) then ! Fixed potential
                    write(stdout,*)'The Potential is stated to be fixed. The points will be excluded from the inverse procedure'
                    if (RMinRDFB(iBond).lt.RMinPotB(iBond)) then
                        write(stdout,*) 'Bond',iBond,': RMinRDF=',RMinRDFB(iBond),' is smaller than RMinPot',RMinPotB(iBond)
                        write(stdout,*) 'RDF min range will be shifted/cut to ',RMinPotB(iBond)
                        dr=(RMaxRDFB(iBond)-RMinRDFB(iBond))/NPointsRDFB(iBond)
                        np=nint((RMinPotB(iBond)-RMinRDFB(iBond))/dr)
                        write(stdout,*) np,' points will be cut of the left tail of the RDF'
                        RMinRDFB(iBond)=RMinPotB(iBond)
                        call reshape(tmpU,NPointsRDFB(iBond)-np)
                        tmpU(:)=RDFref_B_riBond(np+1:NPointsRDFB(iBond),iBond)
                        RDFref_B_riBond(:,iBond)=0
                        NPointsRDFB(iBond)=NPointsRDFB(iBond)-np
                        RDFref_B_riBond(1:NPointsRDFB(iBond),iBond)=tmpU(:)
                        deallocate(tmpU)
                    endif
                    if (RMinRDFB(iBond).gt.RMinPotB(iBond)) then
                        write(stdout,*) 'Bond',iBond,': RMinRDF=',RMinRDFB(iBond),' is larger than RMinPot',RMinPotB(iBond)
                        write(stdout,*) 'RDF min range will be extended to ',RMinPotB(iBond),' and filled with zeros'
                        call reshape(tmpU,NPointsRDFB(iBond))
                        tmpU(:)=RDFref_B_riBond(1:NPointsRDFB(iBond),iBond)
                        call reshape2(RDFref_B_riBond,NPointsPotB(iBond), NBond)
                        dr=(RMaxRDFB(iBond)-RMinRDFB(iBond))/NPointsRDFB(iBond)
                        np=nint((RMinRDFB(iBond)-RMinPotB(iBond))/dr)
                        write(stdout,*) np,' zero points will be added to the left tail of RDF'
                        RMinRDFB(iBond)=RMinPotB(iBond)
                        RDFref_B_riBond(1:np,iBond)=0.0
                        RDFref_B_riBond(np+1:np+NPointsRDFB(iBond),iBond)=tmpU(:)
                        NPointsRDFB(iBond)=np+NPointsRDFB(iBond)
                        deallocate(tmpU)
                    endif

                    if (RMaxRDFB(iBond).gt.RMaxPotB(iBond)) then
                        write(stdout,*) 'Bond',iBond,': RMaxRDF=',RMaxRDFB(iBond),' is larger than RMaxPot',RMaxPotB(iBond)
                        write(stdout,*) 'RDF max range will be shifted/cut to ',RMaxPotB(iBond)
                        dr=(RMaxRDFB(iBond)-RMinRDFB(iBond))/NPointsRDFB(iBond)
                        np=nint((RMaxRDFB(iBond)-RMaxPotB(iBond))/dr)
                        write(stdout,*) np,' points will be cut of the right tail of the RDF'
                        call reshape(tmpU,NPointsRDFB(iBond)-np)
                        tmpU(:)=RDFref_B_riBond(1:NPointsRDFB(iBond)-np,iBond)
                        RDFref_B_riBond(:,iBond)=0
                        NPointsRDFB(iBond)=NPointsRDFB(iBond)-np
                        RDFref_B_riBond(1:NPointsRDFB(iBond),iBond)=tmpU(:)
                        RMaxRDFB(iBond)=RMaxPotB(iBond)
                        deallocate(tmpU)
                    endif

                    if (RMaxRDFB(iBond).lt.RMaxPotB(iBond)) then
                        write(stdout,*) 'Bond',iBond,': RMaxRDF=',RMaxRDFB(iBond),' is smaller than RMaxPot',RMaxPotB(iBond)
                        write(stdout,*) 'RDF max range will be extended to ',RMaxPotB(iBond),' and filled with zeros'
                        call reshape(tmpU,NPointsRDFB(iBond))
                        tmpU(:)=RDFref_B_riBond(1:NPointsRDFB(iBond),iBond)
                        call reshape2(RDFref_B_riBond,NPointsPotB(iBond), NBond)
                        dr=(RMaxRDFB(iBond)-RMinRDFB(iBond))/NPointsRDFB(iBond)
                        np=nint((RMaxPotB(iBond)-RMaxRDFB(iBond))/dr)
                        write(stdout,*) np,' zero points will be added to the right tail of RDF'
                        RMaxRDFB(iBond)=RMaxPotB(iBond)
                        RDFref_B_riBond(1:NPointsRDFB(iBond)+np,iBond)=0.0
                        RDFref_B_riBond(1:NPointsRDFB(iBond),iBond)=tmpU(:)
                        NPointsRDFB(iBond)=np+NPointsRDFB(iBond)
                        deallocate(tmpU)
                    endif
                    if (NPointsRDFB(iBond).ne.NPointsPotB(iBond)) then
                        write(stdout,*) 'ERROR Initiate: Bond',iBond,' NPointsRDFB(iBond) ',NPointsRDFB(iBond),&
                            '.ne.NPointsPotB(iBond) ',NPointsPotB(iBond)
                        stop 1
                    endif

                    call reshape2(INDI,max(maxval(NPointsPotB),1),NBond)
                    INDI(1:NPointsPotB(iBond),iBond)=-3
                else ! Non fixed potential
                    write(stdout,*)' The potential is not fixed. Inverse mode is enabled'
                    call reshape2(INDI,max(maxval(NPointsPotB),1),NBond)
                    do iR=1, NPointsPotB(iBond)
                        if (RDFref_B_riBond(iR,iBond).gt.0.d0) then ! RDF has a nonzero value at this point
                        ! Mark the point as allowed for inverse
                            INDI(iR,iBond)=1
                        else ! Block point for inverse
                            INDI(iR,iBond)=0
                        endif
                    enddo
                endif
                ! Check consistency of RMax,RMin,Npoints
                if (BTest(StateB(iBond),2)) then
                    if (NPointsPotB(iBond).ne.NPointsRDFB(iBond)) then
                        write(stdout,*) 'ERROR: Number of points stated in RDF ',NPointsRDFB(iBond), &
                                    ' and in Potential ',NPointsPotB(iBond),' for this bond are inconsistent '
                        stop 1
                    endif
                    if (RMaxPotB(iBond).ne.RMaxRDFB(iBond)) then
                        write(stdout,*) 'ERROR: RMax stated in RDF ',RMaxRDFB(iBond),&
                                    ' and in Potential ',RMaxPotB(iBond),' for this bond are inconsistent '
                        stop 1
                    endif
                    if (RMinPotB(iBond).ne.RMinRDFB(iBond))then
                        write(stdout,*) 'ERROR: RMin stated in RDF ',RMinRDFB(iBond),&
                                    ' and in Potential ',RMinPotB(iBond),' for this bond are inconsistent '
                        stop 1
                    endif
                else
                                    if (NPointsPotB(iBond).ne.NPointsRDFB(iBond)) then
                        write(stdout,*) 'WARNING: Number of points stated in RDF ',NPointsRDFB(iBond),&
                                    ' and in Potential ',NPointsPotB(iBond),' for this bond are inconsistent '
                        write(stdout,*) 'Since the potential is FIXED, the RDF is excluded from inverse procedure.'
                    endif
                    if (RMaxPotB(iBond).ne.RMaxRDFB(iBond)) then
                        write(stdout,*) 'ERROR: RMax stated in RDF ',RMaxRDFB(iBond),&
                                    ' and in Potential ',RMaxPotB(iBond),' for this bond are inconsistent '
                        write(stdout,*) 'Since the potential is FIXED, the RDF is excluded from inverse procedure.'
                    endif
                    if (RMinPotB(iBond).ne.RMinRDFB(iBond))then
                        write(stdout,*) 'ERROR: RMin stated in RDF ',RMinRDFB(iBond),&
                                    ' and in Potential ',RMinPotB(iBond),' for this bond are inconsistent '
                        write(stdout,*) 'Since the potential is FIXED, the RDF is excluded from inverse procedure.'
                    endif
                endif

            endif ! End of four cases
            write(stdout,*) ''
        enddo ! End of loop over bonds
    else ! No bonds are present
        ! Allocate dummy arrays
        call reshape2(PotB_riBond,1,1)
        call reshape2(rPotB_riBond,1,1)
        call reshape2(INDI, 1, 1)
    endif ! If any bonds present
    ! Set 5th bit of the state-array for sitepairs if they are allowed to move or not
    do iSType=1, NSType
        do jSType=iSType, NSType
            if(LMoveMType(MTypeSType(iSType)).or.LMoveMType(MTypeSType(jSType))) then
                StateNB(jSType,iSType)=ibset(StateNB(jSType,iSType),5)
                StateNB(iSType,jSType)=StateNB(jSType,iSType)
            endif
        end do
    end do
    do iBond=1, NBond
        if (LMoveMType(MTypeBond(iBond))) StateB(iBond)=ibset(StateB(iBond),5)
    enddo

! Create additional bits in the bit mask - state, which specify RDF sampling, potential correction, potential output
    ! RDF sampling
    do iSType=1, NSType
        do jSType=iSType, NSType
            if   ( BTest(StateNB(jSType,iSType),4) & ! If RDFref
              .and.BTest(StateNB(jSType,iSType),5)) then  ! Allowed to move
                StateNB(jSType,iSType)=ibset(StateNB(jSType,iSType),6)
                StateNB(iSType,jSType)=StateNB(jSType,iSType)
            endif
        end do
    end do
    do iBond=1, NBond
        if   ( BTest(StateB(iBond),4) & ! If RDFref or Pot exits
              .and. BTest(StateB(iBond),5)) &  ! Allowed to move
            StateB(iBond)=ibset(StateB(iBond),6)
    enddo

    ! Potential Correction
    do iSType=1, NSType
        do jSType=iSType, NSType
            if   ( (BTest(StateNB(jSType,iSType),4) .and. & ! If RDFref exits AND
                    BTest(StateNB(jSType,iSType),3) .and. & ! If Pot exits
                    BTest(StateNB(jSType,iSType),2) .and. & ! If pot is not fixed
                    BTest(StateNB(jSType,iSType),5))) then ! Allowed to move
                StateNB(jSType,iSType)=ibset(StateNB(jSType,iSType),8)
                StateNB(iSType,jSType)=StateNB(jSType,iSType)
            endif
        end do
    end do
    do iBond=1, NBond
            if   ( (BTest(StateB(iBond),4) .and. & ! If RDFref exits AND
                    BTest(StateB(iBond),3) .and. & ! If Pot exits
                    BTest(StateB(iBond),2) .and. & ! If pot in not fixed
                    BTest(StateB(iBond),5))) & ! Allowed to move
             StateB(iBond)=ibset(StateB(iBond),8)
    enddo

    ! Potential Output
    do iSType=1, NSType
        do jSType=iSType, NSType
            if   (  BTest(StateNB(jSType,iSType),3) .and. & ! If Pot exits
                    BTest(StateNB(jSType,iSType),5)) then ! Allowed to move
                StateNB(jSType,iSType)=ibset(StateNB(jSType,iSType),7)
                StateNB(iSType,jSType)=StateNB(jSType,iSType)
            endif
        end do
    end do
    do iBond=1, NBond
            if   ( (BTest(StateB(iBond),3) .and. & ! If Pot exits
                    BTest(StateB(iBond),5))) & ! Allowed to move
             StateB(iBond)=ibset(StateB(iBond),7)
    enddo

    call ProcessSameAsBondRecords(RDFSameAsBondNameMType, PotSameAsBondNameMType, RDFSameAsBondNumber, PotSameAsBondNumber)
    call CheckBondsAreSame(NPointsRDFB, RMinRDFB, RMaxRDFB, RDFref_B_riBond)
    
    ! Populate global RDF arrays
    ! Construct Global RDF array
    NPointsGlobNB=0
    do iSType=1, NSType  ! Total number of points where RDF-NB will be accumulated
        do jSType=iSType, NSType
            if(BTest(StateNB(jSType,iSType),6)) NPointsGlobNB=NPointsGLobNB+NA
        end do
    end do
    if (.not. allocated(indexGlobNB_riSjS)) allocate(indexGlobNB_riSjS(NA,NSType,NSType))
    i=0
    do iSType=1, NSType
        do jSType=iSType, NSType
            if (BTest(StateNB(jSType,iSType),6)) then ! If RDF is calculated for this pair
                do iR=1,NA
                    i=i+1
                    IndexGlobNB_riSjS(iR,iSType,jSType)=i
                    IndexGlobNB_riSjS(iR,jSType,iSType)=i
                enddo
            endif
        enddo
    enddo

    NPointsGlobNB=i
    NPointsGlobB=0
    do iBond=1, NBond
        if (BondSameBond(iBond).eq.iBond) then ! the bond is real (not linked)
            StateB(iBond)=ibset(StateB(iBond),9)
            if (BTest(StateB(iBond),6)) NPointsGlobB=NPointsGlobB+NPointsRDFB(iBond)
        else ! the bond is linked to another one
            StateB(iBond)=ibclr(StateB(iBond),8)
            StateB(iBond)=ibclr(StateB(iBond),9)
        endif
    enddo
    NPointsGlobTotal=NPointsGlobNB+NPointsGlobB
    write(stdout,*)' NPointsGlobNB=',NPointsGlobNB,' NPointsGlobB=',NPointsGlobB,' NPointsGlobTotal=',NPointsGlobTotal

    call reshape2(IndexGlobB_riBond, maxval([1, NPointsRDFB(:)]), max(NBond, 1)) ! dummy array
    do iBond=1, NBond 
        if (Btest(StateB(iBond),6) .and. Btest(StateB(iBond),9)) then ! Calculate RDF and the bond is "original"
           do iR=1, NPointsRDFB(iBond)
              i=i+1
              IndexGlobB_riBond(iR,iBond)=i
           enddo
        endif
    enddo
    do iBond=1, NBond
        if (Btest(StateB(iBond),6) .and. (.not.Btest(StateB(iBond),9))) then ! Calculate RDF and bond is linked
            IndexGlobB_riBond(1:NPointsRDFB(iBond),iBond) = IndexGlobB_riBond(1:NPointsRDFB(iBond), BondSameBond(iBond))
        endif
    enddo
    
    allocate(AdrGlobIndexAtomij(NAtom,NAtom)); AdrGlobIndexAtomij=-1
    do iAtom=1, NAtom
        iSType=STypeAtom(iAtom)
        do jAtom=iAtom+1, NAtom
            jSType=STypeAtom(jAtom)            
            if (MolAtom(iAtom).eq.MolAtom(jAtom)) then                
                iBond=Bond_isjs(SiteAtom(jAtom),SiteAtom(iAtom))
                if (iBond.le.0) then
                    if (BTest(StateNB(jSType,iSType),6)) &
                        AdrGlobIndexAtomij(jAtom,iAtom)=IndexGlobNB_riSjS(1,jSType,iSType)-1
                elseif ((BTypeBond(iBond).eq.1).and.(btest(StateB(iBond),4))) then
                    AdrGlobIndexAtomij(jAtom,iAtom)=IndexGlobB_riBond(1,iBond)-1 !
                endif
                
            else if (BTest(StateNB(jSType,iSType),6)) then
                    AdrGlobIndexAtomij(jAtom,iAtom)=IndexGlobNB_riSjS(1,jSType,iSType)-1
            endif                
            AdrGlobIndexAtomij(iAtom,jAtom)=AdrGlobIndexAtomij(jAtom,iAtom)
        enddo
    enddo
 !  Write linked bonds:
    do iBond=1, NBond
        if (.not.Btest(StateB(iBond),9)) &
            write(stdout, *) 'Bond ',iBond, ' is same as bond ',  BondSameBond(iBond)
    enddo
!  Different cut-offs
!  short-range
    RCUT=RMaxRDFNB
!  electrostatic
    if(RECUT.le.0.)then
       RECUT=RMaxRDFNB
       write(stdout,*)' Set real-space Ewald cut-off to RDF-s cutoff (default) ',RECUT
    end if
!  potential correction cutoff
    if(PotRcut.le.0.)then
       PotRcut=RMaxRDFNB
       write(stdout,*)' Set potential correcion cutoff to RDF-s cutoff (default) ',RMaxRDFNB
    end if
    write(stdout,*)' Real-space Ewald cut-off: ',RECUT
    write(stdout,*)' Real-space NB cut-off: ',RCUT
    
! Creating list of Molecules accessible for rotation/translation steps:
    ! if not read from input file, then set from defaults
    if (.not. allocated(MCStepMolBoostChance)) then 
        allocate(MCStepMolBoostChance(NMType))        
        MCStepMolBoostChance = 1
    endif
    
    ! Set MCStepMolBoostChance to zero for single-atom molecules
    where (NSiteMType .eq. 1) MCStepMolBoostChance = 0
   
    ! sanity check values
    if (.not. all(MCStepMolBoostChance(1:NMType) .ge. 0)) then
        write(*,*) 'Error: Negative value in MCStepMolBoostChance:', MCStepMolBoostChance
        stop 625        
    endif
    
    ! create MCStepMolMolecules
    allocate(MCStepMolMolecules(sum(MCStepMolBoostChance*NMolMType)))
    i = 0
    do iMType = 1, NMType 
        if (LMoveMType(iMType)) then 
            do j = 1, MCStepMolBoostChance(iMType)
                do iMol = 1, NMolMType(iMType)
                    i = i + 1
                    MCStepMolMolecules(i) = AdrMolMType(iMType) + iMol
                enddo
            enddo
        endif
    enddo   
    NMCStepMolMolecules = i
    
end subroutine Initiate
