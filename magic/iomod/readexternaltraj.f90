!
! To change this license header, choose License Headers in Project Properties.
! To change this template file, choose Tools | Templates
! and open the template in the editor.
!

!     
! File:   ReadExternalTraj.f03
! Author: amirzoev
!
! Created on 14 December, 2016, 2:26 PM
! Module for opening and reading frames from external trajectory

MODULE ReadExternalTraj
    use precision
    implicit none
    character(5) :: ExternalTrajFormat
    
    character(filename_text), private :: FileTrajFull
!    logical, public:: leof
    public :: OpenExternalTraj, ReadFrame
    real(8) box_(3)
contains
    
subroutine OpenExternalTraj(ID)
    use iomod, only : FileExternalTraj
    use strings
    use InOutUnit
    use utilsmod, only: MakeSaveFileName
    implicit none    
    integer(4), intent(in) :: ID
    integer(4) ierr
    character(filename_text) :: FileTraj_head
    logical l_exist
    box_ = -1.0 ! Default value for non-read box
    ierr = 0
    
    ExternalTrajFormat = trim(FileExternalTraj(index(FileExternalTraj,'.', back=.true.)+1:))    
    FileTraj_head = trim(FileExternalTraj(1:index(FileExternalTraj,'.', back=.true.)-1))    
    FileTrajFull = MakeSaveFileName(FileTraj_head, '.'//trim(ExternalTrajFormat), ID=ID)
    inquire(file=FileTrajFull, exist=l_exist)
    write(stdout,*) 'OpenExternalTraj: Accessing file=',trim(FileTrajFull), '  exist=', l_exist
    if (.not.l_exist) then 
        FileTrajFull = MakeSaveFileName(FileTraj_head, '.'//trim(ExternalTrajFormat))
        inquire(file=FileTrajFull, exist=l_exist)
        write(stdout,*) 'OpenExternalTraj Accessing file=',trim(FileTrajFull), '  exist=', l_exist
        if (.not.l_exist) then 
            write(stdout,*) 'Error: OpenExternalTraj: File does not exist'
            stop 655
        endif
    endif    
    if (trim(uppercase(ExternalTrajFormat))=='XMOL') then
        call OpenExternalTrajXMOL(ierr)
#ifdef XDR        
    else if (trim(uppercase(ExternalTrajFormat))=='XTC') then
        call OpenExternalTrajXTC(ierr)
#endif        
    else
        write(stdout,*) 'Error OpenExteralTraj - unsupported trajectory format:',trim(uppercase(ExternalTrajFormat))
        stop 657
    endif
    
    if (ierr .ne. 0 ) then
        write(stdout, *) 'Error OpenExternalTraj: Can not open file:', FileExternalTraj, 'iostat=',ierr
        stop 670
    endif       
end subroutine

subroutine ReadFrame()
    use InOutUnit
    use strings
    use iomod, only: BOX
    implicit none 
    
    if (trim(uppercase(ExternalTrajFormat))=='XMOL') then
        call ReadFrameXMOL()
#ifdef XDR        
    else if (trim(uppercase(ExternalTrajFormat))=='XTC') then
        call ReadFrameXTC()
#endif        
    endif    
    
    if (box_(1).gt.0) then  ! If box size read from trajectory
        if (sum(abs(BOX_ - BOX)).gt.3e-5) then
                write(stdout, *) 'Error: Periodic box dimensions of the external trajectory', BOX_,&
                    ' differ from the system specifications', BOX
                stop 658
            endif
    endif
    
end subroutine

subroutine OpenExternalTrajXMOL(ierr)
    use strings
    use InOutUnit
    implicit none  
    integer(4), intent(out) :: ierr
    
    write(stdout,*) 'OpenExternalTrajXMOL=', FileTrajFull
    open(unit=stdin_externaltraj, file=FileTrajFull, status='OLD', iostat=ierr, action='READ')         
end subroutine

subroutine ReadFrameXMOL()
    use iomod, only : NAtom
    use mcmod, only : Ri_vec
    use InOutUnit 
    use precision
    implicit none
    integer(4) NAtom_, iAtom, pos_box    
    character(input_text) line
    character(atom_name_len) atomname
        
    read(stdin_externaltraj, *) NAtom_
    read(stdin_externaltraj, *) line
    if (NAtom_.ne.NAtom) then
        write(stdout) 'Error ReadFrameXMOL: Incorrect number of atoms in the frame:', NAtom_, ' Shall be:', NAtom
        stop 659
    endif
    pos_box = index(line, 'BOX:')
    if (pos_box.ne.0) then
        read(line(pos_box+4:), *) BOX_(:)        
    endif
    
    do iAtom=1, NAtom
        read(stdin_externaltraj,*) atomname, Ri_vec(:,iAtom)
    enddo            
    
end subroutine

#ifdef XDR     
subroutine OpenExternalTrajXTC(ierr)
    use strings
    use InOutUnit
    use iomod, only: NAtom
    use xdrwrap

    implicit none  
    integer(4), intent(out) :: ierr
    integer(4) NAtom_

    write(stdout,*) 'OpenExternalTrajXTC=', FileTrajFull
    if (trim(uppercase(ExternalTrajFormat))=='XTC') then
        call xtc_open(FileTrajFull,NAtom_,ierr)    
        if (NAtom_.ne.NAtom) then
            write(stdout) 'Error OpenExternalTrajXTC: Incorrect number of atoms in the frame:', NAtom_, ' Shall be:', NAtom
            stop 659
        endif
    endif
end subroutine

subroutine ReadFrameXTC
    use mcmod, only : Ri_vec
    use InOutUnit 
    use xdrwrap
    use precision
    implicit none
    integer(4) ierr
    real(4) FULTIM    
    
    call xtc_read(BOX_, Ri_vec, FULTIM, ierr)
    if (ierr.ne.0) then
        write(stdout,*) 'Error ReadFrameXTC: Error while reading trajectory.', ierr
        stop 656
    endif
end subroutine
#endif    

END MODULE ReadExternalTraj
