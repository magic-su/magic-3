    subroutine InitMagic()            
        implicit none
#ifdef mpi
    include 'mpif.h'
#endif    
        integer ierr
        character*64 LABEL,SControl*64,DATE*64! Labels for program version and date (not used)

        call TimerInit() ! Start timer 
        call TimerStart(T_Total)
    !Init MPI, detecting number of processes and the process ID
#ifdef mpi
        call TimerStart(T_MPI)
        call MPI_INIT(ierr)
        call MPI_COMM_SIZE(MPI_COMM_WORLD,psize,ierr)
        call MPI_COMM_RANK(MPI_COMM_WORLD,rank,ierr)
        call TimerStop(T_MPI)
#endif
        if (psize.ge.1) ID=ID+1+rank ! Set ID if in parallel mode
        if ((psize.ge.1).and.(rank.eq.0)) then
            write((6),*) 'Running in parallel mode on ',psize,' processes'
        elseif ( psize .eq. 0) then
            write((6),*) 'Running in serial mode'
        endif

     ! Version identification
    include 'version.h'
        if ((rank.eq.0)) then
            write((6),*)'======================================'
            write((6),*)' Molecular Simulation Program MagiC '
            write((6),*)' General output '
#ifndef test            
            write((6),'(4a)')'  Version ',trim(LABEL)
            write((6),'(2a)')'  from ',trim(DATE)
        write((6),'(2a)') '  Source control:  ', trim(SCONTROL)
#endif            
            write((6),*)'======================================'
            write((6),*) 'Thank',"'",'s for citing the software paper:'
            write((6),*)'A. Mirzoev, A.P.Lyubartsev, MagiC: Software Package for Multiscale Modeling'
            write((6),*)'J. Chem. Theory and Computations, 9(3), 1512-1520 (2013). DOI:10.1021/ct301019v'
            write((6),*)'======================================'
#ifdef PBC_MULT
            write((6),*)' PBC_Mult enabled ... '
#endif        
            write((6),*)' starting ... '
        endif    

    end subroutine
